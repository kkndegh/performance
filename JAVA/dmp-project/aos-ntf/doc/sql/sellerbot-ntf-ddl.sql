/**
 * 2022-07-11
 * NTF 템플릿 관리는 셀러봇캐시에서 진행.
 * 임의로 aos-ntf에 doc로 관리중.
**/

CREATE TABLE `t_ntf_template` (
  `tmplt_no` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '템플릿 시퀀스',
  `tmplt_code` varchar(10) NOT NULL COMMENT '템플릿 코드',
  `tmplt_context` text DEFAULT NULL COMMENT '템플릿 내용',
  `tmplt_btn_context` varchar(1000) DEFAULT NULL COMMENT '템플릿 버튼 내용',
  `tmplt_version` bigint(5) DEFAULT NULL COMMENT '템플릿 버전',
  `tmplt_params` varchar(500) DEFAULT NULL COMMENT '템플릿 파라미터',
  `ntf_type` varchar(10) NOT NULL COMMENT 'NTF 타입',
  `use_yn` char(1) DEFAULT 'N' COMMENT '사용여부',
  `reg_dt` datetime DEFAULT current_timestamp() COMMENT '저장일시',
  `reg_id` varchar(20) DEFAULT NULL COMMENT '작성자',
  PRIMARY KEY (`tmplt_no`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='NTF발송 템플릿관리';


CREATE TABLE `t_ntf_service_reg` (
  `tmplt_group` varchar(10) NOT NULL COMMENT '템플릿 그룹',
  `tmplt_code` varchar(10) NOT NULL COMMENT '템플릿 코드',
  `use_yn` char(1) DEFAULT 'N' COMMENT '사용여부',
  `reg_dt` datetime DEFAULT current_timestamp() COMMENT '저장일시',
  `reg_id` varchar(20) DEFAULT NULL COMMENT '작성자',
  PRIMARY KEY (`tmplt_group`,`tmplt_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='NTF발송 서비스 등록';