package com.only.aos.ntf.kakao.ifmodel.kakaobiztalk;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class MsgResult {
    private String msgIdx;
    private String resultCode;
    private String requestAt;
    private String receivedAt;
}
