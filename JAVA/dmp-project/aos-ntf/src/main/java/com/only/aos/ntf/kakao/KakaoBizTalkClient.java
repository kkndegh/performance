package com.only.aos.ntf.kakao;

import java.io.IOException;

import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.AlimTalkRequest;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.AlimTalkResult;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.MsgRequest;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.MsgResultSet;


public interface KakaoBizTalkClient {

    public static final String RESULT_SUCCESS = "1000";
    
    public static final String ATTACH_BUTTON = "button";
    
    /**
     * 알림톡 전송을 요청
     * @param req
     * @return null
     * @throws IOException
     */
    AlimTalkResult sendAlimTalk(AlimTalkRequest req);
    
    /**
     *  모든 메시지 결과를 가지고 온다.
     * @return
     * @throws IOException
     */
    MsgResultSet getMsgResultAll();

    /**
     *  단건 메시지 결과를 가지고 온다.
     * @param ArrayList<String> msgIdxArr 
     * @return
     * @throws IOException
     */
    MsgResultSet getMsgResult(MsgRequest msgIdxArr);
}
