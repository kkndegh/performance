package com.only.aos.ntf.kakao.ifmodel.kakaobiztalk;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthTokenRequest {
    private String bsid;
    private String passwd;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer expire;
}
