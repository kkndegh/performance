package com.only.aos.ntf.service;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;

import javax.mail.internet.MimeMessage;

import com.only.aos.ntf.dto.EmailModelDTO.EmailObjectDataDto;

import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;

/**
 * 이메일 발송 서비스
 * 
 * @since 2022.06.24
 * @author kh
 */
@Slf4j
@Service
public class EmailService {

    private final JavaMailSender javaMailSender;

    public EmailService(JavaMailSender javaMailSender) {

        this.javaMailSender = javaMailSender;
    }

    // 메일 발송
    @Async
    public boolean sendMail(EmailObjectDataDto emailData, boolean isMultipart) {
        boolean result = false;

        // 메일 정보
        String addr = emailData.getSndEmailAddr();
        String subject = emailData.getMailSubject();
        String context = emailData.getMailContext();
        String jhipsterMailFrom = emailData.getJhipsterMailFrom();

        log.debug("[[메일발송 셋팅]]");
        log.debug("addr :: "+addr);
        log.debug("subject :: "+subject);
        log.debug("context :: "+context);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setTo(addr);
            message.setFrom(jhipsterMailFrom);
            message.setSubject(subject);
            message.setText(context, true);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", addr);

            result = true;
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.warn("Email could not be sent to user '{}':{}", addr, e);
            } else {
                log.warn("Email could not be sent to user '{}':{}", addr, e.getMessage());
            }
        }

        return result;
    }
}
