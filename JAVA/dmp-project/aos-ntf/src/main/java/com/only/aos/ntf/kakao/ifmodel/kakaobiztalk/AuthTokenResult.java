package com.only.aos.ntf.kakao.ifmodel.kakaobiztalk;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthTokenResult {
    private boolean success;
    private String token;
}
