package com.only.aos.ntf.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.only.aos.ntf.dto.common.CommonDTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MobleMsgModelDTO extends CommonDTO {

    private List<MobleMsgObjectDataDto> list;
    private String ekey;

    @Getter
    @Setter
    @ToString
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    @JsonIgnoreProperties(ignoreUnknown=true)
    public static class MobleMsgObjectDataDto {
        private String msgNo = "";
        private String sendType = "";
        private String toNum = "";
        private String fromNum = "";
        private String msgType = "";
        private String msgSubject = "";
        private String msgContext = "";
        private String val = "";
        private String image = "";
        private String cmpgId = "";
        private String datetime = "";
        private String rltCode = "";
        private String rltMsg = "";
        private String custSeqNo = "";
        private String sendCnt = "";
        private String tmpltNo = "";
        private String tmpltGroup = "";
        private String tmpltCode = "";
        private String tmpltVersion = "";
        private String regId = "";
        private String regDt = "";

        private String apiExternalCertKey = "";
        private String apiKey;
        private String apiSecret;
        private String encType;
    }
}
