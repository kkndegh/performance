/** mailsung */
package com.only.aos.ntf.scheduler;

import com.only.aos.ntf.service.AosNtfService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * NTF 발송 스케줄러
 * 
 * @since 2022.05.25
 * @author kh
 */
@Component
public class NtfScheduler {

    @Value("${scheduler.run.msg.send.enabled:false}")
    private boolean runMsgScheduler;

    @Value("${scheduler.run.email.send.enabled:false}")
    private boolean runEmailScheduler;

    @Value("${scheduler.run.kkont.send.enabled:false}")
    private boolean runKkontScheduler;

    @Value("${scheduler.run.kkont.result.enabled:false}")
    private boolean runKkontResultScheduler;

    @Autowired(required = false)
    private AosNtfService aosNtfService;

    //이메일 발송 스케쥴
    @Scheduled(cron = "${scheduler.run.email.send.cron}")
    public void emailExecute() throws Exception {
        if(!runEmailScheduler) return;

        this.runEmailSend();
    }

    //문자 발송 스케쥴
    @Scheduled(cron = "${scheduler.run.msg.send.cron}")
    public void msgExecute() throws Exception {
        if(!runMsgScheduler) return;

        this.runMobileMsgSend();
    }

    //알림톡 발송 스케쥴
    @Scheduled(cron = "${scheduler.run.kkont.send.cron}")
    public void kkontExecute() throws Exception {
        if(!runKkontScheduler) return;

        this.runKkotSend();
    }

    //알림톡 결과 스케쥴
    @Scheduled(cron = "${scheduler.run.kkont.result.cron}")
    public void resultKkontExecute() throws Exception {
        if(!runKkontResultScheduler) return;

        this.runResultkkont();
    }

    //이메일 발송 메서드
    private void runEmailSend() {
        aosNtfService.sendEmail();
    }

    //문자 발송 메서드
    private void runMobileMsgSend() {
        aosNtfService.sendMobileMsg();
    }

    //알림톡 발송 메서드
    private void runKkotSend() {
        aosNtfService.sendkkont();
    }

    //알림톡 결과 저장 메서드
    private void runResultkkont() {
        aosNtfService.sendResultkkont();
    }
}