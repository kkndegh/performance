package com.only.aos.ntf.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Application constants.
 */
@Component
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String ANONYMOUS_USER = "anonymoususer";
    public static final String DEFAULT_LANGUAGE = "ko";
    public static final String BROKER_RESULT_CHECK = "result";
    public static final String RESULT_SUCCESS = "success";

    //외부 API에 대한 AUTH ID 처리를 위함 
    //RequestAttributes req = RequestContextHolder.getRequestAttributes(); instanceof ServletRequestAttributes 을 확인 할 것
    public static final String CUSTOMER_AUDTITOR_NAME = "customer_id";
    
    public static final String HEADER_NAME_SVC_ID = "svc_id";
    public static final String HEADER_NAME_SVC_CERT_KEY = "svc_cert_key";
    
    // Kakao Biz Talk에서 사용하는 default 값
    public static final String KKONT_COUNTRY_CODE= "82";
    public static final String KKONT_RES_METHOD = "PUSH";

    // SMP JPA Query SmpDefaultQuery에서 사용됨 시작 //
    public static final String SMP_DEFAULT_QUERY_DATE_TYPE = "date";
    public static final String SMP_DEFAULT_QUERY_DATETIME_TYPE = "datetime";
    
    // SMP JPA Query SmpDefaultQuery에서 사용됨 끝 //
    
    public static String INICIS_MID;
    public static String INICIS_API_BASE_URL;
    public static String INICIS_API_KEY;

    public static final String INICIS_CHARSET = "UTF-8";
    public static final String INICIS_FORMAT = "JSON";

    @Value("${inicis.mid:}")
    public void setInicisMid(String inicisMid) {
        Constants.INICIS_MID = inicisMid;
    }

    @Value("${inicis.api-base-url:}")
    public void setInicisApiBaseUrl(String inicisApiBaseUrl) {
        Constants.INICIS_API_BASE_URL = inicisApiBaseUrl;
    }

    @Value("${inicis.api-key:}")
    public void setInicisApiKey(String inicisApiKey) {
        Constants.INICIS_API_KEY = inicisApiKey;
    }
}
