package com.only.aos.ntf.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.only.aos.ntf.dto.common.CommonDTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class KakaoModelDTO extends CommonDTO {

    private List<KakaoObjectDataDto> list;

    @Getter
    @Setter
    @ToString
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    @JsonIgnoreProperties(ignoreUnknown=true)
    public static class KakaoObjectDataDto {
        private String kkontNo = "";
        private String kkontTplId = "";
        private String kkontContext = "";
        private String kkontBtnContext = "";
        private String sndTgtCustNo = "";
        private String tmpltCode = "";
        private String sendCnt = "";
        private String regId = "";
        private String regDt = "";
        private String apiExternalCertKey = "";

        private String senderKey = "";
    }
}
