package com.only.aos.ntf.kakao.impl;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.only.aos.ntf.kakao.KakaoBizTalkClient;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.AlimTalkRequest;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.AlimTalkResult;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.AuthTokenRequest;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.AuthTokenResult;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.MsgRequest;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.MsgResultSet;

@Component
public class KakaoBizTalkClientImpl implements KakaoBizTalkClient {

    private final Logger log = LoggerFactory.getLogger(KakaoBizTalkClient.class);
    @Value("${kakao.biztalk.baseUrl:}") private String host;
    @Value("${kakao.biztalk.bsId:}") private String bsId;
    @Value("${kakao.biztalk.bsPw:}") private String bsPw;
    @Value("${kakao.biztalk.senderKey:}") private String senderKey;
    
    @Autowired ObjectMapper objectMapper;
    private String token = null;
    
    private void getToken() {
        if( token == null) {
            AuthTokenRequest req = new AuthTokenRequest();
            req.setBsid(bsId);
            req.setPasswd(bsPw);
            getToken(req);
        }
    }
    
    public AuthTokenResult getToken(AuthTokenRequest req){
        try {
            log.debug("### KakaoBizTalkClientImpl getToken  ###");
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(host + "/v1/auth/getToken");

            StringEntity entity = new StringEntity(objectMapper.writeValueAsString(req),"UTF-8");
            entity.setContentType("application/json");
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            StatusLine status = response.getStatusLine();
            HttpEntity resEntity = response.getEntity();
            String responseString = EntityUtils.toString(resEntity);
            if(status.getStatusCode() != 200) {
                log.error(responseString);
            }else {
                AuthTokenResult result = objectMapper.readValue(responseString, AuthTokenResult.class);
                token = result.getToken();
                return result;
            }
            
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
        throw new RuntimeException("getToken Error Check your Ids");
        
    }
    
    private HttpResponse callPost(HttpRequestBase httpReq) {
        try {
            HttpClient client = HttpClients.createDefault();
            log.debug("bt-token:" + token );
            httpReq.addHeader("bt-token", token);
            httpReq.addHeader("Content-Type", "application/json; charset=UTF-8");
            return client.execute(httpReq);
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    private <T> T callTokenCheck(HttpRequestBase httpReq, Class<T> clazz) {
        //Token이 있는지 확인 해야 함.
        getToken();
        
        if(token == null) {
            throw new RuntimeException("Can't Get token");
        }
        
        HttpResponse response = callPost(httpReq);
        StatusLine status = response.getStatusLine();
        if( status.getStatusCode() == 401) {
            token = null;
            getToken();
            response = callPost(httpReq);
        }
        
        status = response.getStatusLine();
        HttpEntity resEntity = response.getEntity();
        try {
            String responseString = EntityUtils.toString(resEntity);
            log.debug("kakao Biz Talk Result : {}" , responseString);
            if(status.getStatusCode() == 200) {
                T result = objectMapper.readValue(responseString, clazz);
                return result;
            }
        }catch(Exception e) {
            throw new RuntimeException("Read Data or JSON to Object Error ", e);
        }
        return null;
    }


    @Override
    public AlimTalkResult sendAlimTalk(AlimTalkRequest req){

        log.debug("### KakaoBizTalkClientImpl sendAlimTalk  ###");
        
        //req.setSenderKey(senderKey);
        HttpPost post = new HttpPost(host + "/v1/kko/sendAlimTalk");
        try {
            String strentity = objectMapper.writeValueAsString(req);
            StringEntity entity = new StringEntity(strentity,"UTF-8");
            entity.setContentType("application/json");
            post.setEntity(entity);            
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
        
        AlimTalkResult result = callTokenCheck(post, AlimTalkResult.class);
        
        if( result != null) return result;

        throw new RuntimeException("kakao BizTalk Send Error");
        
    }


    @Override
    public MsgResultSet getMsgResultAll(){
        log.debug("### KakaoBizTalkClientImpl getMsgResultAll  ###");
        
        HttpGet get = new HttpGet(host + "/v1/kko/getMsgResultAll");
        MsgResultSet result = callTokenCheck(get, MsgResultSet.class);
        if( result != null) return result;
            
        throw new RuntimeException("kakao BizTalk Msg Result Error");
    }

    @Override
    public MsgResultSet getMsgResult(MsgRequest msgIdxArr) {
        log.debug("### KakaoBizTalkClientImpl getMsgResult  ###");
        
        HttpPost post = new HttpPost(host + "/v1/kko/getMsgResult");
        try {
            String strentity = objectMapper.writeValueAsString(msgIdxArr);
            StringEntity entity = new StringEntity(strentity,"UTF-8");
            entity.setContentType("application/json");
            post.setEntity(entity);
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
        
        MsgResultSet result = callTokenCheck(post, MsgResultSet.class);
        
        if( result != null) return result;

        throw new RuntimeException("kakao BizTalk Msg One Result Error");
    }

    
}
