package com.only.aos.ntf.kakao.ifmodel.kakaobiztalk;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MsgResultRequest {
    private List<String> msgIdx;
}
