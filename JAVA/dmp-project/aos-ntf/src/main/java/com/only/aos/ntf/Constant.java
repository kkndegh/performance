/** mailsung */
package com.only.aos.ntf;

import java.nio.charset.Charset;

public class Constant {
    public static final Charset DEFAULT_CHARSET = Charset.forName("EUC-KR");

    //HttpParam 셋팅시 Title 목록
    public static final String MAIL_LIST_TITLE = "[메일발송 리스트 조회 Params]";
    public static final String MAIL_LOG = "[메일로그 저장 데이터 Params]";
    public static final String MAIL_RESULT = "[메일 발송 상태결과 저장 데이터 Params]";

    public static final String MOBILE_MSG = "[문자발송 리스트 조회 데이터 Params]";
    public static final String MOBILE_MSG_LOG = "[문자발송 로그용 Params]";
    public static final String MOBILE_MSG_RESULT = "[문자발송 상태결과 저장 데이터 Params]";

    public static final String KKONT_INFO = "[알림톡 발송 리스트 조회 데이터 Params]";
    public static final String KKONT_LOG = "[알림톡 로그용 Params]";
    public static final String KKONT_RST_RESULT = "[알림톡 상태결과 저장 데이터 Params]";
} 