package com.only.aos.ntf.kakao.ifmodel.kakaobiztalk;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AlimTalkRequest {

    private String msgIdx;
    private String countryCode;
    private String recipient;
    private String senderKey;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String orgCode;
    private String message;
    private String tmpltCode;
    private String resMethod;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String, AlimTalkButton[]> attach;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer timeout;
}
