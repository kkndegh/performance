package com.only.aos.ntf.kakao.ifmodel.kakaobiztalk;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class MsgResultSet {
    private String success;
    private List<MsgResult> list;
}
