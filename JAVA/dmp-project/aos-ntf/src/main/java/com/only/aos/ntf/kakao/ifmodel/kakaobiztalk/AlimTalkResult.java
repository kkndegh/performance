package com.only.aos.ntf.kakao.ifmodel.kakaobiztalk;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AlimTalkResult {
    private String result;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String msg;
}
