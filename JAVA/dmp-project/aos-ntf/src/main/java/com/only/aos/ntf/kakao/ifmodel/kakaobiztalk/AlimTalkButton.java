package com.only.aos.ntf.kakao.ifmodel.kakaobiztalk;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AlimTalkButton {
    public String name;
    public String type;
    public String scheme_android;
    public String scheme_ios;
    public String url_mobile;
    public String url_pc;
    public String chart_extra;
    public String chart_event;
}
