package com.only.aos.ntf.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.only.aos.ntf.dto.common.CommonDTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailModelDTO extends CommonDTO {

    private List<EmailObjectDataDto> list;

    @Getter
    @Setter
    @ToString
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    @JsonIgnoreProperties(ignoreUnknown=true)
    public static class EmailObjectDataDto {
        private String mailNo = "";
        private String sndEmailAddr = "";
        private String mailSubject = "";
        private String mailContext = "";
        private String regId = "";
        private String regDt = "";
        private String apiExternalCertKey = "";

        private String jhipsterMailFrom = "";
    }
}
