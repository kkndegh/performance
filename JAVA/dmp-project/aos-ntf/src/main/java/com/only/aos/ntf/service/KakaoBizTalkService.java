package com.only.aos.ntf.service;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.only.aos.ntf.config.Constants;
import com.only.aos.ntf.dto.KakaoModelDTO.KakaoObjectDataDto;
import com.only.aos.ntf.kakao.KakaoBizTalkClient;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.AlimTalkButton;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.AlimTalkRequest;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.AlimTalkResult;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.MsgResult;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.MsgResultSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * 카카오톡 발송 서비스
 * 
 * @since 2022.06.24
 * @author kh
 */
@Slf4j
@Service
public class KakaoBizTalkService {
    @Autowired
    private KakaoBizTalkClient kakaoBizTalkClient;

    @Autowired
    private ObjectMapper objectMapper;
    
    /**
     * 알림톡 발송
     * 
     * @param JSONObject kakaoInfoObj
     * @return HashMap<String, Object>
     * @throws Exception
     */
    public HashMap<String, Object> sendkkont(KakaoObjectDataDto kkontData){
        HashMap<String, Object> result = new HashMap<String, Object>();
        
        String phNum = kkontData.getSndTgtCustNo();
        String content = kkontData.getKkontContext();
        String kkontTplId = kkontData.getKkontTplId();
        String trsRltSeqNo = kkontData.getKkontNo();
        String btnCont = kkontData.getKkontBtnContext();
        String senderKey = kkontData.getSenderKey();
        
        try {
            AlimTalkRequest alimtalk = new AlimTalkRequest();
            alimtalk.setCountryCode(Constants.KKONT_COUNTRY_CODE);
            alimtalk.setResMethod(Constants.KKONT_RES_METHOD);
            alimtalk.setTmpltCode(kkontTplId);
            alimtalk.setMsgIdx(trsRltSeqNo);
            alimtalk.setRecipient(phNum);
            alimtalk.setSenderKey(senderKey);

            if (!StringUtils.isEmpty(btnCont)) {
                Map<String, AlimTalkButton[]> attach = new TreeMap<String, AlimTalkButton[]>();
                alimtalk.setAttach(attach);
                AlimTalkButton[] buttons = objectMapper.readValue(btnCont, AlimTalkButton[].class);
                attach.put(KakaoBizTalkClient.ATTACH_BUTTON, buttons);
            }
            alimtalk.setMessage(content);

            // 전송
            AlimTalkResult retval = kakaoBizTalkClient.sendAlimTalk(alimtalk);
            result.put("resultCode", retval.getResult());
            result.put("resultMsg", retval.getMsg());

            if (KakaoBizTalkClient.RESULT_SUCCESS.equals(retval.getResult())) {
                // addedEnt.setKkontTrsYn("Y"); // 전송 성공
                // addedEnt.setSuccRlt("SY02"); // 전송 성공 & 결과 수신 전
            } else {
                // addedEnt.setKkontTrsYn("N"); // 전송 실패
                // addedEnt.setSuccRlt("SY01"); // 카카오톡 전송 실패
            }
            return result;
        } catch (Exception e) {
            log.error("에러 :: " + e.toString());
            result.put("res_data", "{\"result\":\"9999\",\"msg\":\"요청 처리 실패\"}");
            return result;
        }
    }

    /**
     * 알림톡 결과 수신 및 저장
     * 
     * @param JSONObject kakaoInfoObj
     * @return HashMap<String, Object>
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public HashMap<String, Object> sendResultkkont(){
        HashMap<String, Object> result = new HashMap<String, Object>();
        // JSONArray msgResultArray = new JSONArray();
        JSONArray msgOriginResultArray = new JSONArray();

        try {
            //통신결과를 JSON문자열로 셋팅하여 넘기기 위해 모델값을 JSON ARRAY로 생성.
            MsgResultSet msgResult = kakaoBizTalkClient.getMsgResultAll();
            for(MsgResult rst : msgResult.getList()){
                JSONObject msgOriginResultObj = new JSONObject();
                msgOriginResultObj.put("msgIdx", rst.getMsgIdx());
                msgOriginResultObj.put("resultCode", rst.getResultCode());
                msgOriginResultObj.put("requestAt", rst.getRequestAt());
                msgOriginResultObj.put("receiveAt", rst.getReceivedAt());
                msgOriginResultArray.add(msgOriginResultObj);

                //NTF내 결과코드를 내부에서 선언한 코드로 사용하기위하여 결과코드를 내부코드로 변환한 JSON ARRAY도 생성
                // JSONObject msgResultObj = new JSONObject();
                // msgResultObj.put("msgIdx", rst.getMsgIdx());
                // msgResultObj.put("requestAt", rst.getRequestAt());
                // msgResultObj.put("receiveAt", rst.getReceivedAt());
                // if("1000".equals(rst.getResultCode().toString())){
                //     msgResultObj.put("resultCode", "R000");
                // }else{
                //     msgResultObj.put("resultCode", "R001");
                // }
                // msgResultArray.add(msgResultObj);
            }
            result.put("resultCode", msgResult.getSuccess());
            //result.put("list", msgResultArray);
            result.put("list", msgOriginResultArray);
            
            return result;
        } catch (Exception e) {
            log.error("에러 :: " + e.toString());
            result.put("resData", "{\"result\":\"9999\",\"msg\":\"요청 처리 실패\"}");
            return result;
        }
    }
}
