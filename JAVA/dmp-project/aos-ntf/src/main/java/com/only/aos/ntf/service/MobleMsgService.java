package com.only.aos.ntf.service;

import java.util.HashMap;
import java.util.Random;

import com.only.aos.ntf.common.util.EncUtil;
import com.only.aos.ntf.common.util.HttpUtil;
import com.only.aos.ntf.dto.HttpParam;
import com.only.aos.ntf.dto.MobleMsgModelDTO.MobleMsgObjectDataDto;
import com.eclipsesource.json.JsonObject;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 문자 발송 서비스
 * 
 * @since 2022.06.24
 * @author kh
 */
@Slf4j
@Service
public class MobleMsgService {

    @Value("${uplus.url}")
    private String uplusUrl;

    @Autowired
    private HttpUtil httpUtil;

    // 메시지 발송
    public HashMap<String, Object> send(MobleMsgObjectDataDto mobleMsgData) {
        HashMap<String, Object> result = new HashMap<String, Object>();

        try {
            //기본정보
            String url = uplusUrl+"/v1/send";
            String apiKey = mobleMsgData.getApiKey();
            String apiSecret = mobleMsgData.getApiSecret();
            String encType = mobleMsgData.getEncType();

            // 인증정보(HMAC)
            // 값 셋팅
            HashMap<String, String> encHashHmac = new HashMap<String, String>();
            encHashHmac.put("apiKey", apiKey);
            encHashHmac.put("apiSecret", apiSecret);
            encHashHmac.put("encType", encType);
            // 결과
            HashMap<String, String> hmacObj = encHashHmac(encHashHmac);
            String hmac = hmacObj.get("hmac");
            String salt = hmacObj.get("salt");
            String timestamp = hmacObj.get("timestamp");

            // ====================================================================
            // 메일발송 통신 셋팅
            // ====================================================================
            // 헤더셋팅
            JsonObject headerParams = new JsonObject();
            headerParams.add("hash_hmac", hmac); // Hash된 인증정보
            headerParams.add("api_key", apiKey); // 고유 식별 Key
            headerParams.add("timestamp", timestamp); // 암호화시간 - 13자리(Millisecond 포함)
            headerParams.add("cret_txt", salt); // HMAC 생성 시 사용한 10자리 랜덤 숫자(salt)
            headerParams.add("algorithm", encType); // 암호화방식(0:SHA-256, 1:SHA-1, 2:MD5)

            // 바디셋팅
            JsonObject bodyParams = new JsonObject();
            bodyParams.add("send_type", mobleMsgData.getSendType()); // 발송형태(R:예약,S:즉시)
            bodyParams.add("cmpg_id", mobleMsgData.getCmpgId()); // 예약 ID(영문, 숫자, -, _) 40byte
            bodyParams.add("to", mobleMsgData.getToNum()); // 수신자번호
            bodyParams.add("from", mobleMsgData.getFromNum()); // 발송전화번호
            bodyParams.add("msg_type", mobleMsgData.getMsgType()); // 메시지종류 (S,M,L)
            bodyParams.add("subject", mobleMsgData.getMsgSubject()); // 메시지제목
            bodyParams.add("datetime", mobleMsgData.getDatetime()); // 예약시간(YYYYMMDDHH24MI)
            bodyParams.add("msg", mobleMsgData.getMsgContext()); // 메시지 내용
            bodyParams.add("val", mobleMsgData.getVal()); // 메시지제한 시, 합성항목(파이프라인('|')으로 구분)
            bodyParams.add("image", mobleMsgData.getImage()); // MMS 발송 시 사용(이미지 3개 까지 첨부가능)

            result.put("reqHeader", headerParams.toString());
            result.put("reqBody", bodyParams.toString());

            HttpParam params = new HttpParam("[params]", url, "POST", headerParams.toString(), bodyParams.toString());    //통신파라미터 초기화
            result.put("resData", httpUtil.callApiHttps(params));

            return result;
        } catch (Exception e) {
            log.error("에러 :: " + e.toString());
            result.put("resData", "{\"rcode\":\"9999\",\"rdesc\":\"요청 처리 실패\"}");
            return result;
        }
    }

    //결과 조회
    public HashMap<String, Object> pop(JSONObject paramObj){
        HashMap<String, Object> result = new HashMap<String, Object>();

        try {
            //기본정보
            String url = uplusUrl+"/v1/pop";
            String apiKey = (String) paramObj.get("apiKey");
            String apiSecret = (String) paramObj.get("apiSecret");
            String encType = (String) paramObj.get("encType");

            //인증정보(HMAC)
            //값 셋팅
            HashMap<String, String> encHashHmac = new HashMap<String, String>();
            encHashHmac.put("apiKey", apiKey);
            encHashHmac.put("apiSecret", apiSecret);
            encHashHmac.put("encType", encType);
            //결과
            HashMap<String, String> hmacObj = encHashHmac(encHashHmac);
            String hmac = hmacObj.get("hmac");
            String salt = hmacObj.get("salt");
            String timestamp = hmacObj.get("timestamp");

            //====================================================================
            //메일발송 통신 셋팅
            //====================================================================
            //헤더셋팅
            JsonObject headerParams = new JsonObject();
            headerParams.add("hash_hmac", hmac);        //Hash된 인증정보
            headerParams.add("api_key", apiKey);        //고유 식별 Key
            headerParams.add("timestamp", timestamp);   //암호화시간 - 13자리(Millisecond 포함)
            headerParams.add("cret_txt", salt);         //HMAC 생성 시 사용한 10자리 랜덤 숫자(salt)
            headerParams.add("algorithm", encType);     //암호화방식(0:SHA-256, 1:SHA-1, 2:MD5)

            //바디셋팅
            JsonObject bodyParams = new JsonObject();
            bodyParams.add("limit", (String) paramObj.get("limit"));       //요청 개수

            result.put("reqHeader", headerParams.toString());
            result.put("reqBody", bodyParams.toString());

            HttpParam params = new HttpParam("[params]", url, "POST", headerParams.toString(), bodyParams.toString());    //통신파라미터 초기화
            result.put("resData", httpUtil.callApiHttps(params));

            return result;
        } catch (Exception e) {
            log.error("에러 :: " + e.toString());
            result.put("resData", "{\"rcode\":\"9999\",\"rdesc\":\"요청 처리 실패\"}");
            return result;
        }
    }

    //발송건별 조회
    public HashMap<String, Object> sent(JSONObject paramObj){
        HashMap<String, Object> result = new HashMap<String, Object>();

        try {
            //기본정보
            String url = uplusUrl+"/v1/sent";
            String apiKey = (String) paramObj.get("apiKey");
            String apiSecret = (String) paramObj.get("apiSecret");
            String encType = (String) paramObj.get("encType");

            //인증정보(HMAC)
            //값 셋팅
            HashMap<String, String> encHashHmac = new HashMap<String, String>();
            encHashHmac.put("apiKey", apiKey);
            encHashHmac.put("apiSecret", apiSecret);
            encHashHmac.put("encType", encType);
            //결과
            HashMap<String, String> hmacObj = encHashHmac(encHashHmac);
            String hmac = hmacObj.get("hmac");
            String salt = hmacObj.get("salt");
            String timestamp = hmacObj.get("timestamp");

            //====================================================================
            //메일발송 통신 셋팅
            //====================================================================
            //헤더셋팅
            JsonObject headerParams = new JsonObject();
            headerParams.add("hash_hmac", hmac);        //Hash된 인증정보
            headerParams.add("api_key", apiKey);        //고유 식별 Key
            headerParams.add("timestamp", timestamp);   //암호화시간 - 13자리(Millisecond 포함)
            headerParams.add("cret_txt", salt);         //HMAC 생성 시 사용한 10자리 랜덤 숫자(salt)
            headerParams.add("algorithm", encType);     //암호화방식(0:SHA-256, 1:SHA-1, 2:MD5)

            //바디셋팅
            JsonObject bodyParams = new JsonObject();
            bodyParams.add("msg_id", (String) paramObj.get("msgId"));         //메시지 발송 ID
            bodyParams.add("limit", (String) paramObj.get("limit"));          //요청 개수
            bodyParams.add("page", (String) paramObj.get("from"));            //페이지

            result.put("reqHeader", headerParams.toString());
            result.put("reqBody", bodyParams.toString());

            HttpParam params = new HttpParam("[params]", url, "POST", headerParams.toString(), bodyParams.toString());    //통신파라미터 초기화
            result.put("resData", httpUtil.callApiHttps(params));

            return result;
        } catch (Exception e) {
            log.error("에러 :: " + e.toString());
            result.put("resData", "{\"rcode\":\"9999\",\"rdesc\":\"요청 처리 실패\"}");
            return result;
        }
    }

    //예약 취소
    public HashMap<String, Object> cancel(JSONObject paramObj){
        HashMap<String, Object> result = new HashMap<String, Object>();

        try {
            //기본정보
            String url = uplusUrl+"/v1/cancel";
            String apiKey = (String) paramObj.get("apiKey");
            String apiSecret = (String) paramObj.get("apiSecret");
            String encType = (String) paramObj.get("encType");

            //인증정보(HMAC)
            //값 셋팅
            HashMap<String, String> encHashHmac = new HashMap<String, String>();
            encHashHmac.put("apiKey", apiKey);
            encHashHmac.put("apiSecret", apiSecret);
            encHashHmac.put("encType", encType);
            //결과
            HashMap<String, String> hmacObj = encHashHmac(encHashHmac);
            String hmac = hmacObj.get("hmac");
            String salt = hmacObj.get("salt");
            String timestamp = hmacObj.get("timestamp");

            //====================================================================
            //메일발송 통신 셋팅
            //====================================================================
            //헤더셋팅
            JsonObject headerParams = new JsonObject();
            headerParams.add("hash_hmac", hmac);        //Hash된 인증정보
            headerParams.add("api_key", apiKey);        //고유 식별 Key
            headerParams.add("timestamp", timestamp);   //암호화시간 - 13자리(Millisecond 포함)
            headerParams.add("cret_txt", salt);         //HMAC 생성 시 사용한 10자리 랜덤 숫자(salt)
            headerParams.add("algorithm", encType);     //암호화방식(0:SHA-256, 1:SHA-1, 2:MD5)

            //바디셋팅
            JsonObject bodyParams = new JsonObject();
            bodyParams.add("msg_id", (String) paramObj.get("msgId"));         //메시지 발송 ID
            bodyParams.add("cmpg_id", (String) paramObj.get("cmpgId"));       //예약 ID
            bodyParams.add("grp_id", (String) paramObj.get("grpId"));         //페이지

            result.put("reqHeader", headerParams.toString());
            result.put("reqBody", bodyParams.toString());

            HttpParam params = new HttpParam("[params]", url, "POST", headerParams.toString(), bodyParams.toString());    //통신파라미터 초기화
            result.put("resData", httpUtil.callApiHttps(params));

            return result;
        } catch (Exception e) {
            log.error("에러 :: " + e.toString());
            result.put("resData", "{\"rcode\":\"9999\",\"rdesc\":\"요청 처리 실패\"}");
            return result;
        }
    }

    //선불 잔액조회
    public HashMap<String, Object> cash(JSONObject paramObj){
        HashMap<String, Object> result = new HashMap<String, Object>();

        try {
            //기본정보
            String url = uplusUrl+"/v1/cash";
            String apiKey = (String) paramObj.get("apiKey");
            String apiSecret = (String) paramObj.get("apiSecret");
            String encType = (String) paramObj.get("encType");

            //인증정보(HMAC)
            //값 셋팅
            HashMap<String, String> encHashHmac = new HashMap<String, String>();
            encHashHmac.put("apiKey", apiKey);
            encHashHmac.put("apiSecret", apiSecret);
            encHashHmac.put("encType", encType);
            //결과
            HashMap<String, String> hmacObj = encHashHmac(encHashHmac);
            String hmac = hmacObj.get("hmac");
            String salt = hmacObj.get("salt");
            String timestamp = hmacObj.get("timestamp");

            //====================================================================
            //메일발송 통신 셋팅
            //====================================================================
            //헤더셋팅
            JsonObject headerParams = new JsonObject();
            headerParams.add("hash_hmac", hmac);        //Hash된 인증정보
            headerParams.add("api_key", apiKey);        //고유 식별 Key
            headerParams.add("timestamp", timestamp);   //암호화시간 - 13자리(Millisecond 포함)
            headerParams.add("cret_txt", salt);         //HMAC 생성 시 사용한 10자리 랜덤 숫자(salt)
            headerParams.add("algorithm", encType);     //암호화방식(0:SHA-256, 1:SHA-1, 2:MD5)

            //바디셋팅
            JsonObject bodyParams = new JsonObject();

            result.put("reqHeader", headerParams.toString());
            result.put("reqBody", bodyParams.toString());

            HttpParam params = new HttpParam("[params]", url, "POST", headerParams.toString(), bodyParams.toString());    //통신파라미터 초기화
            result.put("resData", httpUtil.callApiHttps(params));

            return result;
        } catch (Exception e) {
            log.error("에러 :: " + e.toString());
            result.put("resData", "{\"rcode\":\"9999\",\"rdesc\":\"요청 처리 실패\"}");
            return result;
        }
    }

    //통계 조회
    public HashMap<String, Object> status(JSONObject paramObj){
        HashMap<String, Object> result = new HashMap<String, Object>();

        try {
            //기본정보
            String url = uplusUrl+"/v1/status";
            String apiKey = (String) paramObj.get("apiKey");
            String apiSecret = (String) paramObj.get("apiSecret");
            String encType = (String) paramObj.get("encType");

            //인증정보(HMAC)
            //값 셋팅
            HashMap<String, String> encHashHmac = new HashMap<String, String>();
            encHashHmac.put("apiKey", apiKey);
            encHashHmac.put("apiSecret", apiSecret);
            encHashHmac.put("encType", encType);
            //결과
            HashMap<String, String> hmacObj = encHashHmac(encHashHmac);
            String hmac = hmacObj.get("hmac");
            String salt = hmacObj.get("salt");
            String timestamp = hmacObj.get("timestamp");

            //====================================================================
            //메일발송 통신 셋팅
            //====================================================================
            //헤더셋팅
            JsonObject headerParams = new JsonObject();
            headerParams.add("hash_hmac", hmac);        //Hash된 인증정보
            headerParams.add("api_key", apiKey);        //고유 식별 Key
            headerParams.add("timestamp", timestamp);   //암호화시간 - 13자리(Millisecond 포함)
            headerParams.add("cret_txt", salt);         //HMAC 생성 시 사용한 10자리 랜덤 숫자(salt)
            headerParams.add("algorithm", encType);     //암호화방식(0:SHA-256, 1:SHA-1, 2:MD5)

            //바디셋팅
            JsonObject bodyParams = new JsonObject();
            bodyParams.add("type", (String) paramObj.get("type"));               //m : 월 , d: 일
            bodyParams.add("send_date", (String) paramObj.get("sendDate"));      //발송월(yyyymm), 발송일자(yyyymmdd)

            result.put("reqHeader", headerParams.toString());
            result.put("reqBody", bodyParams.toString());

            HttpParam params = new HttpParam("[params]", url, "POST", headerParams.toString(), bodyParams.toString());    //통신파라미터 초기화
            result.put("resData", httpUtil.callApiHttps(params));

            return result;
        } catch (Exception e) {
            log.error("에러 :: " + e.toString());
            result.put("resData", "{\"rcode\":\"9999\",\"rdesc\":\"요청 처리 실패\"}");
            return result;
        }
    }

    // 인증정보(HMAC) 생성
    private static HashMap<String, String> encHashHmac(HashMap<String, String> encData) {
        log.info("encData  :: "+encData);
        String apiKey       = encData.get("apiKey");        // 고유 식별 Key
        long timestamp      = System.currentTimeMillis();   // 암호화시간 - 13자리(Millisecond 포함)
        String salt         = "";                           // 10자리 랜덤숫자
        String apiSecret    = encData.get("apiSecret");     // 사용자 API_KEY 비밀번호
        String encType      = encData.get("encType");       // 암호화방식(0:SHA-256, 1:SHA-1, 2:MD5)
        String hmac=""; //암호화 값

        //10자리 랜덤숫자 셋팅
        while(salt.length()<10){
            Random r=new Random();
            int j=r.nextInt(10);
            salt+=String.valueOf(j);
        }

        StringBuilder sb = new StringBuilder();
        sb.append( apiKey );
        sb.append( timestamp );
        sb.append( salt ); // 10자리 랜덤숫자
        sb.append( apiSecret ); // 사용자 API_KEY 비밀번호

        switch (encType) {
            case "0":
                hmac = EncUtil.encSHA256(sb.toString());
                break;
            case "1":
                hmac = EncUtil.encSHA1(sb.toString());
                break;
            case "2":
                hmac = EncUtil.encMD5(sb.toString());
                break;
        }

        //결과 셋팅
        HashMap<String, String> rstData = new HashMap<String, String>();    //결과맵
        rstData.put("hmac", hmac);
        rstData.put("salt", salt);
        rstData.put("timestamp", String.valueOf(timestamp));

        return rstData;
    }
}
