package com.only.aos.ntf.kakao.ifmodel.kakaobiztalk;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MsgRequest {
    String[] msgIdx;
}