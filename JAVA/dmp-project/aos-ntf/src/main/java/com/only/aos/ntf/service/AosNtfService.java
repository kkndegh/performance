/** mailsung */
package com.only.aos.ntf.service;

import java.util.HashMap;

import com.only.aos.ntf.Constant;
import com.only.aos.ntf.common.BaseValue;
import com.only.aos.ntf.dto.EmailApiKeyDTO;
import com.only.aos.ntf.dto.EmailModelDTO;
import com.only.aos.ntf.dto.HttpParam;
import com.only.aos.ntf.dto.KakaoApiKeyDTO;
import com.only.aos.ntf.dto.KakaoModelDTO;
import com.only.aos.ntf.dto.MobleMsgApiKeyDTO;
import com.only.aos.ntf.dto.MobleMsgModelDTO;
import com.only.aos.ntf.dto.EmailModelDTO.EmailObjectDataDto;
import com.only.aos.ntf.dto.KakaoModelDTO.KakaoObjectDataDto;
import com.only.aos.ntf.dto.MobleMsgModelDTO.MobleMsgObjectDataDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * ntf 발송 서비스
 * 
 * @since 2022.05.25
 * @author kh
 */
@Slf4j
@Service
public class AosNtfService extends BaseValue{

    @Autowired(required = false)
    private EmailService eMailService;

    @Autowired(required = false)
    private MobleMsgService mobleMsgService;

    @Autowired(required = false)
    private KakaoBizTalkService kakaoBizTalkService;

    @Value("${api.https.boolean}")
    public boolean apiHttpsBoolean;

    /**
     * 메일 발송
     * 
     * @return String
     */
    @SuppressWarnings("unchecked")
    public String sendEmail() {
        HashMap<String, String> rstObj = new HashMap<String, String>();

        try {
            String mailInfoStr = "";    //메일발송 응답값
            
            /*********************************************
             * 1. 메일발송 정보 조회 
             *********************************************/
            String mailInfoUrl = aosBaseUrl + "/api/v1/mail/select";
            String mailInfoMethod = "GET";
            String mailInfoHeader = "{\"access_token\":\"qwerty\"}";
            String mailInfoBody = "{}";

            HttpParam mailInfoParam = new HttpParam(Constant.MAIL_LIST_TITLE, mailInfoUrl, mailInfoMethod, mailInfoHeader, mailInfoBody);    //통신파라미터 초기화
            log.debug(mailInfoParam.toString());

            //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
            if(!apiHttpsBoolean){
                mailInfoStr = httpUtil.callApiHttp(mailInfoParam);
            }else{
                mailInfoStr = httpUtil.callApiHttps(mailInfoParam);
            }
            EmailModelDTO emailModelDTO = objectMapper.readValue(mailInfoStr, EmailModelDTO.class);

            if (emailModelDTO.getList().size() > 0) {
                for(int i = 0; i < emailModelDTO.getList().size(); i++){
                    EmailObjectDataDto emailData = emailModelDTO.getList().get(i);
                    
                    if(StringUtils.isNotEmpty(emailData.getApiExternalCertKey())){
                        //API키 셋팅
                        EmailApiKeyDTO emailApiKeyDTO = objectMapper.readValue(emailData.getApiExternalCertKey(), EmailApiKeyDTO.class);
                        emailData.setJhipsterMailFrom(emailApiKeyDTO.getJhipsterMailFrom());
                        
                        log.debug("[[메일발송 셋팅("+i+")]]");
                        log.debug("emailData :: "+emailData);

                        /*********************************************
                         * 2.1 메일 발송
                         *********************************************/
                        boolean sendMail = eMailService.sendMail(emailData, false);

                        if(sendMail){
                            /*********************************************
                             * 2.2 메일 발송 로그 저장
                             *********************************************/
                            JSONObject mailLogObj = new JSONObject();
                            mailLogObj.put("mail_no", emailData.getMailNo());
                            mailLogObj.put("sendYn", String.valueOf(sendMail));

                            String mailLogUrl = aosBaseUrl + "/api/v1/mail/log";
                            String mailLogMethod = "POST";
                            String mailLogHeader = "{\"access_token\":\"qwerty\"}";
                            String mailLogBody = mailLogObj.toJSONString();

                            HttpParam mailLogParam = new HttpParam(Constant.MAIL_LOG, mailLogUrl, mailLogMethod, mailLogHeader, mailLogBody);    //통신파라미터 초기화
                            log.debug(mailLogParam.toString());

                            //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
                            if(!apiHttpsBoolean){
                                httpUtil.callApiHttp(mailLogParam);
                            }else{
                                httpUtil.callApiHttps(mailLogParam);
                            }

                            /*********************************************
                             * 2.3 메일 발송 상태결과 저장
                             *********************************************/
                            //상태값 코드 변경 데이터 셋팅
                            JSONObject obj = new JSONObject();
                            if(sendMail){
                                obj.put("msgNo", emailData.getMailNo());
                                obj.put("rcode", "S000");
                                obj.put("rmsg", "발송완료");
                            }
                            String mailResultUrl = aosBaseUrl + "/api/v1/mail/rstResult";
                            String mailResultMethod = "POST";
                            String mailResultHeader = "{\"access_token\":\"qwerty\"}";
                            String mailResultBody = obj.toJSONString();

                            HttpParam mailResultParam = new HttpParam(Constant.MAIL_RESULT, mailResultUrl, mailResultMethod, mailResultHeader, mailResultBody);    //통신파라미터 초기화
                            log.debug(mailResultParam.toString());

                            //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
                            if(!apiHttpsBoolean){
                                httpUtil.callApiHttp(mailResultParam);
                            }else{
                                httpUtil.callApiHttps(mailResultParam);
                            }
                        }
                    }
                }
            } else {
                log.info("발송대상 데이터 없음.");
            }

            rstObj.put("result_code", "P000");
            rstObj.put("result_msg", "정상");
            return rstObj.toString();
        } catch (Exception e) {
            return commonUtil.resultObjMk(commonUtil.getClassName(),commonUtil.getMethodName(),"P999", e.toString());
        }
    }

    /**
     * 문자 발송
     * 
     * @return String
     */
    @SuppressWarnings("unchecked")
    public String sendMobileMsg() {
        HashMap<String, String> rstObj = new HashMap<String, String>();

        try {
            String mobileMsgInfoStr = "";    //문자발송 정보

            /*********************************************
             * 1. 문자발송 정보 조회 
             *********************************************/
            String mobileMsgInfoUrl = aosBaseUrl + "/api/v1/mobmsg/select";
            String mobileMsgInfoMethod = "GET";
            String mobileMsgInfoHeader = "{\"access_token\":\"qwerty\"}";
            String mobileMsgInfoBody = "{}";

            HttpParam mobileMsgParam = new HttpParam(Constant.MOBILE_MSG, mobileMsgInfoUrl, mobileMsgInfoMethod, mobileMsgInfoHeader, mobileMsgInfoBody);    //통신파라미터 초기화
            log.debug(mobileMsgParam.toString());

            //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
            if(!apiHttpsBoolean){
                mobileMsgInfoStr = httpUtil.callApiHttp(mobileMsgParam);
            }else{
                mobileMsgInfoStr = httpUtil.callApiHttps(mobileMsgParam);
            }

            MobleMsgModelDTO mobleMsgModelDTO = objectMapper.readValue(mobileMsgInfoStr, MobleMsgModelDTO.class);
            
            if (mobleMsgModelDTO.getList().size() > 0) {
                for (int i = 0; i < mobleMsgModelDTO.getList().size(); i++) {
                    MobleMsgObjectDataDto mobleMsgData = mobleMsgModelDTO.getList().get(i);

                    if(StringUtils.isNotEmpty(mobleMsgData.getApiExternalCertKey())){
                        //API키 셋팅
                        MobleMsgApiKeyDTO mobleMsgApiKeyDTO = objectMapper.readValue(mobleMsgData.getApiExternalCertKey(), MobleMsgApiKeyDTO.class);
                        mobleMsgData.setApiKey(mobleMsgApiKeyDTO.getApiKey());
                        mobleMsgData.setApiSecret(mobleMsgApiKeyDTO.getApiSecret());
                        mobleMsgData.setEncType(mobleMsgApiKeyDTO.getEncType());

                        log.debug("[[문자발송 셋팅("+i+")]]");
                        log.debug("mobleMsgData :: "+mobleMsgData);

                        /*********************************************
                         * 2.1 문자발송
                         *********************************************/
                        HashMap<String, Object> sendRstMap = mobleMsgService.send(mobleMsgData);

                        //문자발송 응답 결과값
                        String sendRst = (String) sendRstMap.get("resData");
                        String sendReqHeader = (String) sendRstMap.get("reqHeader");
                        String sendReqBody = (String) sendRstMap.get("reqBody");

                        JSONParser jsonParser = new JSONParser();

                        Object resData = jsonParser.parse( sendRst );
                        JSONObject resDataObj = (JSONObject) resData;
                        Object reqHeader = jsonParser.parse(sendReqHeader);
                        Object reqBody = jsonParser.parse(sendReqBody);

                        /*********************************************
                         * 2.2 문자 발송 로그저장
                         *********************************************/
                        //로그용 파라미터 셋팅
                        //msgNo + 문자발송 응답값[rcode] + 문자발송헤더 + 문자발송바디 + 문자발송 응답값 전체 
                        JSONObject mobileMsgLogJsonObj = new JSONObject();
                        mobileMsgLogJsonObj.put("msgNo", mobleMsgData.getMsgNo());              //메시지 시퀀스
                        mobileMsgLogJsonObj.put("rcode", resDataObj.get("rcode"));              //Uplus 응답코드
                        mobileMsgLogJsonObj.put("reqHeader", reqHeader);                        //Uplus 요청 헤더
                        mobileMsgLogJsonObj.put("reqBody", reqBody);                            //Uplus 요청 바디
                        mobileMsgLogJsonObj.put("resData", resData);                            //Uplus 응답값

                        String mobileMsgLogUrl = aosBaseUrl + "/api/v1/mobmsg/log";
                        String mobileMsgLogMethod = "POST";
                        String mobileMsgLogHeader = "{\"access_token\":\"qwerty\"}";
                        String mobileMsgLogBody = mobileMsgLogJsonObj.toJSONString();

                        HttpParam mobileMsgLogParam = 
                            new HttpParam(Constant.MOBILE_MSG_LOG, mobileMsgLogUrl, mobileMsgLogMethod, mobileMsgLogHeader, mobileMsgLogBody);    //통신파라미터 초기화
                        log.debug(mobileMsgLogParam.toString());

                        //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
                        if(!apiHttpsBoolean){
                            httpUtil.callApiHttp(mobileMsgLogParam);
                        }else{
                            httpUtil.callApiHttps(mobileMsgLogParam);
                        }

                        /*********************************************
                         * 2.3 문자 발송 상태결과 저장
                         *********************************************/
                        //상태값 코드 변경 데이터 셋팅
                        JSONObject obj = new JSONObject();
                        if("1000".equals(resDataObj.get("rcode"))){
                            obj.put("msgNo", mobleMsgData.getMsgNo());
                            obj.put("rcode", "S000");
                        }
                        String mobileMsgResultUrl = aosBaseUrl + "/api/v1/mobmsg/rstResult";
                        String mobileMsgResultMethod = "POST";
                        String mobileMsgResultHeader = "{\"access_token\":\"qwerty\"}";
                        String mobileMsgResultBody = obj.toJSONString();

                        HttpParam mobileMsgResultParam = 
                            new HttpParam(Constant.MOBILE_MSG_RESULT, mobileMsgResultUrl, mobileMsgResultMethod, mobileMsgResultHeader, mobileMsgResultBody);    //통신파라미터 초기화
                        log.debug(mobileMsgResultParam.toString());

                        //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
                        if(!apiHttpsBoolean){
                            httpUtil.callApiHttp(mobileMsgResultParam);
                        }else{
                            httpUtil.callApiHttps(mobileMsgResultParam);
                        }
                    }
                }
            } else {
                log.info("발송대상 데이터 없음.");
            }

            rstObj.put("result_code", "P000");
            rstObj.put("result_msg", "정상");
            return rstObj.toString();
        } catch (Exception e) {
            return commonUtil.resultObjMk(commonUtil.getClassName(),commonUtil.getMethodName(),"P999", e.toString());
        }
    }

    /**
     * 알림톡 발송
     * 
     * @return String
     */
    @SuppressWarnings("unchecked")
    public String sendkkont() {
        HashMap<String, String> rstObj = new HashMap<String, String>();

        try {
            String kkontInfoStr = "";    //알림톡 정보

            /*********************************************
             * 1. 알림톡 발송 정보 조회 
             *********************************************/
            String kkontInfoUrl = aosBaseUrl + "/api/v1/kkont/select";
            String kkontInfoMethod = "GET";
            String kkontInfoHeader = "{\"access_token\":\"qwerty\"}";
            String kkontInfoBody = "{}";

            log.debug("[[알림톡 발송 리스트 조회 데이터 셋팅]]");
            log.debug("kkontInfoUrl :: "+kkontInfoUrl);
            log.debug("kkontInfoMethod :: "+kkontInfoMethod);
            log.debug("kkontInfoHeader :: "+kkontInfoHeader);
            log.debug("kkontInfoBody :: "+kkontInfoBody);

            HttpParam kkontInfoParam = 
                new HttpParam(Constant.KKONT_INFO, kkontInfoUrl, kkontInfoMethod, kkontInfoHeader, kkontInfoBody);    //통신파라미터 초기화
            log.debug(kkontInfoParam.toString());


            //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
            if(!apiHttpsBoolean){
                kkontInfoStr = httpUtil.callApiHttp(kkontInfoParam);
            }else{
                kkontInfoStr = httpUtil.callApiHttps(kkontInfoParam);
            }

            KakaoModelDTO kakaoModelDTO = objectMapper.readValue(kkontInfoStr, KakaoModelDTO.class);

            log.debug("==알림톡 발송 리스트 조회결과==");
            log.debug("==result_msg :: " + kakaoModelDTO.getResultMsg());
            log.debug("==result_code :: " + kakaoModelDTO.getResultCode());
            log.debug("==list :: " + kakaoModelDTO.getList());

            if(kakaoModelDTO.getList().size() > 0){
                for(int i=0; i < kakaoModelDTO.getList().size(); i++){
                    KakaoObjectDataDto kkontData = kakaoModelDTO.getList().get(i);
                    
                    if(StringUtils.isNotEmpty(kkontData.getApiExternalCertKey())){
                        //API키 셋팅
                        KakaoApiKeyDTO kakaoApiKeyDTO = objectMapper.readValue(kkontData.getApiExternalCertKey(), KakaoApiKeyDTO.class);
                        kkontData.setSenderKey(kakaoApiKeyDTO.getSenderKey());

                        log.debug("[[알림톡 셋팅("+i+")]]");
                        log.debug("kkontData :: "+kkontData);

                        /*********************************************
                         * 2.1 알림톡 발송
                         *********************************************/
                        HashMap<String, Object> sendRstMap = kakaoBizTalkService.sendkkont(kkontData);
                        
                        /*********************************************
                         * 2.2 알림톡 발송 로그저장
                         *********************************************/
                        //로그용 파라미터 셋팅
                        //kkontNo + 알림톡 발송 응답값
                        JSONObject kkontLogJsonObj = new JSONObject();
                        kkontLogJsonObj.put("kkontNo", kkontData.getKkontNo());    //카카오톡 시퀀스
                        kkontLogJsonObj.put("resData", sendRstMap.toString());     //카카오톡 응답값
                        
                        String kkontLogUrl = aosBaseUrl + "/api/v1/kkont/log";
                        String kkontLogMethod = "POST";
                        String kkontLogHeader = "{\"access_token\":\"qwerty\"}";
                        String kkontLogBody = kkontLogJsonObj.toJSONString();

                        HttpParam kkontLogParam = 
                            new HttpParam(Constant.KKONT_LOG, kkontLogUrl, kkontLogMethod, kkontLogHeader, kkontLogBody);    //통신파라미터 초기화
                        log.debug(kkontLogParam.toString());

                        //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
                        if(!apiHttpsBoolean){
                            httpUtil.callApiHttp(kkontLogParam);
                        }else{
                            httpUtil.callApiHttps(kkontLogParam);
                        }

                        /*********************************************
                         * 2.3 알림톡 상태결과 저장
                         *********************************************/
                        //상태값 코드 변경 데이터 셋팅
                        JSONObject obj = new JSONObject();
                        if("1000".equals(sendRstMap.get("resultCode"))){
                            obj.put("uid", "");
                            obj.put("msgIdx", kkontData.getKkontNo());
                            obj.put("resultCode", "S000");
                            obj.put("requestAt", "");
                            obj.put("receivedAt", "");
                            obj.put("bsid", "");
                            obj.put("sendType", "");
                        }
                        String kkontRstResultUrl = aosBaseUrl + "/api/v1/kkont/rstResult";
                        String kkontRstResultMethod = "POST";
                        String kkontRstResultHeader = "{\"access_token\":\"qwerty\"}";
                        String kkontRstResultBody = obj.toJSONString();

                        HttpParam kkontRstResultParam = 
                            new HttpParam(Constant.KKONT_RST_RESULT, kkontRstResultUrl, kkontRstResultMethod, kkontRstResultHeader, kkontRstResultBody);    //통신파라미터 초기화
                        log.debug(kkontRstResultParam.toString());

                        //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
                        if(!apiHttpsBoolean){
                            httpUtil.callApiHttp(kkontRstResultParam);
                        }else{
                            httpUtil.callApiHttps(kkontRstResultParam);
                        }
                    }
                }
            }else{
                log.info("발송대상 데이터 없음.");
            }

            rstObj.put("result_code", "P000");
            rstObj.put("result_msg", "정상");
            return rstObj.toString();
        } catch (Exception e) {
            return commonUtil.resultObjMk(commonUtil.getClassName(),commonUtil.getMethodName(),"P999", e.toString());
        }
    }

    /**
     * 알림톡 결과 저장
     * 
     * @return String
     */
    @SuppressWarnings("unchecked")
    public String sendResultkkont() {
        HashMap<String, String> rstObj = new HashMap<String, String>();

        try {
            /*********************************************
             * 1. 알림톡 발송결과 통신
             *********************************************/
            HashMap<String, Object> sendRstMap = kakaoBizTalkService.sendResultkkont();
            JSONArray sendResultObjList = (JSONArray) sendRstMap.get("list");

            for(int i=0; i<sendResultObjList.size(); i++){
                JSONParser jsonParser = new JSONParser();
                JSONObject sendResultObj = (JSONObject) jsonParser.parse(sendResultObjList.get(i).toString());

                /*********************************************
                 * 2.1 알림톡 상태결과 저장
                 *********************************************/
                String kkontRstResultUrl = aosBaseUrl + "/api/v1/kkont/rstResult";
                String kkontRstResultMethod = "POST";
                String kkontRstResultHeader = "{\"access_token\":\"qwerty\"}";
                String kkontRstResultBody = sendResultObj.toJSONString();

                HttpParam kkontRstResultParam = 
                    new HttpParam(Constant.KKONT_RST_RESULT+"["+i+"]", kkontRstResultUrl, kkontRstResultMethod, kkontRstResultHeader, kkontRstResultBody);    //통신파라미터 초기화
                log.debug(kkontRstResultParam.toString());

                //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
                if(!apiHttpsBoolean){
                    httpUtil.callApiHttp(kkontRstResultParam);
                }else{
                    httpUtil.callApiHttps(kkontRstResultParam);
                }

                /*********************************************
                 * 2.2 알림톡 상태결과 통신 로그저장
                 *********************************************/
                //로그용 파라미터 셋팅
                //kkontNo + 알림톡 발송 응답값
                JSONObject kkontLogJsonObj = new JSONObject();
                kkontLogJsonObj.put("kkontNo", sendResultObj.get("msgIdx"));            //카카오톡 시퀀스
                kkontLogJsonObj.put("resData", sendRstMap.get("list"));                 //카카오톡 응답값
                
                String kkontLogUrl = aosBaseUrl + "/api/v1/kkont/log";
                String kkontLogMethod = "POST";
                String kkontLogHeader = "{\"access_token\":\"qwerty\"}";
                String kkontLogBody = kkontLogJsonObj.toJSONString();

                HttpParam kkontLogParam = 
                    new HttpParam(Constant.KKONT_LOG, kkontLogUrl, kkontLogMethod, kkontLogHeader, kkontLogBody);    //통신파라미터 초기화
                log.debug(kkontLogParam.toString());

                //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
                if(!apiHttpsBoolean){
                    httpUtil.callApiHttp(kkontLogParam);
                }else{
                    httpUtil.callApiHttps(kkontLogParam);
                }
            }

            rstObj.put("result_code", "P000");
            rstObj.put("result_msg", "정상");
            return rstObj.toString();
        } catch (Exception e) {
            return commonUtil.resultObjMk(commonUtil.getClassName(),commonUtil.getMethodName(),"P999", e.toString());
        }
    }
}
