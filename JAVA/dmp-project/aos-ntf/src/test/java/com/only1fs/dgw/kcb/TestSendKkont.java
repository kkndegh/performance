package com.only1fs.dgw.kcb;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.ObjectMapper;
//import com.only.aos.ntf.common.util.HttpUtil;
import com.only.aos.ntf.kakao.KakaoBizTalkClient;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.AlimTalkButton;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.AlimTalkRequest;
import com.only.aos.ntf.kakao.ifmodel.kakaobiztalk.AlimTalkResult;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.only.aos.ntf.config.Constants;
import org.apache.commons.lang3.StringUtils;

public class TestSendKkont {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private KakaoBizTalkClient kakaoBizTalkClient;

    //@Autowired
    //private HttpUtil httpUtil;

    @SuppressWarnings("unchecked")
    @Test
    // @SuppressWarnings("unchecked")
    public void ddd() {
        HashMap<String, String> rstObj = new HashMap<String, String>();

        try {
            String url = "";
            // url = "http://localhost:9190";
            url = "https://localaosapi.sellerbot.co.kr";

            String kkotInfoStr = ""; // 알림톡 정보

            // 알림톡 발송 정보 조회
            String kkotInfoUrl = url + "/api/v1/kkont/select";
            String kkotInfoMethod = "GET";
            String kkotInfoHeader = "{\"access_token\":\"asdf\"}";
            String kkotInfoBody = "{}";

            System.out.println("[[알림톡 발송 리스트 조회 데이터 셋팅]]");
            System.out.println("kkotInfoUrl :: " + kkotInfoUrl);
            System.out.println("kkotInfoMethod :: " + kkotInfoMethod);
            System.out.println("kkotInfoHeader :: " + kkotInfoHeader);
            System.out.println("kkotInfoBody :: " + kkotInfoBody);

            // ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
            //kkotInfoStr = httpUtil.callApiHttps(kkotInfoMethod, kkotInfoUrl, kkotInfoHeader, kkotInfoBody);

            // jsonString -> object
            JSONParser jsonParser = new JSONParser();
            JSONObject kkotInfoObj = (JSONObject) jsonParser.parse(kkotInfoStr);

            System.out.println("==알림톡 발송 리스트 조회결과==");
            System.out.println("==result_msg :: " + kkotInfoObj.get("result_msg"));
            System.out.println("==result_code :: " + kkotInfoObj.get("result_code"));
            System.out.println("==external_key :: " + kkotInfoObj.get("external_key"));
            System.out.println("==list :: " + kkotInfoObj.get("list"));

            // API키 체크
            JSONObject externalKeyStrObj = new JSONObject();
            if (!"".equals(kkotInfoObj.get("external_key"))) {
                String externalKeyStr = kkotInfoObj.get("external_key").toString();
                externalKeyStrObj = (JSONObject) jsonParser.parse(externalKeyStr);
                System.out.println("==API 키 :: " + externalKeyStrObj);
            } else {
                rstObj.put("result_code", "S999");
                rstObj.put("result_msg", "API키 조회 실패");
                System.out.println(rstObj.toString());
            }

            if (!kkotInfoStr.equals("") && kkotInfoStr.length() > 0) {
                // jsonListString -> jsonArray
                JSONArray kkotInfoList = (JSONArray) kkotInfoObj.get("list");
                for (int i = 0; i < kkotInfoList.size(); i++) {
                    JSONObject kkotInfoListObj = (JSONObject) jsonParser.parse(kkotInfoList.get(i).toString());
                    // API키 셋팅
                    kkotInfoListObj.put("senderKey", externalKeyStrObj.get("senderKey"));

                    System.out.println("[[알림톡 셋팅(" + i + ")]]");
                    System.out.println("kkotInfoListObj :: " + kkotInfoListObj);

                    // 알림톡 발송
                    HashMap<String, Object> sendRstMap = sendkkot(kkotInfoListObj);

                    System.out.println("################################################");
                    System.out.println("################################################");
                    System.out.println("################################################");
                    System.out.println("################################################");
                    System.out.println(sendRstMap.toString());
                    System.out.println("################################################");
                    System.out.println("################################################");
                    System.out.println("################################################");
                    System.out.println("################################################");

                    // 알림톡 응답 결과값
                    // String sendRst = (String) sendRstMap.get("resData");
                    // String sendReqHeader = (String) sendRstMap.get("reqHeader");
                    // String sendReqBody = (String) sendRstMap.get("reqBody");

                    // Object resData = jsonParser.parse( sendRst );
                    // JSONObject resDataObj = (JSONObject) resData;
                    // Object reqHeader = jsonParser.parse(sendReqHeader);
                    // Object reqBody = jsonParser.parse(sendReqBody);

                    // 로그용 파라미터 셋팅
                    // msgNo + 알림톡 발송 응답값[rcode] + 알림톡 발송헤더 + 알림톡 발송바디 + 알림톡 발송 응답값 전체
                    // JSONObject kkotLogJsonObj = new JSONObject();
                    // kkotLogJsonObj.put("msgNo", kkotInfoListObj.get("msgNo")); //메시지 시퀀스
                    // kkotLogJsonObj.put("rcode", resDataObj.get("rcode")); //Uplus 응답코드
                    // kkotLogJsonObj.put("reqHeader", reqHeader); //Uplus 요청 헤더
                    // kkotLogJsonObj.put("reqBody", reqBody); //Uplus 요청 바디
                    // kkotLogJsonObj.put("resData", resData); //Uplus 응답값

                    // 알림톡 발송 정보 로그 업데이트
                    // String kkotLogUrl = url + "/api/v1/kkont/log";
                    // String kkotLogMethod = "POST";
                    // String kkotLogHeader = "{\"access_token\":\"asdf\"}";
                    // String kkotLogBody = kkotLogJsonObj.toJSONString();

                    // ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
                    // HttpUtil.callApiHttps(kkotLogMethod, kkotLogUrl, kkotLogHeader, kkotLogBody);
                }
            } else {
                System.out.println("발송대상 데이터 없음.");
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    /**
     * 알림톡 발송
     * 
     * @param JSONObject kkotInfoObj
     * @return HashMap<String, Object>
     * @throws Exception
     */
    public HashMap<String, Object> sendkkot(JSONObject kkotInfoObj) {
        String phNum = (String) kkotInfoObj.get("sndTgtCustNo");
        String content = (String) kkotInfoObj.get("tmplt_context");
        String kkontTplId = (String) kkotInfoObj.get("tmpltMasterCode");
        String trsRltSeqNo = (String) kkotInfoObj.get("rltCode");
        String btnCont = (String) kkotInfoObj.get("btn_context");
        String senderKey = (String) kkotInfoObj.get("senderKey");

        try {
            // 알림톡 발송 entity 설정
            // TcKkontRlt entity = new TcKkontRlt();
            // TcKkontRlt addedEnt = tcKkontRltRepository.saveAndFlush(entity);

            AlimTalkRequest alimtalk = new AlimTalkRequest();
            alimtalk.setCountryCode(Constants.KKONT_COUNTRY_CODE);
            alimtalk.setResMethod(Constants.KKONT_RES_METHOD);
            alimtalk.setTmpltCode(kkontTplId);
            alimtalk.setMsgIdx(trsRltSeqNo);
            alimtalk.setRecipient(phNum);
            alimtalk.setSenderKey(senderKey);

            if (!StringUtils.isEmpty(btnCont)) {
                Map<String, AlimTalkButton[]> attach = new TreeMap<String, AlimTalkButton[]>();
                alimtalk.setAttach(attach);
                AlimTalkButton[] buttons = objectMapper.readValue(btnCont, AlimTalkButton[].class);
                attach.put(KakaoBizTalkClient.ATTACH_BUTTON, buttons);
            }
            alimtalk.setMessage(content);
            // tcKkontRltRepository.saveAndFlush(addedEnt);

            // 전송
            AlimTalkResult retval = kakaoBizTalkClient.sendAlimTalk(alimtalk);
            System.out.println("##카카오톡 발송 결과 :: "+retval);
            if (KakaoBizTalkClient.RESULT_SUCCESS.equals(retval.getResult())) {
                // addedEnt.setKkontTrsYn("Y"); // 전송 성공
                // addedEnt.setSuccRlt("SY02"); // 전송 성공 & 결과 수신 전
            } else {
                // addedEnt.setKkontTrsYn("N"); // 전송 실패
                // addedEnt.setSuccRlt("SY01"); // 카카오톡 전송 실패
            }
            // addedEnt.setSuccYn("N");
            // addedEnt.setSendReqTs(LocalDateTime.now()); // 요청 시간
            // tcKkontRltRepository.saveAndFlush(addedEnt);
        } catch (Exception e) {
            System.out.println("에러발생 :: "+e.toString());
        }
        return null;
    }
}