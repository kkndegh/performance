package com.only1fs.dgw.kcb;

import java.util.HashMap;
import java.util.Random;

import com.eclipsesource.json.JsonObject;
import com.only.aos.ntf.common.util.EncUtil;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class TestSms {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        String url = "";
        //url = "http://localhost:9190";
        url = "https://localaosapi.sellerbot.co.kr";
        try {
            String mobileMsgInfoStr = "";    //문자발송 정보

            //문자발송 정보 조회 
            String mobileMsgInfoUrl = url + "/api/v1/mobmsg/select";
            String mobileMsgInfoMethod = "GET";
            String mobileMsgInfoHeader = "{\"access_token\":\"asdf\"}";
            String mobileMsgInfoBody = "{}";

            System.out.println("[[문자발송 리스트 조회 데이터 셋팅]]");
            System.out.println("mobileMsgInfoUrl :: "+mobileMsgInfoUrl);
            System.out.println("mobileMsgInfoMethod :: "+mobileMsgInfoMethod);
            System.out.println("mobileMsgInfoHeader :: "+mobileMsgInfoHeader);
            System.out.println("mobileMsgInfoBody :: "+mobileMsgInfoBody);

            //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
            //mobileMsgInfoStr = HttpUtil.callApiHttp(mobileMsgInfoMethod, mobileMsgInfoUrl, mobileMsgInfoHeader, mobileMsgInfoBody);

            //jsonString -> object
            JSONParser jsonParser = new JSONParser();
            JSONObject mobileMsgInfoObj = (JSONObject) jsonParser.parse(mobileMsgInfoStr);

            System.out.println("==문자발송 리스트 조회결과==");
            System.out.println("==result_msg :: " + mobileMsgInfoObj.get("result_msg"));
            System.out.println("==result_code :: " + mobileMsgInfoObj.get("result_code"));
            System.out.println("==list :: " + mobileMsgInfoObj.get("list"));
            
            if(mobileMsgInfoStr.equals("") && mobileMsgInfoStr.length() < 1){
                //jsonListString -> jsonArray
                JSONArray mobileMsgInfoList = (JSONArray) mobileMsgInfoObj.get("list");
                for(int i=0; i<mobileMsgInfoList.size(); i++){
                    //System.out.println("schedulerList["+i+"] :: " + mobileMsgInfoList.get(i));
                    JSONObject mobileMsgInfoListObj = (JSONObject) jsonParser.parse(mobileMsgInfoList.get(i).toString());

                    System.out.println("[[문자발송 셋팅("+i+")]]");
                    System.out.println("mobileMsgInfoListObj :: "+mobileMsgInfoListObj);

                    //문자발송
                    HashMap<String, Object> sendRstMap = send(mobileMsgInfoListObj);

                    //문자발송 응답 결과값
                    String sendRst = (String) sendRstMap.get("resData");
                    String sendReqHeader = (String) sendRstMap.get("reqHeader");
                    String sendReqBody = (String) sendRstMap.get("reqBody");

                    Object resData = jsonParser.parse( sendRst );
                    JSONObject resDataObj = (JSONObject) resData;
                    Object reqHeader = jsonParser.parse(sendReqHeader);
                    Object reqBody = jsonParser.parse(sendReqBody);

                    //로그용 파라미터 셋팅
                    //msgNo + 문자발송 응답값[rcode] + 문자발송헤더 + 문자발송바디 + 문자발송 응답값 전체 
                    JSONObject mobileMsgLogJsonObj = new JSONObject();
                    mobileMsgLogJsonObj.put("msgNo", mobileMsgInfoListObj.get("msgNo"));    //메시지 시퀀스
                    mobileMsgLogJsonObj.put("rcode", resDataObj.get("rcode"));              //Uplus 응답코드
                    mobileMsgLogJsonObj.put("reqHeader", reqHeader);                        //Uplus 요청 헤더
                    mobileMsgLogJsonObj.put("reqBody", reqBody);                            //Uplus 요청 바디
                    mobileMsgLogJsonObj.put("resData", resData);                            //Uplus 응답값

                    //문자발송 정보 로그 업데이트 
                    // String mobileMsgLogUrl = url + "/api/v1/mobmsg/log";
                    // String mobileMsgLogMethod = "POST";
                    // String mobileMsgLogHeader = "{\"access_token\":\"asdf\"}";
                    // String mobileMsgLogBody = mobileMsgLogJsonObj.toJSONString();

                    //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
                    //mobileMsgInfoStr = HttpUtil.callApiHttp(mobileMsgLogMethod, mobileMsgLogUrl, mobileMsgLogHeader, mobileMsgLogBody);

                    System.out.println("로그저장 결과 :: "+mobileMsgInfoStr);
                }
            }else{
                System.out.println("발송대상 데이터 없음.");
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    // 메시지 발송
    public static HashMap<String, Object> send(JSONObject paramObj) {
        HashMap<String, Object> result = new HashMap<String, Object>();

        try {
            // 기본정보
            //String url = "https://openapi.sms.uplus.co.kr:4443/v1/send";
            // String apiKey = (String) paramObj.get("apiKey");
            // String apiSecret = (String) paramObj.get("apiSecret");
            // String encType = (String) paramObj.get("encType");

            String apiKey = "UP-1655360178-3761";
            String apiSecret = "ka12932";
            String encType = "0";

            // 인증정보(HMAC)
            // 값 셋팅
            HashMap<String, String> encHashHmac = new HashMap<String, String>();
            encHashHmac.put("apiKey", apiKey);
            encHashHmac.put("apiSecret", apiSecret);
            encHashHmac.put("encType", encType);
            // 결과
            HashMap<String, String> hmacObj = encHashHmac(encHashHmac);
            String hmac = hmacObj.get("hmac");
            String salt = hmacObj.get("salt");
            String timestamp = hmacObj.get("timestamp");

            // ====================================================================
            // 메일발송 통신 셋팅
            // ====================================================================
            // 헤더셋팅
            JsonObject headerParams = new JsonObject();
            headerParams.add("hash_hmac", hmac); // Hash된 인증정보
            headerParams.add("api_key", apiKey); // 고유 식별 Key
            headerParams.add("timestamp", timestamp); // 암호화시간 - 13자리(Millisecond 포함)
            headerParams.add("cret_txt", salt); // HMAC 생성 시 사용한 10자리 랜덤 숫자(salt)
            headerParams.add("algorithm", encType); // 암호화방식(0:SHA-256, 1:SHA-1, 2:MD5)

            // 바디셋팅
            JsonObject bodyParams = new JsonObject();
            bodyParams.add("send_type", (String) paramObj.get("sendType")); // 발송형태(R:예약,S:즉시)
            bodyParams.add("cmpg_id", (String) paramObj.get("cmpgId")); // 예약 ID(영문, 숫자, -, _) 40byte
            bodyParams.add("to", (String) paramObj.get("toNum")); // 수신자번호
            bodyParams.add("from", (String) paramObj.get("fromNum")); // 발송전화번호
            bodyParams.add("msg_type", (String) paramObj.get("msgType")); // 메시지종류 (S,M,L)
            bodyParams.add("subject", (String) paramObj.get("subject")); // 메시지제목
            bodyParams.add("datetime", (String) paramObj.get("datetime")); // 예약시간(YYYYMMDDHH24MI)
            bodyParams.add("msg", (String) paramObj.get("msg")); // 메시지 내용
            bodyParams.add("val", (String) paramObj.get("val")); // 메시지제한 시, 합성항목(파이프라인('|')으로 구분)
            bodyParams.add("image", (String) paramObj.get("image")); // MMS 발송 시 사용(이미지 3개 까지 첨부가능)

            result.put("reqHeader", headerParams.toString());
            result.put("reqBody", bodyParams.toString());
            //result.put("resData", HttpUtil.callApiHttps("POST", url, headerParams.toString(), bodyParams.toString()));

            // 결과는 사용자 시스템에 맞게 처리
            System.out.println("result :: " + result);
            return result;
        } catch (Exception e) {
            System.out.println("에러 :: " + e.toString());
            result.put("resData", "{\"rcode\":\"9999\",\"rdesc\":\"요청 처리 실패\"}");
            return result;
        }
    }

    // 인증정보(HMAC) 생성
    private static HashMap<String, String> encHashHmac(HashMap<String, String> encData) {
        String apiKey       = encData.get("apiKey");        // 고유 식별 Key
        long timestamp      = System.currentTimeMillis();   // 암호화시간 - 13자리(Millisecond 포함)
        String salt         = "";                           // 10자리 랜덤숫자
        String apiSecret    = encData.get("apiSecret");     // 사용자 API_KEY 비밀번호
        String encType      = encData.get("encType");       // 암호화방식(0:SHA-256, 1:SHA-1, 2:MD5)
        String hmac=""; //암호화 값

        //10자리 랜덤숫자 셋팅
        while(salt.length()<10){
            Random r=new Random();
            int j=r.nextInt(10);
            salt+=String.valueOf(j);
        }

        StringBuilder sb = new StringBuilder();
        sb.append( apiKey );
        sb.append( timestamp );
        sb.append( salt ); // 10자리 랜덤숫자
        sb.append( apiSecret ); // 사용자 API_KEY 비밀번호

        switch (encType) {
            case "0":
                hmac = EncUtil.encSHA256(sb.toString());
                break;
            case "1":
                hmac = EncUtil.encSHA1(sb.toString());
                break;
            case "2":
                hmac = EncUtil.encMD5(sb.toString());
                break;
        }

        //결과 셋팅
        HashMap<String, String> rstData = new HashMap<String, String>();    //결과맵
        rstData.put("hmac", hmac);
        rstData.put("salt", salt);
        rstData.put("timestamp", String.valueOf(timestamp));

        return rstData;
    }
}