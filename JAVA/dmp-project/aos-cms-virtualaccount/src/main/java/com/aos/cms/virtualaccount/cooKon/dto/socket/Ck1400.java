package com.aos.cms.virtualaccount.cooKon.dto.socket;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 가상계좌입금취소 요청
 * 
 * @since 2022.11.04
 * @author kh
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Ck1400 extends CkHeader implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Ck1400Dp1;
    private String Ck1400Dp2;
    private String Ck1400Dp3;
    private String Ck1400Dp4;
    private String Ck1400Dp5;
    private String Ck1400Dp6;
    private String Ck1400Dp7;
    private String Ck1400Dp8;
    private String Ck1400Dp9;
    private String Ck1400Dp10;
    private String Ck1400Dp11;
    private String Ck1400Dp12;
    private String Ck1400Dp13;
    private String Ck1400Dp14;
    private String Ck1400Dp15;
    private String Ck1400Dp16;
    private String Ck1400Dp17;
    private String Ck1400Dp18;
    private String Ck1400Dp19;
    private String Ck1400Dp20;
}
