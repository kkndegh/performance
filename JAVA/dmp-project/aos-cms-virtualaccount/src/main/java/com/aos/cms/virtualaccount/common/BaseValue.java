package com.aos.cms.virtualaccount.common;

import com.aos.cms.virtualaccount.common.util.CommonUtil;
import com.aos.cms.virtualaccount.common.util.HttpUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

/**
 * 공통 value
 */
public class BaseValue {
    
    /** 
     * ObjectMapper
    */
    @Autowired 
    public ObjectMapper objectMapper;

    /** 
     * HTTP 유틸
    */
    @Autowired
    public HttpUtil httpUtil;

    /** 
     * 공통 유틸
    */
    @Autowired
    public CommonUtil commonUtil;

    /** 
     * 프로파일
    */
    @Value("${spring.profiles.active}")
    public String profiles;

    /** 
     * AOS URL
    */
    @Value("${aos.base.url}")
    public String aosBaseUrl;
}