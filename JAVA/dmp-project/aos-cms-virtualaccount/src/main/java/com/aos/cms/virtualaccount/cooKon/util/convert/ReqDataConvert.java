package com.aos.cms.virtualaccount.cooKon.util.convert;

import java.io.Serializable;
import java.nio.charset.Charset;

import com.aos.cms.virtualaccount.cooKon.dto.socket.Ck1200;
import com.aos.cms.virtualaccount.cooKon.dto.socket.Ck1300;
import com.aos.cms.virtualaccount.cooKon.dto.socket.Ck1400;
import com.aos.cms.virtualaccount.server.Packet;

import org.springframework.util.ObjectUtils;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.Builder;
import lombok.Data;

/**
 * 요청 전문 생성
 * 
 * @since 2022.10.13
 * @author sung
 */
@Data
@Builder
public class ReqDataConvert implements Packet, Serializable {

    private static final long serialVersionUID = 1L;
    private String type;

    private Ck1200 ck1200;              // 쿠콘 가상계좌수취조회 요청
    private Ck1300 ck1300;              // 쿠콘 가상계좌입금 통보
    private Ck1400 ck1400;              // 가상계좌입금취소 요청

    @Override
    public ByteBuf getByteBuf(Charset charset) {
        int LENGTH = 0;
        ByteBuf bb = Unpooled.buffer(LENGTH);
        
        if ("1200".equals(type)) {
            bb = Unpooled.buffer(400);

            //쿠콘 가상계좌수취조회 요청
            this.getByteBuf1200Packet(bb, charset);
        } else if ("1300".equals(type)) {
            bb = Unpooled.buffer(400);

            //쿠콘 가상계좌입금 통보
            this.getByteBuf1300Packet(bb, charset);
        } else if ("1400".equals(type)) {
            bb = Unpooled.buffer(400);

            //가상계좌입금취소 요청
            this.getByteBuf1400Packet(bb, charset);
        }

        return bb;
    }

    //쿠콘 가상계좌수취조회 요청
    private void getByteBuf1200Packet(ByteBuf bb, Charset charset) {
        //쿠콘 가상계좌 공통부
        getByteBuf(bb, "%-4s",        ck1200.getCkHeaderDp1()             , charset, 0, 4);
        getByteBuf(bb, "%-8s",        ck1200.getCkHeaderDp2()             , charset, 0, 8);
        getByteBuf(bb, "%-6s",        ck1200.getCkHeaderDp3()             , charset, 0, 6);
        getByteBuf(bb, "%-12s",       ck1200.getCkHeaderDp4()             , charset, 0, 12);
        getByteBuf(bb, "%-4s",        ck1200.getCkHeaderDp5()             , charset, 0, 4);
        getByteBuf(bb, "%-4s",        ck1200.getCkHeaderDp6()             , charset, 0, 4);
        getByteBuf(bb, "%-4s",        ck1200.getCkHeaderDp7()             , charset, 0, 4);
        getByteBuf(bb, "%-8s",        ck1200.getCkHeaderDp8()             , charset, 0, 8);
        getByteBuf(bb, "%-5s",        ck1200.getCkHeaderDp9()             , charset, 0, 5);
        getByteBuf(bb, "%-2s",        ck1200.getCkHeaderDp10()            , charset, 0, 2);
        getByteBuf(bb, "%-6s",        ck1200.getCkHeaderDp11()            , charset, 0, 6);
        getByteBuf(bb, "%-3s",        ck1200.getCkHeaderDp12()            , charset, 0, 3);
        getByteBuf(bb, "%-1s",        ck1200.getCkHeaderDp13()            , charset, 0, 1);
        getByteBuf(bb, "%-1s",        ck1200.getCkHeaderDp14()            , charset, 0, 1);
        getByteBuf(bb, "%-32s",       ck1200.getCkHeaderDp15()            , charset, 0, 32);

        //데이터부
        getByteBuf(bb, "%-16s",         ck1200.getCk1200Dp1()            , charset, 0, 16);
        getByteBuf(bb, "%-30s",         ck1200.getCk1200Dp2()            , charset, 0, 30);
        getByteBuf(bb, "%-30s",         ck1200.getCk1200Dp3()            , charset, 0, 30);
        getByteBuf(bb, "%-2s",          ck1200.getCk1200Dp4()            , charset, 0, 2);
        getByteBuf(bb, "%-20s",         ck1200.getCk1200Dp5()            , charset, 0, 20);
        getByteBuf(bb, "%-20s",         ck1200.getCk1200Dp6()            , charset, 0, 20);
        getByteBuf(bb, "%-12s",         ck1200.getCk1200Dp7()            , charset, 0, 12);
        getByteBuf(bb, "%-12s",         ck1200.getCk1200Dp8()            , charset, 0, 12);
        getByteBuf(bb, "%-12s",         ck1200.getCk1200Dp9()            , charset, 0, 12);
        getByteBuf(bb, "%-2s",          ck1200.getCk1200Dp10()           , charset, 0, 2);
        getByteBuf(bb, "%-60s",         ck1200.getCk1200Dp11()           , charset, 0, 60);
        getByteBuf(bb, "%-8s",          ck1200.getCk1200Dp12()           , charset, 0, 8);
        getByteBuf(bb, "%-3s",          ck1200.getCk1200Dp13()           , charset, 0, 3);
        getByteBuf(bb, "%-1s",          ck1200.getCk1200Dp14()           , charset, 0, 1);
        getByteBuf(bb, "%-3s",          ck1200.getCk1200Dp15()           , charset, 0, 3);
        getByteBuf(bb, "%-16s",         ck1200.getCk1200Dp16()           , charset, 0, 16);
        getByteBuf(bb, "%-53s",         ck1200.getCk1200Dp17()           , charset, 0, 53);
    }

    //쿠콘 가상계좌입금 통보
    private void getByteBuf1300Packet(ByteBuf bb, Charset charset) {
        //쿠콘 가상계좌 공통부
        getByteBuf(bb, "%-4s",        ck1300.getCkHeaderDp1()             , charset, 0, 4);
        getByteBuf(bb, "%-8s",        ck1300.getCkHeaderDp2()             , charset, 0, 8);
        getByteBuf(bb, "%-6s",        ck1300.getCkHeaderDp3()             , charset, 0, 6);
        getByteBuf(bb, "%-12s",       ck1300.getCkHeaderDp4()             , charset, 0, 12);
        getByteBuf(bb, "%-4s",        ck1300.getCkHeaderDp5()             , charset, 0, 4);
        getByteBuf(bb, "%-4s",        ck1300.getCkHeaderDp6()             , charset, 0, 4);
        getByteBuf(bb, "%-4s",        ck1300.getCkHeaderDp7()             , charset, 0, 4);
        getByteBuf(bb, "%-8s",        ck1300.getCkHeaderDp8()             , charset, 0, 8);
        getByteBuf(bb, "%-5s",        ck1300.getCkHeaderDp9()             , charset, 0, 5);
        getByteBuf(bb, "%-2s",        ck1300.getCkHeaderDp10()            , charset, 0, 2);
        getByteBuf(bb, "%-6s",        ck1300.getCkHeaderDp11()            , charset, 0, 6);
        getByteBuf(bb, "%-3s",        ck1300.getCkHeaderDp12()            , charset, 0, 3);
        getByteBuf(bb, "%-1s",        ck1300.getCkHeaderDp13()            , charset, 0, 1);
        getByteBuf(bb, "%-1s",        ck1300.getCkHeaderDp14()            , charset, 0, 1);
        getByteBuf(bb, "%-32s",       ck1300.getCkHeaderDp15()            , charset, 0, 32);

        //데이터부
        getByteBuf(bb, "%-16s",        ck1300.getCk1300Dp1()            , charset, 0, 16);
        getByteBuf(bb, "%-30s",        ck1300.getCk1300Dp2()            , charset, 0, 30);
        getByteBuf(bb, "%-30s",        ck1300.getCk1300Dp3()            , charset, 0, 30);
        getByteBuf(bb, "%-2s",         ck1300.getCk1300Dp4()            , charset, 0, 2);
        getByteBuf(bb, "%-20s",        ck1300.getCk1300Dp5()            , charset, 0, 20);
        getByteBuf(bb, "%-20s",        ck1300.getCk1300Dp6()            , charset, 0, 20);
        getByteBuf(bb, "%-12s",        ck1300.getCk1300Dp7()            , charset, 0, 12);
        getByteBuf(bb, "%-12s",        ck1300.getCk1300Dp8()            , charset, 0, 12);
        getByteBuf(bb, "%-12s",        ck1300.getCk1300Dp9()            , charset, 0, 12);
        getByteBuf(bb, "%-2s",         ck1300.getCk1300Dp10()           , charset, 0, 2);
        getByteBuf(bb, "%-60s",        ck1300.getCk1300Dp11()           , charset, 0, 60);
        getByteBuf(bb, "%-8s",         ck1300.getCk1300Dp12()           , charset, 0, 8);
        getByteBuf(bb, "%-3s",         ck1300.getCk1300Dp13()           , charset, 0, 3);
        getByteBuf(bb, "%-12s",        ck1300.getCk1300Dp14()           , charset, 0, 12);
        getByteBuf(bb, "%-1s",         ck1300.getCk1300Dp15()           , charset, 0, 1);
        getByteBuf(bb, "%-3s",         ck1300.getCk1300Dp16()           , charset, 0, 3);
        getByteBuf(bb, "%-16s",        ck1300.getCk1300Dp17()           , charset, 0, 16);
        getByteBuf(bb, "%-3s",         ck1300.getCk1300Dp18()           , charset, 0, 3);
        getByteBuf(bb, "%-38s",        ck1300.getCk1300Dp19()           , charset, 0, 38);
    }

    //가상계좌입금취소 요청
    private void getByteBuf1400Packet(ByteBuf bb, Charset charset) {
        //쿠콘 가상계좌 공통부
        getByteBuf(bb, "%-4s",        ck1400.getCkHeaderDp1()             , charset, 0, 4);
        getByteBuf(bb, "%-8s",        ck1400.getCkHeaderDp2()             , charset, 0, 8);
        getByteBuf(bb, "%-6s",        ck1400.getCkHeaderDp3()             , charset, 0, 6);
        getByteBuf(bb, "%-12s",       ck1400.getCkHeaderDp4()             , charset, 0, 12);
        getByteBuf(bb, "%-4s",        ck1400.getCkHeaderDp5()             , charset, 0, 4);
        getByteBuf(bb, "%-4s",        ck1400.getCkHeaderDp6()             , charset, 0, 4);
        getByteBuf(bb, "%-4s",        ck1400.getCkHeaderDp7()             , charset, 0, 4);
        getByteBuf(bb, "%-8s",        ck1400.getCkHeaderDp8()             , charset, 0, 8);
        getByteBuf(bb, "%-5s",        ck1400.getCkHeaderDp9()             , charset, 0, 5);
        getByteBuf(bb, "%-2s",        ck1400.getCkHeaderDp10()            , charset, 0, 2);
        getByteBuf(bb, "%-6s",        ck1400.getCkHeaderDp11()            , charset, 0, 6);
        getByteBuf(bb, "%-3s",        ck1400.getCkHeaderDp12()            , charset, 0, 3);
        getByteBuf(bb, "%-1s",        ck1400.getCkHeaderDp13()            , charset, 0, 1);
        getByteBuf(bb, "%-1s",        ck1400.getCkHeaderDp14()            , charset, 0, 1);
        getByteBuf(bb, "%-32s",       ck1400.getCkHeaderDp15()            , charset, 0, 32);

        //데이터부
        getByteBuf(bb, "%-16s",        ck1400.getCk1400Dp1()            , charset, 0, 16);
        getByteBuf(bb, "%-30s",        ck1400.getCk1400Dp2()            , charset, 0, 30);
        getByteBuf(bb, "%-30s",        ck1400.getCk1400Dp3()            , charset, 0, 30);
        getByteBuf(bb, "%-2s",         ck1400.getCk1400Dp4()            , charset, 0, 2);
        getByteBuf(bb, "%-20s",        ck1400.getCk1400Dp5()            , charset, 0, 20);
        getByteBuf(bb, "%-20s",        ck1400.getCk1400Dp6()            , charset, 0, 20);
        getByteBuf(bb, "%-12s",        ck1400.getCk1400Dp7()            , charset, 0, 12);
        getByteBuf(bb, "%-12s",        ck1400.getCk1400Dp8()            , charset, 0, 12);
        getByteBuf(bb, "%-12s",        ck1400.getCk1400Dp9()            , charset, 0, 12);
        getByteBuf(bb, "%-2s",         ck1400.getCk1400Dp10()           , charset, 0, 2);
        getByteBuf(bb, "%-60s",        ck1400.getCk1400Dp11()           , charset, 0, 60);
        getByteBuf(bb, "%-8s",         ck1400.getCk1400Dp12()           , charset, 0, 8);
        getByteBuf(bb, "%-3s",         ck1400.getCk1400Dp13()           , charset, 0, 3);
        getByteBuf(bb, "%-12s",        ck1400.getCk1400Dp14()           , charset, 0, 12);
        getByteBuf(bb, "%-1s",         ck1400.getCk1400Dp15()           , charset, 0, 1);
        getByteBuf(bb, "%-3s",         ck1400.getCk1400Dp16()           , charset, 0, 3);
        getByteBuf(bb, "%-16s",        ck1400.getCk1400Dp17()           , charset, 0, 16);
        getByteBuf(bb, "%-3s",         ck1400.getCk1400Dp18()           , charset, 0, 3);
        getByteBuf(bb, "%-38s",        ck1400.getCk1400Dp19()           , charset, 0, 38);
    }
    
    public void getByteBuf(ByteBuf bb, String format, Object args, Charset charset, int srcIndex, int length) {
        if (ObjectUtils.isEmpty(charset)) {
            bb.writeBytes(String.format(format,    args).getBytes(Charset.forName("EUC-KR")), srcIndex, length);
        } else {
            bb.writeBytes(String.format(format,    args).getBytes(charset), srcIndex, length);
        }
    }

    @Override
    public byte[] getBytes(Charset charset) {
        return getByteBuf(charset).array();
    }

    @Override
    public String getString(Charset charset) {
        return getByteBuf(charset).toString(charset);
    }
}
