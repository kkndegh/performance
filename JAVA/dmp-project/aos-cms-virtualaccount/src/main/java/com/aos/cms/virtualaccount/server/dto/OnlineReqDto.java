package com.aos.cms.virtualaccount.server.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OnlineReqDto {

    private byte[] reqByteData;

    public OnlineReqDto(byte[] data) {
        this.setReqByteData(data);
    }
}
