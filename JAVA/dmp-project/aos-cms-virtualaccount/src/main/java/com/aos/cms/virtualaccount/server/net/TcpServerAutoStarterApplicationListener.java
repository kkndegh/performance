package com.aos.cms.virtualaccount.server.net;

import com.aos.cms.virtualaccount.server.TcpServerProperties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStoppedEvent;

public class TcpServerAutoStarterApplicationListener implements ApplicationListener<ApplicationContextEvent> {

    @Autowired(required = false)
    private TcpServerProperties properties;

    @Autowired
    private ServerNet serverNet;

    @Override
    public void onApplicationEvent(ApplicationContextEvent event) {
        if( event instanceof ContextRefreshedEvent) {
            boolean autoStart = properties != null ? properties.getAutoStart() : false;
            if (autoStart){
                serverNet.setPort(properties.getPort());
                serverNet.start();
            }
            
        }else if( event instanceof ContextStoppedEvent) {
            serverNet.stop();
        }
    }

}