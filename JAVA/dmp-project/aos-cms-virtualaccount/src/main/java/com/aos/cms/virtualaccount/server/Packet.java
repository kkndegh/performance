package com.aos.cms.virtualaccount.server;

import java.nio.charset.Charset;

import io.netty.buffer.ByteBuf;

/**
 * 패킷 인터페이스
 * 
 * @since 2022.10.13
 * @author sung
 */
public interface Packet {
    public ByteBuf getByteBuf(Charset charset);
    public byte[] getBytes(Charset charset);
    public String getString(Charset charset);
}
