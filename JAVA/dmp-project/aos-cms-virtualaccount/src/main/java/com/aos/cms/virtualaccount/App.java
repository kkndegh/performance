package com.aos.cms.virtualaccount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.Banner;
import org.springframework.scheduling.annotation.EnableScheduling;

import lombok.extern.slf4j.Slf4j;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;


@Slf4j
@EnableScheduling
@SpringBootApplication
public class App {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(App.class);
        app.setBannerMode(Banner.Mode.OFF);

        ApplicationContext ctx = app.run(args);
        Environment env = ctx.getEnvironment();
        String name = env.getProperty("spring.application.name");

        Runtime.getRuntime().addShutdownHook(new Thread(name + "-shutdown-hook") {
            public void run() {
                log.info("=== {} shutdown! ===", name);
                ((ConfigurableApplicationContext)ctx).close();
            }
        });
    }
}
 