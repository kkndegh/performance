package com.aos.cms.virtualaccount.server;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * TCP SERVER 프로퍼티
 * 
 * @since 2022.10.13
 * @author sung
 */
@ConfigurationProperties(prefix = "tcp.server", ignoreUnknownFields = true)
public class TcpServerProperties {
    
    private int port;
    private boolean autoStart;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean getAutoStart() {
        return autoStart;
    }

    public void setAutoStart(boolean autoStart) {
        this.autoStart = autoStart;
    }
}