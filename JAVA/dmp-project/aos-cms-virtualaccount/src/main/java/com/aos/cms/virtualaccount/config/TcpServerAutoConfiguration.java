package com.aos.cms.virtualaccount.config;

import com.aos.cms.virtualaccount.server.TcpServerProperties;
import com.aos.cms.virtualaccount.server.net.ServerNet;
import com.aos.cms.virtualaccount.server.net.TcpControllerBeanPostProcessor;
import com.aos.cms.virtualaccount.server.net.TcpServer;
import com.aos.cms.virtualaccount.server.net.TcpServerAutoStarterApplicationListener;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableConfigurationProperties(TcpServerProperties.class)
@ConditionalOnProperty(prefix = "tcp.server", name = {"port", "autoStart"})
public class TcpServerAutoConfiguration {

    @Bean
    TcpServerAutoStarterApplicationListener tcpServerAutoStarterApplicationListener() {
        return new TcpServerAutoStarterApplicationListener();
    }

    @Bean
    TcpControllerBeanPostProcessor tcpControllerBeanPostProcessor() {
        return new TcpControllerBeanPostProcessor();
    }

    @Bean
    ServerNet serverNet(){
        return new TcpServer();
    }
}