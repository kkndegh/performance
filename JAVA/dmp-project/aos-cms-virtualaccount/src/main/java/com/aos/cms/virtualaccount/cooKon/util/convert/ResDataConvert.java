package com.aos.cms.virtualaccount.cooKon.util.convert;

import java.nio.charset.Charset;

import com.aos.cms.virtualaccount.cooKon.dto.socket.Ck1200;
import com.aos.cms.virtualaccount.cooKon.dto.socket.Ck1300;
import com.aos.cms.virtualaccount.cooKon.dto.socket.Ck1400;

import org.springframework.stereotype.Component;

/**
 * 응답 전문 생성
 * 
 * @since 2022.10.21
 * @author kh
 */
@Component
public class ResDataConvert {

    //가상계좌수취조회 요청
    public Ck1200 Ck1200Res(String data, Charset charset) {
        Ck1200 ck1200Res = new Ck1200();
        byte[] byteArray = data.getBytes(charset);

        // 공통정보부
        ck1200Res.setCkHeaderDp1(getData(byteArray        ,0, 4   ,charset));     // LENGTH
        ck1200Res.setCkHeaderDp2(getData(byteArray        ,4, 8   ,charset));     // 거래일자
        ck1200Res.setCkHeaderDp3(getData(byteArray        ,12, 6  ,charset));     // 거래시간
        ck1200Res.setCkHeaderDp4(getData(byteArray        ,18, 12 ,charset));     // 전문일련번호
        ck1200Res.setCkHeaderDp5(getData(byteArray        ,30, 4  ,charset));     // 전문종류
        ck1200Res.setCkHeaderDp6(getData(byteArray        ,34, 4  ,charset));     // 거래코드
        ck1200Res.setCkHeaderDp7(getData(byteArray        ,38, 4  ,charset));     // 응답코드
        ck1200Res.setCkHeaderDp8(getData(byteArray        ,42, 8  ,charset));     // 기관코드
        ck1200Res.setCkHeaderDp9(getData(byteArray        ,50, 5  ,charset));     // 기관지점코드
        ck1200Res.setCkHeaderDp10(getData(byteArray       ,55, 2  ,charset));     // 연계은행코드
        ck1200Res.setCkHeaderDp11(getData(byteArray       ,57, 6  ,charset));     // 텔러
        ck1200Res.setCkHeaderDp12(getData(byteArray       ,63, 3  ,charset));     // 연계은행코드
        ck1200Res.setCkHeaderDp13(getData(byteArray       ,66, 1  ,charset));     // 입금발생구분 
        ck1200Res.setCkHeaderDp14(getData(byteArray       ,67, 1  ,charset));     // 입금매체구분 
        ck1200Res.setCkHeaderDp15(getData(byteArray       ,68, 32 ,charset));     // FILLER 

        // 요청정보부
        ck1200Res.setCk1200Dp1(getData(byteArray    , 100	,16 , charset));   // 가상계좌번호
        ck1200Res.setCk1200Dp2(getData(byteArray    , 116	,30 , charset));   // 수취인명
        ck1200Res.setCk1200Dp3(getData(byteArray    , 146	,30 , charset));   // 입금인명
        ck1200Res.setCk1200Dp4(getData(byteArray    , 176	,2  , charset));   // 입금은행
        ck1200Res.setCk1200Dp5(getData(byteArray    , 178	,20 , charset));   // 입금은행명
        ck1200Res.setCk1200Dp6(getData(byteArray    , 198	,20 , charset));   // 지점명
        ck1200Res.setCk1200Dp7(getData(byteArray    , 218	,12 , charset));   // 총금액
        ck1200Res.setCk1200Dp8(getData(byteArray    , 230	,12 , charset));   // 현금금액
        ck1200Res.setCk1200Dp9(getData(byteArray    , 242	,12 , charset));   // 수표금액
        ck1200Res.setCk1200Dp10(getData(byteArray   , 254	,2  , charset));   // 입금매체
        ck1200Res.setCk1200Dp11(getData(byteArray   , 256	,60 , charset));   // 오류메세지
        ck1200Res.setCk1200Dp12(getData(byteArray   , 316	,8  , charset));   // 정산예정일
        ck1200Res.setCk1200Dp13(getData(byteArray   , 324	,3  , charset));   // 입금은행2
        ck1200Res.setCk1200Dp14(getData(byteArray   , 327	,1  , charset));   // 수취조회발생구분
        ck1200Res.setCk1200Dp15(getData(byteArray   , 328	,3  , charset));   // 출금은행
        ck1200Res.setCk1200Dp16(getData(byteArray   , 331	,16 , charset));   // 출금계좌번호
        ck1200Res.setCk1200Dp17(getData(byteArray   , 347	,53 , charset));   // FILLER

        return ck1200Res;
    }

    //가상계좌입금 통보
    public Ck1300 Ck1300Res(String data, Charset charset) {
        Ck1300 ck1300Res = new Ck1300();
        byte[] byteArray = data.getBytes(charset);

        // 공통정보부
        ck1300Res.setCkHeaderDp1(getData(byteArray        ,0, 4   ,charset));     // LENGTH
        ck1300Res.setCkHeaderDp2(getData(byteArray        ,4, 8   ,charset));     // 거래일자
        ck1300Res.setCkHeaderDp3(getData(byteArray        ,12, 6  ,charset));     // 거래시간
        ck1300Res.setCkHeaderDp4(getData(byteArray        ,18, 12 ,charset));     // 전문일련번호
        ck1300Res.setCkHeaderDp5(getData(byteArray        ,30, 4  ,charset));     // 전문종류
        ck1300Res.setCkHeaderDp6(getData(byteArray        ,34, 4  ,charset));     // 거래코드
        ck1300Res.setCkHeaderDp7(getData(byteArray        ,38, 4  ,charset));     // 응답코드
        ck1300Res.setCkHeaderDp8(getData(byteArray        ,42, 8  ,charset));     // 기관코드
        ck1300Res.setCkHeaderDp9(getData(byteArray        ,50, 5  ,charset));     // 기관지점코드
        ck1300Res.setCkHeaderDp10(getData(byteArray       ,55, 2  ,charset));     // 연계은행코드
        ck1300Res.setCkHeaderDp11(getData(byteArray       ,57, 6  ,charset));     // 텔러
        ck1300Res.setCkHeaderDp12(getData(byteArray       ,63, 3  ,charset));     // 연계은행코드
        ck1300Res.setCkHeaderDp13(getData(byteArray       ,66, 1  ,charset));     // 입금발생구분 
        ck1300Res.setCkHeaderDp14(getData(byteArray       ,67, 1  ,charset));     // 입금매체구분 
        ck1300Res.setCkHeaderDp15(getData(byteArray       ,68, 32 ,charset));     // FILLER 

        // 요청정보부
        ck1300Res.setCk1300Dp1(getData(byteArray    , 100	,16 , charset));    // 가상계좌번호
        ck1300Res.setCk1300Dp2(getData(byteArray    , 116	,30 , charset));    // 수취인명
        ck1300Res.setCk1300Dp3(getData(byteArray    , 146	,30 , charset));    // 입금인명
        ck1300Res.setCk1300Dp4(getData(byteArray    , 176	,2  , charset));    // 입금은행
        ck1300Res.setCk1300Dp5(getData(byteArray    , 178	,20 , charset));    // 입금은행명
        ck1300Res.setCk1300Dp6(getData(byteArray    , 198	,20 , charset));    // 지점명
        ck1300Res.setCk1300Dp7(getData(byteArray    , 218	,12 , charset));    // 총금액
        ck1300Res.setCk1300Dp8(getData(byteArray    , 230	,12 , charset));    // 현금금액
        ck1300Res.setCk1300Dp9(getData(byteArray    , 242	,12 , charset));    // 수표금액
        ck1300Res.setCk1300Dp10(getData(byteArray   , 254	,2  , charset));    // 입금매체
        ck1300Res.setCk1300Dp11(getData(byteArray   , 256	,60 , charset));    // 오류메세지
        ck1300Res.setCk1300Dp12(getData(byteArray   , 316	,8  , charset));    // 정산예정일
        ck1300Res.setCk1300Dp13(getData(byteArray   , 324	,3  , charset));    // 입금은행2
        ck1300Res.setCk1300Dp14(getData(byteArray   , 327	,12 , charset));    // 수취조회전문일련번호
        ck1300Res.setCk1300Dp15(getData(byteArray   , 339	,1  , charset));    // 요청구분
        ck1300Res.setCk1300Dp16(getData(byteArray   , 340	,3  , charset));    // 출금은행
        ck1300Res.setCk1300Dp17(getData(byteArray   , 343	,16 , charset));    // 출금계좌번호
        ck1300Res.setCk1300Dp18(getData(byteArray   , 359	,3  , charset));    // 통화구분
        ck1300Res.setCk1300Dp19(getData(byteArray   , 362	,38 , charset));    // FILLER

        return ck1300Res;
    }

    // 회원증빙 - 시작
    public Ck1400 Ck1400Res(String data, Charset charset) {
        Ck1400 ck1400Res = new Ck1400();
        byte[] byteArray = data.getBytes(charset);

        // 요청정보부
        ck1400Res.setCkHeaderDp1(getData(byteArray        ,0, 4   ,charset));     // LENGTH
        ck1400Res.setCkHeaderDp2(getData(byteArray        ,4, 8   ,charset));     // 거래일자
        ck1400Res.setCkHeaderDp3(getData(byteArray        ,12, 6  ,charset));     // 거래시간
        ck1400Res.setCkHeaderDp4(getData(byteArray        ,18, 12 ,charset));     // 전문일련번호
        ck1400Res.setCkHeaderDp5(getData(byteArray        ,30, 4  ,charset));     // 전문종류
        ck1400Res.setCkHeaderDp6(getData(byteArray        ,34, 4  ,charset));     // 거래코드
        ck1400Res.setCkHeaderDp7(getData(byteArray        ,38, 4  ,charset));     // 응답코드
        ck1400Res.setCkHeaderDp8(getData(byteArray        ,42, 8  ,charset));     // 기관코드
        ck1400Res.setCkHeaderDp9(getData(byteArray        ,50, 5  ,charset));     // 기관지점코드
        ck1400Res.setCkHeaderDp10(getData(byteArray       ,55, 2  ,charset));     // 연계은행코드
        ck1400Res.setCkHeaderDp11(getData(byteArray       ,57, 6  ,charset));     // 텔러
        ck1400Res.setCkHeaderDp12(getData(byteArray       ,63, 3  ,charset));     // 연계은행코드
        ck1400Res.setCkHeaderDp13(getData(byteArray       ,66, 1  ,charset));     // 입금발생구분 
        ck1400Res.setCkHeaderDp14(getData(byteArray       ,67, 1  ,charset));     // 입금매체구분 
        ck1400Res.setCkHeaderDp15(getData(byteArray       ,68, 32 ,charset));     // FILLER  

        // 요청정보부
        ck1400Res.setCk1400Dp1(getData(byteArray    , 100	,16 , charset));    // 가상계좌번호
        ck1400Res.setCk1400Dp2(getData(byteArray    , 116	,30 , charset));    // 수취인명
        ck1400Res.setCk1400Dp3(getData(byteArray    , 146	,30 , charset));    // 입금인명
        ck1400Res.setCk1400Dp4(getData(byteArray    , 176	,2  , charset));    // 입금은행
        ck1400Res.setCk1400Dp5(getData(byteArray    , 178	,20 , charset));    // 입금은행명
        ck1400Res.setCk1400Dp6(getData(byteArray    , 198	,20 , charset));    // 지점명
        ck1400Res.setCk1400Dp7(getData(byteArray    , 218	,12 , charset));    // 총금액
        ck1400Res.setCk1400Dp8(getData(byteArray    , 230	,12 , charset));    // 현금금액
        ck1400Res.setCk1400Dp9(getData(byteArray    , 242	,12 , charset));    // 수표금액
        ck1400Res.setCk1400Dp10(getData(byteArray   , 254	,2  , charset));    // 입금매체
        ck1400Res.setCk1400Dp11(getData(byteArray   , 256	,60 , charset));    // 오류메세지
        ck1400Res.setCk1400Dp12(getData(byteArray   , 316	,8  , charset));    // 정산예정일
        ck1400Res.setCk1400Dp13(getData(byteArray   , 324	,3  , charset));    // 입금은행2
        ck1400Res.setCk1400Dp14(getData(byteArray   , 327	,12 , charset));    // 수취조회전문일련번호
        ck1400Res.setCk1400Dp15(getData(byteArray   , 339	,1  , charset));    // 요청구분
        ck1400Res.setCk1400Dp16(getData(byteArray   , 340	,3  , charset));    // 출금은행
        ck1400Res.setCk1400Dp17(getData(byteArray   , 343	,16 , charset));    // 출금계좌번호
        ck1400Res.setCk1400Dp18(getData(byteArray   , 359	,3  , charset));    // 통화구분
        ck1400Res.setCk1400Dp19(getData(byteArray   , 362	,38 , charset));    // FILLER

        return ck1400Res;
    }

    public String getData(byte[] byteArray, int offset, int length, Charset charset) {
        if (byteArray == null)
            return "";

        String str = null;
        str = new String(byteArray, offset, length, charset);
        return str.trim();
    }
}