package com.aos.cms.virtualaccount.server.net;

import java.io.IOException;
import java.net.SocketAddress;
import java.time.LocalDateTime;

import com.aos.cms.virtualaccount.server.dto.OnlineReqDto;

public interface Connection {
    SocketAddress getRemoteAddress();
    void send(byte[] data) throws IOException;
    void addListener(Listener listener);
    void start();
    void close();
    void resetKeepTime();
    LocalDateTime getKeepTime();
    
    interface Listener {
        void messageReceived(Connection connection, OnlineReqDto message);
        void connected(Connection connection);
        void disconnected(Connection connection);
    }
}