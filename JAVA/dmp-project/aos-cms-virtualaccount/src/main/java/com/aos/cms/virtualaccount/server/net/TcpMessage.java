package com.aos.cms.virtualaccount.server.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TcpMessage {
    public final static int MAX_SIZE = 1024;

    private byte[] reqByteData;

    public int readFromData(InputStream inputStream) throws IOException {
        byte[] data = new byte[MAX_SIZE];
        int readbyte = inputStream.read(data);
        if(readbyte == -1) return readbyte;

        //원본 수신 바이트 저장
        reqByteData = new byte[readbyte];
        System.arraycopy(data, 0, reqByteData, 0, readbyte);

        return readbyte;
    }

    public void writeToStream(OutputStream outputStream, byte[] data) {
        try {
            outputStream.write(data);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
