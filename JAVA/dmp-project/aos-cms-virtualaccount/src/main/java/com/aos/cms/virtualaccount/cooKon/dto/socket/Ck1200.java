package com.aos.cms.virtualaccount.cooKon.dto.socket;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 쿠콘 가상계좌수취조회 요청
 * 
 * @since 2022.11.04
 * @author kh
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Ck1200 extends CkHeader implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Ck1200Dp1;
    private String Ck1200Dp2;
    private String Ck1200Dp3;
    private String Ck1200Dp4;
    private String Ck1200Dp5;
    private String Ck1200Dp6;
    private String Ck1200Dp7;
    private String Ck1200Dp8;
    private String Ck1200Dp9;
    private String Ck1200Dp10;
    private String Ck1200Dp11;
    private String Ck1200Dp12;
    private String Ck1200Dp13;
    private String Ck1200Dp14;
    private String Ck1200Dp15;
    private String Ck1200Dp16;
    private String Ck1200Dp17;
}
