package com.aos.cms.virtualaccount.cooKon.dto.socket;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 쿠콘 가상계좌 공통부
 * 
 * @since 2022.11.04
 * @author kh
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CkHeader{
    private String ckHeaderDp1;
    private String ckHeaderDp2;
    private String ckHeaderDp3;
    private String ckHeaderDp4;
    private String ckHeaderDp5;
    private String ckHeaderDp6;
    private String ckHeaderDp7;
    private String ckHeaderDp8;
    private String ckHeaderDp9;
    private String ckHeaderDp10;
    private String ckHeaderDp11;
    private String ckHeaderDp12;
    private String ckHeaderDp13;
    private String ckHeaderDp14;
    private String ckHeaderDp15;
}
