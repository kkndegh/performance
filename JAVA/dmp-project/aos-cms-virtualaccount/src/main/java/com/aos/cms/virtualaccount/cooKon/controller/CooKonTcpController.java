package com.aos.cms.virtualaccount.cooKon.controller;

import com.aos.cms.virtualaccount.server.dto.OnlineReqDto;
import com.aos.cms.virtualaccount.server.net.Connection;
import com.aos.cms.virtualaccount.server.net.TcpController;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;

import com.aos.cms.virtualaccount.Constant;
import com.aos.cms.virtualaccount.cooKon.service.CookonVirtualService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Base64;
import java.util.HashMap;

import com.aos.cms.virtualaccount.cooKon.util.CookonUtil;
import com.aos.cms.virtualaccount.cooKon.util.SEED.SeedUtil;

/**
 * 쿠콘 TCP 컨트롤러
 * 
 * @since 2022.10.13
 * @author sung
 */
@TcpController
@RequiredArgsConstructor
@Slf4j
public class CooKonTcpController {

    @Value("${cookon.data.key}")
    private String cKey;
    private final SeedUtil seedUtil;
    private final CookonVirtualService cookonVirtualService;
    private final CookonUtil cookonUtil;

    public void receiveData(Connection connection, OnlineReqDto reqData) {
        log.info("############쿠콘 TcpController 시작############");

        try {
            String decryptText = "";
            String decData = "";

            byte[] reqBytedata = reqData.getReqByteData();
            byte[] reqPacket = new byte[reqBytedata.length - 4];
            String packetSize = new String(reqBytedata, Constant.COOKON_DEFAULT_CHARSET).substring(0,4);
            log.info("패킷 사이즈 :: " + packetSize);

            //패킷 셋팅
            for (int i = 4; i < reqBytedata.length; i++)
            {
                reqPacket[i - 4] = reqBytedata[i];
            }
            
            //SEED 복호화 작업
            try {
                //SEED복호화
                decryptText = seedUtil.decryptAsString(reqPacket, cKey.getBytes()).trim();
                //Base64디코딩
                decData = new String(Base64.getDecoder().decode(decryptText), "EUC-KR");
            } catch (Exception e) {
                log.error("복호화 에러 :: " + e.getMessage());
            }
            
            // 메시지가 전달 될 경우 KeepTime을 초기화 한다.
            connection.resetKeepTime();

            log.info("[socket 요청 데이터 복호화] size("+ decData.getBytes().length +" byte) :: " + decData);

            /*********************************************
             * 가상계좌 REQ 로그 저장 
             *********************************************/
            HashMap<String, Object> mapObj = new HashMap<String, Object>();
            String trade_dt = cookonUtil.getMaxByteSubString(decData, 4, 8, Constant.DEFAULT_CHARSET);
            mapObj.put("trade_dt",  trade_dt.substring(0, 4)+"-"+trade_dt.substring(4, 6)+"-"+trade_dt.substring(6, 8));       //거래일자
            mapObj.put("bank_cd",   cookonUtil.getMaxByteSubString(decData, 63, 3, Constant.DEFAULT_CHARSET));                 //은행코드
            mapObj.put("acc_no",    cookonUtil.getMaxByteSubString(decData, 100, 16, Constant.DEFAULT_CHARSET));               //가상계좌
            mapObj.put("comp_cd",   cookonUtil.getMaxByteSubString(decData, 42, 8, Constant.DEFAULT_CHARSET));                 //기관코드
            mapObj.put("doc_no",    cookonUtil.getMaxByteSubString(decData, 18, 12, Constant.DEFAULT_CHARSET));                //전문일련번호
            mapObj.put("trade_tm",  cookonUtil.getMaxByteSubString(decData, 12, 6, Constant.DEFAULT_CHARSET));                 //거래시간
            mapObj.put("doc_type",  cookonUtil.getMaxByteSubString(decData, 30, 4, Constant.DEFAULT_CHARSET));                 //전문종류
            mapObj.put("trade_cd",  cookonUtil.getMaxByteSubString(decData, 34, 4, Constant.DEFAULT_CHARSET));                 //거래코드
            mapObj.put("res_cd",    cookonUtil.getMaxByteSubString(decData, 38, 4, Constant.DEFAULT_CHARSET));                 //응답코드
            mapObj.put("user_nm",   cookonUtil.getMaxByteSubString(decData, 146, 30, Constant.DEFAULT_CHARSET));               //입금인명
            mapObj.put("amount",    Integer.parseInt(cookonUtil.getMaxByteSubString(decData, 218, 12, Constant.DEFAULT_CHARSET)));  //총금액
            if(!mapObj.get("trade_cd").equals("1200")){
                mapObj.put("req_type", cookonUtil.getMaxByteSubString(decData, 339, 1, Constant.DEFAULT_CHARSET));  //수취조회발생구분
            }
            mapObj.put("type", "virtual");
            mapObj.put("message", decData);                                                             //전문메시지
            mapObj.put("packet_message", new String(Base64.getEncoder().encodeToString(reqBytedata)));  //패킷 메시지(수신 바이트를 Base64 인코딩하여 저장)
            // map.put("cust_no", "");

            JSONObject reqObj = new JSONObject(mapObj);
            cookonUtil.saveLog(reqObj);
            
            //전문데이터 가공 및 응답
            cookonVirtualService.cookonVirtualAccountDoc(connection, decData, packetSize);
        } catch(Exception e){
            log.error("socket 데이터 요청 및 응답 오류 :: " + e.toString());
        }
    }
}
