package com.aos.cms.virtualaccount.server.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import com.aos.cms.virtualaccount.server.dto.OnlineReqDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TcpConnection implements Connection {
    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private Socket socket;
    private List<Listener> listeners = new ArrayList<>();
    private LocalDateTime keepTime;

    public TcpConnection(Socket socket) {
        this.socket = socket;
        try {
            inputStream = new DataInputStream(socket.getInputStream());
            outputStream = new DataOutputStream(socket.getOutputStream());
            keepTime = LocalDateTime.now();
        } catch (IOException e) {
            log.error("TcpConnection Constructor", e);
        }
    }

    @Override
    public SocketAddress getRemoteAddress() {
        return socket.getRemoteSocketAddress();
    }

    @Override
    public void send(byte[] data) throws IOException {
        TcpMessage objectToSend = new TcpMessage();
        objectToSend.writeToStream(outputStream, data);
    }

    @Override
    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    @Override
    public void start() {
        Executors.newCachedThreadPool().execute(() -> {
            while (true) {
                try {
                    TcpMessage message = new TcpMessage();
                    int byteCnt = message.readFromData(inputStream);

                    if(byteCnt < 0){
                        break;
                    }
                    
                    if (socket.isClosed() || socket.isInputShutdown()) {
                        log.info("socket is close or inputShutdown");
                        break;
                    }
                    
                    OnlineReqDto ord = new OnlineReqDto(message.getReqByteData());
                    
                    for (Listener listener : listeners) {
                        listener.messageReceived(this, ord);
                    }
                } catch (Exception e) {
                    log.error("receve error and connection closed", e);
                    break;
                }
            }
            broadcaseDisconnected();
            close();
        });
    }

    private void broadcaseDisconnected() {
        for (Listener listener : listeners) {
            listener.disconnected(this);
        }
    }

    @Override
    public void resetKeepTime() {
        keepTime = LocalDateTime.now();
    }
    
    @Override
    public LocalDateTime getKeepTime() {
        return keepTime;
    }
    
    @Override
    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            log.error("close socket", e);
        }
    }
}