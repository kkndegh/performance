package com.aos.cms.virtualaccount;

import java.nio.charset.Charset;

public class Constant {
    public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
    public static final Charset COOKON_DEFAULT_CHARSET = Charset.forName("EUC-KR");

    //HttpParam 셋팅시 Title 목록
    public static final String CMS_LOG_TITLE = "[쿠콘 가상계좌 REQ 로그 Params]";
} 