package com.aos.cms.virtualaccount.config;

import org.springframework.stereotype.Component;

/**
 * Application constants.
 */
@Component
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String ANONYMOUS_USER = "anonymoususer";
    public static final String DEFAULT_LANGUAGE = "ko";
    public static final String BROKER_RESULT_CHECK = "result";
    public static final String RESULT_SUCCESS = "success";
}
