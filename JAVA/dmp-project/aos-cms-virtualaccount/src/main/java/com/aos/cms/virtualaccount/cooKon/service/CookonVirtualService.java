package com.aos.cms.virtualaccount.cooKon.service;

import java.io.ByteArrayOutputStream;
import java.util.Base64;
import java.util.HashMap;

import com.aos.cms.virtualaccount.Constant;
import com.aos.cms.virtualaccount.common.BaseValue;
import com.aos.cms.virtualaccount.cooKon.util.CookonUtil;
import com.aos.cms.virtualaccount.cooKon.util.SEED.SeedUtil;
import com.aos.cms.virtualaccount.cooKon.util.convert.ReqDataConvert;
import com.aos.cms.virtualaccount.cooKon.util.convert.ResDataConvert;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import com.aos.cms.virtualaccount.cooKon.dto.socket.Ck1200;
import com.aos.cms.virtualaccount.cooKon.dto.socket.Ck1300;
import com.aos.cms.virtualaccount.cooKon.dto.socket.Ck1400;

import com.aos.cms.virtualaccount.server.net.Connection;

/**
 * 쿠콘 가상계좌 서비스
 * 
 * @author kh
 * @since 2022.11.04
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class CookonVirtualService extends BaseValue{
    
    @Value("${cookon.data.key}")
    private String cKey;
    private final SeedUtil seedUtil;
    private final ResDataConvert resDataConvert;
    private final CookonUtil cookonUtil;
    
    /**
     * 쿠콘 가상계좌 Process
     * 
     * @param reqDataStr String
     * @param connection Connection
     */
    public void cookonVirtualAccountDoc(Connection connection, String reqDataStr, String packetSize) {
        String rtnData = null;
        String encData = null;
        byte[] encByte = null;
        String resCode = reqDataStr.substring(34, 38);

        try {
            log.info("[전문 요청 Code] :: " + resCode);

            if(resCode.equals("1200")){
                log.info("[가상계좌수취 조회 전문 실행]");
                rtnData = Ck1200(reqDataStr);
            } else if(resCode.equals("1300")){
                log.info("[가상계좌입금 통보 전문 실행]");
                rtnData = Ck1300(reqDataStr);
            } else if(resCode.equals("1400")){
                log.info("[가상계좌입금 취소 전문 실행]");
                rtnData = Ck1400(reqDataStr);
            }

            //응답데이터 암호화
            try {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                outputStream.write(packetSize.getBytes());
                outputStream.write(seedUtil.encrypt(rtnData, cKey.getBytes()));
                encByte = outputStream.toByteArray();

                encData = new String(Base64.getEncoder().encodeToString(encByte));
            } catch (Exception e) {
                log.error("암호화 에러 :: " + e.getMessage());
            }

            log.info("[socket 응답 데이터] size("+ rtnData.getBytes().length +" byte) :: " + rtnData);
            log.info("[socket 응답 데이터 암호화] size("+ encData.getBytes().length +" byte) :: " + encData);

            /*********************************************
             * 가상계좌 RES 로그 저장 
             *********************************************/
            HashMap<String, Object> mapObj = new HashMap<String, Object>();
            String trade_dt = cookonUtil.getMaxByteSubString(rtnData, 4, 8, Constant.DEFAULT_CHARSET);
            mapObj.put("trade_dt",  trade_dt.substring(0, 4)+"-"+trade_dt.substring(4, 6)+"-"+trade_dt.substring(6, 8));       //거래일자
            mapObj.put("bank_cd",   cookonUtil.getMaxByteSubString(rtnData, 63, 3, Constant.DEFAULT_CHARSET));                 //은행코드
            mapObj.put("acc_no",    cookonUtil.getMaxByteSubString(rtnData, 100, 16, Constant.DEFAULT_CHARSET));               //가상계좌
            mapObj.put("comp_cd",   cookonUtil.getMaxByteSubString(rtnData, 42, 8, Constant.DEFAULT_CHARSET));                 //기관코드
            mapObj.put("doc_no",    cookonUtil.getMaxByteSubString(rtnData, 18, 12, Constant.DEFAULT_CHARSET));                //전문일련번호
            mapObj.put("trade_tm",  cookonUtil.getMaxByteSubString(rtnData, 12, 6, Constant.DEFAULT_CHARSET));                 //거래시간
            mapObj.put("doc_type",  cookonUtil.getMaxByteSubString(rtnData, 30, 4, Constant.DEFAULT_CHARSET));                 //전문종류
            mapObj.put("trade_cd",  cookonUtil.getMaxByteSubString(rtnData, 34, 4, Constant.DEFAULT_CHARSET));                 //거래코드
            mapObj.put("res_cd",    cookonUtil.getMaxByteSubString(rtnData, 38, 4, Constant.DEFAULT_CHARSET));                 //응답코드
            mapObj.put("user_nm",   cookonUtil.getMaxByteSubString(rtnData, 146, 30, Constant.DEFAULT_CHARSET));               //입금인명
            mapObj.put("amount",    Integer.parseInt(cookonUtil.getMaxByteSubString(rtnData, 218, 12, Constant.DEFAULT_CHARSET)));  //총금액
            if(!mapObj.get("trade_cd").equals("1200")){
                mapObj.put("req_type", cookonUtil.getMaxByteSubString(rtnData, 339, 1, Constant.DEFAULT_CHARSET));  //수취조회발생구분
            }
            mapObj.put("type", "virtual");
            mapObj.put("message", rtnData);             //전문메시지
            mapObj.put("packet_message", encData);      //패킷 메시지
            // map.put("cust_no", "");

            JSONObject resObj = new JSONObject(mapObj);
            cookonUtil.saveLog(resObj);

            //응답
            connection.send(encByte);
        } catch (Exception e) {
            log.error("쿠콘 가상계좌 Process 오류 :: " + e.toString());
        }
    }
    
    // 가상계좌수취조회 요청
    private String Ck1200(String reqDataStr){
        String result = null;

        try {
            Ck1200 ck1200 = resDataConvert.Ck1200Res(reqDataStr, Constant.COOKON_DEFAULT_CHARSET);

            ck1200.setCkHeaderDp5("0210");
            ck1200.setCkHeaderDp7("0000");
            ck1200.setCk1200Dp3("주식회사 온리원");

            ReqDataConvert docData = ReqDataConvert.builder()
                        .ck1200(ck1200)
                        .type("1200")
                        .build();
            result = docData.getString(Constant.COOKON_DEFAULT_CHARSET);
        } catch (Exception e) {
            log.error("쿠콘 가상계좌수취조회 처리 오류 [ck1200]:: " + e.toString());
        }
        return result;
    }

    // 가상계좌입금 통보 요청
    private String Ck1300(String reqDataStr){
        String result = null;

        try {
            Ck1300 ck1300 = resDataConvert.Ck1300Res(reqDataStr, Constant.COOKON_DEFAULT_CHARSET);

            ck1300.setCkHeaderDp5("0210");
            ck1300.setCkHeaderDp7("0000");
            ck1300.setCk1300Dp3("주식회사 온리원");
            
            ReqDataConvert docData = ReqDataConvert.builder()
                        .ck1300(ck1300)
                        .type("1300")
                        .build();
            result = docData.getString(Constant.COOKON_DEFAULT_CHARSET);
            System.out.println(result.getBytes().length);
            System.out.println();
        } catch (Exception e) {
            log.error("쿠콘 가상계좌입금 통보 처리 오류 [ck1300]:: " + e.toString());
        }
        return result;
    }

    // 가상계좌입금취소 요청
    private String Ck1400(String reqDataStr){
        String result = null;

        try {
            Ck1400 ck1400 = resDataConvert.Ck1400Res(reqDataStr, Constant.COOKON_DEFAULT_CHARSET);

            ck1400.setCkHeaderDp5("0210");
            ck1400.setCkHeaderDp7("0000");
            
            ReqDataConvert docData = ReqDataConvert.builder()
                        .ck1400(ck1400)
                        .type("1400")
                        .build();
            result = docData.getString(Constant.COOKON_DEFAULT_CHARSET);
        } catch (Exception e) {
            log.error("쿠콘 가상계좌입금취소 요청 처리 오류 [ck1400]:: " + e.toString());
        }
        return result;
    }
}