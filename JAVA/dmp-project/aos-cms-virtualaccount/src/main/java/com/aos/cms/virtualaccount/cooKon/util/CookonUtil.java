package com.aos.cms.virtualaccount.cooKon.util;

import java.nio.charset.Charset;

import com.aos.cms.virtualaccount.Constant;
import com.aos.cms.virtualaccount.common.dto.HttpParam;
import com.aos.cms.virtualaccount.common.util.HttpUtil;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.util.ObjectUtils;


/**
 * 쿠콘 유틸
 * 
 * @author kh
 * @since 2022.11.11
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class CookonUtil {

    /** 
     * AOS URL
    */
    @Value("${aos.base.url}")
    private String aosBaseUrl;

    /** 
     * HTTP 유틸
    */
    private final HttpUtil httpUtil;
    
    /**
     * 효성 통신 LOG 저장
     * 
     * @param obj JSONObject
     */
    public void saveLog(JSONObject obj){
        String virtualLogInfoStr = "";    //가상계좌 로그 정보

        try {
            /*********************************************
             * 쿠콘 가상계좌 로그 저장 
             *********************************************/
            String virtualLogInfoUrl = aosBaseUrl + "/api/v1/cmsLog/virtualSaveCk";
            String virtualLogInfoMethod = "POST";
            String virtualLogInfoHeader = "{\"access_token\":\"qwerty\"}";
            String virtualLogInfoBody = obj.toString();

            log.debug("\n[[가상계좌 로그 저장 데이터 셋팅]]");
            log.debug("virtualLogInfoUrl :: "+virtualLogInfoUrl);
            log.debug("virtualLogInfoMethod :: "+virtualLogInfoMethod);
            log.debug("virtualLogInfoHeader :: "+virtualLogInfoHeader);
            log.debug("virtualLogInfoBody :: "+virtualLogInfoBody);

            HttpParam virtualLogInfoParam = 
                new HttpParam(Constant.CMS_LOG_TITLE, virtualLogInfoUrl, virtualLogInfoMethod, virtualLogInfoHeader, virtualLogInfoBody);    //통신파라미터 초기화

            //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
            virtualLogInfoStr = httpUtil.callApiHttps(virtualLogInfoParam);

            log.info("로그 저장 결과 :: " + virtualLogInfoStr + "\n");
        } catch (Exception e) {
            log.info("로그 저장 에러 :: " + e.toString());
        }
    }

    /**
     * 문자열을 바이트 단위로 변환
     * 
     * @param str String
     * @param offset int
     * @param bufSize int
     * @param charset Charset
     * @return String
    */
    public String getMaxByteSubString(String str, int offset, int bufSize, Charset charset){
        int LENGTH = 0;
        ByteBuf bb = Unpooled.buffer(LENGTH);

        bb = Unpooled.buffer(bufSize);
        getByteBuf(bb, "%-"+bufSize+"s", str, charset, offset, bufSize);

        return bb.toString(charset).replaceAll("\\s", "");
    }

    private void getByteBuf(ByteBuf bb, String format, Object args, Charset charset, int srcIndex, int length) {
        if (ObjectUtils.isEmpty(charset)) {
            bb.writeBytes(String.format(format,    args).getBytes(Charset.forName("EUC-KR")), srcIndex, length);
        } else {
            bb.writeBytes(String.format(format,    args).getBytes(charset), srcIndex, length);
        }
    }
}