package com.aos.cms.virtualaccount.cooKon.dto.socket;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 쿠콘 가상계좌입금 통보
 * 
 * @since 2022.11.04
 * @author kh
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Ck1300 extends CkHeader implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Ck1300Dp1;
    private String Ck1300Dp2;
    private String Ck1300Dp3;
    private String Ck1300Dp4;
    private String Ck1300Dp5;
    private String Ck1300Dp6;
    private String Ck1300Dp7;
    private String Ck1300Dp8;
    private String Ck1300Dp9;
    private String Ck1300Dp10;
    private String Ck1300Dp11;
    private String Ck1300Dp12;
    private String Ck1300Dp13;
    private String Ck1300Dp14;
    private String Ck1300Dp15;
    private String Ck1300Dp16;
    private String Ck1300Dp17;
    private String Ck1300Dp18;
    private String Ck1300Dp19;
}
