package com.aos.cms.virtualaccount.server.net;


import java.util.List;

public interface ServerNet {
    int getConnectionsCount();
    void setPort(Integer port);
    void start();
    void stop();
    List<Connection> getConnections();
    void addListener(Connection.Listener listener);
}