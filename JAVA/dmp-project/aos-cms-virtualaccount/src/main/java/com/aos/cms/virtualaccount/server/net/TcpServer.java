package com.aos.cms.virtualaccount.server.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.PreDestroy;

import com.aos.cms.virtualaccount.server.dto.OnlineReqDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TcpServer implements ServerNet, Connection.Listener {
    private ServerSocket serverSocket;
    private volatile boolean isStop;
    private List<Connection> connections = new ArrayList<>();
    private List<Connection.Listener> listeners = new ArrayList<>();
    private final Lock lock = new ReentrantLock();

    @PreDestroy
    public void preDestory() {
        stoppingServer();
    }

    private void stoppingServer() {
        log.info("Tcp Server Stoping ");
        isStop = true;
        if(serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                log.error("stop serverSocket", e);
            }
            serverSocket = null;
        }

        log.info("Tcp Server Connected Socket Close ...");
        for(Connection c : connections) {
            
            disconnected(c);
            c.close();
        }
    }
    
    public void setPort(Integer port) {
        try {
            if (port == null) {
                log.info("Property tcp.server.port not found. Use default port 1234");
                port = 1234;
            }
            serverSocket = new ServerSocket(port);
            log.info("Server start at port " + port);
        } catch (IOException e) {
            log.error("May be port " + port + " busy.");
        }
    }

    @Override
    public int getConnectionsCount() {
        return connections.size();
    }

    @Override
    public void start() {
        new Thread(() -> {
            while (!isStop) {
                try {
                    Socket socket = serverSocket.accept();
                    if (socket.isConnected()) {
                        TcpConnection tcpConnection = new TcpConnection(socket);
                        tcpConnection.addListener(this);
                        tcpConnection.start();
                        connected(tcpConnection);
                    }
                } catch (IOException e) {
                    log.error("" + e);
                }
            }
        }).start();
    }

    @Override
    public void stop() {
        stoppingServer();
    }

    @Override
    public List<Connection> getConnections() {
        return connections;
    }

    @Override
    public void addListener(Connection.Listener listener) {
        listeners.add(listener);
    }

    @Override
    public void messageReceived(Connection connection, OnlineReqDto message) {
        for (Connection.Listener listener : listeners) {
            listener.messageReceived(connection, message);
        }
    }

    @Override
    public void connected(Connection conn) {
        log.info("Connected : {}", conn.getRemoteAddress());
        lock.lock();

        try {
            connections.add(conn);
        } finally {
            lock.unlock();
        }

        log.info("# of connections : {}", connections.size());
        
        for (Connection.Listener listener : listeners) {
            listener.connected(conn);
        }
    }

    @Override
    public void disconnected(Connection conn) {
        log.info("Disconnected : {}", conn.getRemoteAddress());
        lock.lock();

        try {
            connections.remove(conn);
        } finally {
            lock.unlock();
        }

        log.info("# of connections : {}", connections.size());
        
        for (Connection.Listener listener : listeners) {
            listener.disconnected(conn);
        }
    }
}