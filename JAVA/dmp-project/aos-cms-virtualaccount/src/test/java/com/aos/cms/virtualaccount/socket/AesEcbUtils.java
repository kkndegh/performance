package com.aos.cms.virtualaccount.socket;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class AesEcbUtils {
    private static Cipher cipher;
    private static SecretKey secretKey;
    private static String encryptKey;
    
    public AesEcbUtils(String encryptKey) throws NoSuchAlgorithmException, NoSuchPaddingException {
        AesEcbUtils.encryptKey = encryptKey;
        AesEcbUtils.secretKey = new SecretKeySpec(Arrays.copyOf(AesEcbUtils.encryptKey.getBytes(), 16), "AES");
        AesEcbUtils.cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
    }
    
    public byte[] encrypt(byte[] message) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        cipher.init(Cipher.ENCRYPT_MODE, AesEcbUtils.secretKey);
        return cipher.doFinal(message);
    }
    
    public byte[] decrypt(byte[] message) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        return cipher.doFinal(message);
    }
}
