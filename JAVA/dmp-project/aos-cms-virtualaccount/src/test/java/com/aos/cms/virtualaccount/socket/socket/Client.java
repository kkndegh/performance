package com.aos.cms.virtualaccount.socket.socket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Base64;

import lombok.extern.slf4j.Slf4j;

import com.aos.cms.virtualaccount.socket.AesEcbUtils;

import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class Client {
    @Autowired
    private AesEcbUtils aesEcbUtils;
    
    public static void main(String[] args) {
        String ip = "localhost";
        int port = 25050;
        new Client(ip, port);
    }

    public Client(String ip, int port) {
        Socket socket = null;
        try {

            // 서버에 요청 보내기
            socket = new Socket(ip, port);
            log.info(socket.getInetAddress().getHostAddress() + "에 연결됨");

            send(socket);

            // 응답 출력
            log.info("[[소켓 암호화 메시지 수신]]");
            String resData = receive(socket);

            log.info("[[수신 전문" + "(" + resData.getBytes("EUC-KR").length + " byte)]] :: " + resData);

            //SEED 복호화 작업
            try {
                String decryptedReqData = new String(aesEcbUtils.decrypt(Base64.getDecoder().decode(resData)));
                log.info("[[메시지 복호화" + "(" + decryptedReqData.getBytes("EUC-KR").length + " byte)]] :: " + decryptedReqData);
            } catch (Exception e) {
                log.error("복호화 에러 :: " + e.getMessage());
            }
        } catch (IOException e) {
            log.error("소켓 통신 오류 :: " + e.getMessage());
        } finally {
            // 소켓 닫기 (연결 끊기)
            if (!socket.isClosed()) {
                try {
                    socket.close();
                    System.out.println("\n[ Socket closed2 ]\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            log.info("소켓 통신 종료");
        }
    }

    public void send(Socket socket) {
        String reqData = "04002020022815205200000000021402001300    088349000000188      08816                                56212693702974                                e커머스사업본                 26                    06003               0000043323550000043323550000000000004                                                                     026            1                   KRW0000000000000                         ";

        try {
            log.info("[[발신 전문]] :: " + reqData);
            String encryptedReqData = Base64.getEncoder().encodeToString(aesEcbUtils.encrypt(reqData.getBytes("EUC-KR")));

            // 메세지 전달
            log.info("[[메시지 암호화]]");
            log.info("encryptedReqData :: " + encryptedReqData + "\n\n");

            //생성한 person 객체를 byte array로 변환
            byte[] data = encryptedReqData.getBytes("EUC-KR");

            //서버로 내보내기 위한 출력 스트림 뚫음
            OutputStream os = socket.getOutputStream();

            //출력 스트림에 데이터 씀
            os.write(data);

            //보냄
            os.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String receive(Socket socket){
        String resultData = null;

        try {
            //수신 버퍼의 최대 사이즈 지정
            int maxBufferSize = 8000;

            //버퍼 생성
            byte[] recvBuffer = new byte[maxBufferSize];

            //서버로부터 받기 위한 입력 스트림 뚫음
            InputStream is = socket.getInputStream();

            //버퍼(recvBuffer) 인자로 넣어서 받음. 반환 값은 받아온 size
            int nReadSize = is.read(recvBuffer);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.write(recvBuffer, 0, nReadSize);

            byte[] body = baos.toByteArray();

            resultData = new String(body, Charset.forName("EUC-KR"));
        } catch (Exception e) {
            log.info("socket 수신 에러 :: " + e.toString());
        }
        return resultData;
    }
}