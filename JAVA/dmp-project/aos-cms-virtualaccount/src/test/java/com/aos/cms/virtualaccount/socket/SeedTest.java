package com.aos.cms.virtualaccount.socket;

import java.util.Base64;

import com.aos.cms.virtualaccount.Constant;
import com.aos.cms.virtualaccount.cooKon.util.SEED.SeedUtil;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
@RunWith(SpringRunner.class)
public class SeedTest {

    @Autowired
    private SeedUtil seedUtil;

    @Value("${cookon.data.key}")
    private String cKey;
        
    //송수신 데이터 복호화
    @Test
    public void test1(){
        /**
         * 1. BASE64 인코딩 데이터(DB저장 된 packet_message) 을 BASE64 디코딩
         * 2. 앞 4바이트 자른 바이트 저장
         * 3. SEED 복호화
         * 4. BASE64 디코딩 & EUC-KR String 변환
         */
        // given
        String base64_enc_seed_dec_str = "MDQwNI+/Y++5vsM1xHTAw2t0iCeELQH/hW67IpWPjEvUutHhCA8n6qTU+6Wi17jrQ/RxsAaOmFzxaYCNx0GFQzsuQYqICuP8QSb22f0NuP/3sOW37ZUmbhsI8/cv2eWaMrmPSBllO1QFHcBVu2+pmot7sa3tlSZuGwjz9y/Z5ZoyuY9I7ZUmbhsI8/cv2eWaMrmPSH2O8nE7qziCAsMs/+zPQrS0bb/dPnBEIO68MgXUOBrqyMHCTFHEKcshJ7DRHWzyoSsufN/ZJI25b7C+yjv/Fsx1m+inWPsV3AdDNg/VC4E+FSgtDxxYE88d1n9IPmLogRlz/BrYLBxvHc6URlwDqYWCV5fym5kmNKuAHB0K1hDv7ZUmbhsI8/cv2eWaMrmPSO2VJm4bCPP3L9nlmjK5j0jtlSZuGwjz9y/Z5ZoyuY9IQz7D8WXRjvVAweKVSje+vno+Mhai5tfUIpN3t8zWbWvTpGSD0+VGYxCwBI1qPspFvlpDsHJUnusp+jhHKYo0wO2VJm4bCPP3L9nlmjK5j0g=";
        byte[] base64_dec_seed_dec_str;
        byte[] reqPacket;
        
        String originStr = null;
        String decryptText = "";
        String packetSize = null;

        // when
        try {
            base64_dec_seed_dec_str = Base64.getDecoder().decode(base64_enc_seed_dec_str);
            packetSize = new String(base64_dec_seed_dec_str, Constant.COOKON_DEFAULT_CHARSET).substring(0,4);
            reqPacket = new byte[base64_dec_seed_dec_str.length - 4];
            for (int i = 4; i < base64_dec_seed_dec_str.length; i++)
            {
                reqPacket[i - 4] = base64_dec_seed_dec_str[i];
            }

            decryptText = seedUtil.decryptAsString(reqPacket, cKey.getBytes()).trim();
            originStr = new String(Base64.getDecoder().decode(decryptText), "EUC-KR");
        } catch (Exception e) {
            log.error("base64 dec error :: " + e.toString());
        }

        // then
        log.debug("packetSize :: " + packetSize);
        log.debug("originStr :: " + originStr);
    }
}