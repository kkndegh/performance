#!/bin/bash

TAG=$(cat dmp-admin.tag)
YAML=/home/gitlab/yaml/dmp-admin.yaml

sed -i 's/latest/'"$TAG"'/g' $YAML
sudo kubectl apply -f $YAML
