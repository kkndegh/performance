<?php
include($_SERVER["DOCUMENT_ROOT"] . "/system/include/phpHeader.php");
?>
<html>

<head>
    <?php
    include($root . "/system/include/head.php");
    ?>
    <script type="module" src="/system/js/common/common.js?v=<?php echo $config["version"]; ?>" defer></script>
    <script type="module" src="/system/js/manageUser.js?v=<?php echo $config["version"]; ?>" defer></script>
    <link rel="stylesheet" href="/system/css/manageUser.css?v=<?php echo $config["version"]; ?>">
</head>

<body>
    <?php
    include($root . "/system/include/loadingBox.php");
    ?>
    <div id="wrap" class="leftListClose subMenuClose">
        <div class="left">
            <menu>
                <?php
                include($root . "/system/include/menu.php");
                ?>
            </menu>
        </div>
        <div class="right">
            <header>
                <?php
                include($root . "/system/include/header.php");
                ?>
            </header>
            <div class="container">
                <nav class="subMenu"></nav>
                <main>
                    <?php
                    include($root . "/system/include/toolbar.php");
                    ?>
                    <section>
                        <h2>
                            <?php
                            if (str_contains($_SERVER["QUERY_STRING"], 'project=aos')) {
                                echo "AOS User 관리";
                            } else {
                                echo "User 관리";
                            }
                            ?>
                        </h2>
                        <article class="userList">
                            <h3>유저 리스트</h3>
                            <br>
                            <div class="flexBox">
                                <div class="leftBox">
                                    <div class="listWrapper">
                                        <div class="viewOption">
                                            <b>임시삭제 유저 보기</b>
                                            <div class="checkBoxDiv">
                                                <input type="checkBox" class="chkBox" id="viewUnactivatedUser">
                                                <label class="checkBox" for="viewUnactivatedUser"></label>
                                            </div>
                                        </div>
                                        <div class="userListTopDiv listTopDiv">
                                            <button id="addUser" class="btn_style_2 btn_color_point">
                                                <i class="fa-solid fa-plus"></i>
                                                새 유저 추가
                                            </button>
                                            <p class="totalCount">Total Count : <strong>0</strong></p>
                                        </div>
                                        <div class="userListDiv listDiv scrollable"></div>
                                    </div>
                                </div>
                                <div class="rightBox">
                                    <div class="flexArea">
                                        <div class="userArea">
                                            <h4>유저 정보</h4>
                                            <br>
                                            <div class="tableDiv scrollable">
                                                <table>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                            <div class="tableBottomDiv">
                                                <button id="delUser" class="btn_style_2 btn_color_crush">
                                                    <i class="fa-solid fa-trash-can"></i>
                                                    삭제하기
                                                </button>
                                                <button id="modUser" class="btn_style_2 btn_color_point">
                                                    <i class="fa-solid fa-floppy-disk"></i>
                                                    저장하기
                                                </button>
                                            </div>
                                        </div>
                                        <div class="userAccountArea">
                                            <h4>유저 계정 정보</h4>
                                            <br>
                                            <div class="tableDiv scrollable">
                                                <table>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                            <div class="tableBottomDiv">
                                                <button id="delUserAccount" class="btn_style_2 btn_color_crush">
                                                    <i class="fa-solid fa-trash-can"></i>
                                                    삭제하기
                                                </button>
                                                <button id="modUserAccount" class="btn_style_2 btn_color_point">
                                                    <i class="fa-solid fa-floppy-disk"></i>
                                                    저장하기
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article id="token_ip">
                            <h3>토큰/IP 관리</h3>
                            <br>
                            <div class="flexBox">
                                <div class="leftBox">
                                    <h4>토큰 관리</h4>
                                    <div class="searchOption"></div>
                                    <div class="tableTopDiv">
                                        <div class="buttonsDiv">
                                            <button id="addManualToken" class="btn_style_2 btn_color_point">
                                                <i class="fa-solid fa-plus"></i>
                                                토큰 수동 발행
                                            </button>
                                            <button id="delExpiredToken" class="btn_style_2 btn_color_crush">
                                                <i class="fa-solid fa-trash-can"></i>
                                                만료 토큰 삭제
                                            </button>
                                        </div>
                                        <p class="totalCount">Total Count : <strong>0</strong></p>
                                    </div>
                                    <div class="tableBox scrollable">
                                        <table>
                                            <thead></thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="rightBox">
                                    <h4>IP 관리</h4>
                                    <div class="searchOption"></div>
                                    <div class="tableTopDiv">
                                        <div class="buttonsDiv">
                                            <button id="addAllowedIP" class="btn_style_2 btn_color_point">
                                                <i class="fa-solid fa-plus"></i>
                                                허용할 IP 추가
                                            </button>
                                            <button id="addBannedIP" class="btn_style_2 btn_color_crush">
                                                <i class="fa-solid fa-ban"></i>
                                                차단할 IP 추가
                                            </button>
                                        </div>
                                        <p class="totalCount">Total Count : <strong>0</strong></p>
                                    </div>
                                    <div class="tableBox scrollable">
                                        <table>
                                            <thead></thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </section>
                </main>
                <aside>
                    <?php
                    include($root . "/system/include/aside.php");
                    ?>
                </aside>
            </div>
        </div>
        <?php
        include($root . "/system/include/modal.php");
        ?>
    </div>
</body>

</html>