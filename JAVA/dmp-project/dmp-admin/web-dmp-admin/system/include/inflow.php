<?php
// inflow에 직접 접근 방지
if (realpath($_SERVER['SCRIPT_FILENAME']) == realpath(__FILE__)) {
    echo "<script>alert('잘못된 접근입니다.')</script>";
    echo "<script>history.back();</script>";
    exit;
}

try {
    $pathurl = $_SERVER['SCRIPT_FILENAME'];


    //세션에 token이 없으면 로그인페이지로 이동
    if (str_contains($pathurl, "/api/v1")) {
    } else {
        // 유효 토큰 확인
        if (
            ($_SERVER["PHP_SELF"] == "/index.php"
                && $_SESSION["access_token"] == $config["access_token"])
            || ($_SERVER["PHP_SELF"] !== "/login.php"
                && $_SERVER["PHP_SELF"] !== "/openIdConnect.php"
                && (!isset($_SESSION["access_token_expire"])
                    || !isset($_SESSION["refresh_token_expire"])
                    || date("Y-m-d H:i:s") > $_SESSION["access_token_expire"]
                    || date("Y-m-d H:i:s") > $_SESSION["refresh_token_expire"])
            )
        ) {
            header("Location: /login");
            die;
        }

        // 페이지 접근 이력 작성 예정
        //
        // 메뉴 권한이 없는 페이지에 접속할 경우 튕겨냄
        // $header = array();
        // $referer = str_replace('.php', '', $_SERVER["PHP_SELF"]);
        // $param = null;
        // if (strpos($_SERVER["QUERY_STRING"], "project=aos") !== false) {
        //     $param = 'project=aos';
        // }
        // $referer = $param !== null ? $referer . "?" . $param : $referer;
        // $token = $_SESSION["access_token"];
        // array_push($header, "Content-Type: application/json");
        // array_push($header, "access_token: $token");
        // array_push($header, "REFERER: $referer");

        // $menuListDataString = ClientAPI("menu", "menuList", '', $header, "null");
        // $menuListData = json_decode($menuListDataString, true);
        // if ($menuListData["result_code"] !== 'P000') {
        //     $errorMessage = $menuListData["result_msg"];
        //     header("Location: /system/errorPage/serverError.php?error=Failed to call 'Menu List' in Inflow");
        // } else {
        //     //echo "<script>let g_menu_data = $menuListDataString;</script>";
        //     $menuList = $menuListData["menu"];
        //     // foreach ($menuList as $idx => $menu) {
        //     //     if ($menu["menu_auth_yn"] == "N") {
        //     //         $bannedURL =  $menu["menu_link_url"];
        //     //         if (strpos($bannedURL, $referer) !== false) {
        //     //             // echo "<script>alert('접근 권한이 없습니다.')</script>";
        //     //             // echo "<script>history.back();</script>";
        //     //         }
        //     //     }
        //     // }
        // }
        // // // 메뉴 use_yn이 'N'인 페이지에 접속할 경우 튕겨냄
        // // $menuUseChk = ClientAPI("menu", "menuUseChk", '', $header, "null");
        // // $menuUse = json_decode($menuUseChk, true);
        // // if ($menuUse["result_code"] !== 'P000') {
        // //     echo "<script>alert('접근 권한이 없습니다.')</script>";
        // //     echo "<script>location.replace('/');</script>";
        // // }

        // // ip check
        // {
        //     $curl = curl_init();
        //     curl_setopt_array($curl, array(
        //         CURLOPT_URL => 'https://api.ip.pe.kr/',
        //         CURLOPT_RETURNTRANSFER => true,
        //         CURLOPT_ENCODING => '',
        //         CURLOPT_MAXREDIRS => 10,
        //         CURLOPT_TIMEOUT => 0,
        //         CURLOPT_FOLLOWLOCATION => true,
        //         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //         CURLOPT_CUSTOMREQUEST => 'GET',
        //     ));
        //     $response = curl_exec($curl);
        //     curl_close($curl);
        //     // echo $response;
        // }
    }
} catch (exception $e) {
    echo "exception:" . $e->getMessage();
}
