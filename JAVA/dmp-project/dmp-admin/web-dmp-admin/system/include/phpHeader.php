<?php
// 세션 스타트
session_start();

header('Content-Type: text/html; charset=UTF-8');
$root = $_SERVER["DOCUMENT_ROOT"];
$serverip = $_SERVER["SERVER_ADDR"];

include $root . "/system/lib/route.php";

include $root . '/system/lib/json.lib.php';

include $root . '/system/lib/clientapi.php';

// VERSION 파일 읽기
$fp = fopen($root . "/VERSION", "r");
$VERSION = fread($fp, filesize($root . "/VERSION"));
fclose($fp);

$config = parse_ini_file($root . '/.config/app.ini');
$config["version"] = $VERSION . date("YmdHis");

$_SESSION["access_token"] = isset($_SESSION["access_token"]) ? $_SESSION["access_token"] : $config["access_token"];
$user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
$access_token = isset($_SESSION['access_token']) ? $_SESSION['access_token'] : null;
$refresh_token = isset($_SESSION["refresh_token"]) ? $_SESSION["refresh_token"] : null;
$access_token_expire = isset($_SESSION["access_token_expire"]) ? $_SESSION["access_token_expire"] : null;
$refresh_token_expire = isset($_SESSION["refresh_token_expire"]) ? $_SESSION["refresh_token_expire"] : null;

if ($serverip == "127.0.0.1") {
    ini_set('display_errors', 'On');
}


include $root . "/system/include/inflow.php";
