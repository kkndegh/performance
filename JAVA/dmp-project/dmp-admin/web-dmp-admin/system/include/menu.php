<div class="logoTop" onclick="goIndex()">
    <img src="/img/logo.svg" alt="logo">
    <span class="version">v.<i><?php echo $VERSION ?></i></span>
</div>
<div class="nav">
    <ul>
        <li onclick="goIndex()">
            <i class="fa-solid fa-chalkboard"></i>
            <a>대쉬보드</a>
        </li>
    </ul>
    <div class="menuChange">
        <i class="fa-solid fa-angles-left"></i>
        <span>Simple Menu</span>
    </div>
</div>
<script>
    function goIndex() {
        location.href = "<?php echo $config["homepath"]; ?>";
    }
</script>