<div class="leftList">
    <h3>심사 | 대출 리스트</h3>
    <span class="close_leftList"><i class="fa-solid fa-xmark"></i></span>
    <div class="listSwitch">
        <button class="btn_style_3 btn_color_point">심사 리스트</button>
        <button class="btn_style_3 btn_color_gray">대출 리스트</button>
    </div>
    <div class="searchOption"></div>
    <div class="listTableBox">
        <table>
            <thead>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<div class="rightList">
    <h3>회원 리스트</h3>
    <div class="searchOption"></div>
    <ul class="scrollable">
    </ul>
</div>