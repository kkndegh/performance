<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $config["title"] ?></title>

<!-- 파비콘 및 스타일시트 -->
<link rel="icon" href="/img/favicon.svg?v=<?php echo $config["version"]; ?>" type="image/x-icon">
<link rel="shortcut icon" href="/img/favicon.svg?v=<?php echo $config["version"]; ?>" type="image/x-icon">
<link rel="stylesheet" href="/system/css/common/common.css?v=<?php echo $config["version"]; ?>">

<!-- 외부 모듈 -->
<script src="https://kit.fontawesome.com/133e10f389.js" crossorigin="anonymous"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>
<link rel="stylesheet" href="/node-modules/codemirror/lib/codemirror.css">
<link rel="stylesheet" href="/node-modules/codemirror/theme/mdn-like.css">
<script src="/node-modules/codemirror/lib/codemirror.js"></script>
<script src="/node-modules/codemirror/mode/javascript/javascript.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="/node-modules/sortablejs/Sortable.min.js"></script>
<link rel="stylesheet" href="/node-modules/dtsel/dtsel.css" />
<script src="/node-modules/dtsel/dtsel.js"></script>
<script src="/node-modules/jquery/dist/jquery.js"></script>
<link rel="stylesheet" href="/node-modules/jquery-ui/jquery-ui.css">
<script src="/node-modules/jquery-ui/jquery-ui.js"></script>
<link rel="stylesheet" href="/node-modules/jquery-timepicker/jquery.timepicker.css">
<script src="/node-modules/jquery-timepicker/jquery.timepicker.js"></script>


<?php
$user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
$user_account_seq_no = isset($_SESSION['user_account_seq_no']) ? $_SESSION['user_account_seq_no'] : null;
$user_account_name = isset($_SESSION['user_account_name']) ? $_SESSION['user_account_name'] : null;
$access_token = isset($_SESSION['access_token']) ? $_SESSION['access_token'] : null;
$refresh_token = isset($_SESSION["refresh_token"]) ? $_SESSION["refresh_token"] : null;
$access_token_expire = isset($_SESSION["access_token_expire"]) ? $_SESSION["access_token_expire"] : null;
$refresh_token_expire = isset($_SESSION["refresh_token_expire"]) ? $_SESSION["refresh_token_expire"] : null;

echo "<script>let g_user_id = '$user_id';</script>";
echo "<script>let g_user_account_seq_no = '$user_account_seq_no';</script>";
echo "<script>let g_user_account_name = '$user_account_name';</script>";
echo "<script>let g_access_token_expire = '$access_token_expire';</script>";
echo "<script>let g_refresh_token_expire = '$refresh_token_expire';</script>";
?>