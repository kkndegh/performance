<div class="header_left">
    <i class="fa-solid fa-bars"></i>
</div>
<div class="header_mid">
    <span class="leftGo goBtn"><i class="fa-solid fa-angle-left"></i></span>
    <span class="rightGo goBtn"><i class="fa-solid fa-angle-right"></i></span>
    <div class="checkList">
        <ul class="listBarWrap">
            <li class="listBar">
                <ul class="alarmBox">
                    <li>
                        <span class="newAlarm"></span>
                        <p>회원가입</p>
                        <small>2건</small>
                    </li>
                    <li>
                        <span class="newAlarm"></span>
                        <p>접수</p>
                        <small>1건</small>
                    </li>
                    <li>
                        <span></span>
                        <p>1차심사</p>
                        <small>2건</small>
                    </li>
                    <li>
                        <span class="newAlarm"></span>
                        <p>진행대기</p>
                        <small>1건</small>
                    </li>
                    <li>
                        <span></span>
                        <p>2차심사</p>
                        <small>2건</small>
                    </li>
                    <li>
                        <span></span>
                        <p>재심사</p>
                        <small>1건</small>
                    </li>
                    <li>
                        <span class="newAlarm"></span>
                        <p>자서대기</p>
                        <small>3건</small>
                    </li>
                    <li>
                        <span class="newAlarm"></span>
                        <p>입금대기</p>
                        <small>3건</small>
                    </li>
                    <li>
                        <span></span>
                        <p>대출완료</p>
                        <small>3건</small>
                    </li>
                    <li>
                        <span class="newAlarm"></span>
                        <p>보류,부결</p>
                        <small>1건</small>
                    </li>
                </ul>
            </li>
            <li class="listBar">
                <p>알림</p>
            </li>
            <li class="listBar">
                <p>공지사항</p>
            </li>
        </ul>
    </div>
    <ul class="checkListPageBtn">
        <li class="on"><span></span></li>
        <li><span></span></li>
        <li><span></span></li>
    </ul>
    <div class="userBox">
        <div class="middleBox">
            <p class="helloUser"><b><?php echo $_SESSION['user_id'] ?></b> 님</p>
            <div class="flex">
                <span class="timeOut"><strong>00:00:00</strong></span>
                <button id="refreshToken" class="btn_style_2 btn_color_point">연장 <i class="fa-solid fa-arrows-rotate"></i></button>
                <a href="/system/mypage"><button id="mypage" class="btn_style_3 btn_color_point">마이페이지</button></a>
            </div>
        </div>
        <span class="logOut">
            <p>로그아웃</p>
            <i class="fa-solid fa-arrow-right-from-bracket"></i>
        </span>
    </div>
</div>
<div class="header_right">
    <i class="fa-solid fa-ellipsis-vertical"></i>
</div>