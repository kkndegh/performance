<div class="toolbar">
    <div class="leftToolBar">
        <p class="currMenuGroup"></p>
        <span class="slash"><i class="fa-solid fa-chevron-right"></i></span>
        <p class="currMenu"></p>
        <a class="siteMap" href="/system/siteMap<?php if (str_contains($_SERVER["QUERY_STRING"], 'popup=yes')) {
                                                    echo "?popup=yes";
                                                } ?>">사이트맵</a>
    </div>
    <div class="rightToolBar">
        <span class="manual">
            <i class="fa-solid fa-circle-question"></i>Need help?
        </span>
        <a class="fullsize">
            <i class="fa-solid fa-up-right-and-down-left-from-center"></i>
            <span>Full Screen</span>
        </a>
        <a class="popup">
            <i class="fa-solid fa-square-arrow-up-right"></i>
            <span>Popup Screen</span>
        </a>
    </div>
</div>