<div class="modal">
    <div class="layerPopup">
        <div class="popupHead">
            <h3></h3>
            <i class="fa-solid fa-xmark"></i>
        </div>
        <div class="popupBody">

        </div>
        <div class="btnBox">
        </div>
    </div>
</div>
<div class="alert_modal">
    <div class="alert_layerPopup">
        <div class="alert_popupHead">
            <h3></h3>
            <i class="fa-solid fa-xmark"></i>
        </div>
        <div class="alert_popupBody">

        </div>
        <div class="alert_btnBox">
        </div>
    </div>
</div>