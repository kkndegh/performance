import { createElement } from './import/CreateElement';
import { currDate, currTime } from './import/CurrentTime';
import { loading, loading_end } from './import/Loading';
import { modalShow } from './import/ModalShow';
import { searchForm } from './import/Searchform';

let apiList = await fetch('/api/v1/api/apiList').then((res) => res.json());

// console.log(apiList);

const tableBox = document.querySelector('#apiStatus .leftBox .tableBox');
const normalCount = document.querySelector('#apiStatus .detailCount .normal p .count');
const cautionCount = document.querySelector('#apiStatus .detailCount .caution p .count');
const errorCount = document.querySelector('#apiStatus .detailCount .error p .count');
const unknownCount = document.querySelector('#apiStatus .detailCount .unknown p .count');
const totalCount = document.querySelector('#apiStatus p.totalCount strong');
const apiReqCount = document.querySelector('#summary .content .apiReq .num b');
const apiErrCount = document.querySelector('#summary .content .apiErr .num b');
const lastCheckTime = document.querySelector('#summary .checkAPIBox p');
const interval = document.querySelector('#summary #interval');

function searchFormMaker() {
	const location = document.querySelector('#apiStatus .flexBox .leftBox .searchOption');
	searchForm(location, {
		options: [
			{
				title: '상태',
				checkBox: {
					ok: '정상',
					caution: '주의',
					error: '오류',
					unknown: '알수없음',
				},
			},
		],
		textInput: false,
		searchBtn: true,
	});
}
searchFormMaker();

let theadKey = ['API Name', 'URL', 'Result Code', 'Status', '개별검사'];
let theadTr = createElement('tr');
tableBox.querySelector('thead').append(theadTr);
for (let key of theadKey) {
	let th = createElement('th', {
		html: key,
	});
	theadTr.append(th);
}

apiStatusTest();
async function apiStatusTest() {
	loading();
	//초기화
	if (tableBox.querySelectorAll('tbody tr')) {
		tableBox.querySelectorAll('tbody tr').forEach((el) => el.remove());
		normalCount.innerHTML = 0;
		cautionCount.innerHTML = 0;
		errorCount.innerHTML = 0;
		unknownCount.innerHTML = 0;
		totalCount.innerHTML = 0;
		apiReqCount.innerHTML = 0;
		apiErrCount.innerHTML = 0;
	}
	let apiInfoArr = [];
	let start = performance.now();
	let results = await Promise.allSettled(
		(() => {
			let apiFetchArr = [];
			for (let api of apiList['list']) {
				let url = api['url'];
				let testHeader = api['test_header'];
				let testBody = api['test_body'];
				// if (testBody !== null && testHeader !== null) {
				if (testBody !== null && testHeader == null) {
					testBody = JSON.stringify((JSON.parse(testBody)['headerData'] = testHeader));
				}
				let method = api['method'];

				let tr = createElement('tr');
				const tbody = tableBox.querySelector('tbody');
				tbody.append(tr);
				let tdKey = [api['api_name'], api['url'], '', '', '검사'];
				for (let key of tdKey) {
					if (key == '검사') {
						let td = createElement('td');
						tr.append(td);
						let button = createElement('button', {
							class: 'btn_style_3',
							html: '<i class="fa-solid fa-plug"></i> 로그보기',
						});
						td.append(button);

						button.addEventListener('click', () => {
							if (method == 'POST') {
								promise = fetch(url, {
									method: method,
									body: testBody,
								})
									.then((res) => res.json())
									.then((data) => {
										fetchThen(data, tr, api);
									})
									.catch((error) => {
										fetchCatch(tr, api);
									});
							} else if (method == 'GET') {
								promise = fetch(url)
									.then((res) => res.json())
									.then((data) => {
										fetchThen(data, tr, api);
									})
									.catch((error) => {
										fetchCatch(tr, api);
									});
							}
						});
					} else {
						let td = createElement('td', {
							html: key,
						});
						tr.append(td);
					}
				}

				let promise;
				if (method == 'POST') {
					promise = fetch(url, {
						method: method,
						body: testBody,
					})
						.then((res) => res.json())
						.then((data) => {
							fetchThen(data, tr, api);
						})
						.catch((error) => {
							fetchCatch(tr, api);
						});
				} else if (method == 'GET') {
					promise = fetch(url)
						.then((res) => res.json())
						.then((data) => {
							fetchThen(data, tr, api);
						})
						.catch((error) => {
							fetchCatch(tr, api);
						});
				}
				apiFetchArr.push(promise);
			}
			return apiFetchArr;
		})()
	);
	if (results) {
		loading_end();
		let end = performance.now();
		let averageGap = (end - start) / results.length;
		// console.log(`Average Gap : ${averageGap}`);
		// console.log(`Total Count : ${results['length']}`);
		const apiTimeGap = document.querySelector('#summary .apiTimeGap p strong');
		apiTimeGap.innerHTML = Math.round(averageGap);

		totalCount.innerHTML = results.length;

		lastCheckTime.innerHTML = `${currDate()} ${currTime()}`;
	}
}

function fetchThen(data, tr, api) {
	if (data['result_code']) {
		if (data['result_code'] == 'P000') {
			tr.querySelectorAll('td')[2].innerHTML = data['result_code'];
			tr.querySelectorAll('td')[3].innerHTML = '정상';
			tr.querySelector('td button').classList.add('btn_color_point');
			normalCount.innerHTML = parseInt(normalCount.innerHTML) + 1;
			apiReqCount.innerHTML = parseInt(apiReqCount.innerHTML) + 1;
			writeLog(data, api, null, null);
			// console.log(`${api['api_name']} \n 정상`);
		} else if (data['result_code'] == 'D999') {
			tr.querySelectorAll('td')[2].innerHTML = data['result_code'];
			tr.querySelectorAll('td')[3].innerHTML = '오류';
			tr.querySelector('td button').classList.add('btn_color_crush');
			tr.classList.add('crush');
			errorCount.innerHTML = parseInt(errorCount.innerHTML) + 1;
			apiErrCountPlus();
			writeLog(data, api, null, null);
			// console.error(`${api['api_name']} \n 오류`);
		} else {
			tr.querySelectorAll('td')[2].innerHTML = data['result_code'];
			tr.querySelectorAll('td')[3].innerHTML = '주의';
			tr.querySelector('td button').classList.add('btn_color_caution');
			tr.classList.add('caution');
			cautionCount.innerHTML = parseInt(cautionCount.innerHTML) + 1;
			apiErrCountPlus();
			writeLog(data, api, null, null);
			// console.error(`${api['api_name']} \n 주의`);
		}
	} else {
		tr.querySelectorAll('td')[2].innerHTML = data['result_code'] ?? '';
		tr.querySelectorAll('td')[3].innerHTML = '알 수 없음';
		tr.querySelector('td button').classList.add('btn_color_gray');
		tr.classList.add('unknown');
		unknownCount.innerHTML = parseInt(unknownCount.innerHTML) + 1;
		apiErrCountPlus();
		writeLog(data, api, 'Result는 있으나 Result Code가 존재하지 않음', null);
		// console.error(`${api['api_name']} \n Result는 있으나 Result Code가 존재하지 않음`);
	}
}

function fetchCatch(tr, api) {
	tr.querySelectorAll('td')[3].innerHTML = '응답 없음';
	tr.querySelector('td button').classList.add('btn_color_gray');
	tr.classList.add('unknown');
	unknownCount.innerHTML = parseInt(unknownCount.innerHTML) + 1;
	apiErrCountPlus();
	let data = {
		result_code: null,
		result_msg: null,
		result_sys_msg: null,
	};
	writeLog(data, api, '응답 없음', '응답 없음');
	// console.error(`${api['api_name']} \n 응답 없음`);
}

function apiErrCountPlus() {
	apiErrCount.innerHTML = parseInt(apiErrCount.innerHTML) + 1;
	document.querySelector('#summary .content .apiErr').classList.add('active');
}

function writeLog(data, api, customResultMsg, customSysMsg) {
	const logTable = document.querySelector('#apiStatus .rightBox .log table tbody');

	let apiGroup = api['api_group'];
	let apiCode = api['api_code'];
	let result_code = data['result_code'] ?? null;
	let result_msg = data['result_msg'] ?? customResultMsg;
	let sys_msg = data['result_sys_msg'] ?? customSysMsg;

	let line1Tr = createElement('tr');
	let line1Td1 = createElement('td', {
		html: `[ ${apiGroup}/${apiCode} ]`,
	});
	let state;
	if (result_code) {
		if (result_code == 'P000') {
			state = 'OK';
		} else if (result_code == 'D999') {
			state = 'Error';
		} else {
			state = 'Caution';
		}
	} else {
		state = 'Unknown';
	}
	let line1Td2 = createElement('td', {
		html: `<i>......Status ${state}</i>`,
	});
	line1Tr.append(line1Td1, line1Td2);

	let line2Tr = createElement('tr');
	let line2Td1 = createElement('td', {
		html: `Result Message...`,
	});
	let line2Td2 = createElement('td', {
		html: `<b>${result_code} : ${result_msg}</b>`,
	});
	line2Tr.append(line2Td1, line2Td2);

	let line3Tr = createElement('tr');
	let line3Td1 = createElement('td', {
		html: `System Message...`,
	});
	let sysmsgHTML = sys_msg !== null ? `<b>${sys_msg}</b>` : null;
	let line3Td2 = createElement('td', {
		html: sysmsgHTML,
	});
	line3Tr.append(line3Td1, line3Td2);

	let line4Tr = createElement('tr');
	let line4Td1 = createElement('td', {
		html: '.',
	});
	let line4Td2 = createElement('td');
	line4Tr.append(line4Td1, line4Td2);

	let line5Tr = createElement('tr');
	let line5Td1 = createElement('td', {
		html: '.',
	});
	let line5Td2 = createElement('td');
	line5Tr.append(line5Td1, line5Td2);

	if (result_code) {
		if (result_code == 'P000') {
			line1Tr.classList.add('ok');
			line2Tr.classList.add('ok');
			line3Tr.classList.add('ok');
			line4Tr.classList.add('ok');
			line5Tr.classList.add('ok');
		} else if (result_code == 'D999') {
			line1Tr.classList.add('error');
			line2Tr.classList.add('error');
			line3Tr.classList.add('error');
			line4Tr.classList.add('error');
			line5Tr.classList.add('error');
		} else {
			line1Tr.classList.add('warning');
			line2Tr.classList.add('warning');
			line3Tr.classList.add('warning');
			line4Tr.classList.add('warning');
			line5Tr.classList.add('warning');
		}
	} else {
		line1Tr.classList.add('unknown');
		line2Tr.classList.add('unknown');
		line3Tr.classList.add('unknown');
		line4Tr.classList.add('unknown');
		line5Tr.classList.add('unknown');
	}

	logTable.append(line1Tr, line2Tr, line3Tr, line4Tr, line5Tr);

	let logBox = document.querySelector('#apiStatus .flexBox .rightBox .log');
	let linesHeight = line1Tr.offsetHeight * 5 + logTable.offsetHeight;
	logBox.scrollTo({
		top: linesHeight,
		behavior: 'smooth',
	});
}

document.querySelector('button#apiAllCheckBtn').addEventListener('click', apiStatusTest);

//자동 API 체크 인터벌 설정
interval.addEventListener('click', () => {
	let obj = {};
	obj.title = '자동 검사 인터벌 설정';
	obj.btn = true;
	obj.btnText = '설정 완료';
	obj.btnColor = 'point';
	obj.func = intervalPopup;

	function intervalPopup() {
		const popupBody = document.querySelector('.modal .layerPopup .popupBody');
		//초기화
		if (popupBody.querySelector('table')) {
			popupBody.querySelector('table').remove();
		}
		let intervalArr = [
			'설정 안함',
			'5분',
			'10분',
			'30분',
			'1시간',
			'2시간',
			'3시간',
			'6시간',
			'12시간',
			'24시간',
		];
		let intervalNumArr = [null, 5, 10, 30, 60, 120, 180, 360, 720, 1440];
		let table = createElement('table');
		let tbody = createElement('tbody');
		let tr = createElement('tr');
		let th = createElement('th', {
			html: '인터벌 설정',
		});
		let td = createElement('td');
		let select = createElement('select');
		popupBody.append(table);
		table.append(tbody);
		tbody.append(tr);
		tr.append(th, td);
		for (let i = 0; i < intervalArr.length; i++) {
			let option = createElement('option', {
				value: intervalNumArr[i],
				html: intervalArr[i],
			});
			select.append(option);
		}
		td.append(select);
	}

	modalShow(obj);
});
