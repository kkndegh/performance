import { currDate, calcDate, thisWeek, thisMonthFirstDay } from './import/CurrentTime';
import { jsonToQueryString } from './import/JsonToQueryString';
import { renderPagination } from './import/Paging';
import { searchForm } from './import/Searchform';
import { createElement } from './import/CreateElement';
import { target_height } from './import/ResizeObserver';
import { IsJson } from './import/IsJson';
import { insertParam } from './import/InsertParam';
import { searchParam } from './import/SearchParam';
import { loading, loading_end } from './import/Loading';

loading();

let urlApiList;
let urlApiHistory;
if (searchParam('project') == 'aos') {
	urlApiList = `/api/v1/api/aosApiList`;
	urlApiHistory = `/api/v1/api/aosApiHistory`;
} else {
	urlApiList = `/api/v1/api/apiList`;
	urlApiHistory = `/api/v1/api/apiHistory`;
}
let apiGroup = await fetch(urlApiList)
	.then((res) => res.json())
	.then((data) => {
		let apis = data['list'];
		let apiGroupArr = [];
		let apiGroupObj = {};
		apis.forEach((el) => {
			if (!apiGroupArr.includes(el['api_group'])) {
				apiGroupArr.push(el['api_group']);
			}
			if (!apiGroupObj[el['api_group']]) {
				apiGroupObj[el['api_group']] = el['api_group'];
			}
		});

		return apiGroupObj;
	});

function searchFormMaker() {
	const location = document.querySelector('article .flexBox .leftBox .searchOption');
	searchForm(location, {
		period: {
			format: 'YYYY-MM-DD',
			// fastBtns: ['당일', '최근 일주일', '최근 한 달', '전체'],
			fastBtns: [
				{
					text: '당일',
					clickHandler: [currDate(), currDate()],
				},
				{
					text: '최근 7일',
					clickHandler: [calcDate('-', 7), currDate()],
				},
				{
					text: '최근 30일',
					clickHandler: [calcDate('-', 30), currDate()],
				},
				{
					text: '이번 주(월요일부터)',
					clickHandler: [thisWeek(1), currDate()],
				},
				{
					text: '이번 달',
					clickHandler: [thisMonthFirstDay(), currDate()],
				},
			],
		},
		options: [
			{
				title: '자동 새로고침',
				select: {
					id: 'autoRefresh',
					option: [
						{ value: 0, html: '꺼짐', selected: true },
						{ value: 10, html: '10초' },
						{ value: 30, html: '30초' },
						{ value: 60, html: '1분' },
						{ value: 300, html: '5분' },
						{ value: 600, html: '10분' },
						{ value: 900, html: '15분' },
						{ value: 1200, html: '20분' },
						{ value: 1500, html: '25분' },
						{ value: 1800, html: '30분' },
					],
				},
			},
			{
				title: 'Method',
				checkBox: {
					GET: 'GET',
					POST: 'POST',
					PUT: 'PUT',
					DELETE: 'DELETE',
				},
			},
			{
				title: 'API Group',
				checkBox: apiGroup,
			},
			{
				title: '응답 상태',
				checkBox: {
					success: '정상',
					userErr: '사용자 오류',
					systemErr: '시스템 오류',
				},
			},
		],
		textInput: false,
		searchBtn: true,
	});
}
searchFormMaker();
document.querySelector('input[name="dateTimePicker1"]').value = currDate();
document.querySelector('input[name="dateTimePicker2"]').value = currDate();

function autoRefresh() {
	let select = document.querySelector('#autoRefresh');

	let interval;
	select.addEventListener('change', () => {
		if (select.value == 0) {
			clearInterval(interval);
		} else {
			clearInterval(interval);
			interval = setInterval(apiHistoryCallAgain, select.value * 1000);
		}
	});
}
autoRefresh();

async function apiHistoryCall(
	page,
	page_limit,
	his_start_dt,
	his_end_dt,
	his_method,
	his_api_group,
	his_result
) {
	let jsonString = jsonToQueryString({
		page: page,
		page_limit: page_limit,
		his_start_dt: his_start_dt,
		his_end_dt: his_end_dt,
		his_method: his_method,
		his_api_group: his_api_group,
		his_result: his_result,
	});
	let result = await fetch(urlApiHistory + '?' + jsonString).then((res) => res.json());
	if (result) {
		loading_end();
		if (result.result_code == 'P000') {
			// console.log(result);
			let totalCount = document.querySelector('p.totalCount strong');
			totalCount.innerHTML = result['total_count'];
			// 처음 페이지네이션 생성
			const pageLocation = document.querySelector('.tableBottomDiv .paging');
			renderPagination(pageLocation, {
				page: page,
				page_limit: page_limit,
				totalCount: result['total_count'],
				numOfBtns: 10,
				pageClick: function () {
					insertParam('page', document.querySelector('#pageBtn.on').dataset.num);
					apiHistoryCall(
						searchParam('page'),
						page_limit,
						his_start_dt,
						his_end_dt,
						his_method,
						his_api_group,
						his_result
					);
				},
			});

			tableMaker();
			function tableMaker() {
				const table = document.querySelector('.flexBox .leftBox .tableBox table');
				if (table.querySelector('thead tr')) {
					table.querySelector('thead tr').remove();
				}
				if (table.querySelectorAll('tbody tr')) {
					table.querySelectorAll('tbody tr').forEach((el) => el.remove());
				}
				let key_kor = {
					api_req_seq_no: 'API 히스토리 시퀀스 넘버',
					result_code: '결과 코드',
					result_msg: '결과 메세지',
					req_method: 'Method',
					// req_body: '전송된 Body Data',
					req_url: '요청 URL',
				};

				//테이블 헤드 생성
				let theadTr = createElement('tr');
				table.querySelector('thead').append(theadTr);
				for (let key in key_kor) {
					let th = createElement('th', {
						html: key_kor[key],
					});
					theadTr.append(th);
				}

				//테이블 바디 생성
				if (result['list'] == null) {
					let tr = createElement('tr');
					table.querySelector('tbody').append(tr);
					let td = createElement('td', {
						html: '데이터가 없습니다.',
						colspan: '5',
						id: 'noData',
					});
					tr.append(td);
				} else {
					result['list'].forEach((el) => {
						let tr = createElement('tr');
						table.querySelector('tbody').append(tr);
						for (let key in key_kor) {
							if (key == 'req_url') {
								let urlText;
								let regex = RegExp(
									/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi
								);
								if (regex.test(el[key]) !== true) {
									urlText = el[key];
								} else {
									urlText = new URL(el[key]).pathname;
								}
								let td = createElement('td', {
									html: urlText,
								});
								tr.append(td);
							} else {
								let td = createElement('td', {
									html: el[key],
								});
								tr.append(td);
							}

							if (key == 'result_code') {
								if (el[key] !== 'P000') {
									let errorCode = el[key];
									if (errorCode == 'P901' || errorCode == 'P902') {
										//아이디, 패스워드 조회 실패
										tr.classList.add('caution');
									} else {
										tr.classList.add('crush');
									}
								}
							}
						}

						tr.addEventListener('click', () => {
							// 히스토리 상세보기 함수
							const rightBox = document.querySelector('.rightBox');
							if (rightBox.querySelectorAll('table tbody tr')) {
								rightBox
									.querySelectorAll('table tbody tr')
									.forEach((el) => el.remove());
							}
							table
								.querySelectorAll('tbody tr')
								.forEach((el) => el.classList.remove('point'));
							tr.classList.add('point');

							let key_kor = {
								api_req_seq_no: 'History 연번',
								req_method: '요청 Method',
								result_code: '결과 코드',
								result_msg: '결과 메세지',
								result_sys_msg: '시스템 메세지',
								req_url: 'URL',
								req_body: '전송한 Body 데이터',
							};

							for (let key in key_kor) {
								let tr = createElement('tr');
								let th = createElement('th', {
									html: key_kor[key],
								});

								let td = createElement('td', {
									html: el[key],
								});
								if (key == 'req_body') {
									td.classList.add('bodyData');
									IsJson(el[key]) == true
										? (td.innerHTML = JSON.stringify(
												JSON.parse(el[key]),
												null,
												2
										  ))
										: (td.innerHTML = el[key]);
								}

								tr.append(th, td);
								rightBox.querySelector('table tbody').append(tr);
							}
						});
					});
				}
			}
		} else {
			console.error(result);
		}
	} else {
		console.error('API History fetch error');
	}
}

// 처음 페이지 로드
let initPage = searchParam('page') ? searchParam('page') : 1;
let initRowInView = document.querySelector('#rowInView').value;
apiHistoryCall(initPage, initRowInView, currDate(), currDate(), null, null, null);

// 검색할때
document.querySelector('#searchBtn').addEventListener('click', apiHistoryCallAgain);
// 몇개씩 볼지 select로 수정할때
document.querySelector('#rowInView').addEventListener('change', apiHistoryCallAgain);

// API 히스토리 호출 다시하기
function apiHistoryCallAgain() {
	// let page = document.querySelector('.btn_style_paging.on').dataset.num;
	let page_limit = document.querySelector('#rowInView').value;
	let his_start_dt = (() => {
		let value = document.querySelectorAll('.searchOption .period input')[0].value;
		if (value !== '') {
			let regex = RegExp(/^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/);
			if (regex.test(value) !== true) {
				document.querySelectorAll('.searchOption .period input')[0].focus();
				return false;
			} else {
				return value;
			}
		} else {
			value = currDate();
			return value;
		}
	})();
	if (his_start_dt == false) return false;
	let his_end_dt = (() => {
		let value = document.querySelectorAll('.searchOption .period input')[1].value;
		if (value !== '') {
			let regex = RegExp(/^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/);
			if (regex.test(value) !== true) {
				document.querySelectorAll('.searchOption .period input')[1].focus();
				return false;
			} else {
				return value;
			}
		} else {
			value = currDate();
			return value;
		}
	})();
	if (his_end_dt == false) return false;
	let his_method = (() => {
		let arr = [];
		document.querySelectorAll('#Method input').forEach((el) => {
			if (el.checked == true) {
				arr.push(el.getAttribute('id'));
			}
		});
		if (arr.length <= 1) {
			return arr[0];
		} else {
			return arr;
		}
	})();
	let his_api_group = (() => {
		let arr = [];
		document.querySelectorAll('div[id="API Group"] input').forEach((el) => {
			if (el.checked == true) {
				arr.push(el.getAttribute('id'));
			}
		});
		if (arr.length <= 1) {
			return arr[0];
		} else {
			return arr;
		}
	})();
	let his_result = (() => {
		let arr = [];
		document.querySelectorAll('div[id="응답 상태"] input').forEach((el) => {
			if (el.checked == true) {
				arr.push(el.getAttribute('id'));
			}
		});
		if (arr.length <= 1) {
			return arr[0];
		} else {
			return arr;
		}
	})();

	insertParam('page', 1);
	apiHistoryCall(1, page_limit, his_start_dt, his_end_dt, his_method, his_api_group, his_result);
}
