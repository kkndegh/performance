import { createElement } from './import/CreateElement';
import { errorHandling } from './import/ErrorHandler';
import { insertParam } from './import/InsertParam';
import { IsJson } from './import/IsJson';
import { jsonToQueryString } from './import/JsonToQueryString';
import { loading, loading_end } from './import/Loading';
import { alertShow, cancelAlert, cancelModal, modalShow } from './import/ModalShow';
import { renderPagination } from './import/Paging';
import { searchParam } from './import/SearchParam';

async function manualListCall(page, page_limit) {
	loading();
	let jsonString = jsonToQueryString({
		page: page,
		page_limit: page_limit,
	});
	let manualList = await fetch('/api/v1/manual/manualList?' + jsonString).then((res) =>
		res.json()
	);
	let menuList = await fetch('/api/v1/menu/menuList').then((res) => res.json());
	let fetchArr = [manualList, menuList];
	let results = await Promise.allSettled(fetchArr);
	if (results) {
		loading_end();
		results.forEach((el, idx) => {
			if (el.status == 'fulfilled') {
				let result = el.value;
				if (result.result_code == 'P000') {
					addNewManual(results[1]['value']['menu']);
					if (idx == 0) {
						console.log(result);
						let totalCount = document.querySelector('p.totalCount strong');
						totalCount.innerHTML = result['total_count'];
						// 처음 페이지네이션 생성
						const pageLocation = document.querySelector('.tableBottomDiv .paging');
						renderPagination(pageLocation, {
							page: page,
							page_limit: page_limit,
							totalCount: result['total_count'],
							numOfBtns: 10,
							pageClick: function () {
								insertParam(
									'page',
									document.querySelector('#pageBtn.on').dataset.num
								);
								manualListCall(searchParam('page'), page_limit);
							},
						});

						tableMaker();
						function tableMaker() {
							const table = document.querySelector(
								'.flexBox .leftBox .tableDiv table'
							);
							if (table.querySelector('thead tr')) {
								table.querySelector('thead tr').remove();
							}
							if (table.querySelectorAll('tbody tr')) {
								table.querySelectorAll('tbody tr').forEach((el) => el.remove());
							}
							let key_kor = {
								// manual_seq_no: '도움말 일련번호',
								menu_seq_no: '도움말 위치',
								manual_name: '도움말 이름',
								// reg_id: '등록자',
								reg_ts: '등록시간',
							};

							//테이블 헤드 생성
							let theadTr = createElement('tr');
							table.querySelector('thead').append(theadTr);
							for (let key in key_kor) {
								let th = createElement('th', {
									html: key_kor[key],
								});
								theadTr.append(th);
							}
							//테이블 바디 생성
							result['list'].forEach((el) => {
								let tr = createElement('tr');
								table.querySelector('tbody').append(tr);
								for (let key in key_kor) {
									if (key == 'menu_seq_no') {
										let menuData = results[1]['value']['menu'];
										menuData.forEach((el2) => {
											if (el[key] == el2['menu_seq_no']) {
												let td = createElement('td', {
													html: el2['menu_name'],
												});
												tr.append(td);
											}
										});
									} else {
										let td = createElement('td', {
											html: el[key],
										});
										tr.append(td);
									}
								}
								tr.addEventListener('click', () => {
									let modEditor;

									const bottomDiv = document.querySelector(
										'.rightBox .tableBottomDiv'
									);
									if (bottomDiv.querySelectorAll('button')) {
										bottomDiv
											.querySelectorAll('button')
											.forEach((el2) => el2.remove());
									}
									let delManualBtn = createElement('button', {
										class: 'btn_style_2 btn_color_crush',
										id: 'delManual',
										html: ' 삭제하기',
									});
									let i = createElement('i', {
										class: 'fa-solid fa-trash-can',
									});
									delManualBtn.prepend(i);
									let modManualBtn = createElement('button', {
										class: 'btn_style_2 btn_color_point',
										id: 'modManual',
										html: ' 저장하기',
									});
									let i2 = createElement('i', {
										class: 'fa-solid fa-floppy-disk',
									});
									modManualBtn.prepend(i2);
									bottomDiv.append(delManualBtn, modManualBtn);

									delManualBtn.addEventListener('click', () => {
										let obj = {};
										obj.title = '도움말 삭제';
										obj.btn = true;
										obj.btnText = '삭제하기';
										obj.btnColor = 'crush';
										obj.text = '선택한 도움말을 삭제하시겠습니까?';
										obj.callback = delManual;
										alertShow(obj);
									});
									function delManual() {
										let bodyData = JSON.stringify({
											manual_seq_no:
												document.querySelector('#manual_seq_no').value,
											menu_seq_no:
												document.querySelector('#menu_seq_no').value,
											reg_id: g_user_id,
											act_evt: 'del',
										});
										fetch('/api/v1/manual/manualIUD', {
											method: 'POST',
											body: bodyData,
										})
											.then((res) => res.json())
											.then((data) => {
												if (data.result_code == 'P000') {
													cancelAlert();
													window.location.reload();
												} else {
													let obj = {};
													obj.title = '오류';
													obj.btn = false;
													obj.text = '도움말 삭제 실패';
													obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
													alertShow(obj);
												}
											})
											.catch((error) => console.error(error));
									}
									modManualBtn.addEventListener('click', () => {
										let obj = {};
										obj.title = '도움말 수정';
										obj.btn = true;
										obj.btnText = '수정하기';
										obj.btnColor = 'point';
										obj.text = '수정내용을 저장하시겠습니까?';
										obj.callback = modManual;
										alertShow(obj);
									});
									function modManual() {
										let bodyData = JSON.stringify({
											manual_seq_no:
												document.querySelector('#manual_seq_no').value,
											manual_name:
												document.querySelector('#manual_name').value,
											// manual_content: encodeURIComponent(
											// 	document.querySelector('#manual_content').value
											// ),
											manual_content: encodeURIComponent(modEditor.getData()),
											reg_id: g_user_id,
											act_evt: 'upd',
										});
										fetch('/api/v1/manual/manualIUD', {
											method: 'POST',
											body: bodyData,
										})
											.then((res) => res.json())
											.then((data) => {
												if (data.result_code == 'P000') {
													cancelAlert();
													window.location.reload();
												} else {
													let obj = {};
													obj.title = '오류';
													obj.btn = false;
													obj.text = '도움말 수정 실패';
													obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
													alertShow(obj);
												}
											})
											.catch((error) => console.error(error));
									}
									const rightBox = document.querySelector('.rightBox');
									if (rightBox.querySelectorAll('table tbody tr')) {
										rightBox
											.querySelectorAll('table tbody tr')
											.forEach((el) => el.remove());
									}
									table
										.querySelectorAll('tbody tr')
										.forEach((el) => el.classList.remove('point'));
									tr.classList.add('point');

									let key_kor = {
										manual_seq_no: '도움말 일련번호',
										menu_seq_no: '도움말 위치',
										manual_name: '도움말 이름',
										manual_content: '내용',
										reg_id: '등록자',
										reg_ts: '등록시간',
									};

									for (let key in key_kor) {
										let tr = createElement('tr');
										let th = createElement('th', {
											html: key_kor[key],
										});
										let td = createElement('td');
										tr.append(th, td);
										rightBox.querySelector('table tbody').append(tr);
										if (key == 'menu_seq_no') {
											let select = createElement('select', {
												id: key,
											});
											let menuData = results[1]['value']['menu'];
											menuData.forEach((el2) => {
												let option = createElement('option', {
													value: el2['menu_seq_no'],
													html: el2['menu_name'],
												});
												select.append(option);
												if (el[key] == el2['menu_seq_no']) {
													option.setAttribute('selected', true);
												}
											});
											td.append(select);
										} else if (key == 'manual_content') {
											let textarea = createElement('textarea', {
												id: key,
											});
											td.append(textarea);
											ClassicEditor.create(
												document.querySelector(`#${key}`),
												{
													toolbar: {
														items: [
															'bold',
															'italic',
															'|',
															'undo',
															'redo',
															'|',
															'link',
															'blockquote',
														],
													},
												}
											)
												.then((newEditor) => {
													modEditor = newEditor;
													newEditor.setData(decodeURIComponent(el[key]));
												})
												.catch((error) => {
													console.error(error);
												});
											// let input = createElement('input', {
											// 	id: key,
											// 	value: decodeURIComponent(el[key]),
											// });
											// input.addEventListener('click', () => {
											// 	let obj = {};
											// 	obj.title = '도움말 내용 편집';
											// 	obj.btn = true;
											// 	obj.btnText = '수정하기';
											// 	obj.btnColor = 'point';
											// 	obj.func = ckeditorPopup;
											// 	obj.param = [el[key], input];
											// 	obj.refresh = false;
											// 	modalShow(obj);
											// });
											// td.append(input);
										} else if (key == 'manual_name') {
											let input = createElement('input', {
												id: key,
												value: el[key] ?? '',
											});
											td.append(input);
										} else {
											let input = createElement('input', {
												id: key,
												value: el[key] ?? '',
												readonly: true,
											});
											td.append(input);
										}
									}
								});
							});
						}
					}
				} else {
					errorHandling(result);
				}
			} else {
				console.error(reason);
			}
		});
	} else {
		console.error('Failed to call Manual list');
	}
}

// 처음 페이지 로드
let initPage = searchParam('page') ? searchParam('page') : 1;
let initRowInView = document.querySelector('#rowInView').value;
manualListCall(initPage, initRowInView);

// function ckeditorPopup(param_data) {
// 	const inputData = decodeURIComponent(param_data[0]);
// 	const location_input = param_data[1];
// 	const popupBody = document.querySelector('.layerPopup .popupBody');
// 	if (popupBody.querySelector('.box')) {
// 		popupBody.querySelector('.box').remove();
// 	}
// 	let newDiv = createElement('div', {
// 		class: 'box',
// 	});
// 	let textarea = createElement('textarea', {
// 		id: 'editor',
// 	});
// 	newDiv.append(textarea);
// 	popupBody.append(newDiv);

// 	let myEditor;

// 	ClassicEditor.create(document.querySelector('#editor'), {
// 		toolbar: {
// 			items: ['bold', 'italic', '|', 'undo', 'redo', '|', 'link', 'blockquote'],
// 		},
// 	})
// 		.then((newEditor) => {
// 			myEditor = newEditor;
// 			myEditor.setData(inputData);

// 			let submit = document.querySelector('.modal > div > div.btnBox > button');
// 			if (submit.clickHandler) {
// 				submit.removeEventListener('click', submit.clickHandler);
// 			}
// 			submit.clickHandler = () => {
// 				let data = myEditor.getData();
// 				location_input.value = data;
// 			};
// 			submit.addEventListener('click', submit.clickHandler);
// 		})
// 		.catch((error) => {
// 			console.error(error);
// 		});
// }

// 몇개씩 볼지 select로 수정할때
document.querySelector('#rowInView').addEventListener('change', manualListCallAgain);

function manualListCallAgain() {
	let page_limit = document.querySelector('#rowInView').value;
	insertParam('page', 1);
	manualListCall(1, page_limit);
}

function addNewManual(menuData) {
	const addManualBtn = document.querySelector('#addNewManual');
	addManualBtn.addEventListener('click', () => {
		let myEditor;

		let obj = {};
		obj.title = '새 도움말 추가';
		obj.btn = true;
		obj.btnText = '추가하기';
		obj.btnColor = 'point';
		obj.func = addManualPopup;
		obj.callback = addManual;

		modalShow(obj);

		function addManualPopup() {
			const popupBody = document.querySelector('.modal .layerPopup .popupBody');

			if (popupBody.querySelector('.box')) {
				popupBody.querySelector('.box').remove();
			}
			let box = createElement('div', {
				class: 'box',
			});
			let table = createElement('table');
			let tbody = createElement('tbody');
			popupBody.appendChild(box);
			box.appendChild(table);
			table.appendChild(tbody);
			let keys = {
				menu_seq_no: '도움말 위치',
				manual_name: '도움말 이름',
				manual_content: '내용',
			};
			let frontId = 'addNew_';
			for (let key in keys) {
				let tr = createElement('tr');
				let th = createElement('th', {
					html: keys[key],
				});
				let td = createElement('td');
				tr.append(th, td);
				tbody.append(tr);
				if (key == 'menu_seq_no') {
					let select = createElement('select', {
						id: frontId + key,
					});
					menuData.forEach((el) => {
						let option = createElement('option', {
							value: el['menu_seq_no'],
							html: el['menu_name'],
						});
						select.append(option);
					});
					td.append(select);
				} else if (key == 'manual_content') {
					td.classList.add('ckeditorTd');
					let textarea = createElement('textarea', {
						id: frontId + key,
					});
					td.append(textarea);
					ClassicEditor.create(document.querySelector(`#${frontId}${key}`), {
						toolbar: {
							items: [
								'bold',
								'italic',
								'|',
								'undo',
								'redo',
								'|',
								'link',
								'blockquote',
							],
						},
					}).then((newEditor) => {
						myEditor = newEditor;
					});
				} else {
					let input = createElement('input', {
						type: 'text',
						id: frontId + key,
					});
					td.append(input);
				}
			}
		}

		function addManual() {
			const popupBody = document.querySelector('.modal .popupBody');
			let frontId = 'addNew_';
			let bodyData = JSON.stringify({
				menu_seq_no: popupBody.querySelector(`#${frontId}menu_seq_no`).value,
				manual_name: popupBody.querySelector(`#${frontId}manual_name`).value,
				manual_content: encodeURIComponent(myEditor.getData()),
				reg_id: g_user_id,
				act_evt: 'ins',
			});
			fetch('/api/v1/manual/manualIUD', {
				method: 'POST',
				body: bodyData,
			})
				.then((res) => res.json())
				.then((data) => {
					if (data['result_code'] == 'P000') {
						window.location.reload();
					} else {
						console.error(data);
					}
				});
		}
	});
}
