import { errorHandling } from './import/ErrorHandler';
import { alertShow } from './import/ModalShow';
import { loading, loading_end } from './import/Loading';

const hiworksBox = document.querySelector('section article .social .hiworks');

function userInfoCall() {
	loading();
	fetch('/api/v1/user/userAccountList')
		.then((res) => res.json())
		.then((data) => {
			loading_end();
			if (data.result_code == 'P000') {
				let userData;
				data['list'].forEach((el) => {
					if (el['user_account_seq_no'] == g_user_account_seq_no) {
						userData = el;
					}
				});
				console.log(userData);
				const infoBoxInput = document.querySelector('#basicInfo .pw.box input');
				infoBoxInput.value = userData['user_account_pw'];

				ssoBoxMaker(userData['hiworks_id']);

				hiworksBox.addEventListener('click', () => {
					if (hiworksBox.classList.contains('unactivated') == true) {
						location.replace('/session_destroy.php?redirect_url=/hiworks_login');
					} else {
						let obj = {};
						obj.title = '하이웍스 ID 연동 해제';
						obj.btn = true;
						obj.btnText = '연동 해제';
						obj.btnColor = 'crush';
						obj.text = '하이웍스 ID 연동을 해제하시겠습니까?';
						obj.text2 = '연동 해제 후 다시 로그인해야 합니다.';
						obj.callback = hiworksIdRemove;
						alertShow(obj);
					}
				});

				function hiworksIdRemove() {
					let bodyData = {};
					bodyData['user_seq_no'] = '';
					bodyData['reg_id'] = g_user_id;
					bodyData['act_evt'] = 'del';
					bodyData = JSON.stringify(bodyData);
					fetch('/api/v1/user/hiworksIUD', {
						method: 'POST',
						body: bodyData,
					})
						.then((res) => res.json())
						.then((data) => {
							if (data.result_code == 'P000') {
								location.href = '/login';
							}
						});
				}
			} else {
				errorHandling(data);
			}
		});
}
userInfoCall();

function ssoBoxMaker(hiworksId) {
	let hiworksInfoConnect = document.querySelector(
		'section article .social .hiworks .loginInfo p.connect'
	);
	let hiworksInfoConnectDate = document.querySelector(
		'section article .social .hiworks .loginInfo p.connectDate'
	);

	if (hiworksId == null || hiworksId == '') {
		hiworksBox.classList.add('unactivated');
		hiworksInfoConnect.innerHTML = '연동 안됨';
		hiworksInfoConnectDate.innerHTML = '연동하려면 클릭하세요';
	} else {
		hiworksInfoConnect.innerHTML = '연동 됨';
		hiworksInfoConnectDate.innerHTML = 'ID : ' + hiworksId;
	}
}

let hiworksHoverBox = document.querySelector('section article .social .hiworks .hoverBox');

hiworksBox.addEventListener('mouseover', () => {
	if (hiworksBox.classList.contains('unactivated') == false) {
		hiworksHoverBox.style.opacity = '1';
	}
});
hiworksBox.addEventListener('mouseout', () => {
	if (hiworksBox.classList.contains('unactivated') == false) {
		hiworksHoverBox.style.opacity = '0';
	}
});

const pwShowBtn = document.querySelector('section article .pw.box span.showPw');
const pwInput = document.querySelector('section article .pw.box input');

pwShowBtn.addEventListener('click', () => {
	if (pwShowBtn.classList.contains('hide') !== true) {
		pwInput.setAttribute('type', 'text');
		pwShowBtn.classList.add('hide');
		pwShowBtn.innerHTML = '<i class="fa-solid fa-eye-slash"></i>';
	} else {
		pwInput.setAttribute('type', 'password');
		pwShowBtn.classList.remove('hide');
		pwShowBtn.innerHTML = '<i class="fa-solid fa-eye"></i>';
	}
});
