import { searchParam } from '../import/SearchParam.js';
import { insertParam } from '../import/InsertParam.js';
import { loading, loading_end } from '../import/Loading.js';
import {
	leftListClose,
	leftListOpen,
	menuChange,
	rightListChange,
	subMenuChange,
} from '../import/LayoutControl.js';
import { errorHandling } from '../import/ErrorHandler.js';
import { getCookie, setCookie } from '../import/Cookie.js';
import { createElement } from '../import/CreateElement.js';
import { alertShow, cancelAlert, modalShow } from '../import/ModalShow.js';
import { searchForm } from '../import/Searchform.js';

//프로토타입 설정----------------------------------------------------------
String.prototype.toHHMMSS = function () {
	var sec_num = parseInt(this, 10); // don't forget the second param
	var hours = Math.floor(sec_num / 3600);
	var minutes = Math.floor((sec_num - hours * 3600) / 60);
	var seconds = sec_num - hours * 3600 - minutes * 60;

	if (hours < 10) {
		hours = '0' + hours;
	}
	if (minutes < 10) {
		minutes = '0' + minutes;
	}
	if (seconds < 10) {
		seconds = '0' + seconds;
	}
	return hours + ':' + minutes + ':' + seconds;
};

String.prototype.caplitalizeFirstLetter = function () {
	var string = this;
	return string.charAt(0).toUpperCase() + string.slice(1);
};

menuChange();
subMenuChange();
leftListClose();
rightListChange();
checkMenuClosed();

function checkMenuClosed() {
	if (getCookie('menu') == 'close') {
		document.querySelector('#wrap').classList.add('menuClose');
	}
}

function resizer() {
	const rightList_ul = document.querySelector('aside .rightList ul');
	const rightList_search = document.querySelector('aside .rightList .searchOption');
	const rightList_h3 = document.querySelector('aside .rightList h3');

	setTimeout(() => {
		rightList_ul.style.height =
			document.querySelector('.right .container').offsetHeight -
			(rightList_search.offsetHeight + rightList_h3.offsetHeight) +
			'px';
	}, 100);
}
resizer();
window.addEventListener('resize', resizer);

//전체화면 클릭
function fullScreen() {
	const wrap = document.querySelector('#wrap');
	const fullscreenBtn = document.querySelector('a.fullsize');
	fullscreenBtn.addEventListener('click', () => {
		if (wrap.classList.contains('fullScreen') !== true) {
			wrap.classList.add('fullScreen');
			fullscreenBtn.innerHTML =
				'<i class="fa-solid fa-down-left-and-up-right-to-center"></i><span>Normal Screen</span>';
		} else {
			wrap.classList.remove('fullScreen');
			fullscreenBtn.innerHTML =
				'<i class="fa-solid fa-up-right-and-down-left-from-center"></i><span>Full Screen</span>';
		}
	});
}
fullScreen();

function popupClick() {
	const popupBtn = document.querySelector('main .toolbar a.popup');
	popupBtn.addEventListener('click', () => {
		let url;
		let path = location.pathname;
		let queryString = location.search ?? location.search;
		let anchor = location.hash ?? location.hash;
		if (window.location.href.includes('?')) {
			url = path + queryString + '&popup=yes' + anchor;
		} else {
			url = path + queryString + '?popup=yes' + anchor;
		}
		window.open(url, '_blank', 'width=1620px, height=805px, top=100, left=50');
	});
}
popupClick();

function isThisPopup() {
	if (searchParam('popup') == 'yes') {
		const wrap = document.querySelector('#wrap');
		wrap.classList.add('fullScreen');
		wrap.classList.add('popup');
	}
}
isThisPopup();

function manualClick(menuData) {
	const manualBtn = document.querySelector('main .toolbar span.manual');
	manualBtn.addEventListener('click', () => {
		let menu_seq_no;
		menuData.forEach((el) => {
			let pathname = location.pathname;
			if (searchParam('project') == 'aos') {
				pathname = `${pathname}?project=aos`;
			}
			if (pathname == el['menu_link_url']) {
				menu_seq_no = el['menu_seq_no'];
			}
		});
		let url = `/system/manualPopup?popup=yes&menu_seq_no=${menu_seq_no}`;
		window.open(url, '_blank', 'width=620px, height=820px, top=100, left=50');
	});
}

function logout() {
	document.querySelector('span.logOut').addEventListener('click', () => {
		let obj = {
			title: '로그아웃',
			btn: true,
			btnText: '로그아웃',
			btnColor: 'crush',
			text: '로그아웃 하시겠습니까?',
			callback: function () {
				location.replace('/logout');
			},
		};

		alertShow(obj);
	});
}
logout();

async function menuMaker() {
	let result = await fetch('/api/v1/menu/menuList').then((res) => res.json());
	if (result.result_code == 'P000') {
		let menuGroupData = result.menu_group;
		let menuData = result.menu;

		manualClick(menuData);

		const nav = document.querySelector('menu .nav ul');
		const subMenu = document.querySelector('nav.subMenu');

		//메뉴 생성
		for (let menuGroup of menuGroupData) {
			if (menuGroup['menu_group_view_yn'] == 'Y') {
				let newUl = createElement('ul', {
					data: {
						'menu-group-seq-no': menuGroup['menu_group_seq_no'],
					},
				});
				subMenu.append(newUl);

				let newLi = createElement('li', {
					data: {
						'menu-group-seq-no': menuGroup['menu_group_seq_no'],
					},
				});
				let newI = createElement('i', {
					class: menuGroup['menu_group_icon'],
				});
				let newA = createElement('a', {
					html: menuGroup['menu_group_name'],
				});
				nav.append(newLi);
				newLi.append(newI, newA);
			}
		}

		//서브 메뉴 생성
		for (let menu of menuData) {
			if (menu['menu_view_yn'] == 'Y') {
				let newLi = createElement('li');
				let newA = createElement('a', {
					href: menu['menu_link_url'],
					html: menu['menu_name'],
				});
				newLi.append(newA);
				subMenu.querySelectorAll('ul').forEach((el) => {
					if (el.dataset.menuGroupSeqNo == menu['menu_group_seq_no']) {
						el.append(newLi);
					}
				});
				if (menu['menu_use_yn'] == 'N') {
					newLi.classList.add('locked');
				}

				//현재 URL에 메뉴가 있으면 액티브
				let pathname = location.pathname;
				if (searchParam('project') == 'aos') {
					pathname = pathname + '?project=aos';
				}
				if (menu['menu_link_url'] == pathname) {
					newLi.classList.add('active');
					newLi.parentNode.classList.add('active');
					nav.querySelectorAll('li').forEach((el) => {
						if (el.dataset.menuGroupSeqNo == menu['menu_group_seq_no']) {
							el.classList.add('active');
						}
					});
					wrap.classList.remove('subMenuClose');
					document.querySelector('p.currMenuGroup').innerHTML = (() => {
						let menuGroupName;
						menuGroupData.forEach((el) => {
							if (menu['menu_group_seq_no'] == el['menu_group_seq_no']) {
								menuGroupName = el['menu_group_name'];
							} else {
								if (menu['menu_group_seq_no'] == null) {
									menuGroupName = '기타 페이지';
								}
							}
						});
						return menuGroupName;
					})();
					document.querySelector('p.currMenu').innerHTML = menu['menu_name'];
				} else if (location.pathname == '/pages/dashboard') {
					nav.querySelectorAll('li')[0].classList.add('active');
				}
			}
		}

		//메뉴 클릭 시
		function menuClick() {
			const leftMenu = document.querySelectorAll('.left menu .nav>ul>li');
			const subMenu = document.querySelectorAll('nav.subMenu>ul');
			leftMenu.forEach((menu) => {
				menu.addEventListener('click', () => {
					leftMenu.forEach((menu2) => {
						menu2.classList.remove('active');
					});
					menu.classList.add('active');
					subMenu.forEach((ul) => {
						if (menu.dataset.menuGroupSeqNo == ul.dataset.menuGroupSeqNo) {
							subMenu.forEach((ul2) => {
								ul2.classList.remove('active');
							});
							ul.classList.add('active');
							const wrap = document.querySelector('#wrap');
							wrap.classList.remove('subMenuClose');
						}
					});
				});
			});
		}
		menuClick();
	} else {
		errorHandling(result['result_code']);
	}
}
menuMaker();

// 토큰 만료 확인------------------------------------------------------------------------------------------
function tokenChecker() {
	const logOutText = document.querySelector(
		'.right header .header_mid .userBox .middleBox span.timeOut'
	);
	const logOutTimeOut = document.querySelector(
		'.right header .header_mid .userBox .middleBox span.timeOut strong'
	);
	const tokenRefresh = document.querySelector(
		'.right header .header_mid .userBox .middleBox #refreshToken'
	);

	// let expireDate = new Date('2022-10-28 13:3:50');
	let expireDate = new Date(g_access_token_expire);
	let now = new Date();
	let leftTime = Math.ceil((expireDate - now) / 1000);
	leftTime = String(leftTime).toHHMMSS();
	logOutTimeOut.innerHTML = leftTime;

	function customSetCookie(name, value, minutes) {
		let exdate = new Date();
		exdate.setMinutes(exdate.getMinutes() + minutes); // 만료될 시간 설정
		let expireDate = 'expires=' + exdate.toUTCString();
		document.cookie = name + '=' + value + ';' + expireDate + ';path=/'; //쿠키 만든다!
	}

	let tokenInterval = setInterval(() => {
		let now = new Date();
		let leftTime = Math.ceil((expireDate - now) / 1000);

		if (leftTime < 300 && leftTime > 0) {
			let timeOutFlag = getCookie('timeOutFlag');
			logOutText.classList.add('crush');
			if (!timeOutFlag) {
				let obj = {};
				obj.title = '로그인 연장 확인';
				obj.btn = true;
				obj.btnText = '연장하기';
				obj.btnColor = 'point';
				obj.text = `로그아웃까지 5분 남았습니다. 로그인 시간을 연장하시겠습니까?`;
				obj.text2 = '연장하기를 누르실 경우 1시간으로 연장됩니다.';
				obj.callback = refreshToken;
				alertShow(obj);
				customSetCookie('timeOutFlag', true, 5);
			}
		} else if (leftTime <= 0) {
			clearInterval(tokenInterval);
			let obj = {};
			obj.title = '로그인 만료';
			obj.btn = true;
			obj.btnText = '로그인 페이지로 이동';
			obj.btnColor = 'point';
			obj.text = '세션이 만료되어 다시 로그인해야 합니다.';
			obj.callback = goLogin;
			alertShow(obj);
			document
				.querySelector('.alert_modal .alert_layerPopup .alert_popupHead i.fa-xmark')
				.addEventListener('click', goLogin);

			function goLogin() {
				location.replace('/login');
			}
		}
		leftTime = String(leftTime).toHHMMSS();
		logOutTimeOut.innerHTML = leftTime;
	}, 1000);

	tokenRefresh.addEventListener('click', refreshToken);

	function refreshToken() {
		let bodyData = {};
		bodyData.user_account_name = g_user_id;
		bodyData.act_evt = 'req';
		bodyData = JSON.stringify(bodyData);
		fetch('/api/v1/token/tokenIUD', {
			method: 'POST',
			body: bodyData,
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data);
				if (data.result_code == 'P000') {
					expireDate = new Date(data.access_token_expire);
					cancelAlert();
					logOutText.classList.remove('crush');
				} else {
					errorHandling(data);
				}
			});
	}
}
tokenChecker();

function checkListBar() {
	const checkList = document.querySelector('header .header_mid .checkList');
	const listBarWrap = checkList.querySelector('ul.listBarWrap');
	const pageBtnLi = document.querySelectorAll('header .header_mid ul.checkListPageBtn li');
	const goBtns = document.querySelectorAll('header .header_mid span.goBtn');

	let i = 0;

	pageBtnLi.forEach((el, idx, arg) => {
		el.addEventListener('click', () => {
			arg.forEach((el2) => el2.classList.remove('on'));
			el.classList.add('on');
			listBarWrap.style.left = -100 * idx + '%';
			i = idx;
		});
	});

	goBtns.forEach((el, idx) => {
		el.addEventListener('click', () => {
			if (idx == 0) {
				i--;
				if (i < 0) i = pageBtnLi.length - 1;
				pageBtnLi.forEach((el2) => el2.classList.remove('on'));
				pageBtnLi[i].classList.add('on');
				listBarWrap.style.left = -100 * i + '%';
			} else {
				i++;
				if (i > pageBtnLi.length - 1) i = 0;
				pageBtnLi.forEach((el2) => el2.classList.remove('on'));
				pageBtnLi[i].classList.add('on');
				listBarWrap.style.left = -100 * i + '%';
			}
		});
	});
}
checkListBar();

// ------------------------------------------------------------------------------------------------

async function call_member_info() {
	const rightSearchOption = document.querySelector('aside .rightList .searchOption');
	searchForm(rightSearchOption, {
		options: [
			// {
			// 	title: '회원유형',
			// 	select: {
			// 		id: 'custType',
			// 		option: [
			// 			{ value: '00', html: '------' },
			// 			{ value: '01', html: '정상' },
			// 			{ value: '02', html: '탈퇴' },
			// 			{ value: '03', html: '블랙리스트' },
			// 			{ value: '04', html: '테스트' },
			// 		],
			// 	},
			// },
			{
				title: '회원유형',
				checkBox: {
					custType_01: '정상',
					custType_02: '탈퇴',
					custType_03: '블랙리스트',
					custType_04: '테스트',
				},
			},
			{
				title: '검색조건',
				select: {
					id: 'custSearchOption',
					option: [
						{ value: null, html: '------' },
						{ value: '상호명', html: '상호명' },
						{ value: '전화번호', html: '전화번호' },
						{ value: '대표자명', html: '대표자명' },
						{ value: '사업자번호', html: '사업자번호' },
					],
				},
			},
		],
		textInput: true,
		searchBtn: true,
	});

	const leftSearchOption = document.querySelector('aside .leftList .searchOption');
	searchForm(leftSearchOption, {
		options: [
			{
				title: '정렬',
				select: {
					id: 'loanSearchOption',
					option: [
						{ value: null, html: '------' },
						{ value: '일자별 오름차순', html: '일자별 오름차순' },
						{ value: '일자별 내림차순', html: '일자별 내림차순' },
						{ value: '차수별 오름차순', html: '차수별 오름차순' },
						{ value: '차수별 내림차순', html: '차수별 내림차순' },
					],
				},
			},
			{
				title: '진행상태',
				select: {
					id: 'loanProcess',
					option: [
						{ value: 'loanProcess_00', html: '접수' },
						{ value: 'loanProcess_01', html: '1차심사중' },
						{ value: 'loanProcess_02', html: '1차승인' },
						{ value: 'loanProcess_03', html: '진행대기' },
						{ value: 'loanProcess_04', html: '2차심사중' },
						{ value: 'loanProcess_05', html: '재심사요청' },
						{ value: 'loanProcess_06', html: '2차승인' },
						{ value: 'loanProcess_07', html: '대출요청' },
						{ value: 'loanProcess_08', html: '자서대기' },
						{ value: 'loanProcess_09', html: '입금대기' },
						{ value: 'loanProcess_10', html: '대출중' },
						{ value: 'loanProcess_11', html: '거래완료' },
						{ value: 'loanProcess_12', html: '대출보류' },
						{ value: 'loanProcess_13', html: '대출부결' },
						{ value: 'loanProcess_14', html: '회수정지' },
						{ value: 'loanProcess_15', html: '회수오류' },
					],
				},
			},
		],
		textInput: false,
		searchBtn: true,
	});

	let result = await fetch('/js/dummy/dummy_member_data.json').then((res) => res.json());
	if (result['result_code'] == 'P000') {
		let memberData = result['data'];
		const rightList_ul = document.querySelector('aside .rightList ul');
		memberData.forEach((el) => {
			let newLi = createElement('li', {
				data: {
					'seq-no': el['seq_no'],
				},
			});
			let newSpan = createElement('span', {
				class: `type ${el['type']}`,
				html: el['type'],
			});
			let newP = createElement('p', {
				class: 'company_name',
				html: el['name'],
			});
			let newSpan2 = createElement('span', {
				class: 'clickToOpen',
				html: '<i class="fa-solid fa-caret-down"></i> 펼치기',
			});
			newLi.append(newSpan, newP, newSpan2);
			rightList_ul.append(newLi);
			if (el['test'] == 'Y') {
				newLi.classList.add('test');
			}
			newLi.addEventListener('click', () => {
				if (event.target !== newLi.querySelector('.apply_test')) {
					const memberList_li = document.querySelectorAll('aside .rightList ul li');
					memberList_li.forEach((el2) => el2.classList.remove('clicked'));
					newLi.classList.add('clicked');
					memberList_li_moreInfo(newLi, el.detail);
					call_member_loan_info();
				}
			});
		});

		function memberList_li_moreInfo(target_li, data) {
			const memberList_li = document.querySelectorAll('aside .rightList ul li');
			memberList_li.forEach((el) => {
				if (el.querySelector('.card_detail')) {
					el.querySelector('.card_detail').remove();
				}
			});
			let newDiv = createElement('div', {
				class: 'card_detail',
			});
			target_li.append(newDiv);

			if (target_li.classList.contains('test')) {
				let newBtn = createElement('button', {
					html: '대출신청',
					class: 'apply_test btn_style_2 btn_color_point',
				});
				newDiv.append(newBtn);
			}

			for (let key in data) {
				let newP = createElement('p', {
					class: 'card_info',
					html: `${key}: ${data[key]}`,
				});
				newDiv.append(newP);
			}
		}

		async function call_member_loan_info() {
			document
				.querySelectorAll('aside .leftList .listTableBox table tbody tr')
				.forEach((el) => {
					el.remove();
				});
			let result = await fetch('/js/dummy/dummy_loan_data.json').then((res) => res.json());
			if (result['result_code'] == 'P000') {
				let loanList = result['대출 리스트'];
				loanList.forEach((el) => {
					let newTr = createElement('tr', {
						data: {
							loan_seq_no: el['loan_seq_no'],
						},
					});
					document
						.querySelector('aside .leftList .listTableBox table tbody')
						.append(newTr);

					newTr.addEventListener('click', () => {
						let memClicked = document.querySelector('aside .rightList ul li.clicked')
							.dataset.seqNo;
						let param = `?member_seq_no=${memClicked}&loan_seq_no=${el['loan_seq_no']}`;
						let url = `/pages/mainInfo${param}`;
						location.href = url;
					});

					let gather_key = ['reg_dt', 'loan_amount', 'manager_name'];
					gather_key.forEach((key) => {
						if (el.hasOwnProperty(key)) {
							let newTd = createElement('td');
							if (key == 'reg_dt') {
								newTd.innerHTML = el[key].split(' ')[0];
							} else if (key == 'loan_amount') {
								newTd.innerHTML = el[key].toLocaleString('en-US');
							} else {
								newTd.innerHTML = el[key];
							}
							newTr.append(newTd);
						}
					});
				});

				const listBtns = document.querySelectorAll(
					'.loan_search_table .list_select th button'
				);
				listBtns.forEach((el, idx) => {
					el.addEventListener('click', () => {
						document
							.querySelectorAll('.loan_search_table .list_select th')
							.forEach((el2) => el2.classList.remove('list_selected'));

						el.parentNode.classList.add('list_selected');

						if (idx == 0) {
						} else {
						}
					});
				});

				leftListOpen();
			} else {
				errorHandling(result['result_code']);
			}
		}
	} else {
		errorHandling(result['result_code']);
	}
}
call_member_info();
