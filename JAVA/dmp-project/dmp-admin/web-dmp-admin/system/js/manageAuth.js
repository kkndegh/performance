import { createElement } from './import/CreateElement';
import { loading, loading_end } from './import/Loading';
import { alertShow, cancelAlert, cancelModal, modalShow } from './import/ModalShow';
import { searchParam } from './import/SearchParam';
import { checkBoxTextClick } from './import/CheckBoxTextClick';

let urlPath;
let urlApiList;
let urlAuthGroupList;
let urlAuthGroupIUD;
let urlApiAuthGroupList;
let urlMenuAuthGroupList;
let urlApiAuthGroupIUD;
let urlMenuAuthGroupIUD;
if (searchParam('project') == 'aos') {
	urlPath = `${location.pathname}?project=aos&`;
	urlApiList = '/api/v1/api/aosApiList';
	urlAuthGroupList = '/api/v1/auth/aosAuthGroupList';
	urlAuthGroupIUD = '/api/v1/auth/aosAuthGroupIUD';
	urlApiAuthGroupList = '/api/v1/api/aosApiAuthGroupList';
	urlApiAuthGroupIUD = '/api/v1/api/aosApiAuthGroupIUD';
	urlMenuAuthGroupList = '/api/v1/auth/menuAuthGroupList';
	urlMenuAuthGroupIUD = '/api/v1/menu/menuGroupIUD';
} else {
	urlPath = `${location.pathname}?`;
	urlApiList = '/api/v1/api/apiList';
	urlAuthGroupList = '/api/v1/auth/authGroupList';
	urlAuthGroupIUD = '/api/v1/auth/authGroupIUD';
	urlApiAuthGroupList = '/api/v1/auth/apiAuthGroupList';
	urlApiAuthGroupIUD = '/api/v1/auth/apiAuthGroupIUD';
	urlMenuAuthGroupList = '/api/v1/auth/menuAuthGroupList';
	urlMenuAuthGroupIUD = '/api/v1/menu/menuGroupIUD';
}

async function callAllApis() {
	loading();
	let authGroupList = await fetch(urlAuthGroupList).then((res) => res.json());
	let apiList = await fetch(urlApiList).then((res) => res.json());
	let menuList = await fetch('/api/v1/menu/menuList').then((res) => res.json());
	let apiAuthGroupList = await fetch(urlApiAuthGroupList).then((res) => res.json());
	let menuAuthGroupList = await fetch(urlMenuAuthGroupList).then((res) => res.json());
	let fetchArr = [authGroupList, apiList, menuList, apiAuthGroupList, menuAuthGroupList];

	let results = await Promise.allSettled(fetchArr);
	if (results) {
		loading_end();
		const apiAuthGroupListData = results[3]['value'];
		const menuAuthGroupListData = results[4]['value'];
		const apiListData = results[1]['value'];
		const menuListData = results[2]['value'];
		addNewAuthGroupPopup(apiAuthGroupListData, menuAuthGroupListData);
		addNewApiAuthPopup(apiListData);
		addNewMenuAuthPopup(menuListData);
		results.forEach((el, idx) => {
			if (el.status == 'fulfilled') {
				let result = el.value;
				if (result.result_code == 'P000') {
					if (idx == 0) {
						// Auth Group List
						let data = result['authGroupList'];
						// console.log(result);
						document.querySelector(
							'#authGroup > div > div.leftBox > div > div.authGroupListTopDiv.listTopDiv > p > strong'
						).innerHTML = data.length;
						data.forEach((authGroup) => {
							let authGroupListDiv = document.querySelector(
								'#authGroup .authGroupListDiv'
							);
							let div = createElement('div', {
								class: 'authGroupDiv depth1',
								data: {
									auth_group_seq_no: authGroup['auth_group_seq_no'],
								},
							});
							let div2 = createElement('div', {
								class: 'authGroupTitle depth1Title',
							});
							let i = createElement('i', {
								class: 'fa-solid fa-list',
							});
							let p = createElement('p', {
								html: authGroup['auth_group_name'],
							});
							div2.append(i, p);
							let ul = createElement('ul', {
								class: 'depth2',
							});
							div.append(div2, ul);
							authGroupListDiv.append(div);

							div.addEventListener('click', () => {
								const bottomDiv = document.querySelector(
									'#authGroup .rightBox .bottomDiv'
								);
								if (bottomDiv.querySelectorAll('button')) {
									bottomDiv
										.querySelectorAll('button')
										.forEach((el2) => el2.remove());
								}
								let delAuthGroupBtn = createElement('button', {
									class: 'btn_style_2 btn_color_crush',
									id: 'delAuthGroup',
									html: ' 삭제하기',
								});
								let i = createElement('i', {
									class: 'fa-solid fa-trash-can',
								});
								delAuthGroupBtn.prepend(i);
								let modAuthGroupBtn = createElement('button', {
									class: 'btn_style_2 btn_color_point',
									id: 'modAuthGroup',
									html: ' 저장하기',
								});
								let i2 = createElement('i', {
									class: 'fa-solid fa-floppy-disk',
								});
								modAuthGroupBtn.prepend(i2);
								bottomDiv.append(delAuthGroupBtn, modAuthGroupBtn);

								delAuthGroupBtn.addEventListener('click', () => {
									let obj = {};
									obj.title = '권한 그룹 삭제';
									obj.btn = true;
									obj.btnText = '삭제하기';
									obj.btnColor = 'crush';
									obj.text = '선택한 권한 그룹을 삭제하시겠습니까?';
									obj.callback = delAuthGroup;
									alertShow(obj);
								});
								function delAuthGroup() {
									let bodyData = JSON.stringify({
										auth_group_seq_no: document.querySelector(
											'#authGroup #auth_group_seq_no'
										).value,
										reg_id: g_user_id,
										act_evt: 'del',
									});
									fetch(urlAuthGroupIUD, {
										method: 'POST',
										body: bodyData,
									})
										.then((res) => res.json())
										.then((data) => {
											if (data.result_code == 'P000') {
												cancelAlert();
												window.location.reload();
											} else {
												let obj = {};
												obj.title = '오류';
												obj.btn = false;
												obj.text = '권한 그룹 삭제 실패';
												obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
												alertShow(obj);
											}
										})
										.catch((error) => console.error(error));
								}
								modAuthGroupBtn.addEventListener('click', () => {
									let obj = {};
									obj.title = '권한 그룹 수정';
									obj.btn = true;
									obj.btnText = '수정하기';
									obj.btnColor = 'point';
									obj.text = '수정내용을 저장하시겠습니까?';
									obj.callback = modAuthGroup;
									alertShow(obj);
								});
								function modAuthGroup() {
									let bodyData = JSON.stringify({
										auth_group_seq_no: document.querySelector(
											'#authGroup #auth_group_seq_no'
										).value,
										menu_auth_group_seq_no:
											document.querySelector('#authGroup #menuAuth').value,
										api_auth_group_seq_no:
											document.querySelector('#authGroup #APIAuth').value,
										auth_group_name: document.querySelector(
											'#authGroup #auth_group_name'
										).value,
										auth_group_content: document.querySelector(
											'#authGroup #auth_group_content'
										).value,
										auth_group_class: document.querySelector(
											'#authGroup #auth_group_class'
										).value,
										reg_id: g_user_id,
										act_evt: 'upd',
									});
									fetch(urlAuthGroupIUD, {
										method: 'POST',
										body: bodyData,
									})
										.then((res) => res.json())
										.then((data) => {
											if (data.result_code == 'P000') {
												cancelAlert();
												window.location.reload();
											} else {
												let obj = {};
												obj.title = '오류';
												obj.btn = false;
												obj.text = '권한 그룹 수정 실패';
												obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
												alertShow(obj);
											}
										})
										.catch((error) => console.error(error));
								}
								document
									.querySelectorAll(
										'#authGroup .leftBox .authGroupListDiv .authGroupTitle'
									)
									.forEach((el2) => {
										el2.classList.remove('point');
									});
								div2.classList.add('point');
								let key_kor = {
									auth_group_name: '권한 그룹 이름',
									auth_group_seq_no: '권한 그룹 일련번호',
									auth_group_content: '내용',
									auth_group_class: '권한 그룹 클래스',
									APIAuth: 'API 권한',
									menuAuth: '페이지 접근 권한',
									reg_id: '등록자',
									reg_ts: '등록시간',
								};
								let authGroupInfoTableTbody = document.querySelector(
									'#authGroup .rightBox table tbody'
								);
								if (authGroupInfoTableTbody.querySelectorAll('tr')) {
									authGroupInfoTableTbody
										.querySelectorAll('tr')
										.forEach((el2) => el2.remove());
								}
								for (let key in authGroup) {
									let tr = createElement('tr');
									let th = createElement('th', {
										html: key_kor[key],
									});
									let td = createElement('td');
									tr.append(th, td);
									if (
										key == 'auth_group_seq_no' ||
										key == 'reg_id' ||
										key == 'reg_ts'
									) {
										let input = createElement('input', {
											id: key,
											value: authGroup[key] ?? '',
											readonly: true,
										});
										td.append(input);
									} else if (key == 'APIAuth') {
										let select = createElement('select', {
											id: key,
										});
										let apiAuthGroup = apiAuthGroupListData['apiAuthGroupList'];
										for (let apiAuth of apiAuthGroup) {
											let option = createElement('option', {
												value: apiAuth['api_auth_group_seq_no'],
												html: apiAuth['api_auth_group_name'],
											});
											select.append(option);
											if (
												authGroup[key]['api_auth_group_seq_no'] ==
												apiAuth['api_auth_group_seq_no']
											) {
												option.setAttribute('selected', true);
											}
										}
										td.append(select);
									} else if (key == 'menuAuth') {
										let select = createElement('select', {
											id: key,
										});
										let menuAuthGroup =
											menuAuthGroupListData['menuAuthGroupList'];
										for (let menuAuth of menuAuthGroup) {
											let option = createElement('option', {
												value: menuAuth['menu_auth_group_seq_no'],
												html: menuAuth['menu_auth_group_name'],
											});
											select.append(option);
											if (
												authGroup[key]['menu_auth_group_seq_no'] ==
												menuAuth['menu_auth_group_seq_no']
											) {
												option.setAttribute('selected', true);
											}
										}
										td.append(select);
									} else {
										let input = createElement('input', {
											id: key,
											value: authGroup[key] ?? '',
										});
										td.append(input);
									}
									authGroupInfoTableTbody.append(tr);
								}
							});
						});
					} else if (idx == 1) {
						// API List
						let data = result['list'];
						data.forEach((el2) => {
							// API 권한 리스트
							let apiGroup = el2['api_group'];
							if (
								!document.querySelector(
									`article#apiAuth .flexBox .rightBox .infoDiv table tbody tr[class="${apiGroup}"]`
								)
							) {
								let tr = createElement('tr', {
									class: apiGroup,
								});
								document
									.querySelector(
										'article#apiAuth .flexBox .rightBox .infoDiv table tbody'
									)
									.append(tr);

								let th = createElement('th');
								let checkBox = createElement('checkbox', {
									id: apiGroup,
									html: apiGroup,
								});
								th.append(checkBox);
								let td = createElement('td');
								tr.append(th, td);

								checkBox.querySelector('input').addEventListener('click', () => {
									if (checkBox.querySelector('input').checked) {
										checkBox.parentNode.parentNode
											.querySelectorAll('td input')
											.forEach((el3) => {
												if (!el3.checked) {
													el3.checked = true;
												}
											});
									} else {
										checkBox.parentNode.parentNode
											.querySelectorAll('td input')
											.forEach((el3) => {
												if (el3.checked) {
													el3.checked = false;
												}
											});
									}
								});
							}

							let location = document.querySelector(
								`article#apiAuth .flexBox .rightBox .infoDiv table tbody tr[class="${apiGroup}"] td`
							);

							let checkBox = createElement('checkbox', {
								id: el2['api_code'],
								html: el2['api_name'],
							});
							checkBox.querySelector('input').dataset.api_seq_no = el2['api_seq_no'];
							location.append(checkBox);

							checkBox.querySelector('input').addEventListener('click', () => {
								if (checkBox.querySelector('input').checked) {
									let flag = true;
									checkBox.parentNode.querySelectorAll('input').forEach((el3) => {
										if (!el3.checked) {
											flag = false;
										}
									});
									if (flag == true) {
										if (
											!checkBox.parentNode.parentNode.querySelector(
												'th input'
											).checked
										) {
											checkBox.parentNode.parentNode.querySelector(
												'th input'
											).checked = true;
										}
									}
								} else {
									if (
										checkBox.parentNode.parentNode.querySelector('th input')
											.checked
									) {
										checkBox.parentNode.parentNode.querySelector(
											'th input'
										).checked = false;
									}
								}
							});
						});

						data.forEach((el2) => {
							// 메뉴 별 허용 API 권한 (API 리스트)
							let apiGroup = el2['api_group'];
							if (
								!document.querySelector(
									`article#menuApiAuth .flexBox .rightBox .infoDiv table tbody tr[class="${apiGroup}"]`
								)
							) {
								let tr = createElement('tr', {
									class: apiGroup,
								});
								document
									.querySelector(
										'article#menuApiAuth .flexBox .rightBox .infoDiv table tbody'
									)
									.append(tr);

								let th = createElement('th');
								let checkBox = createElement('checkbox', {
									id: `menuApiAuth ${apiGroup}`,
									html: apiGroup,
								});

								th.append(checkBox);
								let td = createElement('td');
								tr.append(th, td);

								checkBox.querySelector('input').addEventListener('click', () => {
									if (checkBox.querySelector('input').checked) {
										checkBox.parentNode.parentNode
											.querySelectorAll('td input')
											.forEach((el3) => {
												if (!el3.checked) {
													el3.checked = true;
												}
											});
									} else {
										checkBox.parentNode.parentNode
											.querySelectorAll('td input')
											.forEach((el3) => {
												if (el3.checked) {
													el3.checked = false;
												}
											});
									}
								});
							}

							let location = document.querySelector(
								`article#menuApiAuth .flexBox .rightBox .infoDiv table tbody tr[class="${apiGroup}"] td`
							);

							let checkBox = createElement('checkbox', {
								id: `menuApiAuth ${el2['api_code']}`,
								html: el2['api_name'],
							});
							checkBox.querySelector('input').dataset.api_seq_no = el2['api_seq_no'];
							location.append(checkBox);

							checkBox.querySelector('input').addEventListener('click', () => {
								if (checkBox.querySelector('input').checked) {
									let flag = true;
									checkBox.parentNode.querySelectorAll('input').forEach((el3) => {
										if (!el3.checked) {
											flag = false;
										}
									});
									if (flag == true) {
										if (
											!checkBox.parentNode.parentNode.querySelector(
												'th input'
											).checked
										) {
											checkBox.parentNode.parentNode.querySelector(
												'th input'
											).checked = true;
										}
									}
								} else {
									if (
										checkBox.parentNode.parentNode.querySelector('th input')
											.checked
									) {
										checkBox.parentNode.parentNode.querySelector(
											'th input'
										).checked = false;
									}
								}
							});
						});
					} else if (idx == 2) {
						// Menu List
						let menu_group = result['menu_group'];
						let menu = result['menu'];
						menu.forEach((el2) => {
							// 페이지 접근 권한 리스트
							let menuGroupSeqNo = el2['menu_group_seq_no'];
							let menuGroup = (() => {
								let result;
								menu_group.forEach((el3) => {
									if (el3['menu_group_seq_no'] == menuGroupSeqNo) {
										result = el3['menu_group_name'];
									}
								});
								return result;
							})();
							if (
								!document.querySelector(
									`article#menuAuth .flexBox .rightBox .infoDiv table tbody tr[class="${menuGroup}"]`
								)
							) {
								let tr = createElement('tr', {
									class: menuGroup,
								});
								document
									.querySelector(
										'article#menuAuth .flexBox .rightBox .infoDiv table tbody'
									)
									.append(tr);

								let th = createElement('th');
								let checkBox = createElement('checkbox', {
									id: menuGroup,
									html: menuGroup ?? '메뉴에 속하지 않은 페이지',
								});
								th.append(checkBox);
								let td = createElement('td');
								tr.append(th, td);

								checkBox.querySelector('input').addEventListener('click', () => {
									if (checkBox.querySelector('input').checked) {
										checkBox.parentNode.parentNode
											.querySelectorAll('td input')
											.forEach((el3) => {
												if (!el3.checked) {
													el3.checked = true;
												}
											});
									} else {
										checkBox.parentNode.parentNode
											.querySelectorAll('td input')
											.forEach((el3) => {
												if (el3.checked) {
													el3.checked = false;
												}
											});
									}
								});
							}

							let location = document.querySelector(
								`article#menuAuth .flexBox .rightBox .infoDiv table tbody tr[class="${menuGroup}"] td`
							);

							let checkBox = createElement('checkbox', {
								id: el2['menu_name'],
								html: el2['menu_name'],
							});
							checkBox.querySelector('input').dataset.menu_seq_no =
								el2['menu_seq_no'];
							location.append(checkBox);

							checkBox.querySelector('input').addEventListener('click', () => {
								if (checkBox.querySelector('input').checked) {
									let flag = true;
									checkBox.parentNode.querySelectorAll('input').forEach((el3) => {
										if (!el3.checked) {
											flag = false;
										}
									});
									if (flag == true) {
										if (
											!checkBox.parentNode.parentNode.querySelector(
												'th input'
											).checked
										) {
											checkBox.parentNode.parentNode.querySelector(
												'th input'
											).checked = true;
										}
									}
								} else {
									if (
										checkBox.parentNode.parentNode.querySelector('th input')
											.checked
									) {
										checkBox.parentNode.parentNode.querySelector(
											'th input'
										).checked = false;
									}
								}
							});
						});

						menu_group.forEach((group) => {
							// 메뉴 별 허용 API 권한 (메뉴 리스트)
							let div = createElement('div', {
								class: 'menuGroupDiv depth1',
								data: {
									menu_group_seq_no: group['menu_group_seq_no'],
								},
							});
							if (group['menu_group_view_yn'] == 'N') {
								div.classList.add('unActivated');
							}
							let div2 = createElement('div', {
								class: 'menuGroupTitle depth1Title',
							});
							let i = createElement('i', {
								class: 'fa-solid fa-list',
							});
							let p = createElement('p', {
								html: group['menu_group_name'],
							});
							div2.append(i, p);
							let ul = createElement('ul', {
								class: 'depth2',
							});
							div.append(div2, ul);
							document
								.querySelector(
									'article#menuApiAuth .flexBox .leftBox .listWrapper .menuListDiv'
								)
								.append(div);
						});

						let nonMenuDiv = createElement('div', {
							class: 'menuGroupDiv depth1',
							data: {
								menu_group_seq_no: 'null',
							},
						});
						let div2 = createElement('div', {
							class: 'menuGroupTitle depth1Title',
						});
						let i = createElement('i', {
							class: 'fa-solid fa-list',
						});
						let p = createElement('p', {
							html: '메뉴에 속하지 않은 페이지',
						});
						div2.append(i, p);
						let ul = createElement('ul', {
							class: 'depth2',
						});
						nonMenuDiv.append(div2, ul);
						document
							.querySelector(
								'article#menuApiAuth .flexBox .leftBox .listWrapper .menuListDiv'
							)
							.append(nonMenuDiv);

						menu.forEach((menu) => {
							// 메뉴 별 허용 API 권한 (메뉴 리스트)
							let li = createElement('li', {
								data: {
									menu_seq_no: menu['menu_seq_no'],
								},
							});
							if (menu['menu_use_yn'] == 'N') {
								li.classList.add('unActivated');
							}
							let p = createElement('p', {
								html: menu['menu_name'],
							});
							li.append(p);
							if (menu['menu_group_seq_no'] == null) {
								document
									.querySelector(
										`.menuGroupDiv[data-menu_group_seq_no="null"] ul`
									)
									.append(li);
							} else {
								document
									.querySelector(
										`.menuGroupDiv[data-menu_group_seq_no="${menu['menu_group_seq_no']}"] ul`
									)
									.append(li);
							}
							li.addEventListener('click', () => {
								document
									.querySelectorAll('#menuApiAuth .leftBox .menuListDiv ul li')
									.forEach((el) => el.classList.remove('point'));
								li.classList.add('point');
								document
									.querySelectorAll(`#menuApiAuth .rightBox .infoDiv td input`)
									.forEach((el2) => {
										if (el2.checked) {
											el2.click();
										}
									});
								if (menu['api'] !== null) {
									let apiList = menu['api'];
									apiList.forEach((el2) => {
										let apiSeqNo = el2['api_seq_no'];
										document
											.querySelector(
												`#menuApiAuth .rightBox .infoDiv td input[data-api_seq_no="${apiSeqNo}"]`
											)
											.click();
									});
								}
							});
						});
					} else if (idx == 3) {
						// API Auth Group List
						let data = result['apiAuthGroupList'];
						// console.log(result);
						document.querySelector(
							'#apiAuth > div > div.leftBox > div > .apiAuthListTopDiv > p > strong'
						).innerHTML = data.length;
						data.forEach((apiAuthList) => {
							let apiAuthListDiv = document.querySelector('#apiAuth .apiAuthListDiv');
							let div = createElement('div', {
								class: 'apiAuthDiv depth1',
								data: {
									api_auth_group_seq_no: apiAuthList['api_auth_group_seq_no'],
								},
							});
							let div2 = createElement('div', {
								class: 'apiAuthTitle depth1Title',
							});
							let i = createElement('i', {
								class: 'fa-solid fa-list',
							});
							let p = createElement('p', {
								html: apiAuthList['api_auth_group_name'],
							});
							div2.append(i, p);
							let ul = createElement('ul', {
								class: 'depth2',
							});
							div.append(div2, ul);
							apiAuthListDiv.append(div);

							div.addEventListener('click', () => {
								const bottomDiv = document.querySelector(
									'#apiAuth .rightBox .bottomDiv'
								);
								if (bottomDiv.querySelectorAll('button')) {
									bottomDiv
										.querySelectorAll('button')
										.forEach((el2) => el2.remove());
								}
								let delApiAuthBtn = createElement('button', {
									class: 'btn_style_2 btn_color_crush',
									id: 'delApiAuth',
									html: ' 삭제하기',
								});
								let i = createElement('i', {
									class: 'fa-solid fa-trash-can',
								});
								delApiAuthBtn.prepend(i);
								let modApiAuthBtn = createElement('button', {
									class: 'btn_style_2 btn_color_point',
									id: 'modApiAuth',
									html: ' 저장하기',
								});
								let i2 = createElement('i', {
									class: 'fa-solid fa-floppy-disk',
								});
								modApiAuthBtn.prepend(i2);
								bottomDiv.append(delApiAuthBtn, modApiAuthBtn);

								delApiAuthBtn.addEventListener('click', () => {
									let obj = {};
									obj.title = 'API 권한 삭제';
									obj.btn = true;
									obj.btnText = '삭제하기';
									obj.btnColor = 'crush';
									obj.text = '선택한 API 권한을 삭제하시겠습니까?';
									obj.callback = delApiAuth;
									alertShow(obj);
								});
								function delApiAuth() {
									let bodyData = JSON.stringify({
										api_auth_group_seq_no: document.querySelector(
											'#apiAuth #api_auth_group_seq_no'
										).value,
										reg_id: g_user_id,
										act_evt: 'del',
									});
									fetch(urlApiAuthGroupIUD, {
										method: 'POST',
										body: bodyData,
									})
										.then((res) => res.json())
										.then((data) => {
											if (data.result_code == 'P000') {
												cancelAlert();
												window.location.reload();
											} else {
												let obj = {};
												obj.title = '오류';
												obj.btn = false;
												obj.text = 'API 권한 삭제 실패';
												obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
												alertShow(obj);
											}
										})
										.catch((error) => console.error(error));
								}
								modApiAuthBtn.addEventListener('click', () => {
									let obj = {};
									obj.title = 'API 권한 수정';
									obj.btn = true;
									obj.btnText = '수정하기';
									obj.btnColor = 'point';
									obj.text = '수정내용을 저장하시겠습니까?';
									obj.callback = modApiAuth;
									alertShow(obj);
								});
								function modApiAuth() {
									let bodyData = JSON.stringify({
										api_auth_group_seq_no: document.querySelector(
											'#apiAuth #api_auth_group_seq_no'
										).value,
										api_auth_group_name: document.querySelector(
											'#apiAuth #api_auth_group_name'
										).value,
										api_auth_group_content: document.querySelector(
											'#apiAuth #api_auth_group_content'
										).value,
										reg_id: g_user_id,
										act_evt: 'upd',
										api: (() => {
											let apiArr = [];
											document
												.querySelectorAll(
													'#apiAuth .rightBox .infoDiv td input'
												)
												.forEach((el2) => {
													if (el2.checked) {
														apiArr.push({
															api_seq_no: el2.dataset.api_seq_no,
														});
													}
												});
											return apiArr;
										})(),
									});
									fetch(urlApiAuthGroupIUD, {
										method: 'POST',
										body: bodyData,
									})
										.then((res) => res.json())
										.then((data) => {
											if (data.result_code == 'P000') {
												cancelAlert();
												window.location.reload();
											} else {
												let obj = {};
												obj.title = '오류';
												obj.btn = false;
												obj.text = 'API 권한 수정 실패';
												obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
												alertShow(obj);
											}
										})
										.catch((error) => console.error(error));
								}
								document
									.querySelectorAll(
										'#apiAuth .leftBox .apiAuthListDiv .apiAuthTitle'
									)
									.forEach((el2) => {
										el2.classList.remove('point');
									});
								div2.classList.add('point');
								let key_kor = {
									api_auth_group_seq_no: 'API 권한 일련번호',
									api_auth_group_name: 'API 권한 이름',
									api_auth_group_content: '내용',
									reg_id: '등록자',
									reg_ts: '등록시간',
								};
								let apiAuthInfoTableTbody = document.querySelector(
									'#apiAuth .rightBox .topDiv table tbody'
								);
								if (apiAuthInfoTableTbody.querySelectorAll('tr')) {
									apiAuthInfoTableTbody
										.querySelectorAll('tr')
										.forEach((el2) => el2.remove());
								}
								for (let key in apiAuthList) {
									let tr = createElement('tr');
									if (
										key == 'api_auth_group_seq_no' ||
										key == 'reg_id' ||
										key == 'reg_ts'
									) {
										let th = createElement('th', {
											html: key_kor[key],
										});
										let td = createElement('td');
										tr.append(th, td);
										let input = createElement('input', {
											id: key,
											value: apiAuthList[key] ?? '',
											readonly: true,
										});
										td.append(input);
									} else if (key == 'api') {
										document
											.querySelectorAll(
												`#apiAuth .rightBox .infoDiv td input`
											)
											.forEach((el2) => {
												if (el2.checked) {
													el2.click();
												}
											});
										if (apiAuthList[key][0]['api_seq_no'] !== null) {
											apiAuthList[key].forEach((el2) => {
												let apiSeqNo = el2['api_seq_no'];
												document
													.querySelector(
														`#apiAuth .rightBox .infoDiv td input[data-api_seq_no="${apiSeqNo}"]`
													)
													.click();
											});
										}
									} else {
										let th = createElement('th', {
											html: key_kor[key],
										});
										let td = createElement('td');
										tr.append(th, td);
										let input = createElement('input', {
											id: key,
											value: apiAuthList[key] ?? '',
										});
										td.append(input);
									}
									apiAuthInfoTableTbody.append(tr);
								}
							});
						});
					} else if (idx == 4) {
						// Menu Auth Group List
						console.log(result);
						let data = result['menuAuthGroupList'];
						document.querySelector(
							'#menuAuth > div > div.leftBox > div > .menuAuthListTopDiv > p > strong'
						).innerHTML = data.length;
						data.forEach((menuAuthList) => {
							let menuAuthListDiv = document.querySelector(
								'#menuAuth .menuAuthListDiv'
							);
							let div = createElement('div', {
								class: 'menuAuthDiv depth1',
								data: {
									menu_auth_group_seq_no: menuAuthList['menu_auth_group_seq_no'],
								},
							});
							let div2 = createElement('div', {
								class: 'menuAuthTitle depth1Title',
							});
							let i = createElement('i', {
								class: 'fa-solid fa-list',
							});
							let p = createElement('p', {
								html: menuAuthList['menu_auth_group_name'],
							});
							div2.append(i, p);
							let ul = createElement('ul', {
								class: 'depth2',
							});
							div.append(div2, ul);
							menuAuthListDiv.append(div);

							div.addEventListener('click', () => {
								const bottomDiv = document.querySelector(
									'#menuAuth .rightBox .bottomDiv'
								);
								if (bottomDiv.querySelectorAll('button')) {
									bottomDiv
										.querySelectorAll('button')
										.forEach((el2) => el2.remove());
								}
								let delMenuAuthBtn = createElement('button', {
									class: 'btn_style_2 btn_color_crush',
									id: 'delMenuAuth',
									html: ' 삭제하기',
								});
								let i = createElement('i', {
									class: 'fa-solid fa-trash-can',
								});
								delMenuAuthBtn.prepend(i);
								let modMenuAuthBtn = createElement('button', {
									class: 'btn_style_2 btn_color_point',
									id: 'modMenuAuth',
									html: ' 저장하기',
								});
								let i2 = createElement('i', {
									class: 'fa-solid fa-floppy-disk',
								});
								modMenuAuthBtn.prepend(i2);
								bottomDiv.append(delMenuAuthBtn, modMenuAuthBtn);

								delMenuAuthBtn.addEventListener('click', () => {
									let obj = {};
									obj.title = '페이지 접근 권한 삭제';
									obj.btn = true;
									obj.btnText = '삭제하기';
									obj.btnColor = 'crush';
									obj.text = '선택한 페이지 접근 권한을 삭제하시겠습니까?';
									obj.callback = delMenuAuth;
									alertShow(obj);
								});
								function delMenuAuth() {
									let bodyData = JSON.stringify({
										api_auth_group_seq_no: document.querySelector(
											'#menuAuth #menu_auth_group_seq_no'
										).value,
										reg_id: g_user_id,
										act_evt: 'del',
									});
									fetch(urlMenuAuthGroupIUD, {
										method: 'POST',
										body: bodyData,
									})
										.then((res) => res.json())
										.then((data) => {
											if (data.result_code == 'P000') {
												cancelAlert();
												window.location.reload();
											} else {
												let obj = {};
												obj.title = '오류';
												obj.btn = false;
												obj.text = '페이지 접근 권한 삭제 실패';
												obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
												alertShow(obj);
											}
										})
										.catch((error) => console.error(error));
								}
								modMenuAuthBtn.addEventListener('click', () => {
									let obj = {};
									obj.title = '페이지 접근 권한 수정';
									obj.btn = true;
									obj.btnText = '수정하기';
									obj.btnColor = 'point';
									obj.text = '수정내용을 저장하시겠습니까?';
									obj.callback = modMenuAuth;
									alertShow(obj);
								});
								function modMenuAuth() {
									let bodyData = JSON.stringify({
										menu_auth_group_seq_no: document.querySelector(
											'#menuAuth #menu_auth_group_seq_no'
										).value,
										api_auth_group_name: document.querySelector(
											'#menuAuth #menu_auth_group_name'
										).value,
										api_auth_group_content: document.querySelector(
											'#menuAuth #menu_auth_group_content'
										).value,
										reg_id: g_user_id,
										act_evt: 'upd',
										menu: (() => {
											let menuArr = [];
											document
												.querySelectorAll(
													'#menuAuth .rightBox .infoDiv td input'
												)
												.forEach((el2) => {
													if (el2.checked) {
														menuArr.push({
															menu_seq_no: el2.dataset.menu_seq_no,
														});
													}
												});
											return menuArr;
										})(),
									});
									fetch(urlMenuAuthGroupIUD, {
										method: 'POST',
										body: bodyData,
									})
										.then((res) => res.json())
										.then((data) => {
											if (data.result_code == 'P000') {
												cancelAlert();
												window.location.reload();
											} else {
												let obj = {};
												obj.title = '오류';
												obj.btn = false;
												obj.text = '페이지 접근 권한 수정 실패';
												obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
												alertShow(obj);
											}
										})
										.catch((error) => console.error(error));
								}
								document
									.querySelectorAll(
										'#menuAuth .leftBox .menuAuthListDiv .menuAuthTitle'
									)
									.forEach((el2) => {
										el2.classList.remove('point');
									});
								div2.classList.add('point');
								let key_kor = {
									menu_auth_group_seq_no: '페이지 접근 권한 일련번호',
									menu_auth_group_content: '내용',
									menu_auth_group_name: '페이지 접근 권한 이름',
									reg_id: '등록자',
									reg_ts: '등록시간',
								};
								let menuAuthInfoTableTbody = document.querySelector(
									'#menuAuth .rightBox .topDiv table tbody'
								);
								if (menuAuthInfoTableTbody.querySelectorAll('tr')) {
									menuAuthInfoTableTbody
										.querySelectorAll('tr')
										.forEach((el2) => el2.remove());
								}
								for (let key in menuAuthList) {
									let tr = createElement('tr');
									if (
										key == 'menu_auth_group_seq_no' ||
										key == 'reg_id' ||
										key == 'reg_ts'
									) {
										let th = createElement('th', {
											html: key_kor[key],
										});
										let td = createElement('td');
										tr.append(th, td);
										let input = createElement('input', {
											id: key,
											value: menuAuthList[key] ?? '',
											readonly: true,
										});
										td.append(input);
									} else if (key == 'menu') {
										document
											.querySelectorAll(
												`#menuAuth .rightBox .infoDiv td input`
											)
											.forEach((el2) => {
												if (el2.checked) {
													el2.click();
												}
											});
										if (menuAuthList[key][0]['menu_seq_no'] !== null) {
											menuAuthList[key].forEach((el2) => {
												let menuSeqNo = el2['menu_seq_no'];
												document
													.querySelector(
														`#menuAuth .rightBox .infoDiv td input[data-menu_seq_no="${menuSeqNo}"]`
													)
													.click();
											});
										}
									} else {
										let th = createElement('th', {
											html: key_kor[key],
										});
										let td = createElement('td');
										tr.append(th, td);
										let input = createElement('input', {
											id: key,
											value: menuAuthList[key] ?? '',
										});
										td.append(input);
									}
									menuAuthInfoTableTbody.append(tr);
								}
							});
						});
					}
				} else {
					console.error(result);
				}
			} else {
				console.error(reason);
			}
		});
		checkBoxTextClick();
	}
}
callAllApis();

function addNewAuthGroupPopup(apiAuthGroupList, menuAuthGroupList) {
	const addAuthGroupBtn = document.querySelector('#authGroup #addAuthGroup');
	addAuthGroupBtn.addEventListener('click', () => {
		let obj = {
			title: '새 권한 그룹 추가',
			btn: true,
			btnText: '추가하기',
			btnColor: 'point',
			func: popupMaker,
			callback: addAuthGroup,
			refresh: false,
		};
		modalShow(obj);

		function popupMaker() {
			const popupBody = document.querySelector('.modal .popupBody');
			if (popupBody.querySelector('.box')) {
				popupBody.querySelector('.box').remove();
			}
			let box = createElement('div', {
				class: 'box',
			});
			popupBody.append(box);
			let table = createElement('table');
			let tbody = createElement('tbody');
			table.append(tbody);
			box.append(table);
			let keys = {
				auth_group_name: '권한 그룹 이름',
				auth_group_content: '내용',
				auth_group_class: '권한 그룹 클래스',
				api_auth_group_seq_no: 'API 권한',
				menu_auth_group_seq_no: '페이지 접근 권한',
			};
			let frontId = 'addNew_';
			for (let key in keys) {
				let tr = createElement('tr');
				let th = createElement('th', {
					html: keys[key],
				});
				let td = createElement('td');
				if (
					key == 'auth_group_name' ||
					key == 'auth_group_content' ||
					key == 'auth_group_class'
				) {
					let input = createElement('input', {
						type: 'text',
						id: frontId + key,
					});
					td.append(input);
				} else if (key == 'api_auth_group_seq_no') {
					let select = createElement('select', {
						id: frontId + key,
					});
					let apiAuthGroup = apiAuthGroupList['apiAuthGroupList'];
					for (let apiAuth of apiAuthGroup) {
						let option = createElement('option', {
							value: apiAuth['api_auth_group_seq_no'],
							html: apiAuth['api_auth_group_name'],
						});
						select.append(option);
					}
					td.append(select);
				} else if (key == 'menu_auth_group_seq_no') {
					let select = createElement('select', {
						id: frontId + key,
					});
					let menuAuthGroup = menuAuthGroupList['menuAuthGroupList'];
					for (let menuAuth of menuAuthGroup) {
						let option = createElement('option', {
							value: menuAuth['menu_auth_group_seq_no'],
							html: menuAuth['menu_auth_group_name'],
						});
						select.append(option);
					}
					td.append(select);
				}
				tr.append(th, td);
				tbody.append(tr);
			}
		}

		function addAuthGroup() {
			const popupBody = document.querySelector('.modal .popupBody');
			let frontId = 'addNew_';
			let bodyData = JSON.stringify({
				menu_auth_group_seq_no: popupBody.querySelector(`#${frontId}menu_auth_group_seq_no`)
					.value,
				api_auth_group_seq_no: popupBody.querySelector(`#${frontId}api_auth_group_seq_no`)
					.value,
				auth_group_name: popupBody.querySelector(`#${frontId}auth_group_name`).value,
				auth_group_content: popupBody.querySelector(`#${frontId}auth_group_content`).value,
				auth_group_class: popupBody.querySelector(`#${frontId}auth_group_class`).value,
				reg_id: g_user_id,
				act_evt: 'ins',
			});
			fetch(urlAuthGroupIUD, {
				method: 'POST',
				body: obj,
			})
				.then((res) => res.json())
				.then((data) => {
					if (data['result_code'] == 'P000') {
						window.location.reload();
					} else {
						console.error(data);
					}
				});
		}
	});
}

function addNewApiAuthPopup(apiList) {
	// console.log(apiList);
	const addApiAuthGroupBtn = document.querySelector('#apiAuth #addApiAuth');
	addApiAuthGroupBtn.addEventListener('click', () => {
		let obj = {
			title: '새 API 권한 추가',
			btn: true,
			btnText: '추가하기',
			btnColor: 'point',
			func: popupMaker,
			callback: addApiAuth,
			refresh: false,
		};
		modalShow(obj);

		function popupMaker() {
			const popupBody = document.querySelector('.modal .popupBody');
			if (popupBody.querySelector('.box')) {
				popupBody.querySelector('.box').remove();
			}
			let box = createElement('div', {
				class: 'box',
			});
			popupBody.append(box);
			let table = createElement('table');
			let tbody = createElement('tbody');
			table.append(tbody);
			box.append(table);
			let keys = {
				api_auth_group_name: '권한 그룹 이름',
				api_auth_group_content: '내용',
				api: '허용 API',
			};
			let frontId = 'addNew_';
			for (let key in keys) {
				let tr = createElement('tr');
				let th = createElement('th', {
					html: keys[key],
				});
				let td = createElement('td');
				tr.append(th, td);
				tbody.append(tr);
				if (key == 'api_auth_group_name' || key == 'api_auth_group_content') {
					let input = createElement('input', {
						type: 'text',
						id: frontId + key,
					});
					td.append(input);
				} else if (key == 'api') {
					let table = createElement('table', {
						id: frontId + 'apiTable',
					});
					let tbody = createElement('tbody');
					table.append(tbody);
					td.append(table);
					apiList['list'].forEach((el) => {
						let apiGroup = el['api_group'];
						if (
							!popupBody.querySelector(
								`table tbody tr td table tbody tr[class="${apiGroup}"]`
							)
						) {
							let tr = createElement('tr', {
								class: apiGroup,
							});
							popupBody.querySelector('table tbody tr td table tbody').append(tr);

							let th = createElement('th');
							let checkBox = createElement('checkbox', {
								id: frontId + apiGroup,
								html: apiGroup,
							});
							th.append(checkBox);
							let td = createElement('td');
							tr.append(th, td);

							checkBox.querySelector('input').addEventListener('click', () => {
								if (checkBox.querySelector('input').checked) {
									checkBox.parentNode.parentNode
										.querySelectorAll('td input')
										.forEach((el2) => {
											if (!el2.checked) {
												el2.checked = true;
											}
										});
								} else {
									checkBox.parentNode.parentNode
										.querySelectorAll('td input')
										.forEach((el2) => {
											if (el2.checked) {
												el2.checked = false;
											}
										});
								}
							});
						}

						let location = popupBody.querySelector(
							`table tbody tr td table tbody tr[class="${apiGroup}"] td`
						);

						let checkBox = createElement('checkbox', {
							id: frontId + el['api_code'],
							html: el['api_name'],
						});
						checkBox.querySelector('input').dataset.api_seq_no = el['api_seq_no'];
						location.append(checkBox);

						checkBox.querySelector('input').addEventListener('click', () => {
							if (checkBox.querySelector('input').checked) {
								let flag = true;
								checkBox.parentNode.querySelectorAll('input').forEach((el2) => {
									if (!el2.checked) {
										flag = false;
									}
								});
								if (flag == true) {
									if (
										!checkBox.parentNode.parentNode.querySelector('th input')
											.checked
									) {
										checkBox.parentNode.parentNode.querySelector(
											'th input'
										).checked = true;
									}
								}
							} else {
								if (
									checkBox.parentNode.parentNode.querySelector('th input').checked
								) {
									checkBox.parentNode.parentNode.querySelector(
										'th input'
									).checked = false;
								}
							}
						});
					});
				}
			}
		}

		function addApiAuth() {
			const popupBody = document.querySelector('.modal .popupBody');
			let frontId = 'addNew_';
			let bodyData = JSON.stringify({
				api_auth_group_name: popupBody.querySelector(`#${frontId}api_auth_group_name`)
					.value,
				api_auth_group_content: popupBody.querySelector(`#${frontId}api_auth_group_content`)
					.value,
				api: (() => {
					let apiArr = [];
					popupBody
						.querySelectorAll('#addNew_apiTable tbody tr td input')
						.forEach((el2) => {
							if (el2.checked) {
								apiArr.push({
									api_seq_no: el2.dataset.api_seq_no,
								});
							}
						});
					return apiArr;
				})(),
				reg_id: g_user_id,
				act_evt: 'ins',
			});
			fetch(urlApiAuthGroupIUD, {
				method: 'POST',
				body: bodyData,
			})
				.then((res) => res.json())
				.then((data) => {
					if (data['result_code'] == 'P000') {
						window.location.reload();
					} else {
						console.error(data);
					}
				});
		}
	});
}

function addNewMenuAuthPopup(menuList) {
	// console.log(apiList);
	const addMenuAuthGroupBtn = document.querySelector('#menuAuth #addMenuAuth');
	addMenuAuthGroupBtn.addEventListener('click', () => {
		let obj = {
			title: '새 페이지 접근 권한 추가',
			btn: true,
			btnText: '추가하기',
			btnColor: 'point',
			func: popupMaker,
			callback: addMenuAuth,
			refresh: false,
		};
		modalShow(obj);

		function popupMaker() {
			const popupBody = document.querySelector('.modal .popupBody');
			if (popupBody.querySelector('.box')) {
				popupBody.querySelector('.box').remove();
			}
			let box = createElement('div', {
				class: 'box',
			});
			popupBody.append(box);
			let table = createElement('table');
			let tbody = createElement('tbody');
			table.append(tbody);
			box.append(table);
			let keys = {
				menu_auth_group_name: '권한 그룹 이름',
				menu_auth_group_content: '내용',
				menu: '접근 가능 페이지',
			};
			let frontId = 'addNew_';
			for (let key in keys) {
				let tr = createElement('tr');
				let th = createElement('th', {
					html: keys[key],
				});
				let td = createElement('td');
				tr.append(th, td);
				tbody.append(tr);
				if (key == 'menu_auth_group_name' || key == 'menu_auth_group_content') {
					let input = createElement('input', {
						type: 'text',
						id: frontId + key,
					});
					td.append(input);
				} else if (key == 'menu') {
					let table = createElement('table', {
						id: frontId + 'menuTable',
					});
					let tbody = createElement('tbody');
					table.append(tbody);
					td.append(table);
					let menu_group = menuList['menu_group'];
					let menu = menuList['menu'];
					menu.forEach((el2) => {
						let menuGroupSeqNo = el2['menu_group_seq_no'];
						let menuGroup = (() => {
							let result;
							menu_group.forEach((el3) => {
								if (el3['menu_group_seq_no'] == menuGroupSeqNo) {
									result = el3['menu_group_name'];
								}
							});
							return result;
						})();
						if (
							!popupBody.querySelector(
								`table tbody tr td table tbody tr[class="${menuGroup}"]`
							)
						) {
							let tr = createElement('tr', {
								class: menuGroup,
							});
							popupBody.querySelector('table tbody tr td table tbody').append(tr);

							let th = createElement('th');
							let checkBox = createElement('checkbox', {
								id: frontId + menuGroup,
								html: menuGroup ?? '메뉴에 속하지 않은 페이지',
							});
							th.append(checkBox);
							let td = createElement('td');
							tr.append(th, td);

							checkBox.querySelector('input').addEventListener('click', () => {
								if (checkBox.querySelector('input').checked) {
									checkBox.parentNode.parentNode
										.querySelectorAll('td input')
										.forEach((el3) => {
											if (!el3.checked) {
												el3.checked = true;
											}
										});
								} else {
									checkBox.parentNode.parentNode
										.querySelectorAll('td input')
										.forEach((el3) => {
											if (el3.checked) {
												el3.checked = false;
											}
										});
								}
							});
						}

						let location = popupBody.querySelector(
							`table tbody tr td table tbody tr[class="${menuGroup}"] td`
						);

						let checkBox = createElement('checkbox', {
							id: frontId + el2['menu_name'],
							html: el2['menu_name'],
						});
						checkBox.querySelector('input').dataset.menu_seq_no = el2['menu_seq_no'];
						location.append(checkBox);

						checkBox.querySelector('input').addEventListener('click', () => {
							if (checkBox.querySelector('input').checked) {
								let flag = true;
								checkBox.parentNode.querySelectorAll('input').forEach((el3) => {
									if (!el3.checked) {
										flag = false;
									}
								});
								if (flag == true) {
									if (
										!checkBox.parentNode.parentNode.querySelector('th input')
											.checked
									) {
										checkBox.parentNode.parentNode.querySelector(
											'th input'
										).checked = true;
									}
								}
							} else {
								if (
									checkBox.parentNode.parentNode.querySelector('th input').checked
								) {
									checkBox.parentNode.parentNode.querySelector(
										'th input'
									).checked = false;
								}
							}
						});
					});
				}
			}
		}

		function addMenuAuth() {
			const popupBody = document.querySelector('.modal .popupBody');
			let frontId = 'addNew_';
			let bodyData = JSON.stringify({
				menu_auth_group_name: popupBody.querySelector(`#${frontId}menu_auth_group_name`)
					.value,
				menu_auth_group_content: popupBody.querySelector(
					`#${frontId}menu_auth_group_content`
				).value,
				menu: (() => {
					let menuArr = [];
					popupBody
						.querySelectorAll('#addNew_menuTable tbody tr td input')
						.forEach((el2) => {
							if (el2.checked) {
								menuArr.push({
									menu_seq_no: el2.dataset.menu_seq_no,
								});
							}
						});
					return menuArr;
				})(),
				reg_id: g_user_id,
				act_evt: 'ins',
			});
			fetch(urlMenuAuthGroupIUD, {
				method: 'POST',
				body: bodyData,
			})
				.then((res) => res.json())
				.then((data) => {
					if (data['result_code'] == 'P000') {
						window.location.reload();
					} else {
						console.error(data);
					}
				});
		}
	});
}
