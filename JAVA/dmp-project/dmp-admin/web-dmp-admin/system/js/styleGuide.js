import { renderPagination } from './import/Paging';
import { searchForm } from './import/Searchform';

// 테이블
let location = document.querySelector('article#table .searchOption');

searchForm(location, {
	period: {
		format: 'YYYY-MM-DD',
		fastBtns: ['당일', '최근 일주일', '최근 한 달', '전체'],
	},
	options: [
		{
			title: '검색 조건1',
			checkBox: {
				opt1: '옵션 1',
				opt2: '옵션 2',
				opt3: '옵션 3',
			},
		},
		{
			title: '검색 조건2',
			checkBox: {
				opt4: '옵션 1',
				opt5: '옵션 2',
				opt6: '옵션 3',
				opt7: '옵션 4',
				opt8: '옵션 5',
			},
		},
	],
	textInput: true,
});

let location2 = document.querySelector('.paging');
renderPagination(location2, {
	page: 1,
	page_limit: 20,
	totalCount: 462,
	numOfBtns: 10,
	pageMaker: function () {
		console.log('pageMaker Function');
	},
});

const spanFor = document.querySelectorAll('.stickyIndex span');
const io = new IntersectionObserver((element) => {
	element.forEach((article) => {
		if (article.isIntersecting) {
			spanFor.forEach((el2) => {
				if (el2.getAttribute('for') == article.target.getAttribute('id')) {
					el2.style.color = '#5584ff';
				}
			});
		} else {
			spanFor.forEach((el2) => {
				if (el2.getAttribute('for') == article.target.getAttribute('id')) {
					el2.style.color = '#8492af';
				}
			});
		}
	});
});
const articles = document.querySelectorAll('article');
articles.forEach((el) => io.observe(el));
