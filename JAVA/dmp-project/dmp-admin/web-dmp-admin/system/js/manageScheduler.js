import { createElement } from './import/CreateElement';
import { errorHandling } from './import/ErrorHandler';
import { insertParam } from './import/InsertParam';
import { jsonToQueryString } from './import/JsonToQueryString';
import { loading, loading_end } from './import/Loading';
import { alertShow, cancelAlert, cancelModal, modalShow } from './import/ModalShow';
import { renderPagination } from './import/Paging';
import { searchForm } from './import/Searchform';
import { searchParam } from './import/SearchParam';

async function schedulerListCall(page, page_limit) {
	loading();
	let jsonString = jsonToQueryString({
		page: page,
		page_limit: page_limit,
	});
	let result = await fetch('/api/v1/sche/schedulerList?' + jsonString).then((res) => res.json());
	if (result) {
		loading_end();
		if (result['result_code'] == 'P000') {
			let totalCount = document.querySelector('p.totalCount strong');
			totalCount.innerHTML = result['total_count'];
			console.log(result);
			// 처음 페이지네이션 생성
			const pageLocation = document.querySelector('.tableBottomDiv .paging');
			renderPagination(pageLocation, {
				page: page,
				page_limit: page_limit,
				totalCount: result['total_count'],
				numOfBtns: 10,
				pageClick: function () {
					insertParam('page', document.querySelector('#pageBtn.on').dataset.num);
					schedulerListCall(searchParam('page'), page_limit);
				},
			});

			tableMaker();
			function tableMaker() {
				const table = document.querySelector('.flexBox .leftBox .tableDiv table');
				if (table.querySelector('thead tr')) {
					table.querySelector('thead tr').remove();
				}
				if (table.querySelectorAll('tbody tr')) {
					table.querySelectorAll('tbody tr').forEach((el) => el.remove());
				}
				let key_kor = {
					sche_seq_no: '스케줄러 일련번호',
					sche_name: '스케줄러 이름',
					sche_url: 'URL',
					sche_use_yn: '사용 여부',
				};
				//테이블 헤드 생성
				let theadTr = createElement('tr');
				table.querySelector('thead').append(theadTr);
				for (let key in key_kor) {
					let th = createElement('th', {
						html: key_kor[key],
					});
					theadTr.append(th);
				}
				//테이블 바디 생성
				result['list'].forEach((el) => {
					let tr = createElement('tr');
					table.querySelector('tbody').append(tr);
					for (let key in key_kor) {
						let td = createElement('td', {
							html: el[key],
						});
						tr.append(td);
					}
					tr.addEventListener('click', () => {
						const bottomDiv = document.querySelector('.rightBox .tableBottomDiv');
						if (bottomDiv.querySelectorAll('button')) {
							bottomDiv.querySelectorAll('button').forEach((el2) => el2.remove());
						}
						let delSchedulerBtn = createElement('button', {
							class: 'btn_style_2 btn_color_crush',
							id: 'delScheduler',
							html: ' 삭제하기',
						});
						let i = createElement('i', {
							class: 'fa-solid fa-trash-can',
						});
						delSchedulerBtn.prepend(i);
						let modSchedulerBtn = createElement('button', {
							class: 'btn_style_2 btn_color_point',
							id: 'modScheduler',
							html: ' 저장하기',
						});
						let i2 = createElement('i', {
							class: 'fa-solid fa-floppy-disk',
						});
						modSchedulerBtn.prepend(i2);
						bottomDiv.append(delSchedulerBtn, modSchedulerBtn);

						delSchedulerBtn.addEventListener('click', () => {
							let obj = {};
							obj.title = '스케줄러 삭제';
							obj.btn = true;
							obj.btnText = '삭제하기';
							obj.btnColor = 'crush';
							obj.text = '선택한 스케줄러를 삭제하시겠습니까?';
							obj.callback = delScheduler;
							alertShow(obj);
						});
						function delScheduler() {
							let bodyData = JSON.stringify({
								sche_seq_no: document.querySelector('#sche_seq_no').value,
								reg_id: g_user_id,
								act_evt: 'del',
							});
							fetch('/api/v1/sche/schedulerIUD', {
								method: 'POST',
								body: bodyData,
							})
								.then((res) => res.json())
								.then((data) => {
									if (data.result_code == 'P000') {
										cancelAlert();
										window.location.reload();
									} else {
										let obj = {};
										obj.title = '오류';
										obj.btn = false;
										obj.text = '스케줄러 삭제 실패';
										obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
										alertShow(obj);
									}
								})
								.catch((error) => console.error(error));
						}
						modSchedulerBtn.addEventListener('click', () => {
							let obj = {};
							obj.title = '스케줄러 수정';
							obj.btn = true;
							obj.btnText = '수정하기';
							obj.btnColor = 'point';
							obj.text = '수정내용을 저장하시겠습니까?';
							obj.callback = modScheduler;
							alertShow(obj);
						});
						function modScheduler() {
							let bodyData = JSON.stringify({
								sche_seq_no: document.querySelector('#sche_seq_no').value,
								sche_name: document.querySelector('#sche_name').value,
								reg_id: g_user_id,
								act_evt: 'upd',
							});
							fetch('/api/v1/sche/schedulerIUD', {
								method: 'POST',
								body: bodyData,
							})
								.then((res) => res.json())
								.then((data) => {
									if (data.result_code == 'P000') {
										cancelAlert();
										window.location.reload();
									} else {
										let obj = {};
										obj.title = '오류';
										obj.btn = false;
										obj.text = '스케줄러 수정 실패';
										obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
										alertShow(obj);
									}
								})
								.catch((error) => console.error(error));
						}
						const rightBox = document.querySelector('.rightBox');
						if (rightBox.querySelectorAll('table tbody tr')) {
							rightBox
								.querySelectorAll('table tbody tr')
								.forEach((el) => el.remove());
						}
						table
							.querySelectorAll('tbody tr')
							.forEach((el) => el.classList.remove('point'));
						tr.classList.add('point');

						// let key_kor = {
						// 	sche_seq_no: '스케줄러 일련번호',
						// 	sche_name: '스케줄러 이름',
						// 	reg_id: '등록자',
						// 	reg_ts: '등록시간',
						// };

						let keys = Object.keys(el);
						for (let key of keys) {
							let tr = createElement('tr');
							let th = createElement('th', {
								html: key,
							});
							let td = createElement('td');
							let input = createElement('input', {
								id: key,
								value: el[key] ?? '',
								readonly: true,
							});
							td.append(input);

							tr.append(th, td);
							rightBox.querySelector('table tbody').append(tr);
						}
					});
				});
			}
		} else {
			errorHandling(result['result_code']);
		}
	} else {
		console.error('Failed to call Scheduler list');
	}
}
schedulerListCall();

const location = document.querySelector('.searchOption');
searchForm(location, {
	options: [
		{
			title: '스케줄러 작동',
			toggle: 'schedulerSwitch',
		},
		{
			title: '인터벌 설정',
			select: {
				id: 'schedulerInterval',
				option: [
					{ value: 1, html: '1분' },
					{ value: 3, html: '3분' },
					{ value: 5, html: '5분' },
					{ value: 10, html: '10분' },
					{ value: 30, html: '30분' },
					{ value: 60, html: '1시간', selected: true },
					{ value: 120, html: '2시간' },
					{ value: 240, html: '4시간' },
					{ value: 360, html: '6시간' },
					{ value: 720, html: '12시간' },
					{ value: 1440, html: '1일' },
				],
			},
		},
	],
});
