import { createElement } from './import/CreateElement';
import { alertShow, cancelAlert, cancelModal, modalShow } from './import/ModalShow';
import { searchForm } from './import/Searchform';
import { IsJson } from './import/IsJson';
import { loading, loading_end } from './import/Loading';
import { jsonToQueryString } from './import/JsonToQueryString';
import { renderPagination } from './import/Paging';
import { searchParam } from './import/SearchParam';
import { insertParam } from './import/InsertParam';

async function apiListCall(page, page_limit) {
	loading();
	let jsonString = jsonToQueryString({
		page: page,
		page_limit: page_limit,
	});
	let result = await fetch('/api/v1/cust/custMember' + '?' + jsonString).then((res) => res.json());
	if (result) {
		loading_end();
	}
	if (result.result_code == 'P000') {
		const tableDiv = document.querySelector('.leftBox .tableDiv');
		//테이블 초기화
		tableDiv.querySelectorAll('table tr').forEach((el) => el.remove());

		if (!document.querySelector('#custList .searchOption .optionRow')) {
			searchFormMaker(result['list']);
		}

		let total_count = result['total_count'];
		let api_list = result['list'];

		//토탈 카운트
		document.querySelector('article#custList .leftBox .tableTopDiv > p > strong').innerHTML =
			total_count;

		// 처음 페이지네이션 생성
		const pageLocation = document.querySelector('.tableBottomDiv .paging');
		renderPagination(pageLocation, {
			page: page,
			page_limit: page_limit,
			totalCount: result['total_count'],
			numOfBtns: 10,
			pageClick: function () {
				insertParam('page', document.querySelector('#pageBtn.on').dataset.num);
				apiListCall(searchParam('page'), page_limit);
			},
		});

		//좌측 고객 리스트 생성
		let thead_naming = {
			cust_id: '아이디',
			biz_nm: '사업자 번호',
			ceo_no: '대표자명',
			del_yn: '삭제여부',
			reg_ts : '등록일자'
		};
		let newTr = createElement('tr');
		for (let key in thead_naming) {
			let value = thead_naming[key];
			let newTh = createElement('th', {
				html: value,
			});
			newTr.append(newTh);
		}
		tableDiv.querySelector('table thead').append(newTr);

		api_list.forEach((el) => {
			let newTr = createElement('tr');
			tableDiv.querySelector('table tbody').append(newTr);
			for (let key in el) {
				for (let nameKey in thead_naming) {
					if (key == nameKey) {
						let newTd = createElement('td', {
							html: el[key],
						});
						newTr.append(newTd);

						if (nameKey == 'api_use_yn' && el[key] == 'N') {
							newTr.classList.add('unActivated');
						}
					}
				}
			}
			//tr 클릭 시 고객 상세보기
			newTr.addEventListener('click', () => {
				const bottomDiv = document.querySelector('#custList .rightBox .tableBottomDiv');
				if (bottomDiv.querySelectorAll('button')) {
					bottomDiv.querySelectorAll('button').forEach((el2) => el2.remove());
				}
				let delApiBtn = createElement('button', {
					class: 'btn_style_2 btn_color_crush',
					id: 'delApi',
					html: ' 삭제하기',
				});
				let i = createElement('i', {
					class: 'fa-solid fa-trash-can',
				});
				delApiBtn.prepend(i);
				let modApiBtn = createElement('button', {
					class: 'btn_style_2 btn_color_point',
					id: 'modApi',
					html: ' 저장하기',
				});
				let i2 = createElement('i', {
					class: 'fa-solid fa-floppy-disk',
				});
				modApiBtn.prepend(i2);
				bottomDiv.append(delApiBtn, modApiBtn);

				delApiBtn.addEventListener('click', () => {
					let obj = {};
					obj.title = '고객 삭제';
					obj.btn = true;
					obj.btnText = '삭제하기';
					obj.btnColor = 'crush';
					obj.text = '선택한 고객을 삭제하시겠습니까?';
					obj.callback = delApi;
					alertShow(obj);
				});
				function delApi() {
					let bodyData = JSON.stringify({
						cust_seq_no: document.querySelector('#custList #cust_seq_no').value,
						reg_id: g_user_id,
						act_evt: 'del'
					});
					fetch('/api/v1/cust/custIUD', {
						method: 'POST',
						body: bodyData,
					})
						.then((res) => res.json())
						.then((data) => {
							if (data.result_code == 'P000') {
								cancelAlert();
								window.location.reload();
							} else {
								let obj = {};
								obj.title = '오류';
								obj.btn = false;
								obj.text = '고객 삭제 실패';
								obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
								alertShow(obj);
							}
						})
						.catch((error) => console.error(error));
				}
				modApiBtn.addEventListener('click', () => {
					let obj = {};
					obj.title = '고객 수정';
					obj.btn = true;
					obj.btnText = '수정하기';
					obj.btnColor = 'point';
					obj.text = '수정내용을 저장하시겠습니까?';
					obj.callback = modApi;
					alertShow(obj);
				});
				function modApi() {
					let bodyData = JSON.stringify({
						cust_seq_no: document.querySelector('#custList #cust_seq_no').value,
						cust_id: document.querySelector('#custList #cust_id').value,
						biz_no: document.querySelector('#custList #biz_no').value,
						biz_nm: document.querySelector('#custList #biz_nm').value,
						ceo_nm: document.querySelector('#custList #ceo_nm').value,
						ceo_no: document.querySelector('#custList #ceo_no').value,
						chrg_nm: document.querySelector('#custList #chrg_nm').value,
						chrg_no: document.querySelector('#custList #chrg_no').value,
						reg_id: g_user_id,
						act_evt: 'upd',
					});
					fetch('/api/v1/cust/custIUD', {
						method: 'POST',
						body: bodyData,
					})
						.then((res) => res.json())
						.then((data) => {
							if (data.result_code == 'P000') {
								cancelAlert();
								window.location.reload();
							} else {
								let obj = {};
								obj.title = '오류';
								obj.btn = false;
								obj.text = 'API 수정 실패';
								obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
								alertShow(obj);
							}
						})
						.catch((error) => console.error(error));
				}
				document.querySelector('.tableBottomDiv').classList.add('active');
				document
					.querySelectorAll('article#custList .leftBox .tableDiv table tbody tr')
					.forEach((el) => el.classList.remove('point'));
				newTr.classList.add('point');
				const apiDetailTbody = document.querySelector(
					'article#custList .rightBox table tbody'
				);
				apiDetailTbody.querySelectorAll('tr').forEach((el) => el.remove());

				function ckeditorPopup(param_data) {
					const popupBody = document.querySelector('.layerPopup .popupBody');
					if (popupBody.querySelector('.box')) {
						popupBody.querySelector('.box').remove();
					}
					let newDiv = createElement('div', {
						class: 'box',
					});
					let textarea = createElement('textarea', {
						id: 'editor',
					});
					newDiv.append(textarea);
					popupBody.append(newDiv);

					let myEditor;

					ClassicEditor.create(document.querySelector('#editor'), {
						toolbar: null,
					})
						.then((newEditor) => {
							myEditor = newEditor;
							myEditor.setData(param_data[0]);

							let submit = document.querySelector(
								'.modal > div > div.btnBox > button'
							);
							if (submit.clickHandler) {
								submit.removeEventListener('click', submit.clickHandler);
							}
							submit.clickHandler = () => {
								let data = myEditor.getData().replace(/<[^>]*>?/g, '');
								param_data[1].value = data;
							};
							submit.addEventListener('click', submit.clickHandler);
						})
						.catch((error) => {
							console.error(error);
						});
				}

				const key_kor = {
					cust_seq_no: '고객 시퀀스 넘버',
					cust_id: '고객ID',
					biz_no: '사업자 번호',
					biz_nm: '상호명',
					ceo_nm: '대표자명',
					ceo_no: '대표자 연락처',
					chrg_nm: '관리자명',
					chrg_no: '관리자 연락처',
					passwd_mdfy_dt: '패스워드 변경일자',
					del_yn: '삭제여부',
					free_trial_yn: '무료기간여부',
					free_stt_dt: '무료기간 시작일',
					free_end_dt: '무료기간 종료일',
					reg_id: '등록자	',
					reg_ts: '등록 일자',
					mod_id	: '수정자'
				};
				for (let key in el) {
					let newTr = createElement('tr');
					let newTh;
					if (key_kor[key]) {
						newTh = createElement('th', {
							html: key_kor[key],
						});
					} else {
						newTh = createElement('th', {
							html: key,
						});
					}
					let newTd = createElement('td');
					if (key == 'method') {
						let select = createElement('select', {
							id: 'method',
						});
						let method_list = ['GET', 'POST', 'PUT', 'DELETE'];
						method_list.forEach((element) => {
							let option = createElement('option', {
								value: element,
								html: element,
							});
							select.append(option);
							if (el[key] == element) {
								option.setAttribute('selected', true);
							}
						});
						newTd.append(select);
					} else if (
						key == 'cust_id' ||
						key == 'biz_no' ||
						key == 'biz_nm' ||
						key == 'ceo_nm' ||
						key == 'ceo_no' ||
						key == 'chrg_nm' ||
						key == 'chrg_no'
					) {
						let input = createElement('input', {
							type: 'text',
							value: el[key] ?? '',
							id: key,
						});
						newTd.append(input);
					}else {
						let input = createElement('input', {
							type: 'text',
							value: el[key] ?? '',
							id: key,
							readonly: true,
						});
						newTd.append(input);
					}

					newTr.append(newTh, newTd);
					apiDetailTbody.append(newTr);
				}
			});
		});
	} else {
		console.error('api/apilist ' + result.result_code, result.result_msg);
	}
}
// 처음 페이지 로드
let initPage = searchParam('page') ? searchParam('page') : 1;
let initRowInView = document.querySelector('#rowInView').value;
apiListCall(initPage, initRowInView);

// 몇개씩 볼지 select로 수정할때
document.querySelector('#rowInView').addEventListener('change', apiListCallAgain);

function apiListCallAgain() {
	let page_limit = document.querySelector('#rowInView').value;
	insertParam('page', 1);
	apiListCall(1, page_limit);
}

function searchFormMaker(custList) {
	let apiGroupObj = {};
	custList.forEach((el) => {
		if (!apiGroupObj[el['api_group']]) {
			apiGroupObj[el['api_group']] = el['api_group'];
		}
	});
	const location = document.querySelector('article#custList .flexBox .leftBox .searchOption');
	searchForm(location, {
		textInput: true,
		searchBtn: true,
	});

	// 검색할때
	document.querySelector('#searchBtn').addEventListener('click', apiListCallAgain);
}

function addNewApi() {
	const addAPIBtn = document.querySelector('#custList #addNewApi');
	addAPIBtn.addEventListener('click', () => {
		let obj = {};
		obj.title = '새 고객 추가';
		obj.btn = true;
		obj.btnText = '추가하기';
		obj.btnColor = 'point';
		obj.func = addAPIPopup;
		obj.callback = addAPI;

		modalShow(obj);

		function addAPIPopup() {
			const popupBody = document.querySelector('.modal .layerPopup .popupBody');

			if (popupBody.querySelector('.box')) {
				popupBody.querySelector('.box').remove();
			}
			let box = createElement('div', {
				class: 'box',
			});
			let table = createElement('table');
			let tbody = createElement('tbody');
			popupBody.appendChild(box);
			box.appendChild(table);
			table.appendChild(tbody);
			let keys = {
				cust_id: 'ID',
				biz_no: '사업자 번호',
				biz_nm: '회사명',
				ceo_nm: '대표자명',
				ceo_no: '대표자 연락처',
				chrg_nm: '관리자 명',
				chrg_no: '관리자 연락처'
			};
			let frontId = 'addNew_';
			for (let key in keys) {
				let tr = createElement('tr');
				let th = createElement('th', {
					html: keys[key],
				});
				let td = createElement('td');
				tr.append(th, td);
				tbody.append(tr);
				// if (key == 'method') {
				// 	let select = createElement('select', {
				// 		id: frontId + key,
				// 	});
				// 	let options = ['GET', 'POST', 'PUT', 'DELETE'];
				// 	options.forEach((el) => {
				// 		let option = createElement('option', {
				// 			value: el,
				// 			html: el,
				// 		});
				// 		select.append(option);
				// 	});
				// 	td.append(select);
				// } else if (key == 'api_use_yn' || key == 'log_yn') {
				// 	let toggle = createElement('toggle', {
				// 		id: frontId + key,
				// 	});
				// 	td.append(toggle);
				// } else if (key == 'service_seq_no') {
				// 	let input = createElement('input', {
				// 		type: 'text',
				// 		id: frontId + key,
				// 		placeholder: '숫자만 입력',
				// 	});
				// 	input.addEventListener('keyup', () => {
				// 		input.value = input.value
				// 			.replace(/[^0-9.]/g, '')
				// 			.replace(/(\..*)\./g, '$1');
				// 	});
				// 	td.append(input);
				// } else {
					let input = createElement('input', {
						type: 'text',
						id: frontId + key,
					});
					td.append(input);
				// }
			}
		}

		function addAPI() {
			const popupBody = document.querySelector('.modal .popupBody');
			let frontId = 'addNew_';
			let bodyData = JSON.stringify({
				cust_id: popupBody.querySelector(`#${frontId}cust_id`).value,
				biz_no: popupBody.querySelector(`#${frontId}biz_no`).value,
				biz_nm: popupBody.querySelector(`#${frontId}biz_nm`).value,
				ceo_nm: popupBody.querySelector(`#${frontId}ceo_nm`).value,
				ceo_no: popupBody.querySelector(`#${frontId}ceo_no`).value,
				chrg_nm: popupBody.querySelector(`#${frontId}chrg_nm`).value,
				chrg_no: popupBody.querySelector(`#${frontId}chrg_no`).value,
				reg_id: g_user_id,
				act_evt: 'ins',
			});
			fetch('/api/v1/cust/custIUD', {
				method: 'POST',
				body: bodyData,
			})
				.then((res) => res.json())
				.then((data) => {
					if (data['result_code'] == 'P000') {
						window.location.reload();
					} else {
						console.error(data);
					}
				});
		}
	});
}
addNewApi();

// API Test---------------------------------------------------------------------------------------------------

function tagBoxClick() {
	const tagBoxLi = document.querySelectorAll('article#apiTest .rightBox ul.tagBox>li');
	const apiInputBoxLi = document.querySelectorAll('article#apiTest .rightBox ul.apiInputBox>li');
	tagBoxLi.forEach((el, idx) => {
		el.addEventListener('click', () => {
			tagBoxLi.forEach((el2) => el2.classList.remove('active'));
			el.classList.add('active');
			apiInputBoxLi.forEach((el2) => el2.classList.remove('active'));
			apiInputBoxLi[idx].classList.add('active');
		});
	});
}
tagBoxClick();

function addColumn() {
	const newTd = document.querySelectorAll(
		'article#apiTest .flexBox .rightBox ul.apiInputBox li button#newTd'
	);

	newTd.forEach((el) => {
		el.addEventListener('click', () => {
			addrow('active');
		});
	});
}
addColumn();

let autoAddRow = function () {
	const inputs = document.querySelectorAll(
		'article#apiTest ul.apiInputBox li table tbody tr td input'
	);
	inputs.forEach((el) => {
		el.addEventListener('keyup', () => {
			if (el.value !== '') {
				let allTr = el.parentNode.parentNode.parentNode.querySelectorAll('tr');
				let currTr = el.parentNode.parentNode;
				allTr.forEach((el2, idx2) => {
					if (el2 == currTr && idx2 == allTr.length - 1) {
						el.parentNode.parentNode.parentNode.parentNode.parentNode
							.querySelector('button#newTd')
							.click();
					}
				});
			}
		});
	});
};
autoAddRow();

// const apiRequest = document.querySelector('article#apiTest button#apiRequest');
// apiRequest.addEventListener('click', () => {
// 	const apiMethod = document.querySelector('article#apiTest .method select').value;
// 	let url = document.querySelector('article#apiTest .method input').value;
// 	const param = document.querySelectorAll(
// 		'article#apiTest ul.apiInputBox li.apiParam table tbody tr'
// 	);
// 	const header = document.querySelectorAll(
// 		'article#apiTest ul.apiInputBox li.apiHeader table tbody tr'
// 	);
// 	const body = document.querySelectorAll(
// 		'article#apiTest ul.apiInputBox li.apiBody table tbody tr'
// 	);
// 	const responseBox = document.querySelector('article#apiTest .response .responseBox pre');

// 	let final_url;

// 	if (url == '') {
// 		document.querySelector('article#apiTest .method input').focus();
// 		return false;
// 	}

// 	//url에 파라미터(쿼리스트링) 추가
// 	for (let i = 0; i < param.length; i++) {
// 		const inputs = param[i].querySelectorAll('input');
// 		let key = inputs[0].value;
// 		let value = inputs[1].value;

// 		if (key == '' && value == '') {
// 		} else {
// 			if (url.includes('?') == false) {
// 				url += '?' + key + '=' + value + '&';
// 			} else {
// 				url += key + '=' + value + '&';
// 			}
// 			console.log(param.length, i);
// 			// url = url.slice(0, -1);
// 			//url = url.replace(/.$/, ''); //url 마지막 문자(&) 제거
// 		}
// 	}
// 	if (url.substr(-1) == '&') {
// 		//url 마지막 문자가 '&' 일 경우
// 		url = url.replace(/.$/, ''); //url 마지막 문자(&) 제거
// 	}

// 	//헤더 JSON 형태로 변환
// 	let headerData = {};
// 	for (const tr of header) {
// 		const inputs = tr.querySelectorAll('input');
// 		const key = inputs[0].value;
// 		const value = inputs[1].value;

// 		if (key == '' && value == '') {
// 		} else {
// 			headerData[key] = value;
// 		}
// 	}
// 	if (headerData == {}) {
// 		headerData = null;
// 	}
// 	headerData = JSON.stringify(headerData);

// 	const bodyTypeInput = document.querySelectorAll(
// 		'article#apiTest li.apiBody .bodyType .radioDiv input'
// 	);

// 	let bodyData = {};
// 	bodyData['headerData'] = headerData;
// 	if (apiMethod == 'POST') {
// 		//바디 JSON 형태 string으로 변환
// 		bodyData = {};
// 		for (const tr of body) {
// 			const inputs = tr.querySelectorAll('input');
// 			const key = inputs[0].value;
// 			const value = inputs[1].value;

// 			if (key == '' && value == '') {
// 			} else {
// 				bodyData[key] = value;
// 			}
// 		}
// 	}

// 	let bodyDataHTML = {};
// 	for (let key in bodyData) {
// 		let value = bodyData[key];
// 		if (key !== 'headerData') {
// 			bodyDataHTML[key] = value;
// 		}
// 	}

// 	const requestBox = document.querySelector('article#apiTest .request .requestBox pre');
// 	requestBox.innerHTML =
// 		'<strong>URL</strong>  ' +
// 		url +
// 		'\n' +
// 		'<strong>Method</strong>  ' +
// 		apiMethod +
// 		'\n' +
// 		'<strong>Header</strong>  ' +
// 		JSON.stringify(headerData).replace(/\\/g, '') +
// 		'\n' +
// 		'<strong>Body</strong>  ' +
// 		JSON.stringify(bodyDataHTML);

// 	//fetch
// 	fetch(url, {
// 		method: 'POST',
// 		body: JSON.stringify(bodyData),
// 	})
// 		.then((response) => response.text())
// 		.then((data) => {
// 			IsJson(data) == true
// 				? (responseBox.innerHTML = JSON.stringify(JSON.parse(data), null, 2))
// 				: (responseBox.innerHTML = data);
// 		});
// });

// const apiMethod = document.querySelector('article#apiTest .method select');
// apiMethod.addEventListener('change', () => {
// 	if (apiMethod.value == 'GET') {
// 		tagBoxLi = document.querySelectorAll('article#apiTest .rightBox ul.tagBox>li');
// 		tagBody = document.querySelector('article#apiTest .rightBox ul.tagBox>li.tagBody');

// 		tagBoxLi[0].click();
// 		tagBody.style.display = 'none';
// 	} else {
// 		tagBody = document.querySelector('article#apiTest .rightBox ul.tagBox>li.tagBody');
// 		tagBody.style.display = 'block';
// 	}
// });

function delThisTr() {
	let target = event.target;
	let td = target.parentNode.parentNode.querySelectorAll('td');
	td.forEach((el, idx) => {
		if (idx !== 3) {
			el.querySelector('input').value = '';
		}
	});
}

function apiTestInfo(data) {
	//초기화
	let allTdRemove = document.querySelectorAll(
		'article#apiTest ul.apiInputBox table tbody tr td i.fa-trash-can'
	);
	allTdRemove.forEach((el) => {
		el.click();
	});

	const methodSelect = document.querySelector('article#apiTest .method select#method');
	const urlInput = document.querySelector('article#apiTest .method input');
	const paramTbody = document.querySelector('article#apiTest li.apiParam table tbody');
	const headerTbody = document.querySelector('article#apiTest li.apiHeader table tbody');
	const bodyTbody = document.querySelector('article#apiTest li.apiBody table tbody');

	methodSelect.value = data.method;
	if (data.method == 'GET') {
		let tagBoxLi = document.querySelectorAll('article#apiTest .rightBox ul.tagBox>li');
		let tagBody = document.querySelector('article#apiTest .rightBox ul.tagBox>li.tagBody');

		tagBoxLi[0].click();
		tagBody.style.display = 'none';
	} else {
		let tagBody = document.querySelector('article#apiTest .rightBox ul.tagBox>li.tagBody');
		tagBody.style.display = 'block';
	}

	urlInput.value = data.url;
	let testHeader = JSON.parse(data.test_header);
	let testBody = data.test_body !== '' ? JSON.parse(data.test_body) : null;

	let headerArr = [];
	for (let key in testHeader) {
		let value = testHeader[key];
		headerArr.push({
			key: key,
			value: value,
		});
	}

	for (let i = 0; i < headerArr.length; i++) {
		let headerTr = headerTbody.querySelectorAll('tr');
		let headerTd = headerTr[i].querySelectorAll('td');
		headerTd[0].querySelector('input').value = headerArr[i].key;
		headerTd[1].querySelector('input').value = headerArr[i].value;
		addrow('apiHeader');
	}

	let bodyArr = [];
	for (let key in testBody) {
		let value = testBody[key];
		bodyArr.push({
			key: key,
			value: value,
		});
	}

	for (let i = 0; i < bodyArr.length; i++) {
		let bodyTr = bodyTbody.querySelectorAll('tr');
		let bodyTd = bodyTr[i].querySelectorAll('td');
		bodyTd[0].querySelector('input').value = bodyArr[i].key;
		bodyTd[1].querySelector('input').value = bodyArr[i].value;
		addrow('apiBody');
	}
}

function addrow(tag) {
	const tbody = document.querySelector(
		'.flexBox .rightBox ul.apiInputBox li.' + tag + ' table tbody'
	);
	let newTr = createElement('tr');
	tbody.appendChild(newTr);

	let newTd1 = createElement('td');
	let newInput1 = createElement('input');
	newInput1.setAttribute('type', 'text');
	newInput1.setAttribute('placeholder', 'Key');
	newTd1.appendChild(newInput1);
	newTr.appendChild(newTd1);

	let newTd2 = createElement('td');
	let newInput2 = createElement('input');
	newInput2.setAttribute('type', 'text');
	newInput2.setAttribute('placeholder', 'Value');
	newTd2.appendChild(newInput2);
	newTr.appendChild(newTd2);

	let newTd3 = createElement('td');
	let newInput3 = createElement('input');
	newInput3.setAttribute('type', 'text');
	newInput3.setAttribute('placeholder', 'Description');
	newTd3.appendChild(newInput3);
	newTr.appendChild(newTd3);

	let newTd4 = createElement('td');
	let newI = createElement('i');
	newI.classList.add('fa-solid');
	newI.classList.add('fa-trash-can');
	newI.setAttribute('onClick', 'this.parentNode.parentNode.remove();');
	newTd4.appendChild(newI);
	newTr.appendChild(newTd4);

	let autoAddRow = function () {
		const inputs = document.querySelectorAll(
			'article#apiTest ul.apiInputBox li table tbody tr td input'
		);
		inputs.forEach((el) => {
			el.addEventListener('keyup', () => {
				if (el.value !== '') {
					let allTr = el.parentNode.parentNode.parentNode.querySelectorAll('tr');
					let currTr = el.parentNode.parentNode;
					allTr.forEach((el2, idx2) => {
						if (el2 == currTr && idx2 == allTr.length - 1) {
							el.parentNode.parentNode.parentNode.parentNode.parentNode
								.querySelector('button#newTd')
								.click();
						}
					});
				}
			});
		});
	};
	autoAddRow();
}
