import { errorHandling } from './import/ErrorHandler';
import { searchParam } from './import/SearchParam';

const textarea = document.querySelector('textarea#ckEditor');
let curr_menu_seq_no = searchParam('menu_seq_no');
fetch(`/api/v1/manual/manualList?page=1&page_limit=9999`)
	.then((res) => res.json())
	.then((data) => {
		if (data.result_code == 'P000') {
			let manual_content = decodeURIComponent(whatIsCurrManual(data.list));

			ClassicEditor.create(document.querySelector(`#ckEditor`), {
				toolbar: null,
			}).then((newEditor) => {
				newEditor.setData(manual_content);
				newEditor.enableReadOnlyMode(`#ckEditor`);
			});

			textarea.style.display = 'block';
		} else {
			errorHandling(data);
		}
	});

function whatIsCurrManual(manualList) {
	let manual_content;
	manualList.forEach((el) => {
		let menu_seq_no = el.menu_seq_no;
		if (menu_seq_no == curr_menu_seq_no) {
			manual_content = el.manual_content;
		} else {
			manual_content = '<h5>아직 매뉴얼이 작성되지 않았습니다.</h5>';
		}
	});
	return manual_content;
}
