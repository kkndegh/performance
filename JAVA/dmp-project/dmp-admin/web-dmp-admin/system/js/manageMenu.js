import { createElement } from './import/CreateElement';
import { insertParam } from './import/InsertParam';
import { loading, loading_end } from './import/Loading';
import { alertShow, cancelAlert, modalShow } from './import/ModalShow';
import { target_height } from './import/ResizeObserver';
import { searchParam } from './import/SearchParam';

let iconArr = [
	'fa-solid fa-chalkboard',
	'fa-solid fa-diagram-project',
	'fa-solid fa-message',
	'fa-solid fa-money-check-dollar',
	'fa-solid fa-right-left',
	'fa-regular fa-calendar',
	'fa-solid fa-gears',
	'fa-solid fa-chart-pie',
	'fa-solid fa-calculator',
	'fa-solid fa-gear',
	'fa-solid fa-user-gear',
	'fa-solid fa-users-gear',
	'fa-solid fa-server',
	'fa-solid fa-hard-drive',
	'fa-solid fa-compact-disc',
	'fa-solid fa-floppy-disk',
	'fa-solid fa-satellite-dish',
	'fa-solid fa-desktop',
	'fa-solid fa-code',
	'fa-solid fa-terminal',
	'fa-solid fa-file-code',
	'fa-solid fa-database',
	'fa-solid fa-layer-group',
	'fa-solid fa-user',
	'fa-solid fa-users',
	'fa-solid fa-user-shield',
	'fa-solid fa-circle-user',
	'fa-solid fa-fingerprint',
	'fa-solid fa-address-card',
	'fa-solid fa-location-crosshairs',
	'fa-solid fa-location-dot',
	'fa-solid fa-location-arrow',
	'fa-solid fa-map',
	'fa-solid fa-map-location',
	'fa-solid fa-microphone',
	'fa-solid fa-headphones',
	'fa-solid fa-music',
	'fa-solid fa-volume-high',
	'fa-solid fa-circle-notch',
	'fa-solid fa-hashtag',
	'fa-solid fa-paintbrush',
	'fa-solid fa-paint-roller',
	'fa-solid fa-feather-pointed',
	'fa-solid fa-chart-bar',
	'fa-solid fa-chart-column',
	'fa-solid fa-chart-line',
	'fa-solid fa-flask',
	'fa-solid fa-flask-vial',
	'fa-solid fa-folder',
	'fa-solid fa-folder-closed',
	'fa-solid fa-folder-open',
	'fa-solid fa-folder-tree',
	'fa-solid fa-font',
	'fa-solid fa-inbox',
	'fa-solid fa-keyboard',
	'fa-solid fa-puzzle-piece',
];

loading();

async function menuListCall() {
	let result = await fetch('/api/v1/menu/menuList').then((res) => res.json());
	if (result) {
		loading_end();
		if (result['result_code'] == 'P000') {
			menuListDivMaker(result);
			menuClick(result);
			sortable();
			modifyMenuOrder(result);
			addNewMenuPopup(result);
			paramAutoClick();
		} else {
			console.error(`${result['result_code']} : ${result['result_msg']}`);
		}
	}

	function menuListDivMaker(data) {
		const menuGroupCount = document.querySelector(
			'.menuListTopDiv .totalCount p.menuGroupCount strong'
		);
		const menuCount = document.querySelector('.menuListTopDiv .totalCount p.menuCount strong');
		const menuListDiv = document.querySelector('.menuListDiv');
		let menu_group = data['menu_group'];
		let menu = data['menu'];

		menuGroupCount.innerHTML = menu_group.length;
		menuCount.innerHTML = menu.length;

		menu_group.forEach((group) => {
			let div = createElement('div', {
				class: 'menuGroupDiv depth1',
				data: {
					menu_group_seq_no: group['menu_group_seq_no'],
				},
			});

			if (group['menu_group_view_yn'] == 'N') {
				div.classList.add('unActivated');
			}
			let div2 = createElement('div', {
				class: 'menuGroupTitle depth1Title',
			});
			let i = createElement('i', {
				class: 'fa-solid fa-list',
			});
			let p = createElement('p', {
				html: group['menu_group_name'],
			});
			div2.append(i, p);
			let ul = createElement('ul', {
				class: 'depth2',
			});
			div.append(div2, ul);
			menuListDiv.append(div);
		});

		let nonMenuDiv = createElement('div', {
			class: 'menuGroupDiv depth1',
			data: {
				menu_group_seq_no: 'null',
			},
		});
		let div2 = createElement('div', {
			class: 'menuGroupTitle depth1Title',
		});
		let i = createElement('i', {
			class: 'fa-solid fa-list',
		});
		let p = createElement('p', {
			html: '메뉴에 속하지 않은 페이지',
		});
		div2.append(i, p);
		let ul = createElement('ul', {
			class: 'depth2',
		});
		nonMenuDiv.append(div2, ul);
		menuListDiv.append(nonMenuDiv);

		menu.forEach((menu) => {
			let li = createElement('li', {
				data: {
					menu_seq_no: menu['menu_seq_no'],
				},
			});
			if (menu['menu_use_yn'] == 'N') {
				li.classList.add('unActivated');
			}
			let p = createElement('p', {
				html: menu['menu_name'],
			});
			li.append(p);
			if (menu['menu_group_seq_no'] == null) {
				document
					.querySelector(`.menuGroupDiv[data-menu_group_seq_no="null"] ul`)
					.append(li);
			} else {
				document
					.querySelector(
						`.menuGroupDiv[data-menu_group_seq_no="${menu['menu_group_seq_no']}"] ul`
					)
					.append(li);
			}
		});

		const unAvtivatedShow = document.querySelector(
			'article.menuStructure .flexBox .leftBox .viewOption .checkBoxDiv input'
		);
		unAvtivatedShow.addEventListener('click', () => {
			if (unAvtivatedShow.checked) {
				document
					.querySelectorAll(
						'article.menuStructure .flexBox .leftBox .menuListDiv .menuGroupDiv.unActivated'
					)
					.forEach((el) => {
						el.classList.add('show');
					});

				document
					.querySelectorAll(
						'article.menuStructure .flexBox .leftBox .menuListDiv .menuGroupDiv ul li.unActivated'
					)
					.forEach((el) => {
						el.classList.add('show');
					});
			} else {
				document
					.querySelectorAll(
						'article.menuStructure .flexBox .leftBox .menuListDiv .menuGroupDiv.unActivated'
					)
					.forEach((el) => {
						el.classList.remove('show');
					});
				document
					.querySelectorAll(
						'article.menuStructure .flexBox .leftBox .menuListDiv .menuGroupDiv ul li.unActivated'
					)
					.forEach((el) => {
						el.classList.remove('show');
					});
			}
		});
	}

	function menuClick(data) {
		let menu_group = data['menu_group'];
		let menu = data['menu'];
		document.querySelectorAll('.menuGroupDiv .menuGroupTitle').forEach((el, idx, arg) => {
			el.addEventListener('click', () => {
				let key_kor = {
					menu_group_seq_no: '메뉴그룹 시퀀스 넘버',
					menu_group_name: '메뉴그룹 이름',
					menu_group_content: '내용',
					menu_group_view_yn: '메뉴에 노출여부',
					menu_group_icon: '메뉴그룹 아이콘',
					reg_id: '등록자',
					reg_ts: '등록시간',
				};
				document.querySelector('.menuGroupArea .tableBottomDiv').classList.add('active');
				for (let group of menu_group) {
					if (el.parentNode.dataset.menu_group_seq_no == group['menu_group_seq_no']) {
						insertParam('menuGroup', group['menu_group_seq_no']);
						arg.forEach((el2) => el2.classList.remove('point'));
						el.classList.add('point');
						let infoTbody = document.querySelector(
							'.rightBox .menuGroupArea .tableDiv table tbody'
						);
						if (infoTbody.querySelectorAll('tr')) {
							infoTbody.querySelectorAll('tr').forEach((el2) => el2.remove());
						}
						for (let key in group) {
							let tr = createElement('tr');
							let th = createElement('th', {
								html: key_kor[key],
							});
							let td = createElement('td');
							if (key == 'menu_group_seq_no' || key == 'reg_id' || key == 'reg_ts') {
								let input = createElement('input', {
									id: key,
									value: group[key] ?? '',
									readonly: true,
								});
								td.append(input);
							} else if (key == 'menu_group_view_yn') {
								let toggle = createElement('toggle', { id: key });
								td.append(toggle);
								if (group[key] == 'Y') {
									toggle.querySelector('input').checked = true;
								} else {
									toggle.querySelector('input').checked = false;
								}
							} else if (key == 'menu_group_icon') {
								function iconShow(showIcon, btn, iconList) {
									btn.addEventListener('click', () => {
										if (iconList.classList.contains('active') == false) {
											iconList.classList.add('active');
											btn.innerHTML = '아이콘 리스트 닫기';
										} else {
											iconList.classList.remove('active');
											btn.innerHTML = '아이콘 리스트 열기';
										}
									});
									const icon = iconList.querySelectorAll('i');
									icon.forEach((el) => {
										el.addEventListener('click', () => {
											showIcon.className = '';
											showIcon.className = el.className;
											iconList.classList.remove('active');
											btn.innerHTML = '아이콘 리스트 열기';
										});
									});
								}
								let i = createElement('i', {
									class: group[key],
									id: key,
								});
								let button = createElement('button', {
									class: 'btn_style_1 btn_color_point',
									html: '아이콘 리스트 열기',
								});
								td.append(i, button);
								let div = createElement('div', {
									class: 'iconList',
								});
								td.append(div);
								for (let icon of iconArr) {
									let i = createElement('i', {
										class: icon,
									});
									div.append(i);
								}
								iconShow(i, button, div);
							} else {
								let input = createElement('input', {
									id: key,
									value: group[key] ?? '',
								});
								td.append(input);
							}
							tr.append(th, td);
							infoTbody.append(tr);
						}
					}
				}
				modifyMenuGroup();
				deleteMenuGroup(menu);
			});
		});
		document.querySelectorAll('.menuGroupDiv ul li').forEach((el, idx, arg) => {
			el.addEventListener('click', () => {
				let key_kor = {
					menu_group_seq_no: '상위 메뉴그룹 시퀀스 넘버',
					menu_seq_no: '메뉴 시퀀스 넘버',
					menu_name: '메뉴 이름',
					menu_content: '내용',
					menu_link_url: 'URL',
					menu_use_yn: '활성화 여부',
					menu_view_yn: '메뉴에 표시 여부',
					menu_func: '기능',
					menu_class: '영문 메뉴명(Camel Case)',
					reg_id: '등록자',
					reg_ts: '등록 시간',
				};
				document.querySelector('.menuArea .tableBottomDiv').classList.add('active');
				for (let eachMenu of menu) {
					if (el.dataset.menu_seq_no == eachMenu['menu_seq_no']) {
						insertParam('menu', eachMenu['menu_seq_no']);
						arg.forEach((el2) => el2.classList.remove('point'));
						el.classList.add('point');
						let infoTbody = document.querySelector(
							'.rightBox .menuArea .tableDiv table tbody'
						);
						if (infoTbody.querySelectorAll('tr')) {
							infoTbody.querySelectorAll('tr').forEach((el2) => el2.remove());
						}
						for (let key in eachMenu) {
							let tr = createElement('tr');
							let th = createElement('th', {
								html: key_kor[key],
							});
							let td = createElement('td');
							if (key == 'menu_auth_yn' || key == 'api') {
								continue;
							} else if (key == 'menu_group_seq_no') {
								let input = createElement('input', {
									id: key,
									value: eachMenu[key] ?? '',
									readonly: true,
								});
								let upperGroupName;
								menu_group.forEach((el2) => {
									if (el2['menu_group_seq_no'] == eachMenu[key]) {
										upperGroupName = el2['menu_group_name'];
									}
									if (upperGroupName == undefined) {
										upperGroupName = '메뉴에 속하지 않은 페이지';
									}
								});
								let p = createElement('p', {
									html: upperGroupName,
								});
								td.append(input, p);
							} else if (key == 'menu_seq_no' || key == 'reg_id' || key == 'reg_ts') {
								let input = createElement('input', {
									id: key,
									value: eachMenu[key] ?? '',
									readonly: true,
								});
								td.append(input);
							} else if (key == 'menu_view_yn' || key == 'menu_use_yn') {
								let toggle = createElement('toggle', { id: key });
								td.append(toggle);
								if (eachMenu[key] == 'Y') {
									toggle.querySelector('input').checked = true;
								} else {
									toggle.querySelector('input').checked = false;
								}
							} else if (key == 'menu_func') {
								let temp_funcData = ['read', 'write', 'excel', 'print'];
								temp_funcData.forEach((el2) => {
									let checkbox = createElement('checkbox', {
										id: el2,
										html: el2,
									});
									td.append(checkbox);
								});
							} else {
								let input = createElement('input', {
									id: key,
									value: eachMenu[key] ?? '',
								});
								td.append(input);
							}

							tr.append(th, td);
							infoTbody.append(tr);
						}
					}
				}
				modifyMenu();
				deleteMenu();
			});
		});
	}

	function sortable() {
		new Sortable(document.querySelector('.leftBox .menuListDiv'), {
			handle: '.fa-list',
			animation: 250,
			fallbackOnBody: true,
			swapThreshold: 0.65,
			ghostClass: 'sortable-ghost',
			draggable: '.menuGroupDiv',
		});
		document.querySelectorAll('.leftBox .menuListDiv .menuGroupDiv ul').forEach((el) => {
			new Sortable(el, {
				group: 'li',
				animation: 250,
				fallbackOnBody: true,
				swapThreshold: 0.65,
				selectedClass: 'sortable-selected',
				ghostClass: 'sortable-ghost',
			});
		});
	}
}
menuListCall();

function addNewMenuPopup(menuData) {
	const addMenuBtn = document.querySelector('.menuListTopDiv button#addMenu');
	addMenuBtn.addEventListener('click', () => {
		let obj = {
			title: '새 메뉴 | 메뉴그룹 추가',
			btn: true,
			btnText: '추가하기',
			btnColor: 'point',
			func: popupMaker,
			callback: addMenu,
			refresh: false,
		};
		modalShow(obj);

		function popupMaker() {
			const popupBody = document.querySelector('.modal .popupBody');
			if (popupBody.querySelector('.box')) {
				popupBody.querySelector('.box').remove();
			}
			let box = createElement('div', {
				class: 'box',
			});
			popupBody.append(box);
			let div = createElement('div', {
				class: 'typeDiv',
			});
			let button = createElement('button', {
				class: 'btn_style_1 btn_color_point selected',
				html: '<i class="fa-solid fa-plus"></i> 메뉴그룹 추가',
			});
			let button2 = createElement('button', {
				class: 'btn_style_1 btn_color_point',
				html: '<i class="fa-solid fa-plus"></i> 메뉴 추가',
			});
			div.append(button, button2);
			box.append(div);
			button.addEventListener('click', menuGroupPopupTable);
			button2.addEventListener('click', menuPopupTable);

			function menuGroupPopupTable() {
				button.classList.add('selected');
				button2.classList.remove('selected');
				if (box.querySelector('table')) {
					box.querySelector('table').remove();
				}
				let table = createElement('table');
				let tbody = createElement('tbody');
				table.append(tbody);
				box.append(table);
				let keys = {
					menu_group_name: '메뉴그룹 이름',
					menu_group_content: '내용',
					menu_group_icon: '메뉴그룹 아이콘',
					menu_group_view_yn: '메뉴에 노출 여부',
				};
				let frontId = 'addNew_';
				for (let key in keys) {
					let tr = createElement('tr');
					let th = createElement('th', {
						html: keys[key],
					});
					let td = createElement('td');
					if (key == 'menu_group_icon') {
						function iconShow(showIcon, btn, iconList) {
							btn.addEventListener('click', () => {
								if (iconList.classList.contains('active') == false) {
									iconList.classList.add('active');
									btn.innerHTML = '아이콘 리스트 닫기';
								} else {
									iconList.classList.remove('active');
									btn.innerHTML = '아이콘 리스트 열기';
								}
							});
							const icon = iconList.querySelectorAll('i');
							icon.forEach((el) => {
								el.addEventListener('click', () => {
									showIcon.className = '';
									showIcon.className = el.className;
									iconList.classList.remove('active');
									btn.innerHTML = '아이콘 리스트 열기';
								});
							});
						}
						let i = createElement('i', {
							id: frontId + key,
						});
						let button = createElement('button', {
							class: 'btn_style_1 btn_color_point',
							html: '아이콘 리스트 열기',
						});
						td.append(i, button);
						let div = createElement('div', {
							class: 'iconList',
						});
						td.append(div);
						for (let icon of iconArr) {
							let i = createElement('i', {
								class: icon,
							});
							div.append(i);
						}
						iconShow(i, button, div);
					} else if (key == 'menu_group_view_yn') {
						let toggle = createElement('toggle', {
							id: frontId + key,
						});
						td.append(toggle);
					} else {
						let input = createElement('input', {
							type: 'text',
							id: frontId + key,
						});
						td.append(input);
					}

					tr.append(th, td);
					tbody.append(tr);
				}
			}
			menuGroupPopupTable();

			function menuPopupTable() {
				button2.classList.add('selected');
				button.classList.remove('selected');
				if (box.querySelector('table')) {
					box.querySelector('table').remove();
				}
				let table = createElement('table');
				let tbody = createElement('tbody');
				table.append(tbody);
				box.append(table);
				let keys = {
					menu_group_seq_no: '상위 메뉴그룹',
					menu_name: '메뉴 이름',
					menu_content: '메뉴 설명',
					menu_link_url: 'URL',
					menu_use_yn: '메뉴 활성화 여부',
					menu_view_yn: '메뉴에 노출 여부',
				};
				let frontId = 'addNew_';
				for (let key in keys) {
					let tr = createElement('tr');
					let th = createElement('th', {
						html: keys[key],
					});
					let td = createElement('td');
					if (key == 'menu_use_yn' || key == 'menu_view_yn') {
						let toggle = createElement('toggle', {
							id: frontId + key,
						});
						td.append(toggle);
					} else if (key == 'menu_group_seq_no') {
						let select = createElement('select', {
							id: frontId + key,
						});
						menuData['menu_group'].forEach((el) => {
							let option = createElement('option', {
								value: el['menu_group_seq_no'],
								html: el['menu_group_name'],
							});
							select.append(option);
						});
						let option = createElement('option', {
							value: 'null',
							html: '메뉴에 속하지 않은 페이지',
						});
						select.append(option);
						td.append(select);
					} else {
						let input = createElement('input', {
							type: 'text',
							id: frontId + key,
						});
						td.append(input);
					}
					tr.append(th, td);
					tbody.append(tr);
				}
			}
		}
		function addMenu() {
			const popupBody = document.querySelector('.modal .popupBody');
			const buttons = popupBody.querySelectorAll('.typeDiv button');
			let frontId = 'addNew_';
			if (buttons[0].classList.contains('selected')) {
				let obj = {};
				obj.menu_group_name = popupBody.querySelector(`#${frontId}menu_group_name`).value;
				obj.menu_group_content = popupBody.querySelector(
					`#${frontId}menu_group_content`
				).value;
				obj.menu_group_icon = popupBody.querySelector(
					`#${frontId}menu_group_icon`
				).className;
				obj.menu_group_view_yn = popupBody.querySelector(`#${frontId}menu_group_view_yn`)
					.checked
					? 'Y'
					: 'N';
				obj.reg_id = 'admin'; //임시
				obj.act_evt = 'ins';
				obj = JSON.stringify(obj);
				fetch('/api/v1/menu/menuGroupIUD', {
					method: 'POST',
					body: obj,
				})
					.then((res) => res.json())
					.then((data) => {
						console.log(data);
						location.reload();
					});
			} else if (buttons[1].classList.contains('selected')) {
				let obj = {};
				obj.menu_group_seq_no = popupBody.querySelector(
					`#${frontId}menu_group_seq_no`
				).value;
				obj.menu_name = popupBody.querySelector(`#${frontId}menu_name`).value;
				obj.menu_content = popupBody.querySelector(`#${frontId}menu_content`).value;
				obj.menu_link_url = popupBody.querySelector(`#${frontId}menu_link_url`).value;
				obj.menu_use_yn = popupBody.querySelector(`#${frontId}menu_use_yn`).checked
					? 'Y'
					: 'N';
				obj.menu_view_yn = popupBody.querySelector(`#${frontId}menu_view_yn`).checked
					? 'Y'
					: 'N';
				obj.reg_id = 'admin'; //임시
				obj.act_evt = 'ins';
				obj = JSON.stringify(obj);
				fetch('/api/v1/menu/menuIUD', {
					method: 'POST',
					body: obj,
				})
					.then((res) => res.json())
					.then((data) => {
						console.log(data);
						location.reload();
					});
			}
		}
	});
}

function modifyMenuGroup() {
	document
		.querySelector('.menuGroupArea .tableBottomDiv button.btn_style_2.btn_color_point')
		.addEventListener('click', () => {
			let obj = {
				title: '메뉴그룹 수정',
				btn: true,
				btnText: '수정하기',
				btnColor: 'point',
				text: '수정내용을 적용하시겠습니까?',
				callback: modifyMenuGroupAPI,
			};
			alertShow(obj);

			function modifyMenuGroupAPI() {
				let bodyData = {};
				let keys = [
					'menu_group_seq_no',
					'menu_group_name',
					'menu_group_content',
					'menu_group_view_yn',
					'menu_group_icon',
				].forEach((el) => {
					if (el == 'menu_group_view_yn') {
						let value = document.querySelector(
							`.menuGroupArea .tableDiv table tbody input#${el}`
						).checked
							? 'Y'
							: 'N';

						bodyData[el] = value;
					} else if (el == 'menu_group_icon') {
						let value = document.querySelector(
							`.menuGroupArea .tableDiv table tbody #${el}`
						).className;
						bodyData[el] = value;
					} else {
						let value = document.querySelector(
							`.menuGroupArea .tableDiv table tbody input#${el}`
						).value;
						bodyData[el] = value;
					}
				});
				bodyData['reg_id'] = 'admin'; //임시
				bodyData['act_evt'] = 'upd';
				bodyData = JSON.stringify(bodyData);

				fetch('/api/v1/menu/menuGroupIUD', {
					method: 'POST',
					body: bodyData,
				})
					.then((res) => res.json())
					.then((data) => {
						if (data['result_code'] == 'P000') {
							location.reload();
						} else {
							let obj = {};
							obj.title = '오류';
							obj.btn = false;
							obj.text = data.result_msg;
							obj.text2 = '오류코드 : ' + data.result_code;
							alertShow(obj);
							console.error(data.result_sys_msg);
						}
					});
			}
		});
}

async function deleteMenuGroup(menuData) {
	const menuGroupSeqNo = document.querySelector('.menuGroupArea #menu_group_seq_no').value;
	const menuGroup = document.querySelector('#menu_group_name').value;
	document
		.querySelector('.menuGroupArea .tableBottomDiv button.btn_style_2.btn_color_crush')
		.addEventListener('click', () => {
			let obj = {
				title: '메뉴그룹 삭제',
				btn: true,
				btnText: '삭제하기',
				btnColor: 'crush',
				text: `"${menuGroup}" 메뉴그룹을 삭제하시겠습니까?`,
				text2: `하위 메뉴가 존재할 경우 하위 메뉴를 먼저 삭제해야 합니다. 삭제 이후에는 되돌릴 수 없습니다`,
				callback: delMenuGroupAPI,
			};
			alertShow(obj);

			function delMenuGroupAPI() {
				let flag = true;
				menuData.forEach((el) => {
					if (el['menu_group_seq_no'] == menuGroupSeqNo) {
						flag = false;
					}
				});
				if (flag == true) {
					let bodyData = JSON.stringify({
						menu_group_seq_no: document.querySelector(
							'.menuGroupArea #menu_group_seq_no'
						).value,
						act_evt: 'del',
						reg_id: 'admin', //임시
					});

					fetch('/api/v1/menu/menuGroupIUD', {
						method: 'POST',
						body: bodyData,
					})
						.then((res) => res.json())
						.then((data) => {
							if (data['result_code'] == 'P000') {
								location.reload();
							} else {
								let obj = {};
								obj.title = '오류';
								obj.btn = false;
								obj.text = data.result_msg;
								obj.text2 = '오류코드 : ' + data.result_code;
								alertShow(obj);
								console.error(data.result_sys_msg);
							}
						});
				} else {
					let obj = {
						title: '하위 메뉴 존재',
						btn: false,
						text: `"${menuGroup}" 메뉴그룹에 하위메뉴가 존재합니다.`,
						text2: `메뉴그룹 삭제 전, 하위 메뉴를 먼저 삭제해야 합니다.`,
					};
					alertShow(obj);
				}
			}
		});
}

async function modifyMenu() {
	document
		.querySelector('.menuArea .tableBottomDiv button.btn_style_2.btn_color_point')
		.addEventListener('click', () => {
			let obj = {
				title: '메뉴 수정',
				btn: true,
				btnText: '수정하기',
				btnColor: 'point',
				text: '수정내용을 적용하시겠습니까?',
				callback: modifyMenuAPI,
			};
			alertShow(obj);

			function modifyMenuAPI() {
				let bodyData = {};
				let keys = [
					'menu_group_seq_no',
					'menu_seq_no',
					'menu_name',
					'menu_content',
					'menu_link_url',
					'menu_use_yn',
					'menu_view_yn',
				].forEach((el) => {
					if (el == 'menu_group_seq_no') {
						let value =
							document.querySelector(`.menuArea .tableDiv table tbody input#${el}`)
								.value !== 'null'
								? document.querySelector(
										`.menuArea .tableDiv table tbody input#${el}`
								  ).value
								: null;
						bodyData[el] = value;
					} else if (el == 'menu_view_yn' || el == 'menu_use_yn') {
						let value = document.querySelector(
							`.menuArea .tableDiv table tbody input#${el}`
						).checked
							? 'Y'
							: 'N';

						bodyData[el] = value;
					} else {
						let value = document.querySelector(
							`.menuArea .tableDiv table tbody input#${el}`
						).value;
						bodyData[el] = value;
					}
				});
				bodyData['reg_id'] = 'admin'; //임시
				bodyData['act_evt'] = 'upd';
				bodyData = JSON.stringify(bodyData);

				fetch('/api/v1/menu/menuIUD', {
					method: 'POST',
					body: bodyData,
				})
					.then((res) => res.json())
					.then((data) => {
						if (data['result_code'] == 'P000') {
							location.reload();
						} else {
							let obj = {};
							obj.title = '오류';
							obj.btn = false;
							obj.text = data.result_msg;
							obj.text2 = '오류코드 : ' + data.result_code;
							alertShow(obj);
							console.error(data.result_sys_msg);
						}
					});
			}
		});
}

async function deleteMenu() {
	const menu = document.querySelector('#menu_name').value;
	document
		.querySelector('.menuArea .tableBottomDiv button.btn_style_2.btn_color_crush')
		.addEventListener('click', () => {
			let obj = {
				title: '메뉴 삭제',
				btn: true,
				btnText: '삭제하기',
				btnColor: 'crush',
				text: `"${menu}" 메뉴를 삭제하시겠습니까?`,
				text2: `삭제 이후에는 되돌릴 수 없습니다`,
				callback: delMenuAPI,
			};
			alertShow(obj);

			function delMenuAPI() {
				let bodyData = JSON.stringify({
					menu_seq_no: document.querySelector('.menuArea #menu_seq_no').value,
					act_evt: 'del',
					reg_id: 'admin', //임시
				});

				fetch('/api/v1/menu/menuIUD', {
					method: 'POST',
					body: bodyData,
				})
					.then((res) => res.json())
					.then((data) => {
						if (data['result_code'] == 'P000') {
							location.reload();
						} else {
							let obj = {};
							obj.title = '오류';
							obj.btn = false;
							obj.text = data.result_msg;
							obj.text2 = '오류코드 : ' + data.result_code;
							alertShow(obj);
							console.error(data.result_sys_msg);
						}
					});
			}
		});
}

function modifyMenuOrder(menuData) {
	console.log(menuData);
	document.querySelector('#saveChange').addEventListener('click', () => {
		let obj = {};
		obj.title = '메뉴 순서 수정';
		obj.btn = true;
		obj.btnText = '수정하기';
		obj.btnColor = 'point';
		obj.text = '수정내용을 적용하시겠습니까?';
		obj.callback = modMenuOrder;
		alertShow(obj);

		function modMenuOrder() {
			const menu = document.querySelectorAll('.leftBox > div > .menuListDiv > div > ul > li');
			menu.forEach((el, idx) => {
				let after_menu_group_seq_no = el.parentNode.parentNode.dataset.menu_group_seq_no;
				let before_menu_group_seq_no;
				menuData['menu'].forEach((el2) => {
					if (el2['menu_seq_no'] == el.dataset.menu_seq_no) {
						before_menu_group_seq_no = el2['menu_group_seq_no'];
					}
				});
				if (before_menu_group_seq_no == null) {
					before_menu_group_seq_no = 'null';
				}

				if (before_menu_group_seq_no == after_menu_group_seq_no) {
					cancelAlert();
				} else {
					let thisMenuData;
					menuData['menu'].forEach((el2) => {
						if (el2['menu_seq_no'] == el.dataset.menu_seq_no) {
							thisMenuData = el2;
						}
					});
					delete thisMenuData['api'];
					delete thisMenuData['menu_class'];
					delete thisMenuData['menu_auth_yn'];
					delete thisMenuData['reg_ts'];
					thisMenuData['menu_group_seq_no'] = after_menu_group_seq_no;
					thisMenuData['act_evt'] = 'upd';
					thisMenuData['reg_id'] = g_user_id;
					let bodyData = JSON.stringify(thisMenuData);
					fetch('/api/v1/menu/menuIUD', {
						method: 'POST',
						body: bodyData,
					})
						.then((response) => response.json())
						.then((data) => {
							if (data.result_code !== 'P000') {
								let obj = {};
								obj.title = '오류';
								obj.btn = false;
								obj.text = data.result_msg;
								obj.text2 = '오류코드 : ' + data.result_code;
								alertShow(obj);
								console.error(data.result_sys_msg);
							} else {
								cancelAlert();
							}
						});
				}
			});
		}
	});
}

function paramAutoClick() {
	let menu = searchParam('menu');
	let menuGroup = searchParam('menuGroup');
	if (menuGroup) {
		document.querySelectorAll('.menuListDiv .menuGroupDiv').forEach((el) => {
			if (el.dataset.menu_group_seq_no == menuGroup) {
				el.querySelector('.menuGroupTitle ').click();
			}
		});
	}
	if (menu) {
		document.querySelectorAll('.leftBox .menuListDiv ul > li').forEach((el) => {
			if (el.dataset.menu_seq_no == menu) {
				el.click();
			}
		});
	}
}
