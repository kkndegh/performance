import { searchParam } from './import/SearchParam.js';
import { insertParam } from './import/InsertParam.js';
import { loading } from './import/Loading.js';
import { loading_end } from './import/Loading.js';
import { leftListOpen, leftMenuUnactivate } from './import/LayoutControl.js';

const postalCodeBtn = document.querySelector('.postal_code .fa-solid.fa-magnifying-glass');
postalCodeBtn.addEventListener('click', () => {
	new daum.Postcode({
		oncomplete: function (data) {
			const postal_code_input = document.querySelector('td.postal_code input[type="text"]');
			postal_code_input.value = data.zonecode;
			const address_input_first = document.querySelectorAll('td.location_address input')[0];
			address_input_first.value = data.address;
		},
	}).open();
});

function moreInfo() {
	const moreInfoBtn_userInfo = document.querySelector(
		'article.회원정보 table tr th span.moreInfo'
	);
	const hideTr_userInfo = document.querySelectorAll('article.회원정보 table tr.hide');
	let moreInfoFlag_userInfo = false;

	if (moreInfoBtn_userInfo.clickHandler) {
		moreInfoBtn_userInfo.removeEventListener('click', moreInfoBtn_userInfo.clickHandler);
	}
	moreInfoBtn_userInfo.clickHandler = () => {
		if (moreInfoFlag_userInfo == false) {
			moreInfoFlag_userInfo = true;
			moreInfoBtn_userInfo.innerHTML = '<i class="fa-solid fa-caret-up"></i> 접기';
			moreInfoBtn_userInfo.classList.add('crush');
			hideTr_userInfo.forEach((el) => {
				el.classList.remove('hide');
			});
		} else {
			moreInfoFlag_userInfo = false;
			moreInfoBtn_userInfo.innerHTML = '<i class="fa-solid fa-caret-down"></i> 더보기';
			moreInfoBtn_userInfo.classList.remove('crush');
			hideTr_userInfo.forEach((el) => {
				el.classList.add('hide');
			});
		}
	};
	moreInfoBtn_userInfo.addEventListener('click', moreInfoBtn_userInfo.clickHandler);

	const moreInfoBtn_processHis = document.querySelector(
		'article.진행정보히스토리 table tr th span.moreInfo'
	);
	const hideTr_processHis = document.querySelectorAll('article.진행정보히스토리 table tr.hide');
	let moreInfoFlag_processHis = false;

	if (moreInfoBtn_processHis.clickHandler) {
		moreInfoBtn_processHis.removeEventListener('click', moreInfoBtn_processHis.clickHandler);
	}

	moreInfoBtn_processHis.clickHandler = () => {
		if (moreInfoFlag_processHis == false) {
			moreInfoFlag_processHis = true;
			moreInfoBtn_processHis.innerHTML = '<i class="fa-solid fa-caret-up"></i> 접기';
			moreInfoBtn_processHis.classList.add('crush');
			hideTr_processHis.forEach((el) => {
				el.classList.remove('hide');
			});
		} else {
			moreInfoFlag_processHis = false;
			moreInfoBtn_processHis.innerHTML = '<i class="fa-solid fa-caret-down"></i> 더보기';
			moreInfoBtn_processHis.classList.remove('crush');
			hideTr_processHis.forEach((el) => {
				el.classList.add('hide');
			});
		}
	};

	moreInfoBtn_processHis.addEventListener('click', moreInfoBtn_processHis.clickHandler);

	const moreInfoBtn_registInfo = document.querySelector(
		'article.신청정보 table tr th span.moreInfo'
	);
	const hideTr_registInfo = document.querySelectorAll('article.신청정보 table tr.hide');
	let moreInfoFlag_registInfo = false;
	moreInfoBtn_registInfo.addEventListener('click', () => {
		if (moreInfoFlag_registInfo == false) {
			moreInfoFlag_registInfo = true;
			moreInfoBtn_registInfo.innerHTML = '<i class="fa-solid fa-caret-up"></i> 접기';
			moreInfoBtn_registInfo.classList.add('crush');
			hideTr_registInfo.forEach((el) => {
				el.classList.remove('hide');
			});
		} else {
			moreInfoFlag_registInfo = false;
			moreInfoBtn_registInfo.innerHTML = '<i class="fa-solid fa-caret-down"></i> 더보기';
			moreInfoBtn_registInfo.classList.remove('crush');
			hideTr_registInfo.forEach((el) => {
				el.classList.add('hide');
			});
		}
	});
}

function allMoreInfo() {
	const allMoreInfo = document.querySelector('button.allMoreInfo');
	allMoreInfo.addEventListener('click', () => {
		const moreInfoBtns = document.querySelectorAll('span.moreInfo');
		if (allMoreInfo.dataset.btnState !== 'crush') {
			allMoreInfo.dataset.btnState = 'crush';
			allMoreInfo.className = 'allMoreInfo btn_style_1 btn_color_crush';
			allMoreInfo.innerHTML = '<i class="fa-solid fa-arrows-up-to-line"></i> 모두 접기';
			moreInfoBtns.forEach((el) => {
				if (!el.classList.contains('crush')) {
					el.click();
				}
			});
		} else {
			allMoreInfo.dataset.btnState = '';
			allMoreInfo.className = 'allMoreInfo btn_style_1 btn_color_point';
			allMoreInfo.innerHTML = '<i class="fa-solid fa-arrows-down-to-line"></i> 모두 펼치기';
			moreInfoBtns.forEach((el) => {
				if (el.classList.contains('crush')) {
					el.click();
				}
			});
		}
	});
}
allMoreInfo();

function scrollEventFunc() {
	const section = document.querySelector('section');
	const articles = document.querySelectorAll('article');
	const tabList = document.querySelectorAll('ul.tabList li');

	tabList.forEach((el, idx) => {
		el.addEventListener('click', () => {
			let offsetTop = articles[idx].offsetTop;
			section.scrollTo({
				top: offsetTop - 50,
				behavior: 'smooth',
			});
		});
	});

	section.addEventListener('scroll', () => {
		let scrollTop = section.scrollTop + 69;
		let scrollBottom = section.scrollHeight - (section.scrollTop + section.offsetHeight);
		articles.forEach((el, idx) => {
			let offsetTop = el.offsetTop;
			let height = el.offsetHeight;
			if (offsetTop <= scrollTop && scrollTop <= offsetTop + height && scrollBottom !== 0) {
				tabList.forEach((el) => el.classList.remove('active'));
				tabList[idx].classList.add('active');
			}
			if (scrollBottom == 0) {
				tabList.forEach((el) => el.classList.remove('active'));
				tabList[tabList.length - 1].classList.add('active');
			}
		});
	});
}
scrollEventFunc();

//aside

// function loan_info_detail(data) {
// 	let loan_data = data['data'];
// 	let history = loan_data['history'];
// 	let member_info = loan_data['member_info'];
// 	let apply_info = loan_data['apply_info'];
// 	let loan_info = loan_data['loan_info'];
// 	let contract_info = loan_data['contract_info'];
// 	let collateral = loan_data['collateral'];
// 	let existing_loan = loan_data['existing_loan'];

// 	//진행상태
// 	document.querySelector('article.진행상태 .processFlex strong').innerHTML =
// 		apply_info['진행상태'];

// 	//진행정보 히스토리
// 	document
// 		.querySelectorAll('article.진행정보히스토리 table tbody tr')
// 		.forEach((el) => el.remove());
// 	history.forEach((el, idx) => {
// 		if (idx == 3) {
// 			let newTr = document.createElement('tr');
// 			let newTh = document.createElement('th');
// 			newTh.setAttribute('colspan', '5');
// 			newTh.classList.add('moreInfoTh');
// 			let newSpan = document.createElement('span');
// 			newSpan.classList.add('moreInfo');
// 			newSpan.innerHTML = '<i class="fa-solid fa-caret-down"></i> 더보기';
// 			newTh.appendChild(newSpan);
// 			newTr.appendChild(newTh);
// 			document.querySelector('article.진행정보히스토리 table tbody').appendChild(newTr);
// 		}
// 		let newTr = document.createElement('tr');
// 		if (idx >= 3) {
// 			newTr.classList.add('hide');
// 		}
// 		document.querySelector('article.진행정보히스토리 table tbody').appendChild(newTr);
// 		for (let key in el) {
// 			let value = el[key];
// 			let newTd = document.createElement('td');
// 			newTd.innerHTML = value;
// 			newTr.appendChild(newTd);
// 		}
// 	});

// 	//회원정보
// 	for (let sort in member_info) {
// 		let info = member_info[sort];
// 		for (let key in info) {
// 			let value = info[key] ?? '';

// 			if (key == '사업자구분') {
// 				document
// 					.querySelectorAll(`article.회원정보 table tbody tr.${key} td select option`)
// 					.forEach((el) => {
// 						if (el.value == value) {
// 							el.selected = true;
// 						}
// 					});
// 			} else if (
// 				key == '대표전화번호' ||
// 				key == '대표자핸드폰번호' ||
// 				key == '업태' ||
// 				key == '홈페이지주소' ||
// 				key == '우편번호' ||
// 				key == '설립일자' ||
// 				key == 'SMS번호'
// 			) {
// 				document.querySelector(`article.회원정보 table tbody tr.${key} td input`).value =
// 					value;
// 			} else if (key == '주소' || key == '주소2') {
// 				if (key == '주소') {
// 					document.querySelectorAll(
// 						`article.회원정보 table tbody tr.주소 td input`
// 					)[0].value = value;
// 				} else {
// 					document.querySelectorAll(
// 						`article.회원정보 table tbody tr.주소 td input`
// 					)[1].value = value;
// 				}
// 			} else if (key == '가입경로' || key == '업종' || key == '접수코드') {
// 				document
// 					.querySelectorAll(`article.회원정보 table tbody tr.${key} td select option`)
// 					.forEach((el) => {
// 						if (el.value == value) {
// 							el.selected = true;
// 						}
// 					});
// 			} else {
// 				document.querySelector(`article.회원정보 table tbody tr.${key} td`).innerHTML =
// 					value;
// 			}
// 		}
// 	}

// 	//신청정보
// 	for (let key in apply_info) {
// 		let value = apply_info[key] ?? '';

// 		if (key == '대출상품') {
// 			document
// 				.querySelectorAll(`article.신청정보 table tbody tr.${key} td select option`)
// 				.forEach((el) => {
// 					if (el.value == value) {
// 						el.selected = true;
// 					}
// 				});
// 		} else if (key == '신청상호명') {
// 			document.querySelector(`article.신청정보 table tbody tr.${key} td input`).value = value;
// 		} else if (key == '대출사유' || key == '참고사항') {
// 			document.querySelector(`article.신청정보 table tbody tr.${key} td textarea`).value =
// 				value;
// 		} else {
// 			document.querySelector(`article.신청정보 table tbody tr.${key} td`).innerHTML = value;
// 		}
// 	}
// }

// function call_member_loan_info() {
// 	loading();
// 	// document.querySelectorAll('aside .leftList .listTableBox table tbody tr').forEach((el) => {
// 	// 	el.remove();
// 	// });
// 	fetch('/js/dummy_all_data.json')
// 		.then((res) => res.json())
// 		.then((data) => {
// 			loading_end();
// 			let loanList = data.loan_list;
// 			loanList.forEach((el) => {
// 				// let date = el.date;
// 				// let amount = el.amount;
// 				// let manager = el.manager;

// 				// let newTd = document.createElement('td');
// 				// newTd.innerHTML = date;
// 				// let newTd2 = document.createElement('td');
// 				// newTd2.innerHTML = amount;
// 				// let newTd3 = document.createElement('td');
// 				// newTd3.innerHTML = manager;

// 				// let newTr = document.createElement('tr');
// 				// newTr.dataset.seqNo = el.seq_no;
// 				// newTr.appendChild(newTd);
// 				// newTr.appendChild(newTd2);
// 				// newTr.appendChild(newTd3);

// 				// document
// 				// 	.querySelector('aside .leftList .listTableBox table tbody')
// 				// 	.appendChild(newTr);

// 				// newTr.addEventListener('click', () => {
// 				const allMoreInfo = document.querySelector('button.allMoreInfo');
// 				if (allMoreInfo.dataset.btnState == 'crush') {
// 					allMoreInfo.click();
// 				}
// 				const moreInfoBtns = document.querySelectorAll('span.moreInfo');
// 				moreInfoBtns.forEach((el) => {
// 					if (el.classList.contains('crush')) {
// 						el.click();
// 					}
// 				});
// 				insertParam(
// 					'member_seq_no',
// 					document.querySelector('aside .rightList ul li.clicked').dataset.seqNo
// 				);
// 				insertParam('loan_seq_no', el.seq_no);
// 				loan_info_detail(el);
// 				moreInfo();
// 				leftMenuUnactivate();
// 				// });
// 				if (searchParam('loan_seq_no') !== '') {
// 					if (searchParam('loan_seq_no') == el.seq_no) {
// 						loan_info_detail(el);
// 						moreInfo();
// 						newTr.classList.add('clicked');
// 					}
// 				}
// 			});
// 			document
// 				.querySelectorAll('aside .leftList .listTableBox table tbody tr')
// 				.forEach((el, idx, arg) => {
// 					el.addEventListener('click', () => {
// 						arg.forEach((el2) => el2.classList.remove('clicked'));
// 						el.classList.add('clicked');
// 					});
// 				});
// 		});
// }

// function memberList_li_moreInfo(target_li, data) {
// 	const memberList_li = document.querySelectorAll('aside .rightList ul li');
// 	memberList_li.forEach((el) => {
// 		if (el.querySelector('.card_detail')) {
// 			el.querySelector('.card_detail').remove();
// 		}
// 	});
// 	let newDiv = document.createElement('div');
// 	newDiv.classList.add('card_detail');
// 	target_li.appendChild(newDiv);
// 	let newP = document.createElement('p');
// 	newP.classList.add('card_info');
// 	newP.innerHTML = `접수: ${data['접수']}`;
// 	newDiv.appendChild(newP);
// 	let newP2 = document.createElement('p');
// 	newP2.classList.add('card_info');
// 	newP2.innerHTML = `심사중: ${data['심사중']}`;
// 	newDiv.appendChild(newP2);
// 	let newP3 = document.createElement('p');
// 	newP3.classList.add('card_info');
// 	newP3.innerHTML = `대출중: ${data['대출중']}`;
// 	newDiv.appendChild(newP3);
// 	let newP4 = document.createElement('p');
// 	newP4.classList.add('card_info');
// 	newP4.innerHTML = `부결: ${data['부결']}`;
// 	newDiv.appendChild(newP4);
// }

// async function aside_memberList() {
// 	let result = await fetch('/js/dummy_member_data.json').then((res) => res.json());
// 	let memberData = result.data;
// 	const rightList_ul = document.querySelector('aside .rightList ul');
// 	memberData.forEach((el) => {
// 		let newLi = document.createElement('li');
// 		newLi.dataset.seqNo = el.seq_no;
// 		let newSpan = document.createElement('span');
// 		newSpan.classList.add('type');
// 		newSpan.innerHTML = el.type;
// 		let newP = document.createElement('p');
// 		newP.classList.add('company_name');
// 		newP.innerHTML = el.name;
// 		let newSpan2 = document.createElement('span');
// 		newSpan2.classList.add('clickToOpen');
// 		newSpan2.innerHTML = '<i class="fa-solid fa-caret-down"></i> 펼치기';
// 		newLi.appendChild(newSpan);
// 		newLi.appendChild(newP);
// 		newLi.appendChild(newSpan2);
// 		rightList_ul.appendChild(newLi);
// 		newLi.addEventListener('click', () => {
// 			const memberList_li = document.querySelectorAll('aside .rightList ul li');
// 			memberList_li.forEach((el2) => el2.classList.remove('clicked'));
// 			newLi.classList.add('clicked');
// 			memberList_li_moreInfo(newLi, el.detail);
// 			call_member_loan_info();
// 		});
// 	});
// 	leftListOpen();
// 	asideAutoLoad();
// }
// aside_memberList();

// function asideAutoLoad() {
// 	let param_member_seq_no = searchParam('member_seq_no');
// 	if (param_member_seq_no !== '') {
// 		document.querySelectorAll('aside .rightList ul li').forEach((el) => {
// 			if (param_member_seq_no == el.dataset.seqNo) {
// 				el.click();
// 			}
// 		});
// 	}
// }
