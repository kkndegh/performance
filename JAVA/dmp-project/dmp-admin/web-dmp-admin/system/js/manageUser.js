import { createElement } from './import/CreateElement';
import { insertParam } from './import/InsertParam';
import { loading, loading_end } from './import/Loading';
import { alertShow, cancelAlert, cancelModal, modalShow } from './import/ModalShow';
import { searchForm } from './import/Searchform';
import { searchParam } from './import/SearchParam';

let urlPath;
let urlTokenIUD;
let urlUserIUD;
let urlUserAccountIUD;
let urlAuthGroupList;
let urlUserList;
let urlUserAccountList;
if (searchParam('project') == 'aos') {
	urlPath = `${location.pathname}?project=aos&`;
	urlTokenIUD = `/api/v1/token/aosTokenIUD`;
	urlUserIUD = `/api/v1/user/aosUserIUD`;
	urlUserAccountIUD = `/api/v1/user/aosUserAccountIUD`;
	urlAuthGroupList = `/api/v1/auth/aosAuthGroupList`;
	urlUserList = `/api/v1/user/aosUserList`;
	urlUserAccountList = `/api/v1/user/aosUserAccountList`;
} else {
	urlPath = `${location.pathname}?`;
	urlTokenIUD = `/api/v1/token/tokenIUD`;
	urlUserIUD = `/api/v1/user/userIUD`;
	urlAuthGroupList = `/api/v1/auth/authGroupList`;
	urlUserList = `/api/v1/user/userList`;
	urlUserAccountList = `/api/v1/user/userAccountList`;
}

loading();

let authList = await fetch(urlAuthGroupList).then((res) => res.json());

async function userListCall() {
	let result = await fetch(urlUserList).then((res) => res.json());
	if (result) {
		loading_end();
		if (result['result_code'] == 'P000') {
			userListDivMaker(result);
			userClick(result);
			addNewUserPopup(result);
			document.querySelectorAll('.userDiv').forEach((el) => {
				if (el.dataset.user_seq_no == searchParam('user')) {
					el.querySelector('.depth1Title').click();
				}
			});
			document.querySelectorAll('ul.depth2 li').forEach((el) => {
				if (el.dataset.user_account_seq_no == searchParam('userAccount')) {
					el.click();
				}
			});
		} else {
			console.error(`${result['result_code']} : ${result['result_msg']}`);
		}
	}

	function userListDivMaker(data) {
		console.log(data);
		const totalCount = document.querySelector('.userListTopDiv p.totalCount strong');
		const userListDiv = document.querySelector('.userListDiv');
		let userList = data['list'];

		totalCount.innerHTML = data['total_count'];

		userList.forEach((user) => {
			let div = createElement('div', {
				class: 'userDiv depth1',
				data: {
					user_seq_no: user['user_seq_no'],
				},
			});
			if (user['view_yn'] == 'N') {
				div.classList.add('unActivated');
			}
			let div2 = createElement('div', {
				class: 'depth1Title',
			});
			let i = createElement('i', {
				class: 'fa-solid fa-user',
			});
			let p = createElement('p', {
				html: user['user_name'],
			});
			div2.append(i, p);
			let ul = createElement('ul', {
				class: 'depth2',
			});
			div.append(div2, ul);
			userListDiv.append(div);

			let userAccount = user['user_account'];
			if (userAccount) {
				userAccount.forEach((account) => {
					let li = createElement('li', {
						data: {
							user_account_seq_no: account['user_account_seq_no'],
						},
					});
					if (account['view_yn'] == 'N') {
						li.classList.add('unActivated');
					}
					let p = createElement('p', {
						html: account['user_account_name'],
					});
					li.append(p);
					ul.append(li);
				});
			}
		});

		const unAvtivatedShow = document.querySelector('#viewUnactivatedUser');
		unAvtivatedShow.addEventListener('click', () => {
			if (unAvtivatedShow.checked) {
				document
					.querySelectorAll(
						'article.userList .flexBox .leftBox .userListDiv .userDiv.unActivated'
					)
					.forEach((el) => {
						el.classList.add('show');
					});
			} else {
				document
					.querySelectorAll(
						'article.userList .flexBox .leftBox .userListDiv .userDiv.unActivated'
					)
					.forEach((el) => {
						el.classList.remove('show');
					});
			}
		});
	}

	function userClick(data) {
		let userList = data['list'];
		let userAccountList = (() => {
			let arr = [];
			data['list'].forEach((el) => {
				if (el['user_account']) {
					el['user_account'].forEach((el2) => {
						arr.push(el2);
					});
				}
			});
			return arr;
		})();
		document.querySelectorAll('.userListDiv .userDiv .depth1Title').forEach((el, idx, arg) => {
			el.addEventListener('click', () => {
				let key_kor = {
					user_seq_no: '유저 일련번호',
					user_name: '유저 이름',
					use_start_ts: '사용 시작일자',
					use_end_ts: '사용 종료일자',
					temp_del_ts: '임시 삭제일자',
					del_ts: '삭제일자',
					view_yn: '노출 여부',
					reg_id: '등록자',
					reg_ts: '등록시간',
					auth_group: '부여된 권한',
				};
				document.querySelector('.userArea .tableBottomDiv').classList.add('active');
				for (let user of userList) {
					if (el.parentNode.dataset.user_seq_no == user['user_seq_no']) {
						insertParam('user', user['user_seq_no']);
						arg.forEach((el2) => el2.classList.remove('point'));
						el.classList.add('point');
						if (!el.parentNode.querySelector('.depth2 li.point')) {
							el.parentNode.querySelectorAll('.depth2 li')[0].click();
						}
						let infoTbody = document.querySelector(
							'.rightBox .userArea .tableDiv table tbody'
						);
						if (infoTbody.querySelectorAll('tr')) {
							infoTbody.querySelectorAll('tr').forEach((el2) => el2.remove());
						}
						let userKeys = Object.keys(user);
						userKeys.forEach((key) => {
							if (key == 'user_account') {
							} else {
								let tr = createElement('tr');
								let th = createElement('th', {
									html: key_kor[key],
								});
								let td = createElement('td');
								tr.append(th, td);
								infoTbody.append(tr);
								if (
									key == 'user_seq_no' ||
									key == 'reg_id' ||
									key == 'reg_ts' ||
									key == 'temp_del_ts' ||
									key == 'del_ts'
								) {
									let input = createElement('input', {
										id: key,
										type: 'text',
										value: user[key] ?? '',
										readonly: true,
									});
									td.append(input);
								} else if (key == 'view_yn') {
									let toggle = createElement('toggle', { id: key });
									td.append(toggle);
									if (user[key] == 'Y') {
										toggle.querySelector('input').checked = true;
									} else {
										toggle.querySelector('input').checked = false;
									}
								} else if (key == 'use_start_ts' || key == 'use_end_ts') {
									let input = createElement('input', {
										id: `${key}_date`,
										name: `${key}_date`,
										placeholder: 'YYYY-MM-DD',
										value: user[key].split(' ')[0],
									});
									td.append(input);
									$(`.userArea input[name="${key}_date"]`).datepicker({
										changeMonth: true,
										changeYear: true,
										showButtonPanel: true,
										currentText: '오늘',
										closeText: '닫기',
										dateFormat: 'yy-mm-dd',
										// maxDate: 0,
										monthNamesShort: [
											'1월',
											'2월',
											'3월',
											'4월',
											'5월',
											'6월',
											'7월',
											'8월',
											'9월',
											'10월',
											'11월',
											'12월',
										],
										dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
									});
									let input2 = createElement('input', {
										id: `${key}_time`,
										name: `${key}_time`,
										placeholder: 'HH-MM-SS',
										value: user[key].split(' ')[1],
									});
									td.append(input2);
									$(`input[name="${key}_time"]`).timepicker({
										timeFormat: 'HH:mm:ss',
										interval: 30,
										startTime: '00:00',
										dynamic: false,
										dropdown: true,
										scrollbar: false,
									});
								} else if (key == 'auth_group') {
									let thisAuth = user[key][0];
									let select = createElement('select', {
										id: 'auth_group_seq_no',
									});
									for (let auth of authList['authGroupList']) {
										let option = createElement('option', {
											value: auth['auth_group_seq_no'],
											html: auth['auth_group_name'],
										});
										select.append(option);
										if (
											thisAuth['auth_group_seq_no'] ==
											auth['auth_group_seq_no']
										) {
											option.selected = true;
										}
									}
									td.append(select);
								} else {
									let input = createElement('input', {
										id: key,
										type: 'text',
										value: user[key] ?? '',
									});
									td.append(input);
								}
							}
						});
					}
				}
				modifyUser();
				delUser();
			});
		});
		document.querySelectorAll('.userListDiv .userDiv ul li').forEach((el, idx, arg) => {
			el.addEventListener('click', () => {
				document.querySelector('article#token_ip').style.display = 'block';
				let key_kor = {
					user_account_seq_no: '계정 일련번호',
					user_account_class: '계정 클래스',
					user_account_level: '계정 레벨',
					user_account_name: '계정 이름',
					user_account_id: '계정 아이디',
					hiworks_id: '하이웍스 아이디',
					phone_no: '전화번호',
					email: '이메일',
					use_start_ts: '사용 시작일자',
					use_end_ts: '사용종료일자',
					user_account_pw_fin_mod_ts: '패스워드 최종 변경일자',
					user_account_pw_mod_yn: '패스워드 변경 필요 유무',
					test_user_account_yn: '테스트 유저',
					temp_del_ts: '임시 삭제일자',
					del_ts: '삭제일자',
					view_yn: '노출 여부',
					reg_id: '등록자',
					reg_ts: '등록 시간',
				};
				document.querySelector('.userAccountArea .tableBottomDiv').classList.add('active');
				for (let userAcc of userAccountList) {
					let seqNo = userAcc['user_account_seq_no'];
					if (el.dataset.user_account_seq_no == seqNo) {
						insertParam('userAccount', seqNo);
						arg.forEach((el2) => el2.classList.remove('point'));
						el.classList.add('point');
						if (!el.parentNode.parentNode.querySelector('.depth1Title.point')) {
							el.parentNode.parentNode.querySelector('.depth1Title').click();
						}
						let infoTbody = document.querySelector(
							'.rightBox .userAccountArea .tableDiv table tbody'
						);
						if (infoTbody.querySelectorAll('tr')) {
							infoTbody.querySelectorAll('tr').forEach((el2) => el2.remove());
						}
						for (let key in userAcc) {
							if (key == 'token') {
								tokenTableMaker(userAcc[key]);
							} else {
								let tr = createElement('tr');
								let th = createElement('th', {
									html: key_kor[key],
								});
								let td = createElement('td');

								tr.append(th, td);
								infoTbody.append(tr);

								if (key == 'conn_ip') {
									continue;
								} else if (
									key == 'user_account_seq_no' ||
									key == 'hiworks_id' ||
									key == 'user_account_pw_fin_mod_ts' ||
									key == 'user_account_pw_mod_yn' ||
									key == 'temp_del_ts' ||
									key == 'del_ts' ||
									key == 'reg_id' ||
									key == 'reg_ts'
								) {
									let input = createElement('input', {
										id: key,
										value: userAcc[key] ?? '',
										readonly: true,
									});
									td.append(input);
								} else if (key == 'user_account_class') {
									let select = createElement('select', {
										id: key,
									});
									let options = ['API Service', '웹사이트'];
									for (let opt of options) {
										let option = createElement('option', {
											value: opt,
											html: opt,
										});
										select.append(option);
										if (userAcc[key] == opt) {
											option.selected = true;
										}
									}
									td.append(select);
								} else if (key == 'user_account_level') {
									let select = createElement('select', {
										id: key,
									});
									let options = ['주계정', '부계정', '연동계정'];
									for (let opt of options) {
										let option = createElement('option', {
											value: opt,
											html: opt,
										});
										select.append(option);
										if (userAcc[key] == opt) {
											option.selected = true;
										}
									}
									td.append(select);
								} else if (key == 'view_yn' || key == 'test_user_account_yn') {
									let toggle = createElement('toggle', { id: key });
									td.append(toggle);
									if (userAcc[key] == 'Y') {
										toggle.querySelector('input').checked = true;
									} else {
										toggle.querySelector('input').checked = false;
									}
								} else if (key == 'use_start_ts' || key == 'use_end_ts') {
									let input = createElement('input', {
										id: `${key}_date`,
										name: `${key}_date`,
										placeholder: 'YYYY-MM-DD',
										value: userAcc[key].split(' ')[0],
									});
									td.append(input);
									$(`.userAccountArea input[name="${key}_date"]`).datepicker({
										changeMonth: true,
										changeYear: true,
										showButtonPanel: true,
										currentText: '오늘',
										closeText: '닫기',
										dateFormat: 'yy-mm-dd',
										// maxDate: 0,
										monthNamesShort: [
											'1월',
											'2월',
											'3월',
											'4월',
											'5월',
											'6월',
											'7월',
											'8월',
											'9월',
											'10월',
											'11월',
											'12월',
										],
										dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
									});
									let input2 = createElement('input', {
										id: `${key}_time`,
										name: `${key}_time`,
										placeholder: 'HH-MM-SS',
										value: userAcc[key].split(' ')[1],
									});
									td.append(input2);
									$(`input[name="${key}_time"]`).timepicker({
										timeFormat: 'HH:mm:ss',
										interval: 30,
										startTime: '00:00',
										dynamic: false,
										dropdown: true,
										scrollbar: false,
									});
								} else {
									let input = createElement('input', {
										id: key,
										value: userAcc[key] ?? '',
									});
									td.append(input);
								}
							}
						}
					}
				}
				modifyUserAccount();
				delUserAccount();
			});
		});
	}
}
userListCall();

function tokenTableMaker(data) {
	if (data) {
		const location = document.querySelector('article#token_ip .flexBox .leftBox .searchOption');
		if (!location.querySelector('.optionRow')) {
			searchForm(location, {
				options: [
					{
						title: '유효 토큰만 보기',
						checkBox: {
							Y: '',
						},
					},
				],
			});
		}
		const totalCount = document.querySelector(
			'article#token_ip .flexBox .leftBox .tableTopDiv p.totalCount strong'
		);
		totalCount.innerHTML = data.length;
		const tokenTable = document.querySelector(
			'article#token_ip .flexBox .leftBox .tableBox table'
		);
		let key_kor = {
			access_token: '토큰',
			token_class: '토큰 종류',
			// token_content: '내용',
			use_start_ts: '사용시작일자',
			use_end_ts: '사용종료일자',
			// reg_id: '등록자',
			// reg_ts: '등록일자',
		};
		if (tokenTable.querySelector('thead')) tokenTable.querySelector('thead').remove();
		if (tokenTable.querySelector('tbody')) tokenTable.querySelector('tbody').remove();

		let thead = createElement('thead');
		let tr = createElement('tr');
		for (let key in key_kor) {
			let th = createElement('th', {
				html: key_kor[key],
			});
			tr.append(th);
		}
		thead.append(tr);
		tokenTable.append(thead);
		let tbody = createElement('tbody');
		tokenTable.append(tbody);
		data.forEach((el) => {
			let tr = createElement('tr');
			for (let key in key_kor) {
				let td = createElement('td', {
					html: el[key],
				});
				tr.append(td);
			}
			tbody.append(tr);
			tr.addEventListener('click', () => {
				document
					.querySelectorAll('article#token_ip .leftBox .tableBox table tbody tr')
					.forEach((el2) => {
						el2.classList.remove('point');
					});
				tr.classList.add('point');

				let obj = {};
				obj.title = '토큰 정보';
				obj.btn = true;
				obj.btnText = '수정하기';
				obj.btnColor = 'point';
				obj.func = tokenInfoPopup;
				obj.param = el;
				obj.callback = modTokenInfo;
				modalShow(obj);
			});

			function tokenInfoPopup(data) {
				const popupBody = document.querySelector('.modal .layerPopup .popupBody');
				//초기화
				if (popupBody.querySelector('.box')) {
					popupBody.querySelector('.box').remove();
				}
				let box = createElement('div', {
					class: 'box',
				});
				let table = createElement('table');
				popupBody.append(box);
				box.append(table);
				let tbody = createElement('tbody');
				table.append(tbody);
				for (let key in data) {
					let tr = createElement('tr');
					tbody.append(tr);
					let th = createElement('th', {
						html: key,
					});
					tr.append(th);
					let td = createElement('td');
					tr.append(td);

					if (key == 'use_start_ts' || key == 'use_end_ts') {
						let input = createElement('input', {
							id: `${key}_date`,
							name: `${key}_date`,
							placeholder: 'YYYY-MM-DD',
							value: data[key].split(' ')[0],
						});
						td.append(input);
						$(`input[name="${key}_date"]`).datepicker({
							changeMonth: true,
							changeYear: true,
							showButtonPanel: true,
							currentText: '오늘',
							closeText: '닫기',
							dateFormat: 'yy-mm-dd',
							// maxDate: 0,
							monthNamesShort: [
								'1월',
								'2월',
								'3월',
								'4월',
								'5월',
								'6월',
								'7월',
								'8월',
								'9월',
								'10월',
								'11월',
								'12월',
							],
							dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
						});
						let input2 = createElement('input', {
							id: `${key}_time`,
							name: `${key}_time`,
							placeholder: 'HH-MM-SS',
							value: data[key].split(' ')[1],
						});
						td.append(input2);
						$(`input[name="${key}_time"]`).timepicker({
							timeFormat: 'HH:mm:ss',
							interval: 30,
							startTime: '00:00',
							dynamic: false,
							dropdown: true,
							scrollbar: false,
						});
					} else if (key == 'token_class' || key == 'reg_id' || key == 'reg_ts') {
						let input = createElement('input', {
							id: key,
							value: data[key],
							type: 'text',
							readonly: true,
						});
						td.append(input);
					} else {
						let input = createElement('input', {
							id: key,
							value: data[key],
							type: 'text',
						});
						td.append(input);
					}
				}
				let tr = createElement('tr');
				tbody.append(tr);
				let th = createElement('th', {
					html: '토큰 만료시키기',
				});
				let td = createElement('td');
				let button = createElement('button', {
					class: 'btn_style_3 btn_color_crush',
					html: '만료시키기',
				});
				td.append(button);
				tr.append(th, td);
				button.addEventListener('click', () => {
					let obj = {};
					obj.title = '토큰 만료시키기';
					obj.btn = true;
					obj.btnText = '만료시키기';
					obj.btnColor = 'crush';
					obj.text = '토큰을 만료시키겠습니까?';
					obj.callback = delToken;
					alertShow(obj);
				});
				function delToken() {
					let bodyData = {};
					bodyData.access_token = document.querySelector(
						'.modal .layerPopup .popupBody table tbody tr td input#access_token'
					).value;
					bodyData.user_account_name = g_user_id;
					bodyData.act_evt = 'del';
					bodyData = JSON.stringify(bodyData);
					fetch(urlTokenIUD, {
						method: 'POST',
						body: bodyData,
					})
						.then((response) => response.json())
						.then((data) => {
							if (data.result_code == 'P000') {
								cancelModal();
								window.location.reload();
							} else {
								let obj = {};
								obj.title = '오류';
								obj.btn = false;
								obj.text = '토큰 삭제 실패';
								obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
								alertShow(obj);
							}
						})
						.catch((error) => console.error(error));
				}
			}
			function modTokenInfo() {
				let bodyData = {};
				bodyData.access_token = document.querySelector(
					'.modal .layerPopup .popupBody table tbody tr td input#access_token'
				).value;
				bodyData.use_start_ts = [
					document.querySelector(
						'.modal .layerPopup .popupBody table tbody tr td input#use_start_ts_date'
					).value,
					document.querySelector(
						'.modal .layerPopup .popupBody table tbody tr td input#use_start_ts_time'
					).value,
				].join(' ');
				bodyData.use_end_ts = [
					document.querySelector(
						'.modal .layerPopup .popupBody table tbody tr td input#use_end_ts_date'
					).value,
					document.querySelector(
						'.modal .layerPopup .popupBody table tbody tr td input#use_end_ts_time'
					).value,
				].join(' ');
				bodyData.token_content = document.querySelector(
					'.modal .layerPopup .popupBody table tbody tr td input#token_content'
				).value;
				bodyData.user_account_name = g_user_id;
				bodyData.act_evt = 'upd';
				bodyData = JSON.stringify(bodyData);
				fetch(urlTokenIUD, {
					method: 'POST',
					body: bodyData,
				})
					.then((response) => response.json())
					.then((data) => {
						if (data.result_code == 'P000') {
							cancelModal();
							window.location.reload();
						} else {
							let obj = {};
							obj.title = '오류';
							obj.btn = false;
							obj.text = '토큰 정보 수정 실패';
							obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
							alertShow(obj);
						}
					})
					.catch((error) => console.error(error));
			}
		});
	} else {
		const location = document.querySelector('article#token_ip .flexBox .leftBox .searchOption');
		if (!location.querySelector('.optionRow')) {
			searchForm(location, {
				options: [
					{
						title: '유효 토큰만 보기',
						checkBox: {
							Y: '',
						},
					},
				],
			});
		}
		const totalCount = document.querySelector(
			'article#token_ip .flexBox .leftBox .tableTopDiv p.totalCount strong'
		);
		totalCount.innerHTML = 0;
		const tokenTable = document.querySelector(
			'article#token_ip .flexBox .leftBox .tableBox table'
		);
		let key_kor = {
			access_token: '토큰',
			token_class: '토큰 종류',
			// token_content: '내용',
			use_start_ts: '사용시작일자',
			use_end_ts: '사용종료일자',
			// reg_id: '등록자',
			// reg_ts: '등록일자',
		};
		if (tokenTable.querySelector('thead')) tokenTable.querySelector('thead').remove();
		if (tokenTable.querySelector('tbody')) tokenTable.querySelector('tbody').remove();

		let thead = createElement('thead');
		let tr = createElement('tr');
		for (let key in key_kor) {
			let th = createElement('th', {
				html: key_kor[key],
			});
			tr.append(th);
		}
		thead.append(tr);
		tokenTable.append(thead);
		let tbody = createElement('tbody');
		tokenTable.append(tbody);
		let tr2 = createElement('tr');
		tbody.append(tr2);
		let td = createElement('td', {
			colspan: 4,
			html: 'No Data',
		});
		tr2.append(td);
	}
}

function publishToken() {
	const newTokenBtn = document.querySelector('#addManualToken');

	function newToken() {
		const popupBody = document.querySelector('.modal .layerPopup .popupBody');
		//초기화
		if (popupBody.querySelector('.box')) {
			popupBody.querySelector('.box').remove();
		}
		let key_kor = {
			use_start_ts: '토큰 사용 시작일자',
			use_end_ts: '토큰 만료일자',
			token_content: '내용',
			refresh_yn: 'Refresh Token 동시 발급',
		};
		let box = createElement('div', {
			class: 'box',
		});
		let table = createElement('table');
		popupBody.append(box);
		box.append(table);
		let tbody = createElement('tbody');
		table.append(tbody);
		for (let key in key_kor) {
			let tr = createElement('tr');
			tbody.append(tr);
			let th = createElement('th', {
				html: key_kor[key],
			});
			let td = createElement('td');
			if (key == 'use_start_ts' || key == 'use_end_ts') {
				let input = createElement('input', {
					id: `${key}_date`,
					name: `${key}_date`,
					placeholder: 'YYYY-MM-DD',
				});
				let td2 = createElement('td');
				tr.append(th, td, td2);
				td.append(input);
				$(`input[name="${key}_date"]`).datepicker({
					changeMonth: true,
					changeYear: true,
					showButtonPanel: true,
					currentText: '오늘',
					closeText: '닫기',
					dateFormat: 'yy-mm-dd',
					// maxDate: 0,
					monthNamesShort: [
						'1월',
						'2월',
						'3월',
						'4월',
						'5월',
						'6월',
						'7월',
						'8월',
						'9월',
						'10월',
						'11월',
						'12월',
					],
					dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
				});
				let input2 = createElement('input', {
					id: `${key}_time`,
					name: `${key}_time`,
					placeholder: 'HH-MM-SS',
				});
				td.append(input2);
				$(`input[name="${key}_time"]`).timepicker({
					timeFormat: 'HH:mm:ss',
					interval: 30,
					startTime: '00:00',
					dynamic: false,
					dropdown: true,
					scrollbar: false,
				});
				if (key == 'use_end_ts') {
					let checkbox = createElement('checkbox', {
						id: 'infinite',
						html: '무기한',
					});
					td2.append(checkbox);
					checkbox.querySelector('input').addEventListener('click', () => {
						if (checkbox.querySelector('input').checked) {
							console.log('checked');
							input.setAttribute('readonly', true);
							input2.setAttribute('readonly', true);
						} else {
							input.removeAttribute('readonly');
							input2.removeAttribute('readonly');
						}
					});
				}
			} else if (key == 'refresh_yn') {
				let checkbox = createElement('checkbox', {
					id: key,
				});
				td.append(checkbox);
				let td2 = createElement('td');
				tr.append(th, td, td2);
			} else {
				let input = createElement('input', {
					id: key,
					type: 'text',
				});
				let td2 = createElement('td');
				tr.append(th, td, td2);
				td.append(input);
			}
		}
	}

	function newTokenMaker() {
		let bodyData = {};
		bodyData.user_account_seq_no = document.querySelector(
			'.userAccountArea #user_account_seq_no'
		).value;
		bodyData.use_start_ts = [
			document.querySelector('.modal #use_start_ts_date').value,
			document.querySelector('.modal #use_start_ts_time').value,
		].join(' ');
		bodyData.use_end_ts = document.querySelector('.modal #infinite').checked
			? '9999-12-31 23:59:59'
			: [
					document.querySelector('.modal #use_end_ts_date').value,
					document.querySelector('.modal #use_end_ts_time').value,
			  ].join(' ');
		bodyData.token_content = document.querySelector('.modal #token_content').value;
		bodyData.refresh_yn = document.querySelector('.modal #refresh_yn').checked ? 'Y' : 'N';
		bodyData.user_account_name = g_user_id; //reg_id
		bodyData.act_evt = 'ins';
		console.log(bodyData);
		bodyData = JSON.stringify(bodyData);

		fetch(urlTokenIUD, {
			method: 'POST',
			body: bodyData,
		})
			.then((response) => response.json())
			.then((data) => {
				if (data.result_code == 'P000') {
					cancelModal();
					location.reload();
				} else {
					let obj = {};
					obj.title = '오류';
					obj.btn = false;
					obj.text = '토큰 발급 실패';
					obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
					alertShow(obj);
				}
			})
			.catch((error) => console.error(error));
	}

	newTokenBtn.addEventListener('click', () => {
		let obj = {};
		obj.title = '새 토큰 발행하기';
		obj.btn = true;
		obj.btnText = '토큰 발행';
		obj.btnColor = 'point';
		obj.func = newToken;
		obj.callback = newTokenMaker;

		modalShow(obj);
	});
}
publishToken();

ipTableMaker([{ ip_address: 'test_test' }]);
function ipTableMaker(data) {
	const location = document.querySelector('article#token_ip .flexBox .rightBox .searchOption');
	searchForm(location, {
		options: [
			{
				title: 'IP 보기',
				checkBox: {
					allowed: '허용 IP',
					ban: '차단 IP',
				},
			},
		],
	});
	const totalCount = document.querySelector(
		'article#token_ip .flexBox .rightBox .tableTopDiv p.totalCount strong'
	);
	totalCount.innerHTML = data.length;
	const ipTable = document.querySelector('article#token_ip .flexBox .rightBox .tableBox table');
	let key_kor = {
		ip_address: 'IP 주소',
		// token_class: '토큰 종류',
		// token_content: '내용',
		// use_start_ts: '사용시작일자',
		// use_end_ts: '사용종료일자',
		// reg_id: '등록자',
		// reg_ts: '등록일자',
	};
	if (ipTable.querySelector('thead')) ipTable.querySelector('thead').remove();
	if (ipTable.querySelector('tbody')) ipTable.querySelector('tbody').remove();

	let thead = createElement('thead');
	let tr = createElement('tr');
	for (let key in key_kor) {
		let th = createElement('th', {
			html: key_kor[key],
		});
		tr.append(th);
	}
	thead.append(tr);
	ipTable.append(thead);
	let tbody = createElement('tbody');
	ipTable.append(tbody);
	data.forEach((el) => {
		let tr = createElement('tr');
		for (let key in key_kor) {
			let td = createElement('td', {
				html: el[key],
			});
			tr.append(td);
		}
		tbody.append(tr);
	});
}

function modifyUser() {
	document.querySelector('#modUser').addEventListener('click', () => {
		let obj = {
			title: '유저 정보 수정',
			btn: true,
			btnText: '수정하기',
			btnColor: 'point',
			text: '수정내용을 적용하시겠습니까?',
			callback: modifyUserAPI,
		};
		alertShow(obj);

		function modifyUserAPI() {
			let bodyData = {};
			let keys = [
				'user_seq_no',
				'user_name',
				'use_start_ts',
				'use_end_ts',
				'view_yn',
				'auth_group_seq_no',
			].forEach((el) => {
				if (el == 'user_seq_no') {
					let value = document.querySelector(
						`.userArea .tableDiv table tbody input#${el}`
					).value;
					bodyData[el] = value;
				} else if (el == 'view_yn') {
					let value = document.querySelector(
						`.userArea .tableDiv table tbody input#${el}`
					).checked
						? 'Y'
						: 'N';

					bodyData[el] = value;
				} else if (el == 'use_start_ts' || el == 'use_end_ts') {
					let value = [
						document.querySelector(`.userArea .tableDiv table tbody input#${el}_date`)
							.value,
						document.querySelector(`.userArea .tableDiv table tbody input#${el}_time`)
							.value,
					].join(' ');
					bodyData[el] = value;
				} else if (el == 'auth_group_seq_no') {
					let value = document.querySelector(
						`.userArea .tableDiv table tbody select#${el}`
					).value;
					bodyData[el] = value;
				} else {
					let value = document.querySelector(
						`.userArea .tableDiv table tbody input#${el}`
					).value;
					bodyData[el] = value;
				}
			});
			bodyData['reg_id'] = g_user_id; //임시
			bodyData['act_evt'] = 'upd';
			bodyData = JSON.stringify(bodyData);

			fetch(urlUserIUD, {
				method: 'POST',
				body: bodyData,
			})
				.then((res) => res.json())
				.then((data) => {
					if (data['result_code'] == 'P000') {
						location.reload();
					} else {
						let obj = {};
						obj.title = '오류';
						obj.btn = false;
						obj.text = data.result_msg;
						obj.text2 = '오류코드 : ' + data.result_code;
						alertShow(obj);
						console.error(data.result_sys_msg);
					}
				});
		}
	});
}

function modifyUserAccount() {
	document.querySelector('#modUserAccount').addEventListener('click', () => {
		let obj = {
			title: '유저 계정 정보 수정',
			btn: true,
			btnText: '수정하기',
			btnColor: 'point',
			text: '수정내용을 적용하시겠습니까?',
			callback: modifyUserAccountAPI,
		};
		alertShow(obj);

		function modifyUserAccountAPI() {
			let bodyData = {};
			let keys = [
				'user_account_seq_no',
				'user_account_class',
				'user_account_level',
				'user_account_name',
				'user_account_id',
				'use_start_ts',
				'use_end_ts',
				'test_user_account_yn',
				'view_yn',
			].forEach((el) => {
				if (el == 'user_account_seq_no') {
					let value = document.querySelector(
						`.userAccountArea .tableDiv table tbody input#${el}`
					).value;
					bodyData[el] = value;
				} else if (el == 'user_account_class' || el == 'user_account_level') {
					let value = document.querySelector(
						`.userAccountArea .tableDiv table tbody select#${el}`
					).value;
					bodyData[el] = value;
				} else if (el == 'view_yn' || el == 'test_user_account_yn') {
					let value = document.querySelector(
						`.userAccountArea .tableDiv table tbody input#${el}`
					).checked
						? 'Y'
						: 'N';

					bodyData[el] = value;
				} else if (el == 'use_start_ts' || el == 'use_end_ts') {
					let value = [
						document.querySelector(
							`.userAccountArea .tableDiv table tbody input#${el}_date`
						).value,
						document.querySelector(
							`.userAccountArea .tableDiv table tbody input#${el}_time`
						).value,
					].join(' ');
					bodyData[el] = value;
				} else {
					let value = document.querySelector(
						`.userAccountArea .tableDiv table tbody input#${el}`
					).value;
					bodyData[el] = value;
				}
			});
			bodyData['reg_id'] = g_user_id; //임시
			bodyData['act_evt'] = 'upd';
			bodyData = JSON.stringify(bodyData);

			fetch(urlUserAccountIUD, {
				method: 'POST',
				body: bodyData,
			})
				.then((res) => res.json())
				.then((data) => {
					if (data['result_code'] == 'P000') {
						location.reload();
					} else {
						let obj = {};
						obj.title = '오류';
						obj.btn = false;
						obj.text = data.result_msg;
						obj.text2 = '오류코드 : ' + data.result_code;
						alertShow(obj);
						console.error(data.result_sys_msg);
					}
				});
		}
	});
}

function delUser() {
	document.querySelector('#delUser').addEventListener('click', () => {
		let obj = {
			title: '유저 삭제',
			btn: true,
			btnText: '삭제하기',
			btnColor: 'crush',
			text: `"${
				document.querySelector('.userArea #user_name').value
			}" 유저를 삭제하시겠습니까?`,
			text2: `삭제 이후 되돌릴 수 없습니다`,
			callback: delUserAPI,
		};
		alertShow(obj);

		function delUserAPI() {
			let bodyData = {};
			bodyData.user_seq_no = document.querySelector('.userArea #user_seq_no').value;
			bodyData.reg_id = g_user_id;
			bodyData.act_evt = 'del';
			bodyData = JSON.stringify(bodyData);
			fetch(urlUserIUD, {
				method: 'POST',
				body: bodyData,
			})
				.then((res) => res.json())
				.then((data) => {
					if (data.result_code == 'P000') {
						cancelModal();
						window.location.reload();
					} else {
						let obj = {};
						obj.title = '오류';
						obj.btn = false;
						obj.text = '유저 삭제 실패';
						obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
						alertShow(obj);
					}
				})
				.catch((error) => console.error(error));
		}
	});
}

function delUserAccount() {
	document.querySelector('#delUserAccount').addEventListener('click', () => {
		let obj = {
			title: '계정 삭제',
			btn: true,
			btnText: '삭제하기',
			btnColor: 'crush',
			text: `"${
				document.querySelector('.userAccountArea #user_account_id').value
			}" 계정을 삭제하시겠습니까?`,
			text2: `삭제 이후 되돌릴 수 없습니다`,
			callback: delUserAccountAPI,
		};
		alertShow(obj);

		function delUserAccountAPI() {
			let bodyData = {};
			bodyData.user_account_seq_no = document.querySelector(
				'.userAccountArea #user_account_seq_no'
			).value;
			bodyData.reg_id = g_user_id;
			bodyData.act_evt = 'del';
			bodyData = JSON.stringify(bodyData);
			fetch(urlUserIUD, {
				method: 'POST',
				body: bodyData,
			})
				.then((res) => res.json())
				.then((data) => {
					if (data.result_code == 'P000') {
						cancelModal();
						window.location.reload();
					} else {
						let obj = {};
						obj.title = '오류';
						obj.btn = false;
						obj.text = '계정 삭제 실패';
						obj.text2 = `Error Code : ${data.result_code} Error Message : ${data.result_msg}`;
						alertShow(obj);
					}
				})
				.catch((error) => console.error(error));
		}
	});
}

function addNewUserPopup(data) {
	const addUserBtn = document.querySelector('.userListTopDiv button#addUser');
	addUserBtn.addEventListener('click', () => {
		let obj = {
			title: '새 유저 || 유저 계정 추가',
			btn: true,
			btnText: '추가하기',
			btnColor: 'point',
			func: popupMaker,
			callback: addUser,
			refresh: false,
		};
		modalShow(obj);

		function popupMaker() {
			const popupBody = document.querySelector('.modal .popupBody');
			if (popupBody.querySelector('.box')) {
				popupBody.querySelector('.box').remove();
			}
			let box = createElement('div', {
				class: 'box',
			});
			popupBody.append(box);
			let div = createElement('div', {
				class: 'typeDiv',
			});
			let button = createElement('button', {
				class: 'btn_style_1 btn_color_point selected',
				html: '<i class="fa-solid fa-plus"></i> 유저 추가',
			});
			let button2 = createElement('button', {
				class: 'btn_style_1 btn_color_point',
				html: '<i class="fa-solid fa-plus"></i> 유저 계정 추가',
			});
			div.append(button, button2);
			box.append(div);
			button.addEventListener('click', userPopupTable);
			button2.addEventListener('click', userAccountPopupTable);

			function userPopupTable() {
				button.classList.add('selected');
				button2.classList.remove('selected');
				if (box.querySelector('table')) {
					box.querySelector('table').remove();
				}
				let table = createElement('table');
				let tbody = createElement('tbody');
				table.append(tbody);
				box.append(table);
				let frontId = 'userPopup_';
				let keys = {
					user_name: '유저 이름',
					use_start_ts: '사용시작일자',
					use_end_ts: '사용종료일자',
					view_yn: '노출 여부',
					auth_group_seq_no: '권한',
				};
				for (let key in keys) {
					let tr = createElement('tr');
					let th = createElement('th', {
						html: keys[key],
					});
					let td = createElement('td');
					let td2 = createElement('td');

					tr.append(th, td, td2);
					tbody.append(tr);
					if (key == 'view_yn') {
						let toggle = createElement('toggle', {
							id: frontId + key,
						});
						td.append(toggle);
					} else if (key == 'use_start_ts' || key == 'use_end_ts') {
						let input = createElement('input', {
							id: `${frontId + key}_date`,
							name: `${key}_date`,
							placeholder: 'YYYY-MM-DD',
						});
						td.append(input);
						$(`input[name="${key}_date"]`).datepicker({
							changeMonth: true,
							changeYear: true,
							showButtonPanel: true,
							currentText: '오늘',
							closeText: '닫기',
							dateFormat: 'yy-mm-dd',
							// maxDate: 0,
							monthNamesShort: [
								'1월',
								'2월',
								'3월',
								'4월',
								'5월',
								'6월',
								'7월',
								'8월',
								'9월',
								'10월',
								'11월',
								'12월',
							],
							dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
						});
						let input2 = createElement('input', {
							id: `${frontId + key}_time`,
							name: `${key}_time`,
							placeholder: 'HH-MM-SS',
						});
						td.append(input2);
						$(`input[name="${key}_time"]`).timepicker({
							timeFormat: 'HH:mm:ss',
							interval: 30,
							startTime: '00:00',
							dynamic: false,
							dropdown: true,
							scrollbar: false,
						});
						if (key == 'use_end_ts') {
							let checkbox = createElement('checkbox', {
								id: frontId + 'infinite',
								html: '무기한',
							});
							td2.append(checkbox);
							checkbox.querySelector('input').addEventListener('click', () => {
								if (checkbox.querySelector('input').checked) {
									input.setAttribute('readonly', true);
									input2.setAttribute('readonly', true);
								} else {
									input.removeAttribute('readonly');
									input2.removeAttribute('readonly');
								}
							});
						}
					} else if (key == 'auth_group_seq_no') {
						let select = createElement('select', {
							id: frontId + key,
						});
						for (let auth of authList['authGroupList']) {
							let option = createElement('option', {
								value: auth['auth_group_seq_no'],
								html: auth['auth_group_name'],
							});
							select.append(option);
						}
						td.append(select);
					} else {
						let input = createElement('input', {
							type: 'text',
							id: frontId + key,
						});
						td.append(input);
					}
				}
			}
			userPopupTable();

			function userAccountPopupTable() {
				button2.classList.add('selected');
				button.classList.remove('selected');
				if (box.querySelector('table')) {
					box.querySelector('table').remove();
				}
				let table = createElement('table');
				let tbody = createElement('tbody');
				table.append(tbody);
				box.append(table);
				let frontId = 'userAccountPopup_';
				let keys = {
					user_seq_no: '상위 유저',
					user_account_class: '계정 클래스',
					user_account_level: '계정 레벨',
					user_account_name: '계정 이름',
					user_account_id: '계정 아이디',
					hiworks_id: '하이웍스 아이디',
					phone_no: '전화번호',
					email: '이메일',
					use_start_ts: '사용 시작일자',
					use_end_ts: '사용 종료일자',
					test_user_account_yn: '테스트 유저',
					view_yn: '노출 여부',
				};
				for (let key in keys) {
					let tr = createElement('tr');
					let th = createElement('th', {
						html: keys[key],
					});
					let td = createElement('td');
					let td2 = createElement('td');
					tr.append(th, td, td2);
					tbody.append(tr);
					if (key == 'test_user_account_yn' || key == 'view_yn') {
						let toggle = createElement('toggle', {
							id: frontId + key,
						});
						td.append(toggle);
					} else if (key == 'use_start_ts' || key == 'use_end_ts') {
						let input = createElement('input', {
							id: `${frontId + key}_date`,
							name: `${key}_date`,
							placeholder: 'YYYY-MM-DD',
						});
						td.append(input);
						$(`input[name="${key}_date"]`).datepicker({
							changeMonth: true,
							changeYear: true,
							showButtonPanel: true,
							currentText: '오늘',
							closeText: '닫기',
							dateFormat: 'yy-mm-dd',
							// maxDate: 0,
							monthNamesShort: [
								'1월',
								'2월',
								'3월',
								'4월',
								'5월',
								'6월',
								'7월',
								'8월',
								'9월',
								'10월',
								'11월',
								'12월',
							],
							dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
						});
						let input2 = createElement('input', {
							id: `${frontId + key}_time`,
							name: `${key}_time`,
							placeholder: 'HH-MM-SS',
						});
						td.append(input2);
						$(`input[name="${key}_time"]`).timepicker({
							timeFormat: 'HH:mm:ss',
							interval: 30,
							startTime: '00:00',
							dynamic: false,
							dropdown: true,
							scrollbar: false,
						});
						if (key == 'use_end_ts') {
							let checkbox = createElement('checkbox', {
								id: frontId + 'infinite',
								html: '무기한',
							});
							td2.append(checkbox);
							checkbox.querySelector('input').addEventListener('click', () => {
								if (checkbox.querySelector('input').checked) {
									input.setAttribute('readonly', true);
									input2.setAttribute('readonly', true);
								} else {
									input.removeAttribute('readonly');
									input2.removeAttribute('readonly');
								}
							});
						}
					} else if (key == 'user_account_class') {
						let select = createElement('select', {
							id: frontId + key,
						});
						let options = ['API Service', '웹사이트'];
						for (let opt of options) {
							let option = createElement('option', {
								value: opt,
								html: opt,
							});
							select.append(option);
						}
						td.append(select);
					} else if (key == 'user_account_level') {
						let select = createElement('select', {
							id: frontId + key,
						});
						let options = ['주계정', '부계정', '연동계정'];
						for (let opt of options) {
							let option = createElement('option', {
								value: opt,
								html: opt,
							});
							select.append(option);
						}
						td.append(select);
					} else if (key == 'user_seq_no') {
						let select = createElement('select', {
							id: frontId + key,
						});
						data['list'].forEach((el) => {
							let option = createElement('option', {
								value: el['user_seq_no'],
								html: el['user_name'],
							});
							select.append(option);
						});
						td.append(select);
					} else {
						let input = createElement('input', {
							type: 'text',
							id: frontId + key,
						});
						td.append(input);
					}
				}
			}
		}

		function addUser() {
			const popupBody = document.querySelector('.modal .popupBody');
			const buttons = popupBody.querySelectorAll('.typeDiv button');

			if (buttons[0].classList.contains('selected')) {
				let frontId = 'userPopup_';
				let obj = {};
				obj.user_name = popupBody.querySelector(`#${frontId}user_name`).value;
				obj.use_start_ts = [
					popupBody.querySelector(`#${frontId}use_start_ts_date`).value,
					popupBody.querySelector(`#${frontId}use_start_ts_time`).value,
				].join(' ');
				obj.use_end_ts = popupBody.querySelector(`#${frontId}infinite`).checked
					? '9999-12-31 23:59:59'
					: [
							popupBody.querySelector(`#${frontId}use_end_ts_date`).value,
							popupBody.querySelector(`#${frontId}use_end_ts_time`).value,
					  ].join(' ');
				obj.view_yn = popupBody.querySelector(`#${frontId}view_yn`).checked ? 'Y' : 'N';
				obj.auth_group_seq_no = popupBody.querySelector(
					`#${frontId}auth_group_seq_no`
				).value;
				obj.reg_id = g_user_id; //임시
				obj.act_evt = 'ins';
				obj = JSON.stringify(obj);
				fetch(urlUserIUD, {
					method: 'POST',
					body: obj,
				})
					.then((res) => res.json())
					.then((data) => {
						if (data['result_code'] == 'P000') {
							location.reload();
						} else {
							console.error(data);
						}
					});
			} else if (buttons[1].classList.contains('selected')) {
				let frontId = 'userAccountPopup_';
				let obj = {};
				obj.user_seq_no = popupBody.querySelector(`#${frontId}user_seq_no`).value;
				obj.user_account_class = popupBody.querySelector(
					`#${frontId}user_account_class`
				).value;
				obj.user_account_level = popupBody.querySelector(
					`#${frontId}user_account_level`
				).value;
				obj.user_account_name = popupBody.querySelector(
					`#${frontId}user_account_name`
				).value;
				obj.user_account_id = popupBody.querySelector(`#${frontId}user_account_id`).value;
				obj.hiworks_id = popupBody.querySelector(`#${frontId}hiworks_id`).value;
				obj.phone_no = popupBody.querySelector(`#${frontId}phone_no`).value;
				obj.email = popupBody.querySelector(`#${frontId}email`).value;
				obj.use_start_ts = [
					popupBody.querySelector(`#${frontId}use_start_ts_date`).value,
					popupBody.querySelector(`#${frontId}use_start_ts_time`).value,
				].join(' ');
				obj.use_end_ts = popupBody.querySelector(`#${frontId}infinite`).checked
					? '9999-12-31 23:59:59'
					: [
							popupBody.querySelector(`#${frontId}use_end_ts_date`).value,
							popupBody.querySelector(`#${frontId}use_end_ts_time`).value,
					  ].join(' ');
				obj.test_user_account_yn = popupBody.querySelector(
					`#${frontId}test_user_account_yn`
				).checked
					? 'Y'
					: 'N';
				obj.view_yn = popupBody.querySelector(`#${frontId}view_yn`).checked ? 'Y' : 'N';
				obj.reg_id = g_user_id; //임시
				obj.act_evt = 'ins';
				obj = JSON.stringify(obj);
				fetch(urlUserAccountIUD, {
					method: 'POST',
					body: obj,
				})
					.then((res) => res.json())
					.then((data) => {
						if (data['result_code'] == 'P000') {
							location.reload();
						} else {
							console.error(data);
						}
					});
			}
		}
	});
}
