import { createElement } from './import/CreateElement';
import { searchParam } from './import/SearchParam';

let result = await fetch('/api/v1/menu/menuList').then((res) => res.json());

pageMaker(result);
afterDOMload();
function afterDOMload() {
	const menuBoxUlLi = document.querySelectorAll('article #map .menuBox ul.subMenuBox li');
	const pageBoxUlLi = document.querySelectorAll('article #map .notMenuBox ul.pageBox li');

	menuBoxUlLi.forEach((el) => {
		if (el.classList.contains('locked')) {
			let newI = createElement('i', {
				class: 'fa-solid fa-lock',
			});
			el.append(newI);
		}
		if (searchParam('popup') == 'yes') {
			el.querySelector('a').style.pointerEvents = 'none';
			if (!el.classList.contains('locked')) {
				el.style.cursor = 'pointer';
				el.addEventListener('click', () => {
					opener.location.href = el.querySelector('a').getAttribute('href');
				});
			}
		}
	});
	pageBoxUlLi.forEach((el) => {
		if (el.classList.contains('locked')) {
			let newI = createElement('i', {
				class: 'fa-solid fa-lock',
			});
			el.append(newI);
		}
		if (searchParam('popup') == 'yes') {
			el.querySelector('a').style.pointerEvents = 'none';
			if (!el.classList.contains('locked')) {
				el.style.cursor = 'pointer';
				el.addEventListener('click', () => {
					opener.location.href = el.querySelector('a').getAttribute('href');
				});
			}
		}
	});
}

function pageMaker(data) {
	const map = document.querySelector('article #map');

	let menuList = data.menu;
	let menuGroupList = data.menu_group;

	for (let i = 0; i < menuGroupList.length + 1; i++) {
		let menuGroupDiv = createElement('div');
		let newH5 = createElement('h5');
		menuGroupDiv.append(newH5);
		let menuUl = createElement('ul');

		if (i == menuGroupList.length) {
			menuGroupDiv.classList.add('notMenuBox');
			newH5.innerHTML = '메뉴에 속하지 않은 페이지';
			menuUl.classList.add('pageBox');
			menuGroupDiv.append(menuUl);
		} else {
			menuGroupDiv.classList.add('menuBox');
			newH5.innerHTML = menuGroupList[i].menu_group_name;
			menuUl.classList.add('subMenuBox');
			menuUl.classList.add('_' + menuGroupList[i].menu_group_seq_no);
			menuGroupDiv.append(menuUl);
		}

		map.append(menuGroupDiv);
	}
	for (let i = 0; i < menuList.length; i++) {
		let newLi = createElement('li');
		let newA = createElement('a');
		newA.setAttribute('href', menuList[i].menu_link_url);
		newA.innerHTML = menuList[i].menu_name;
		newLi.append(newA);
		if (menuList[i].menu_group_seq_no !== null) {
			document.querySelector('._' + menuList[i].menu_group_seq_no).append(newLi);
		} else {
			document.querySelector('.pageBox').append(newLi);
		}
		if (menuList[i].menu_use_yn == 'N') {
			newLi.classList.add('locked');
		}
	}
}
