import { setCookie } from './Cookie';

//서브메뉴 없애고 active 삭제
export function leftMenuUnactivate() {
	const leftMenu = document.querySelectorAll('.left menu .nav>ul>li');
	const subMenu = document.querySelectorAll('nav.subMenu>ul');
	const wrap = document.querySelector('#wrap');

	leftMenu.forEach((menu) => {
		menu.classList.remove('active');
	});

	subMenu.forEach((ul) => {
		ul.classList.remove('active');
	});

	wrap.classList.add('subMenuClose');
}

//메뉴 줄이기
export function menuChange() {
	const wrap = document.querySelector('#wrap');
	const menuChange = document.querySelector('.left menu .menuChange');
	menuChange.addEventListener('click', () => {
		if (wrap.classList.contains('menuClose') !== true) {
			wrap.classList.add('menuClose');
			setCookie('menu', 'close', 1);
		} else {
			wrap.classList.remove('menuClose');
			setCookie('menu', null, -1);
		}
	});
}

//서브메뉴 줄이기
export function subMenuChange() {
	const wrap = document.querySelector('#wrap');
	const subMenuChange = document.querySelector('.right header .header_left');
	subMenuChange.addEventListener('click', () => {
		if (wrap.classList.contains('subMenuClose') !== true) {
			wrap.classList.add('subMenuClose');
		} else {
			if (document.querySelector('nav.subMenu>ul.active')) {
				wrap.classList.remove('subMenuClose');
			}
		}
	});
}

//왼쪽리스트 줄이기
export function leftListClose() {
	const wrap = document.querySelector('#wrap');
	const leftListClose = document.querySelector('aside .leftList span.close_leftList');
	leftListClose.addEventListener('click', () => {
		wrap.classList.add('leftListClose');
	});
}

//왼쪽리스트 나타내기
export function leftListOpen() {
	const wrap = document.querySelector('#wrap');
	wrap.classList.remove('leftListClose');
}

//오른쪽리스트 줄이기
export function rightListChange() {
	const wrap = document.querySelector('#wrap');
	const rightListChange = document.querySelector('.header_right i.fa-ellipsis-vertical');
	rightListChange.addEventListener('click', () => {
		if (wrap.classList.contains('rightListClose') !== true) {
			wrap.classList.add('rightListClose');
		} else {
			wrap.classList.remove('rightListClose');
		}
	});
}
