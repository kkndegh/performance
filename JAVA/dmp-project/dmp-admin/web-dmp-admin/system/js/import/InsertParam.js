export function insertParam(key, value) {
	//현재 주소를 가져온다.
	let renewURL = location.href;

	//파라미터 가져오기
	let url = new URL(renewURL);
	let urlParams = url.searchParams;
	urlParams.set(key, value);
	renewURL = url.href;

	//페이지 갱신 실행!
	history.pushState(null, null, renewURL);
}
