export function loading() {
	document.querySelector('.loadingBox').classList.add('on');
	document.querySelector('.loadingBox').style.opacity = 0;
	document.querySelector('.loadingBox').style.transition = 'all .2s';
	setTimeout(() => {
		document.querySelector('.loadingBox').style.opacity = 1;
	}, 1);
}

export function loading_end() {
	document.querySelector('.loadingBox').style.transition = 'all .2s';
	document.querySelector('.loadingBox').style.opacity = 0;
	setTimeout(() => {
		document.querySelector('.loadingBox').classList.remove('on');
	}, 500);
}
