export function jsonToQueryString(json) {
	var queryString = Object.entries(json)
		.map(([key, value]) => value && key + '=' + value)
		.filter((v) => v)
		.join('&');

	return queryString;
}
