//반응형 css 조정
export function target_height(element, minusValue) {
	const section = document.querySelector('section');
	let section_ro = new ResizeObserver((entries) => {
		for (let entry of entries) {
			const cr = entry.contentRect; // this
			// console.log('Element:', entry.target);
			// console.log(`Element size: ${cr.width}px x ${cr.height}px`);
			// console.log(`Element padding: ${cr.top}px ; ${cr.left}px`);
			if (window.innerHeight > 880) {
				element.style.maxHeight = `${cr.height - minusValue}px`;
			}
		}
	});
	section_ro.observe(section);
}
