function createElement(type, attributes) {
	var element = document.createElement(type);
	for (var key in attributes) {
		if (key == 'class') {
			element.className = attributes[key];
		} else if (key == 'data') {
			for (var dataKey in attributes[key]) {
				element.setAttribute(`data-${dataKey}`, attributes[key][dataKey]);
			}
		} else if (key == 'html') {
			element.innerHTML = attributes[key];
		} else if (key == 'for') {
			element.htmlFor = attributes[key];
		} else {
			element[key] = attributes[key];
		}
	}
	return element;
}

export function renderPagination(location, option) {
	// location에 페이지네이션이 있으면 모두 제거
	if (location.querySelectorAll('button')) {
		location.querySelectorAll('button').forEach((el) => el.remove());
	}
	if (option['totalCount'] <= option['page_limit'])
		// 현재 게시물의 전체 개수가 page_limit 이하면 pagination을 숨깁니다.
		return;

	var totalPage = Math.ceil(option['totalCount'] / option['page_limit']);
	var pageGroup = Math.ceil(option['page'] / option['numOfBtns']);

	var last = pageGroup * option['numOfBtns'];
	if (last > totalPage) last = totalPage;
	var first = last - (option['numOfBtns'] - 1) <= 0 ? 1 : last - (option['numOfBtns'] - 1);
	var next = last + 1;
	var prev = first - 1;

	const fragmentPage = document.createDocumentFragment();
	if (prev > 0) {
		var allpreli = createElement('button', {
			id: 'goFirst',
			class: 'btn_style_paging',
			html: '<i class="fa-solid fa-angles-left"></i>',
		});

		var preli = createElement('button', {
			id: 'goPrev',
			class: 'btn_style_paging',
			html: '<i class="fa-solid fa-angle-left"></i>',
		});

		fragmentPage.append(allpreli);
		fragmentPage.append(preli);
	}

	for (var i = first; i <= last; i++) {
		let page = createElement('button', {
			id: `pageBtn`,
			class: 'btn_style_paging',
			data: {
				num: `${i}`,
			},
			html: i,
		});
		fragmentPage.append(page);
	}

	if (last < totalPage) {
		var allendli = createElement('button', {
			id: 'goLast',
			class: 'btn_style_paging',
			html: '<i class="fa-solid fa-angles-right"></i>',
		});

		var endli = createElement('button', {
			id: 'goNext',
			class: 'btn_style_paging',
			html: '<i class="fa-solid fa-angle-right"></i>',
		});

		fragmentPage.append(endli);
		fragmentPage.append(allendli);
	}

	location.append(fragmentPage);
	// 페이지 목록 생성

	location.querySelectorAll('#pageBtn').forEach((el) => {
		if (el.dataset.num == option['page']) {
			el.classList.add('on');
		} else {
			el.classList.remove('on');
		}
	});

	location.querySelectorAll('button').forEach((el) => {
		el.addEventListener('click', () => {
			let id = el.getAttribute('id');
			let selectedPage = el.dataset.num;

			if (id == 'goNext') selectedPage = next;
			if (id == 'goPrev') selectedPage = prev;
			if (id == 'goFirst') selectedPage = 1;
			if (id == 'goLast') selectedPage = totalPage;

			renderPagination(location, {
				page: selectedPage,
				page_limit: option['page_limit'],
				totalCount: option['totalCount'],
				numOfBtns: option['numOfBtns'],
				pageClick: option['pageClick'],
			});

			option['pageClick']();
		});
	});
}
