export function setCookie(name, value, days) {
	let exdate = new Date();
	exdate.setDate(exdate.getDate() + days); // 만료될 날짜 설정
	let expireDate = 'expires=' + exdate.toUTCString();
	document.cookie = name + '=' + value + ';' + expireDate + ';path=/'; //쿠키 만든다!
}

export function getCookie(name) {
	let cookieArr = document.cookie.split(';');
	for (let i in cookieArr) {
		if (cookieArr[i].split('=')[0].trim() == name)
			if (cookieArr[i][cookieArr[i].length - 1] != '=') return cookieArr[i].split('=')[1];
	}
	return '';
}
