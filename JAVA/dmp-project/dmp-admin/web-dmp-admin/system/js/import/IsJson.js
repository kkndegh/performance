export function IsJson(str) {
	try {
		let json = JSON.parse(str);
		return typeof json === 'object';
	} catch (e) {
		return false;
	}
}

export function JsonView(str) {
	let obj = JSON.parse(str);
	return JSON.stringify(obj, undefined, 4);
}
