import { createElement } from './CreateElement';

export function searchForm(target_location, attributes) {
	for (let key in attributes) {
		if (key == 'period') {
			let div = createElement('div', {
				class: 'period',
			});
			let b = createElement('b', {
				html: '기간 조회',
			});
			let input = createElement('input', {
				type: 'text',
				name: 'dateTimePicker1',
			});
			let span = createElement('span', {
				html: '~',
			});
			let input2 = createElement('input', {
				type: 'text',
				name: 'dateTimePicker2',
			});

			let div2 = createElement('div', {
				class: 'fastBtns',
			});

			div.append(b, input, span, input2, div2);
			target_location.append(div);

			for (let eachKey in attributes[key]) {
				if (eachKey == 'format') {
					if (attributes[key][eachKey] == 'YYYY-MM-DD') {
						input.setAttribute('placeholder', 'YYYY-MM-DD');
						input2.setAttribute('placeholder', 'YYYY-MM-DD');
						$('input[name="dateTimePicker1"]').datepicker({
							changeMonth: true,
							changeYear: true,
							showButtonPanel: true,
							currentText: '오늘',
							closeText: '닫기',
							dateFormat: 'yy-mm-dd',
							monthNamesShort: [
								'1월',
								'2월',
								'3월',
								'4월',
								'5월',
								'6월',
								'7월',
								'8월',
								'9월',
								'10월',
								'11월',
								'12월',
							],
							dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
						});
						$('input[name="dateTimePicker2"]').datepicker({
							changeMonth: true,
							changeYear: true,
							showButtonPanel: true,
							currentText: '오늘',
							closeText: '닫기',
							dateFormat: 'yy-mm-dd',
							monthNamesShort: [
								'1월',
								'2월',
								'3월',
								'4월',
								'5월',
								'6월',
								'7월',
								'8월',
								'9월',
								'10월',
								'11월',
								'12월',
							],
							dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
						});
					} else if (attributes[key][eachKey] == 'YYYY-MM-DD, HH-MM-SS') {
						input.setAttribute('placeholder', 'YYYY-MM-DD, HH-MM-SS');
						input2.setAttribute('placeholder', 'YYYY-MM-DD, HH-MM-SS');
						$('input[name="dateTimePicker1"]').datepicker({
							changeMonth: true,
							changeYear: true,
							showButtonPanel: true,
							currentText: '오늘',
							closeText: '닫기',
							dateFormat: 'yy-mm-dd',
							monthNamesShort: [
								'1월',
								'2월',
								'3월',
								'4월',
								'5월',
								'6월',
								'7월',
								'8월',
								'9월',
								'10월',
								'11월',
								'12월',
							],
							dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
						});
						$('input[name="dateTimePicker2"]').datepicker({
							changeMonth: true,
							changeYear: true,
							showButtonPanel: true,
							currentText: '오늘',
							closeText: '닫기',
							dateFormat: 'yy-mm-dd',
							monthNamesShort: [
								'1월',
								'2월',
								'3월',
								'4월',
								'5월',
								'6월',
								'7월',
								'8월',
								'9월',
								'10월',
								'11월',
								'12월',
							],
							dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
						});
					}
				} else if (eachKey == 'fastBtns') {
					attributes[key][eachKey].forEach((el) => {
						let btn = createElement('button', {
							class: 'btn_style_2 btn_color_point',
							html: el['text'],
						});
						div2.append(btn);

						btn.addEventListener('click', () => {
							input.value = el['clickHandler'][0];
							input2.value = el['clickHandler'][1];
						});
					});
				}
			}
		}
		if (key == 'options') {
			for (let row of attributes[key]) {
				let div = createElement('div', {
					class: 'optionRow',
				});
				let b = createElement('b', {
					html: row['title'],
				});
				let div2 = createElement('div', {
					class: 'checkBoxRow',
					id: row['title'],
				});

				if (row['checkBox']) {
					let checkBox = row['checkBox'];
					for (let optKey in checkBox) {
						let div3 = createElement('div', {
							class: 'checkBoxDiv',
						});
						let input = createElement('input', {
							type: 'checkBox',
							class: 'chkBox',
							id: optKey,
						});
						let label = createElement('label', {
							class: 'checkBox',
							for: optKey,
						});
						let span = createElement('span', {
							html: checkBox[optKey],
						});
						div3.append(input, label, span);
						div2.append(div3);

						div3.addEventListener('click', () => {
							if (event.target !== label) {
								label.click();
							}
						});
					}
				} else if (row['select']) {
					let selectOpt = row['select'];
					let select = createElement('select', {
						id: selectOpt['id'],
					});
					for (let optKey of selectOpt['option']) {
						let option = createElement('option', {
							value: optKey['value'],
							html: optKey['html'],
						});
						select.append(option);
						if (optKey['selected']) {
							option.setAttribute('selected', true);
						}
					}
					div2.append(select);
				} else if (row['toggle']) {
					let toggleBox = row['toggle'];
					let toggle = createElement('toggle', {
						id: toggleBox,
					});
					div2.append(toggle);
				}
				div.append(b, div2);

				target_location.append(div);
			}
		}
		if (key == 'textInput' && attributes[key] == true) {
			let div = createElement('div', {
				class: 'textInputRow',
			});
			let b = createElement('b', {
				html: '리스트 내 검색',
			});
			let input = createElement('input', {
				name: 'searchInput',
				type: 'text',
			});
			let button = createElement('button', {
				class: 'btn_style_3 btn_color_point',
				id: 'searchBtn',
				html: '<i class="fa-solid fa-magnifying-glass"></i> 검색',
			});
			div.append(b, input, button);

			target_location.append(div);
		}
		if (key == 'searchBtn' && attributes[key] == true && attributes['textInput'] == false) {
			let div = createElement('div', {
				class: 'searchRow',
			});
			let button = createElement('button', {
				class: 'btn_style_3 btn_color_point',
				id: 'searchBtn',
				html: '<i class="fa-solid fa-magnifying-glass"></i> 검색',
			});
			div.append(button);
			target_location.append(div);
		}
	}
}

//예시
// searchForm(location, {
// 	period: true,
// 	options: [
// 		{
// 			title: '검색 조건1',
// 			checkBox: {
// 				opt1: '옵션 1',
// 				opt2: '옵션 2',
// 				opt3: '옵션 3',
// 			},
// 		},
// 		{
// 			title: '검색 조건2',
// 			checkBox: {
// 				opt4: '옵션 1',
// 				opt5: '옵션 2',
// 				opt6: '옵션 3',
// 				opt7: '옵션 4',
// 				opt8: '옵션 5',
// 			},
// 		},
// 		{
// 			title: '스위치 작동',
// 			toggle: 'switchId',
// 		},
// 		{
// 			title: 'select 설정',
// 			select: {
// 				id: 'interval',
// 				option: [
// 					{ value: 1, html: '1분' },
// 					{ value: 3, html: '3분' },
// 					{ value: 5, html: '5분' },
// 					{ value: 10, html: '10분' },
// 					{ value: 30, html: '30분' },
// 					{ value: 60, html: '1시간', selected: true },
// 					{ value: 120, html: '2시간' },
// 					{ value: 240, html: '4시간' },
// 					{ value: 360, html: '6시간' },
// 					{ value: 720, html: '12시간' },
// 					{ value: 1440, html: '1일' },
// 				],
// 			},
// 		},
// 	],
// 	textInput: true,
// });

// Datepicker;
// $('.datepicker')
// 	.datepicker({
// 		autoclose: true,
// 		format: 'yyyy-mm-dd',
// 		immediateUpdates: true,
// 		todayBtn: true,
// 		todayHighlight: true,
// 	})
// 	.datepicker('setDate', '0');
