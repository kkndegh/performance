export function modalShow(obj) {
	const modal = document.querySelector('.modal');
	const title = document.querySelector('.modal .layerPopup .popupHead h3');
	const popupBody = document.querySelector('.modal .layerPopup .popupBody');
	const btnBox = document.querySelector('.modal .layerPopup .btnBox');
	const cancelBtn = document.querySelector('.modal .layerPopup .popupHead i.fa-xmark');

	if (popupBody.querySelector('.box')) {
		popupBody.querySelector('.box').remove();
	}
	if (btnBox.querySelector('button')) {
		btnBox.querySelector('button').remove();
	}
	if (cancelBtn.clickHandler) {
		cancelBtn.removeEventListener('click', cancelBtn.clickHandler);
	}

	modal.style.display = 'flex';
	title.innerHTML = obj.title;

	cancelBtn.addEventListener('click', () => {
		modal.style.display = 'none';
	});

	if (obj.refresh == true) {
		cancelBtn.addEventListener('click', () => {
			location.reload();
		});
	}
	if (obj.btn == true) {
		let newBtn = document.createElement('button');
		newBtn.innerHTML = obj.btnText;
		newBtn.classList.add(obj.btnColor);
		btnBox.appendChild(newBtn);
		if (obj.callback !== undefined) {
			newBtn.addEventListener('click', () => {
				if (obj.callback_param !== undefined) {
					obj.callback(obj.callback_param);
				} else {
					obj.callback();
				}
			});
		} else {
			newBtn.addEventListener('click', () => {
				cancelBtn.click();
			});
		}
	}
	if (obj.func !== undefined) {
		if (obj.param !== undefined) {
			obj.func(obj.param);
		} else {
			obj.func();
		}
	}
}

// let obj = {};
// obj.title = '타이틀';
// obj.btn = true; //bntBox에 버튼 생성
// obj.btnText = '버튼내용';
// obj.btnColor = '버튼색상'; //magenta, point, gray
// obj.func = '팝업 안에서 실행될 함수', 팝업이 켜지자마자 실행됨;
// obj.param = ['함수 파라미터'];
// obj.callback = '콜백함수', 팝업의 확인버튼을 누르면 실행됨;
// obj.callback_param = ['콜백함수 파라미터1', '콜백함수 파라미터2'];
// obj.refresh = true; //닫기 누르면 새로고침

export function alertShow(obj) {
	const modal = document.querySelector('.alert_modal');
	const title = document.querySelector('.alert_modal .alert_layerPopup .alert_popupHead h3');
	const popupBody = document.querySelector('.alert_modal .alert_layerPopup .alert_popupBody');
	const btnBox = document.querySelector('.alert_modal .alert_layerPopup .alert_btnBox');
	const cancelBtn = document.querySelector(
		'.alert_modal .alert_layerPopup .alert_popupHead i.fa-xmark'
	);

	if (popupBody.querySelector('.box')) {
		popupBody.querySelector('.box').remove();
	}
	if (btnBox.querySelector('button')) {
		btnBox.querySelector('button').remove();
	}

	modal.style.display = 'flex';
	title.innerHTML = obj.title;

	let newBox = document.createElement('div');
	newBox.classList.add('box');
	popupBody.appendChild(newBox);

	cancelBtn.addEventListener('click', () => {
		modal.style.display = 'none';
	});

	if (obj.btn == true) {
		let newBtn = document.createElement('button');
		newBtn.innerHTML = obj.btnText;
		newBtn.classList.add(obj.btnColor);
		btnBox.appendChild(newBtn);
		if (obj.callback !== undefined) {
			newBtn.addEventListener('click', () => {
				if (obj.callback_param !== undefined) {
					obj.callback(obj.callback_param);
				} else {
					obj.callback();
				}
			});
		} else if (obj.refresh == true) {
			newBtn.addEventListener('click', () => {
				location.reload();
			});
		} else {
			newBtn.addEventListener('click', () => {
				cancelBtn.click();
			});
		}
	}

	let newP = document.createElement('p');
	newP.innerHTML = obj.text;
	newBox.appendChild(newP);
	if (obj.text2 !== undefined) {
		let newP2 = document.createElement('p');
		let newSmall = document.createElement('small');
		newP2.appendChild(newSmall);
		newSmall.innerHTML = obj.text2;
		newBox.appendChild(newP2);
	}
}

// let obj={};
// obj.title = '타이틀';
// obj.btn = true; //bntBox에 버튼 생성
// obj.btnText = '버튼내용';
// obj.btnColor = '버튼색상'; //magenta, point, gray
// obj.text = '모달 안에 들어갈 텍스트';
// obj.text2 = '모달 안에 들어갈 텍스트 (small)';
// obj.callback = '콜백함수';
// obj.callback_param = ['콜백함수 파라미터1', '콜백함수 파라미터2'];
// obj.refresh = true; //닫기 누르면 새로고침

export function cancelAlert() {
	const modal = document.querySelector('.alert_modal');
	modal.style.display = 'none';
}
export function cancelModal() {
	const modal = document.querySelector('.modal');
	modal.style.display = 'none';
}
