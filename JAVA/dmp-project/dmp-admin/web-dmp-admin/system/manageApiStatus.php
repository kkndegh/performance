<?php
include($_SERVER["DOCUMENT_ROOT"] . "/system/include/phpHeader.php");
?>
<html>

<head>
    <?php
    include($root . "/system/include/head.php");
    ?>
    <script type="module" src="/system/js/common/common.js?v=<?php echo $config["version"]; ?>" defer></script>
    <script type="module" src="/system/js/manageApiStatus.js?v=<?php echo $config["version"]; ?>" defer></script>
    <link rel="stylesheet" href="/system/css/manageApiStatus.css?v=<?php echo $config["version"]; ?>">
</head>

<body>
    <?php
    include($root . "/system/include/loadingBox.php");
    ?>
    <div id="wrap" class="leftListClose subMenuClose">
        <div class="left">
            <menu>
                <?php
                include($root . "/system/include/menu.php");
                ?>
            </menu>
        </div>
        <div class="right">
            <header>
                <?php
                include($root . "/system/include/header.php");
                ?>
            </header>
            <div class="container">
                <nav class="subMenu"></nav>
                <main>
                    <?php
                    include($root . "/system/include/toolbar.php");
                    ?>
                    <section>
                        <h2>API 상태 관리</h2>
                        <article id="summary">
                            <div class="title">
                                <div class="leftTitle">
                                    <p>실시간 API 현황</p>
                                </div>
                                <button id="interval" class="interval">
                                    <p>자동 검사 인터벌</p>
                                    <i class="fa-regular fa-clock duration"></i>
                                </button>
                            </div>
                            <div class="content">
                                <div class="apiTimeGap">
                                    <h5>Response Time</h5>
                                    <p>평균 <strong>0</strong> ms</p>
                                </div>
                                <div class="arrowAnimation">
                                    <i class="fa-solid fa-chevron-right"></i>
                                    <i class="fa-solid fa-chevron-right"></i>
                                    <i class="fa-solid fa-chevron-right"></i>
                                </div>
                                <div class="apiReq">
                                    <i class="fa-solid fa-cloud-arrow-down"></i>
                                    <h5>정상 API</h5>
                                    <h5 class="num"><b>0</b>건</h5>
                                </div>
                                <div class="apiErr">
                                    <i class="fa-solid fa-triangle-exclamation"></i>
                                    <i class="triangle fa-solid fa-triangle-exclamation"></i>
                                    <h5>API 오류</h5>
                                    <h5 class="num"><b>0</b>건</h5>
                                </div>
                                <div class="checkAPIBox">
                                    <h5>마지막 검사 시간</h5>
                                    <p>0000-00-00 00:00:00</p>
                                    <button id="apiAllCheckBtn" class="btn_style_3 btn_color_point">전체 API 검사 <i class="fa-solid fa-plug-circle-check"></i></button>
                                </div>
                            </div>
                        </article>
                        <article id="apiStatus">
                            <div class="flexBox">
                                <div class="leftBox">
                                    <h3>개별 API 상태</h3>
                                    <br>
                                    <div class="searchOption"></div>
                                    <div class="tableTopDiv">
                                        <div class="detailCount">
                                            <div class="normal countBox">
                                                <p>정상 : <b class="count">0</b></p>
                                            </div>
                                            <div class="caution countBox">
                                                <p>주의 : <b class="count">0</b></p>
                                            </div>
                                            <div class="error countBox">
                                                <p>오류 : <b class="count">0</b></p>
                                            </div>
                                            <div class="unknown countBox">
                                                <p>알수없음 : <b class="count">0</b></p>
                                            </div>
                                        </div>
                                        <p class="totalCount">Total Count : <strong>0</strong></p>
                                    </div>
                                    <div class="tableBox scrollable">
                                        <table>
                                            <thead></thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="rightBox">
                                    <h3>로그</h3>
                                    <br>
                                    <div class="log scrollable">
                                        <table>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </section>
                </main>
                <aside>
                    <?php
                    include($root . "/system/include/aside.php");
                    ?>
                </aside>
            </div>
        </div>
        <?php
        include($root . "/system/include/modal.php");
        ?>
    </div>
</body>

</html>