<?php
include($_SERVER["DOCUMENT_ROOT"] . "/system/include/phpHeader.php");
?>
<html>

<head>
    <?php
    include($root . "/system/include/head.php");
    ?>
    <script type="module" src="/system/js/common/common.js?v=<?php echo $config["version"]; ?>" defer></script>
    <script type="module" src="/system/js/mypage.js?v=<?php echo $config["version"]; ?>" defer></script>
    <link rel="stylesheet" href="/system/css/mypage.css?v=<?php echo $config["version"]; ?>">
</head>

<body>
    <?php
    include($root . "/system/include/loadingBox.php");
    ?>
    <div id="wrap" class="leftListClose subMenuClose">
        <div class="left">
            <menu>
                <?php
                include($root . "/system/include/menu.php");
                ?>
            </menu>
        </div>
        <div class="right">
            <header>
                <?php
                include($root . "/system/include/header.php");
                ?>
            </header>
            <div class="container">
                <nav class="subMenu"></nav>
                <main>
                    <?php
                    include($root . "/system/include/toolbar.php");
                    ?>
                    <section>
                        <h2>마이페이지</h2>
                        <br>
                        <article id="basicInfo">
                            <h3>기본 정보</h3>
                            <div class="id box">
                                <p class="leftKey">아이디</p>
                                <input value="<?php echo $_SESSION['user_id']; ?>" readonly></p>
                            </div>
                            <div class="pw box">
                                <p class="leftKey">비밀번호</p>
                                <input type="password" value=""></p>
                                <span class="showPw"><i class="fa-solid fa-eye"></i></span>
                            </div>
                            <div class="userName box">
                                <p class="leftKey">이름</p>
                                <input value="<?php echo $_SESSION['user_account_name']; ?>"></p>
                            </div>
                        </article>
                        <article id="socialInfo">
                            <h3>소셜 로그인 정보</h3><br>
                            <h4>현재 연동된 소셜 아이디</h4>
                            <p>DMP 계정과 연동되어 있는 소셜 계정을 보여줍니다.</p>
                            <div class="social">
                                <div class="hiworks box">
                                    <img src="/system/img/hiworks_icon.png" alt="hiworks">
                                    <div class="loginInfo">
                                        <p class="connect"></p>
                                        <p class="connectDate"></p>
                                    </div>
                                    <div class="hoverBox">
                                        <i class="fa-solid fa-link-slash"></i> 연동 해제
                                    </div>
                                </div>
                            </div>
                        </article>
                    </section>
                </main>
                <aside>
                    <?php
                    include($root . "/system/include/aside.php");
                    ?>
                </aside>
            </div>
        </div>
        <?php
        include($root . "/system/include/modal.php");
        ?>
    </div>
</body>

</html>