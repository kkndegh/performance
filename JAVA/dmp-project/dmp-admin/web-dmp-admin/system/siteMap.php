<?php
include($_SERVER["DOCUMENT_ROOT"] . "/system/include/phpHeader.php");
?>
<html>

<head>
    <?php
    include($root . "/system/include/head.php");
    ?>
    <script type="module" src="/system/js/common/common.js?v=<?php echo $config["version"]; ?>" defer></script>
    <script type="module" src="/system/js/siteMap.js?v=<?php echo $config["version"]; ?>" defer></script>
    <link rel="stylesheet" href="/system/css/siteMap.css?v=<?php echo $config["version"]; ?>">
</head>

<body>
    <?php
    include($root . "/system/include/loadingBox.php");
    ?>
    <div id="wrap" class="leftListClose subMenuClose">
        <div class="left">
            <menu>
                <?php
                include($root . "/system/include/menu.php");
                ?>
            </menu>
        </div>
        <div class="right">
            <header>
                <?php
                include($root . "/system/include/header.php");
                ?>
            </header>
            <div class="container">
                <nav class="subMenu"></nav>
                <main>
                    <?php
                    include($root . "/system/include/toolbar.php");
                    ?>
                    <section>
                        <h2>사이트 맵</h2>
                        <br>
                        <article>
                            <div id="map"></div>
                        </article>
                    </section>
                </main>
                <aside>
                    <?php
                    include($root . "/system/include/aside.php");
                    ?>
                </aside>
            </div>
        </div>
        <?php
        include($root . "/system/include/modal.php");
        ?>
    </div>
</body>

</html>