<?php
include($_SERVER["DOCUMENT_ROOT"] . "/system/include/phpHeader.php");
?>
<html>

<head>
    <?php
    include($root . "/system/include/head.php");
    ?>
    <script type="module" src="/system/js/common/common.js?v=<?php echo $config["version"]; ?>" defer></script>
    <script type="module" src="/system/js/manageApi.js?v=<?php echo $config["version"]; ?>" defer></script>
    <link rel="stylesheet" href="/system/css/manageApi.css?v=<?php echo $config["version"]; ?>">
</head>

<body>
    <?php
    include($root . "/system/include/loadingBox.php");
    ?>
    <div id="wrap" class="leftListClose subMenuClose">
        <div class="left">
            <menu>
                <?php
                include($root . "/system/include/menu.php");
                ?>
            </menu>
        </div>
        <div class="right">
            <header>
                <?php
                include($root . "/system/include/header.php");
                ?>
            </header>
            <div class="container">
                <nav class="subMenu"></nav>
                <main>
                    <?php
                    include($root . "/system/include/toolbar.php");
                    ?>
                    <section>
                        <h2>API 관리 | API Test</h2>
                        <article id="apiList">
                            <h3>API List</h3>
                            <br>
                            <div class="flexBox">
                                <div class="leftBox">
                                    <div class="searchOption">

                                    </div>
                                    <div class="tableTopDiv">
                                        <button id="addNewApi" class="btn_style_2 btn_color_point">
                                            <i class="fa-solid fa-plus"></i>
                                            새 API 추가
                                        </button>
                                        <p class="totalCount">Total Count : <strong>0</strong></p>
                                    </div>
                                    <div class="tableDiv scrollable">
                                        <table>
                                            <thead></thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <div class="tableBottomDiv">
                                        <div class="rowInViewDiv">
                                            <b>Show</b>
                                            <select name="rowInView" id="rowInView">
                                                <option value="10">10</option>
                                                <option value="30">30</option>
                                                <option value="50" selected>50</option>
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                            </select>
                                        </div>
                                        <div class="paging"></div>
                                    </div>
                                </div>
                                <div class="rightBox">
                                    <h4>API 상세</h4>
                                    <br>
                                    <div class="tableDiv scrollable">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>왼쪽 리스트에서 API를 클릭하세요</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tableBottomDiv"></div>
                                </div>
                            </div>
                        </article>
                        <article id="apiTest">
                            <h3>API Test</h3>
                            <br>
                            <div class="flexBox">
                                <div class="leftBox">
                                    <div class="listWrapper sortable">
                                        <div class="apiListDiv listDiv scrollable"></div>
                                    </div>
                                </div>
                                <div class="rightBox">
                                    <div class="method">
                                        <select name="method" id="method">
                                            <option value="POST">POST</option>
                                            <option value="GET">GET</option>
                                        </select>
                                        <input type="text" placeholder="API를 요청할 URL을 입력하세요">
                                        <button class="apiRequest" id="apiRequest">Send</button>
                                    </div>
                                    <ul class="tagBox">
                                        <li class="active">Parameter</li>
                                        <li>Header</li>
                                        <li class="tagBody">Body</li>
                                    </ul>
                                    <ul class="apiInputBox scrollable">
                                        <li class="apiParam active">
                                            <h5>Parameter (Query String)</h5>
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <td>Key</td>
                                                        <td>Value</td>
                                                        <td>Description</td>
                                                        <td></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><input type="text" placeholder="Key"></td>
                                                        <td><input type="text" placeholder="Value"></td>
                                                        <td><input type="text" placeholder="Description"></td>
                                                        <td><i class="fa-solid fa-trash-can" onclick=delThisTr()></i></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <button id="newTd" class="btn_style_1 btn_color_point"><i class="fa-solid fa-plus"></i> Add another row</button>
                                        </li>
                                        <li class="apiHeader">
                                            <h5>Header</h5>
                                            <!-- <button class="hiddenHeader"><i class="fa-solid fa-eye"></i> See Default Header</button> -->
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <td>Key</td>
                                                        <td>Value</td>
                                                        <td>Description</td>
                                                        <td></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><input type="text" placeholder="Key"></td>
                                                        <td><input type="text" placeholder="Value"></td>
                                                        <td><input type="text" placeholder="Description"></td>
                                                        <td><i class="fa-solid fa-trash-can" onclick=delThisTr()></i></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <button id="newTd" class="btn_style_1 btn_color_point"><i class="fa-solid fa-plus"></i> Add another row</button>
                                        </li>
                                        <li class="apiBody">
                                            <h5>Body</h5>
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <td>Key</td>
                                                        <td>Value</td>
                                                        <td>Description</td>
                                                        <td></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><input type="text" placeholder="Key"></td>
                                                        <td><input type="text" placeholder="Value"></td>
                                                        <td><input type="text" placeholder="Description"></td>
                                                        <td><i class="fa-solid fa-trash-can" onclick=delThisTr()></i></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <button id="newTd" class="btn_style_1 btn_color_point"><i class="fa-solid fa-plus"></i> Add another row</button>
                                            <div id="bodyEditor"></div>
                                        </li>
                                    </ul>
                                    <div class="request">
                                        <h5>Request</h5>
                                        <div class="requestBox scrollable">
                                            <pre></pre>
                                        </div>
                                    </div>
                                    <div class="response">
                                        <h5>Response</h5>
                                        <div class="responseBox scrollable">
                                            <pre></pre>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </section>
                </main>
                <aside>
                    <?php
                    include($root . "/system/include/aside.php");
                    ?>
                </aside>
            </div>
        </div>
        <?php
        include($root . "/system/include/modal.php");
        ?>
    </div>
    <script>
        function delThisTr() {
            let target = event.target;
            let td = target.parentNode.parentNode.querySelectorAll('td');
            td.forEach((el, idx) => {
                if (idx !== 3) {
                    el.querySelector('input').value = '';
                }
            });
        }
    </script>
</body>

</html>