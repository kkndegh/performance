<?php
function ClientAPI($group, $code, $param, $header, $body)
{
    //global $svc_id;

    $ip = $_SERVER['REMOTE_ADDR'];
    $host = $_SERVER['HTTP_HOST'];

    if ($body == "null") {
        $method = "GET";
    } else {
        $method = "POST";
    }

    $config = parse_ini_file($_SERVER["DOCUMENT_ROOT"] . '/.config/app.ini');

    //개발서버용

    $url = $config["curl_url"];
    $url .= "/api/v1/" . $group . "/" . $code . $param;

    // $header = json_decode($header, true);

    // $header_arr = array();

    // foreach ($header as $key => $value) {
    //     array_push($header_arr, $key.": ".$value);
    // }

    // array_push($header_arr, "Content-Type: application/json");
    // array_push($header_arr, "access_token: asdf");

    // return var_dump($header_arr) . '<br>' . var_dump($body);
    // $toheader = array(
    //         "Content-Type: ".$header["Content-Type"]
    //         ,"access_token: ".$header["access_token"]
    //         );

    // if(isset($_SESSION["serviceToken"])){
    //     $header = array('serviceToken: '.$_SESSION["serviceToken"]);
    // }
    // else{
    //     $header = array();
    // }

    //echo var_dump($headers);

    $ch = curl_init();                                 //curl 초기화
    curl_setopt($ch, CURLOPT_URL, $url);               //URL 지정하기
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    //요청 결과를 문자열로 반환
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);      //connection timeout 10초
    //curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);   //원격 서버의 인증서가 유효한지 검사 안함
    // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);   //원격 서버의 인증서가 유효한지 검사 안함
    curl_setopt($ch, CURLOPT_SSLVERSION, 1);   // HTTPS 접속
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);       //POST data
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_POST, true);              //true시 post 전송

    if ($header !== null) {
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    }
    curl_setopt($ch, CURLOPT_FAILONERROR, true);

    $response = curl_exec($ch);

    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

    curl_close($ch);

    $resHeaders = substr($response, 0, $header_size);
    $resBody = substr($response, $header_size);

    if ($http_code == "200") {
        return $resBody;
        //return '<pre>' . var_export($header_arr, true) . '</pre>';
    } else {
        http_response_code($http_code);
        return "{\"httpcode\":\"" . $http_code . "\",\"error\":\"" . curl_error($ch) . "\"}";
    }
}
