<?php
    function array_iconv( $Current, $Next, $Array , $AutoDetect = true ) {
        $new_array = array();
        $Current = strtoupper($Current);
        $Next = strtoupper($Next);
        $encode = array($Current,str_replace('//IGNORE','',$Next));
        foreach($Array as $key => $val) {
            if(is_string($key) && (($AutoDetect == true && mb_detect_encoding($key,$encode) == $Current) || $AutoDetect == false)) {
                $key = iconv($Current, $Next, $key);
            }
            if(is_string($val) && (($AutoDetect == true && mb_detect_encoding($val,$encode) == $Current) || $AutoDetect == false)) {
                $val = iconv($Current, $Next, $val);
            }
            if(is_array($val)) $val = array_iconv( $Current, $Next, $val, $AutoDetect);
            $new_array[$key] = $val;
        }
        return $new_array;
    }

    function han ($s) { return reset(json_decode('{"s":"'.$s.'"}')); }
    function to_han ($str) { return preg_replace('/(\\\u[a-f0-9]+)+/e','han("$0")',$str); }
    
    function ToEucKr( $val ){
        return iconv("UTF-8","euc-kr//IGNORE",$val );
    }
    function ToUTF8( $val ){
        return iconv("euc-kr","UTF-8",$val );
    }
    
    //euc-kr용
    function jsonencode($val) {
        $tmp = obj_change_charset($val, 'utf-8', true);
        $tmp = json_encode( $tmp );
        return $tmp;
    }
    //euc-kr용
    function jsondecode($val) {
        $tmp = obj_change_charset($val, 'utf-8', true);
        $tmp = json_decode( $tmp );
        obj_change_charset( $tmp, 'euc-kr' );
        return $tmp;
    }
    
    function &obj_change_charset( &$obj, $tocharset='utf-8', $clone=false ) {
        $obj_tmp = NULL;
        if ($clone) {
            $obj_tmp = is_object($obj) ? clone $obj : $obj;
        }else{
            $obj_tmp =& $obj;
        }
        
        switch ( gettype($obj_tmp) ) {
            case 'string' :
                //$obj_tmp = mb_convert_encoding( $obj_tmp, $tocharset, mb_detect_encoding( $obj_tmp ) ); //짧은 값은 제대로 인식하지 못하더군요.
                if ('euc-kr'==$tocharset) {
                    $obj_tmp = ToEucKr($obj_tmp);
                }else{
                    $obj_tmp = ToUTF8($obj_tmp);
                }
                break;
            case 'array':
                foreach ($obj_tmp as &$val) {
                    $val = obj_change_charset( $val, $tocharset, $clone );
                }
                break;
            case 'object':
                $object_vars = get_object_vars( $obj_tmp );
                foreach ($object_vars as $key=>$val) {
                    $obj_tmp->$key = obj_change_charset( $val, $tocharset, $clone );
                }
                break;
        }
        
        return $obj_tmp;
    }
?>