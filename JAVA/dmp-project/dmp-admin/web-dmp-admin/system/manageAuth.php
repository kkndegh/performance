<?php
include($_SERVER["DOCUMENT_ROOT"] . "/system/include/phpHeader.php");
?>
<html>

<head>
    <?php
    include($root . "/system/include/head.php");
    ?>
    <script type="module" src="/system/js/common/common.js?v=<?php echo $config["version"]; ?>" defer></script>
    <script type="module" src="/system/js/manageAuth.js?v=<?php echo $config["version"]; ?>" defer></script>
    <link rel="stylesheet" href="/system/css/manageAuth.css?v=<?php echo $config["version"]; ?>">
</head>

<body>
    <?php
    include($root . "/system/include/loadingBox.php");
    ?>
    <div id="wrap" class="leftListClose subMenuClose">
        <div class="left">
            <menu>
                <?php
                include($root . "/system/include/menu.php");
                ?>
            </menu>
        </div>
        <div class="right">
            <header>
                <?php
                include($root . "/system/include/header.php");
                ?>
            </header>
            <div class="container">
                <nav class="subMenu"></nav>
                <main>
                    <?php
                    include($root . "/system/include/toolbar.php");
                    ?>
                    <section>
                        <h2>
                            <?php
                            if (str_contains($_SERVER["QUERY_STRING"], 'project=aos')) {
                                echo "AOS 권한 관리";
                            } else {
                                echo "권한 관리";
                            }
                            ?>
                        </h2>
                        <p class="tip">이 페이지는 권한 자체를 관리하는 페이지 입니다. 유저에게 권한 부여, 수정을 원하시면 <a href="/system/manageUser">유저 관리 페이지</a>로 이동해 주세요.</p>
                        <article id="authGroup">
                            <h3>권한 그룹(API 권한 + 페이지 접근 권한)</h3>
                            <p>실제로 유저에게 부여되는 권한 묶음 입니다. 유저는 권한 묶음 하위의 API 권한과 페이지 접근 권한을 부여받습니다.</p>
                            <br>
                            <div class="flexBox">
                                <div class="leftBox">
                                    <h4>권한 그룹 리스트</h4>
                                    <br>
                                    <div class="listWrapper">
                                        <div class="authGroupListTopDiv listTopDiv">
                                            <button id="addAuthGroup" class="btn_style_2 btn_color_point">
                                                <i class="fa-solid fa-plus"></i>
                                                새 권한그룹 추가
                                            </button>
                                            <p class="totalCount">Total Count : <strong>0</strong></p>
                                        </div>
                                        <div class="authGroupListDiv listDiv scrollable"></div>
                                    </div>
                                </div>
                                <div class="rightBox">
                                    <h4>권한 그룹 정보</h4>
                                    <br>
                                    <table>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <div class="bottomDiv"></div>
                                </div>
                            </div>
                        </article>
                        <article id="apiAuth">
                            <h3>API 권한</h3>
                            <p>API 요청에 대한 권한입니다. 권한 그룹에 종속됩니다.</p>
                            <br>
                            <div class="flexBox">
                                <div class="leftBox">
                                    <h4>API 권한 리스트</h4>
                                    <br>
                                    <div class="listWrapper">
                                        <div class="apiAuthListTopDiv listTopDiv">
                                            <button id="addApiAuth" class="btn_style_2 btn_color_point">
                                                <i class="fa-solid fa-plus"></i>
                                                새 API 권한 추가
                                            </button>
                                            <p class="totalCount">Total Count : <strong>0</strong></p>
                                        </div>
                                        <div class="apiAuthListDiv listDiv scrollable"></div>
                                    </div>
                                </div>
                                <div class="rightBox">
                                    <h4>API 권한 정보</h4>
                                    <br>
                                    <div class="topDiv">
                                        <table>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="infoDiv scrollable">
                                        <table>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <div class="bottomDiv"></div>
                                </div>
                            </div>
                        </article>
                        <article id="menuAuth">
                            <h3>페이지 접근 권한</h3>
                            <p>페이지 접근에 대한 권한입니다. 권한 그룹에 종속됩니다.</p>
                            <br>
                            <div class="flexBox">
                                <div class="leftBox">
                                    <h4>페이지 접근 권한 리스트</h4>
                                    <br>
                                    <div class="listWrapper">
                                        <div class="menuAuthListTopDiv listTopDiv">
                                            <button id="addMenuAuth" class="btn_style_2 btn_color_point">
                                                <i class="fa-solid fa-plus"></i>
                                                새 페이지 접근 권한 추가
                                            </button>
                                            <p class="totalCount">Total Count : <strong>0</strong></p>
                                        </div>
                                        <div class="menuAuthListDiv listDiv scrollable"></div>
                                    </div>
                                </div>
                                <div class="rightBox">
                                    <h4>페이지 접근 권한 정보</h4>
                                    <br>
                                    <div class="topDiv">
                                        <table>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="infoDiv scrollable">
                                        <table>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <div class="bottomDiv"></div>
                                </div>
                            </div>
                        </article>
                        <article id="menuApiAuth">
                            <h3>메뉴 별 허용 API 권한</h3>
                            <p>회원이 권한을 가지고 있지 않더라도 페이지 자체에서 API에 대한 권한이 필요할 경우 여기서 설정해 주세요.</p>
                            <br>
                            <div class="flexBox">
                                <div class="leftBox">
                                    <h4>메뉴 리스트</h4>
                                    <br>
                                    <div class="listWrapper">
                                        <div class="menuListDiv listDiv scrollable"></div>
                                    </div>
                                </div>
                                <div class="rightBox">
                                    <h4>API 리스트</h4>
                                    <br>
                                    <div class="infoDiv scrollable">
                                        <table>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <div class="bottomDiv">
                                        <button class="btn_style_2 btn_color_point">
                                            <i class="fa-solid fa-floppy-disk"></i> 저장하기
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </section>
                </main>
                <aside>
                    <?php
                    include($root . "/system/include/aside.php");
                    ?>
                </aside>
            </div>
        </div>
        <?php
        include($root . "/system/include/modal.php");
        ?>
    </div>
</body>

</html>