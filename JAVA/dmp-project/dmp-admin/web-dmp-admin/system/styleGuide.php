<?php
include($_SERVER["DOCUMENT_ROOT"] . "/system/include/phpHeader.php");
?>
<html>

<head>
    <?php
    include($root . "/system/include/head.php");
    ?>
    <script type="module" src="/system/js/common/common.js?v=<?php echo $config["version"]; ?>" defer></script>
    <script type="module" src="/system/js/styleGuide.js?v=<?php echo $config["version"]; ?>" defer></script>
    <link rel="stylesheet" href="/system/css/styleGuide.css?v=<?php echo $config["version"]; ?>">
</head>

<body>
    <?php
    include($root . "/system/include/loadingBox.php");
    ?>
    <div id="wrap" class="leftListClose subMenuClose">
        <div class="left">
            <menu>
                <?php
                include($root . "/system/include/menu.php");
                ?>
            </menu>
        </div>
        <div class="right">
            <header>
                <?php
                include($root . "/system/include/header.php");
                ?>
            </header>
            <div class="container">
                <nav class="subMenu"></nav>
                <main>
                    <?php
                    include($root . "/system/include/toolbar.php");
                    ?>
                    <section>
                        <h2>Style Guide</h2>
                        <div class="stickyIndex">
                            <h4>목차</h4>
                            <br>
                            <span for="table">테이블과 검색옵션</span>
                            <span for="listDiv">리스트</span>
                            <span for="article3">목차 3</span>
                        </div>
                        <article id="table">
                            <h3>테이블과 검색옵션</h3>
                            <br>
                            <div class="searchOption"></div>
                            <div class="tableTopDiv">
                                <div class="sortDiv">
                                    <button class="btn_style_1 btn_color_point">
                                        시간 오름차순
                                        <i class="fa-solid fa-caret-up"></i>
                                    </button>
                                    <button class="btn_style_1 btn_color_crush">
                                        시간 내림차순
                                        <i class="fa-solid fa-caret-down"></i>
                                    </button>
                                    <button class="btn_style_1 btn_color_point">
                                        이름 오름차순
                                        <i class="fa-solid fa-caret-up"></i>
                                    </button>
                                    <button class="btn_style_1 btn_color_crush">
                                        이름 내림차순
                                        <i class="fa-solid fa-caret-down"></i>
                                    </button>
                                </div>
                                <p class="totalCount">Total Count : <strong>0</strong></p>
                            </div>
                            <table>
                                <thead>
                                    <tr>
                                        <th>JSON Data Key</th>
                                        <th>JSON Data Key</th>
                                        <th>JSON Data Key</th>
                                        <th>JSON Data Key</th>
                                        <th>JSON Data Key</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                    </tr>
                                    <tr>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                    </tr>
                                    <tr>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                    </tr>
                                    <tr>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                    </tr>
                                    <tr>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                    </tr>
                                    <tr>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                        <td>JSON Data Value</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="tableBottomDiv">
                                <div class="rowInViewDiv">
                                    <b>Show</b>
                                    <select name="rowInView" id="rowInView">
                                        <option value="10">10</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="200">200</option>
                                        <option value="500">500</option>
                                        <option value="1000">1000</option>
                                    </select>
                                </div>
                                <div class="paging">
                                </div>
                            </div>
                        </article>
                        <article id="listDiv">
                            <h3>리스트</h3>
                            <br>
                            <div class="listWrapper">
                                <div class="viewOption">
                                    <b>옵션 선택</b>
                                    <div class="checkBoxDiv">
                                        <input type="checkBox" class="chkBox" id="listOption">
                                        <label class="checkBox" for="listOption"></label>
                                    </div>
                                </div>
                                <div class="listTopDiv">
                                    <button id="listBtn" class="btn_style_2 btn_color_point">
                                        <i class="fa-solid fa-plus"></i>
                                        리스트 버튼
                                    </button>
                                    <div class="totalCountDiv">
                                        <p class="totalCount">Total Count : <strong>0</strong></p>
                                    </div>
                                </div>
                                <div class="listDiv scrollable">
                                    <div class="depth1">
                                        <div class="depth1Title">
                                            <i class="fa-solid fa-list"></i>
                                            <p>Depth 1</p>
                                        </div>
                                        <ul class="depth2">
                                            <li>
                                                <p>Depth 2</p>
                                            </li>
                                            <li>
                                                <p>Depth 2</p>
                                            </li>
                                            <li>
                                                <p>Depth 2</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="depth1">
                                        <div class="depth1Title">
                                            <i class="fa-solid fa-list"></i>
                                            <p>Depth 1</p>
                                        </div>
                                    </div>
                                    <div class="depth1 unActivated show">
                                        <div class="depth1Title">
                                            <i class="fa-solid fa-list"></i>
                                            <p>Depth 1</p>
                                        </div>
                                        <ul class="depth2">
                                            <li class="unActivated show">
                                                <p>Depth 2</p>
                                            </li>
                                            <li class="unActivated show">
                                                <p>Depth 2</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article id="button">
                            <h3>버튼</h3>
                            <br>
                            <div>
                                <button>Default</button>
                            </div>
                            <br>
                            <table>
                                <tbody>
                                    <tr>
                                        <th>Style / Color</th>
                                        <th>btn_color_gray</th>
                                        <th>btn_color_point</th>
                                        <th>btn_color_crush</th>
                                        <th>btn_color_caution</th>
                                    </tr>
                                    <tr>
                                        <th>btn_style_1</th>
                                        <td><button class="btn_style_1 btn_color_gray">Gray <i class="fa-solid fa-box"></i></button></td>
                                        <td><button class="btn_style_1 btn_color_point">Point <i class="fa-solid fa-box"></i></button></td>
                                        <td><button class="btn_style_1 btn_color_crush">Crush <i class="fa-solid fa-box"></i></button></td>
                                        <td><button class="btn_style_1 btn_color_caution">Caution <i class="fa-solid fa-box"></i></button></td>
                                    </tr>
                                    <tr>
                                        <th>btn_style_2</th>
                                        <td><button class="btn_style_2 btn_color_gray">Gray <i class="fa-solid fa-box"></i></button></td>
                                        <td><button class="btn_style_2 btn_color_point">Point <i class="fa-solid fa-box"></i></button></td>
                                        <td><button class="btn_style_2 btn_color_crush">Crush <i class="fa-solid fa-box"></i></button></td>
                                        <td><button class="btn_style_2 btn_color_caution">Caution <i class="fa-solid fa-box"></i></button></td>
                                    </tr>
                                    <tr>
                                        <th>btn_style_3</th>
                                        <td><button class="btn_style_3 btn_color_gray">Gray <i class="fa-solid fa-box"></i></button></td>
                                        <td><button class="btn_style_3 btn_color_point">Point <i class="fa-solid fa-box"></i></button></td>
                                        <td><button class="btn_style_3 btn_color_crush">Crush <i class="fa-solid fa-box"></i></button></td>
                                        <td><button class="btn_style_3 btn_color_caution">Caution <i class="fa-solid fa-box"></i></button></td>
                                    </tr>
                                </tbody>
                            </table>
            </div>
            </article>
            </section>
            </main>
            <aside>
                <?php
                include($root . "/system/include/aside.php");
                ?>
            </aside>
        </div>
    </div>
    <?php
    include($root . "/system/include/modal.php");
    ?>
    </div>
</body>

</html>