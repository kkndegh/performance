<?php
include($_SERVER["DOCUMENT_ROOT"] . "/system/include/phpHeader.php");
?>
<html>

<head>
    <?php
    include($root . "/system/include/head.php");
    ?>
    <script type="module" src="/system/js/common/common.js?v=<?php echo $config["version"]; ?>" defer></script>
    <script type="module" src="/system/js/manageMenu.js?v=<?php echo $config["version"]; ?>" defer></script>
    <link rel="stylesheet" href="/system/css/manageMenu.css?v=<?php echo $config["version"]; ?>">
</head>

<body>
    <?php
    include($root . "/system/include/loadingBox.php");
    ?>
    <div id="wrap" class="leftListClose subMenuClose">
        <div class="left">
            <menu>
                <?php
                include($root . "/system/include/menu.php");
                ?>
            </menu>
        </div>
        <div class="right">
            <header>
                <?php
                include($root . "/system/include/header.php");
                ?>
            </header>
            <div class="container">
                <nav class="subMenu"></nav>
                <main>
                    <?php
                    include($root . "/system/include/toolbar.php");
                    ?>
                    <section>
                        <h2>메뉴 관리</h2>
                        <article class="menuStructure">
                            <h3>메뉴 설정</h3>
                            <br>
                            <div class="flexBox">
                                <div class="leftBox">
                                    <div class="listWrapper sortable">
                                        <div class="viewOption">
                                            <b>비활성화 메뉴 보기</b>
                                            <div class="checkBoxDiv">
                                                <input type="checkBox" class="chkBox" id="viewUnactivatedMenu">
                                                <label class="checkBox" for="viewUnactivatedMenu"></label>
                                            </div>
                                        </div>
                                        <div class="menuListTopDiv listTopDiv">
                                            <div class="flex">
                                                <button id="addMenu" class="btn_style_2 btn_color_point">
                                                    <i class="fa-solid fa-plus"></i>
                                                    새 항목 추가
                                                </button>
                                                <button id="saveChange" class="btn_style_2 btn_color_point">
                                                    <i class="fa-solid fa-floppy-disk"></i>
                                                    순서 저장
                                                </button>
                                            </div>
                                            <div class="totalCount">
                                                <p class="menuGroupCount">메뉴그룹 : <strong>0</strong></p>
                                                <p class="menuCount">메뉴 : <strong>0</strong></p>
                                            </div>
                                        </div>
                                        <div class="menuListDiv listDiv scrollable"></div>
                                    </div>
                                </div>
                                <div class="rightBox">
                                    <div class="flexArea">
                                        <div class="menuGroupArea">
                                            <h4>메뉴그룹 상세</h4>
                                            <br>
                                            <div class="tableDiv scrollable">
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td>왼쪽 리스트에서 메뉴그룹을 클릭하세요</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="tableBottomDiv">
                                                <button class="btn_style_2 btn_color_crush">
                                                    <i class="fa-solid fa-trash-can"></i>
                                                    삭제하기
                                                </button>
                                                <button class="btn_style_2 btn_color_point">
                                                    <i class="fa-solid fa-floppy-disk"></i>
                                                    저장하기
                                                </button>
                                            </div>
                                        </div>
                                        <div class="menuArea">
                                            <h4>메뉴 상세</h4>
                                            <br>
                                            <div class="tableDiv scrollable">
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td>왼쪽 리스트에서 메뉴를 클릭하세요</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="tableBottomDiv">
                                                <button class="btn_style_2 btn_color_crush">
                                                    <i class="fa-solid fa-trash-can"></i>
                                                    삭제하기
                                                </button>
                                                <button class="btn_style_2 btn_color_point">
                                                    <i class="fa-solid fa-floppy-disk"></i>
                                                    저장하기
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </section>
                </main>
                <aside>
                    <?php
                    include($root . "/system/include/aside.php");
                    ?>
                </aside>
            </div>
        </div>
        <?php
        include($root . "/system/include/modal.php");
        ?>
    </div>
</body>

</html>