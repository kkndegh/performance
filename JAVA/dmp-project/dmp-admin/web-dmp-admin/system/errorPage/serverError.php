<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>시스템 점검 페이지</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/system/css/serverError.css">
</head>

<body>
    <div id="wrap">
        <div class="container">
            <img class="logo" src="/img/logo.svg" alt="logo">
            <img src="/img/serverCheck.png" alt="systemcheck">
            <h1 class="title">시스템 점검중</h1>
            <p>안정적인 서비스 제공을 위하여 시스템 점검을 실시합니다.<br>점검시간 동안 서비스 이용이 일시 중단되오니 고객님들의 양해 부탁드립니다.</p>
            <p class="error"><?php echo $_GET["error"]; ?></p>
            <p class="contact">오류가 계속될경우 고객센터(1666-0001)로 문의하여 주시기 바랍니다.</p>
            <span class="goback" onclick="history.back()">이전 페이지로 돌아가기</span><br>
        </div>
    </div>
</body>

</html>