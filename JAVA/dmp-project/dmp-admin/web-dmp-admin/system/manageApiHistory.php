<?php
include($_SERVER["DOCUMENT_ROOT"] . "/system/include/phpHeader.php");
?>
<html>

<head>
    <?php
    include($root . "/system/include/head.php");
    ?>
    <script type="module" src="/system/js/common/common.js?v=<?php echo $config["version"]; ?>" defer></script>
    <link rel="stylesheet" href="/system/css/manageApiHistory.css?v=<?php echo $config["version"]; ?>">
    <script type="module" src="/system/js/manageApiHistory.js?v=<?php echo $config["version"]; ?>"></script>
</head>

<body>
    <?php
    include($root . "/system/include/loadingBox.php");
    ?>
    <div id="wrap" class="leftListClose subMenuClose">
        <div class="left">
            <menu>
                <?php
                include($root . "/system/include/menu.php");
                ?>
            </menu>
        </div>
        <div class="right">
            <header>
                <?php
                include($root . "/system/include/header.php");
                ?>
            </header>
            <div class="container">
                <nav class="subMenu"></nav>
                <main>
                    <?php
                    include($root . "/system/include/toolbar.php");
                    ?>
                    <section>
                        <h2>
                            <?php
                            if (str_contains($_SERVER["QUERY_STRING"], 'project=aos')) {
                                echo "AOS API 히스토리";
                            } else {
                                echo "API 히스토리";
                            }
                            ?>
                        </h2>
                        <article>
                            <h3>API 히스토리 테이블</h3>
                            <br>
                            <div class="flexBox">
                                <div class="leftBox">
                                    <div class="searchOption"></div>
                                    <div class="tableTopDiv">
                                        <div class="sortDiv">
                                            <button class="btn_style_1 btn_color_point">
                                                시간 오름차순
                                                <i class="fa-solid fa-caret-up"></i>
                                            </button>
                                            <button class="btn_style_1 btn_color_crush">
                                                시간 내림차순
                                                <i class="fa-solid fa-caret-down"></i>
                                            </button>
                                        </div>
                                        <p class="totalCount">Total Count : <strong>0</strong></p>
                                    </div>
                                    <div class="tableBox scrollable">
                                        <table>
                                            <thead></thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <div class="tableBottomDiv">
                                        <div class="rowInViewDiv">
                                            <b>Show</b>
                                            <select name="rowInView" id="rowInView">
                                                <option value="10">10</option>
                                                <option value="30">30</option>
                                                <option value="50" selected>50</option>
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                            </select>
                                        </div>
                                        <div class="paging"></div>
                                    </div>
                                </div>
                                <div class="rightBox">
                                    <h4>히스토리 상세보기</h4>
                                    <br>
                                    <table>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </article>
                    </section>
                </main>
                <aside>
                    <?php
                    include($root . "/system/include/aside.php");
                    ?>
                </aside>
            </div>
        </div>
        <?php
        include($root . "/system/include/modal.php");
        ?>
    </div>
</body>

</html>