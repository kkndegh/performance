<?php
session_start();
//URL 직접 입력하여 접근하는것 방지
// if ($_SERVER['HTTP_REFERER'] == '') {
//     echo "<script>alert('잘못된 접근입니다.')</script>";
//     echo "<script>history.back();</script>";
//     die;
// }
include($_SERVER["DOCUMENT_ROOT"] . "/system/include/phpHeader.php");

if (isset($_SESSION['hiworks_ID']) == true) {
    $hiworksId = $_SESSION['hiworks_ID'];
    echo "<script>let hiworksId = '$hiworksId'</script>";
    echo "<script>let snsFlag = 'hiworks';</script>";
} else {
    echo "<script>alert('잘못된 접근입니다.')</script>";
    echo "<script>history.back();</script>";
    die;
}
?>

<head>
    <?php
    include($root . "/system/include/head.php");
    ?>
    <link rel="stylesheet" href="/system/css/openIdConnect.css?v=<?php echo $config["version"]; ?>">
</head>

<body>
    <div class="loadingBox">
        <div class="loading"></div>
    </div>
    <div id="wrap">
        <span class="version">version <b><?php echo $VERSION; ?></b></span>
        <div class="login">
            <div class="logoBox">
                <img class="logo" src="/img/logo.svg" alt="logo">
                <i class="fa-solid fa-x"></i>
                <img src='/system/img/hiworks_logo.png' alt='hiworks_logo'>
            </div>
            <div class="loginBox">
                <div class="leftBox">
                    <div class="idDiv">
                        <span class="chkId">아이디를 입력하세요</span>
                        <input type="text" autocomplete="off" name="userId" id="userId" class="input 1" maxlength="35" placeholder="아이디">
                    </div>
                    <div class="pwDiv">
                        <span class="chkPw">비밀번호를 입력하세요</span>
                        <input type="password" autocomplete="new-password" name="userPw" id="userPw" class="input 2" maxlength="15" placeholder="비밀번호">
                    </div>
                </div>
                <div class="rightBox">
                    <button id="connectBtn">계정연동</button>
                </div>
            </div>
            <div class="bottomTextBox">
                <h4>연동된 계정이 존재하지 않습니다</h4>
                <p>기존 DMP 계정 정보를 입력하여 SNS계정과 연동하세요</p>
            </div>
            <div class="goLogin">
                <a href="/login"><i class="fa-solid fa-arrow-right-from-bracket"></i>로그인페이지로 돌아가기</a>
            </div>
        </div>
    </div>
</body>

<script>
    let idInput = document.querySelector('.loginBox .idDiv input#userId');
    let pwInput = document.querySelector('.loginBox .pwDiv input#userPw');

    idInput.addEventListener('keyup', () => {
        if (window.event.keyCode == 13) {
            document.querySelector('#connectBtn').click();
        }
        if (!idInput.value == '') {
            document.querySelector('span.chkId').style.display = 'none';
        }
    })
    pwInput.addEventListener('keyup', () => {
        if (window.event.keyCode == 13) {
            document.querySelector('#connectBtn').click();
        }
        if (!pwInput.value == '') {
            document.querySelector('span.chkPw').style.display = 'none';
        }
    })

    function idCheckSpan() {
        document.querySelector('span.chkId').style.display = 'block';
        document.querySelector('span.chkId').style.animationName = 'shake';
        document.querySelector('span.chkId').style.animationDuration = '.5s';
        idInput.focus();
        return false;
    };

    function pwCheckSpan() {
        document.querySelector('span.chkPw').style.display = 'block';
        document.querySelector('span.chkPw').style.animationName = 'shake';
        document.querySelector('span.chkPw').style.animationDuration = '.5s';
        pwInput.focus();
        return false;
    };

    document.querySelector('#connectBtn').addEventListener('click', () => {
        let userId = idInput.value;
        let userPw = pwInput.value;

        if (userId == '') {
            document.querySelector('span.chkId').innerHTML = '아이디를 입력하세요';
            idCheckSpan();
        } else if (userPw == '') {
            document.querySelector('span.chkPw').innerHTML = '비밀번호를 입력하세요';
            pwCheckSpan();
        } else {
            let bodyData = {};
            bodyData['user_id'] = userId;
            bodyData['user_pw'] = userPw;
            bodyData['login_class'] = 'dmp';
            bodyData = JSON.stringify(bodyData);
            fetch('/api/v1/user/login', {
                    method: "POST",
                    body: bodyData
                })
                .then(res => res.json())
                .then((data) => {
                    if (data.result_code == 'P000') {
                        let bodyData = {};
                        bodyData["user_account_seq_no"] = data.user_account_seq_no;
                        bodyData["hiworks_id"] = hiworksId;
                        bodyData["reg_id"] = userId;
                        bodyData["act_evt"] = 'ins';
                        bodyData = JSON.stringify(bodyData);
                        fetch('/api/v1/user/hiworksIUD', {
                                method: "POST",
                                body: bodyData
                            })
                            .then(res => res.json())
                            .then((data) => {
                                if (data.result_code == 'P000') {
                                    location.href = "<?php echo $config["homepath"]; ?>";
                                } else {
                                    console.log(data);
                                }
                            })
                    } else {
                        // console.log(json.result_msg);
                        if (data.result_code == "P902") {
                            document.querySelector('span.chkPw').innerHTML = data.result_msg;
                            pwCheckSpan();
                        } else if (data.result_code == "P901") {
                            document.querySelector('span.chkId').innerHTML = data.result_msg;
                            idCheckSpan();
                        } else {
                            let obj = {};
                            obj.title = '오류';
                            obj.text = data.result_code;
                            obj.text2 = data.result_msg;
                            alertShow(obj);
                        }
                    }
                })
        }
    })
</script>

</html>