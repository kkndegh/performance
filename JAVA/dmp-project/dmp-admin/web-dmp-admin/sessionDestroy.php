<?php
    //URL 직접 입력하여 접근하는것 방지
    // if($_SERVER['HTTP_REFERER'] == ''){
    //     echo "<script>alert('잘못된 접근입니다.')</script>";
    //     echo "<script>history.back();</script>";
    //     die;
    // }
    $redirect = $_GET['redirect_url'];

    if(!isset($_GET['redirect_url'])){
        echo "<script>alert('잘못된 접근입니다.')</script>";
        echo "<script>history.back();</script>";
        die;
    }else{
        if(strpos($redirect, "hiworks_login") !== false){
            session_start();
            isset($_SESSION)?session_destroy():null;
            header("Location: ".$_SERVER["REMOTE_HOST"]."$redirect");
        }else{
            echo "<script>alert('잘못된 접근입니다.')</script>";
            echo "<script>history.back();</script>";
            die;
        }
    }
?>