<?php
session_start();
isset($_SESSION) ? session_destroy() : null;

include($_SERVER["DOCUMENT_ROOT"] . "/system/include/phpHeader.php");
?>
<html>

<head>
    <?php
    include($root . "/system/include/head.php");
    ?>
    <link rel="stylesheet" href="/system/css/login.css?v=<?php echo $config["version"]; ?>">
</head>

<body>
    <div class="loadingBox">
        <div class="loading"></div>
    </div>
    <div id="wrap">
        <span class="version">version <b><?php echo $VERSION; ?></b></span>
        <div class="login">
            <img class="logo" src="/img/logo.svg" alt="logo">
            <div class="loginBox">
                <div class="leftBox">
                    <div class="idDiv">
                        <span class="chkId">아이디를 입력하세요</span>
                        <input type="text" autocomplete="off" name="userId" id="userId" class="input 1" maxlength="35" placeholder="아이디">
                    </div>
                    <div class="pwDiv">
                        <span class="chkPw">비밀번호를 입력하세요</span>
                        <input type="password" autocomplete="new-password" name="userPw" id="userPw" class="input 2" maxlength="15" placeholder="비밀번호">
                    </div>
                </div>
                <div class="rightBox">
                    <button id="loginOkBtn">LOGIN</button>
                </div>
            </div>
            <div class="idSaveDiv">
                <div class="checkBoxDiv">
                    <input type="checkBox" class="chkBox" id="idSave">
                    <label class="checkBox" for="idSave"></label>
                </div>
                <span>아이디 저장</span>
            </div>
            <div class="ssoLogin">
                <a href="/hiworks_login">
                    <img src="/system/img/hiworks.svg" alt="hiworks">
                </a>
            </div>
        </div>
        <div class="alert_modal">
            <div class="alert_layerPopup">
                <div class="alert_popupHead">
                    <h3></h3>
                    <i class="fa-solid fa-xmark"></i>
                </div>
                <div class="alert_popupBody">

                </div>
                <div class="alert_btnBox">
                </div>
            </div>
        </div>
    </div>

    <script type="module">
        import {
            loading,
            loading_end
        } from '/system/js/import/Loading.js';
        import {
            setCookie,
            getCookie
        } from '/system/js/import/Cookie.js';
        import {
            alertShow
        } from '/system/js/import/ModalShow.js';
        import {
            IsJson
        } from '/system/js/import/IsJson.js';

        let idInput = document.querySelector('.loginBox .idDiv input#userId');
        let pwInput = document.querySelector('.loginBox .pwDiv input#userPw');

        if (getCookie('login_id') !== "") {
            idInput.value = getCookie('login_id');
            document.querySelector('#idSave').checked = true;
            pwInput.focus();
        } else {
            idInput.focus();
        }

        function idCheckSpan() {
            document.querySelector('span.chkId').style.display = 'block';
            document.querySelector('span.chkId').style.animationName = 'shake';
            document.querySelector('span.chkId').style.animationDuration = '.5s';
            idInput.focus();
            return false;
        };

        function pwCheckSpan() {
            document.querySelector('span.chkPw').style.display = 'block';
            document.querySelector('span.chkPw').style.animationName = 'shake';
            document.querySelector('span.chkPw').style.animationDuration = '.5s';
            pwInput.focus();
            return false;
        };

        idInput.addEventListener('keyup', () => {
            if (window.event.keyCode == 13) {
                document.querySelector('#loginOkBtn').click();
            }
            if (!idInput.value == '') {
                document.querySelector('span.chkId').style.display = 'none';
            }
        })
        pwInput.addEventListener('keyup', () => {
            if (window.event.keyCode == 13) {
                document.querySelector('#loginOkBtn').click();
            }
            if (!pwInput.value == '') {
                document.querySelector('span.chkPw').style.display = 'none';
            }
        })

        document.querySelector('#loginOkBtn').addEventListener('click', () => {
            loading();

            let userId = idInput.value;
            let userPw = pwInput.value;

            if (userId == '') {
                document.querySelector('span.chkId').innerHTML = '아이디를 입력하세요';
                idCheckSpan();
                loading_end();
            } else if (userPw == '') {
                document.querySelector('span.chkPw').innerHTML = '비밀번호를 입력하세요';
                pwCheckSpan();
                loading_end();
            } else {
                var obj = new Object();

                obj.user_id = userId;
                obj.user_pw = userPw;
                obj.login_class = "dmp";

                var jsonString = JSON.stringify(obj);

                var error = new Object();

                var url = "/api/v1/user/login";

                fetch(url, {
                        method: 'POST',
                        body: jsonString
                    })
                    .then((response) => response.text())
                    .then((data) => {
                        loading_end();

                        console.log(data);

                        if (IsJson(data)) {
                            var json = JSON.parse(data);
                            if (typeof json.result_code !== 'undefined') {
                                // JSON Type
                                if (json.result_code == 'P000') {

                                    // console.log(json);
                                    //쿠키에 아이디 저장
                                    function setCookie(name, value, days) {
                                        let exdate = new Date(); //
                                        exdate.setDate(exdate.getDate() + days); // 만료될 날짜 설정
                                        let expireDate = 'expires=' + exdate.toUTCString();
                                        document.cookie = name + '=' + value + ';' + expireDate + ';path=/'; //쿠키 만든다!
                                    }
                                    if (document.querySelector('#idSave').checked == true) {
                                        setCookie('login_id', userId, 30);
                                    } else {
                                        setCookie('login_id', userId, 0);
                                    }

                                    //로그인 성공시 인덱스 페이지로 이동
                                    location.href = "<?php echo $config["homepath"]; ?>";

                                } else {
                                    // console.log(json.result_msg);
                                    if (json.result_code == "P902") {
                                        document.querySelector('span.chkPw').innerHTML = json.result_msg;
                                        pwCheckSpan();
                                    } else if (json.result_code == "P901") {
                                        document.querySelector('span.chkId').innerHTML = json.result_msg;
                                        idCheckSpan();
                                    } else {
                                        let obj = {};
                                        obj.title = '오류';
                                        obj.text = json.result_code;
                                        obj.text2 = json.result_msg;
                                        alertShow(obj);
                                    }
                                }
                            } else {
                                // JSON Data가 잘못되었습니다.
                                error.errorCode = "JSON Data Failed";
                                // error.errorMessage = data;
                                console.log('json data 잘못됨')
                                let obj = {};
                                obj.title = '오류';
                                obj.text = error.errorCode;
                                // obj.text2 = error.errorMessage;
                                alertShow(obj);
                            }
                        } else {
                            // JSON Type이 아닙니다.
                            error.errorCode = "JSON Type Failed";
                            // error.errorMessage = data;
                            console.log('json data 아님')
                            let obj = {};
                            obj.title = '오류';
                            obj.text = error.errorCode;
                            // obj.text2 = error.errorMessage;
                            alertShow(obj);
                        }
                    })
                    .catch((error) => {
                        error.errorCode = "Fetch Error";
                        error.errorMessage = error;
                        let obj = {};
                        obj.title = '오류';
                        obj.text = error.errorCode;
                        obj.text2 = error.errorMessage;
                        alertShow(obj);
                    })

                return false;
            }
        });
    </script>
</body>

</html>