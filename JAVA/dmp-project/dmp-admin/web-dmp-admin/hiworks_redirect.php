<?php

header('Content-Type: text/html; charset=UTF-8');
$root = $_SERVER["DOCUMENT_ROOT"];
include $root . '/system/lib/clientapi.php';
include $root . '/system/lib/curl.lib.php';

session_start();
if ($_SERVER['HTTP_REFERER'] == '') {
  echo "<script>alert('잘못된 접근입니다.')</script>";
  echo "<script>history.back();</script>";
  die;
} else {
  // Access Code 요청-----------------------------
  $CLIENT_ID = "ohcoqnsiksqp2g73pkbpvzsxkypncoxz626f5e0abe00a0.15511895.open.apps";
  $CLIENT_SECRET = "x4d76ovvncldoion129nbs4re1qbgpug";
  $AUTH_CODE = $_GET['auth_code'];
  $ACCESS_TOKEN_URL = "https://api.hiworks.com/open/auth/accesstoken";
  $FIELDS = array(
    'client_id' => $CLIENT_ID,
    'client_secret' => $CLIENT_SECRET,
    'grant_type' => 'authorization_code',
    'auth_code' => $AUTH_CODE,
    'access_type' => 'offline'
  );
  $opts = array(
    CURLOPT_URL => $ACCESS_TOKEN_URL,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_SSLVERSION => 1, // TLS
    CURLOPT_POST => true,
    CURLOPT_POSTFIELDS => $FIELDS,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HEADER => false
  );
  $ACCESS_CURL = curl_init();
  curl_setopt_array($ACCESS_CURL, $opts);
  $response = curl_exec($ACCESS_CURL);
  curl_close($ACCESS_CURL);

  $ACCESS_TOKEN_RESPONSE = json_decode($response, true);

  if ($ACCESS_TOKEN_RESPONSE['code'] == 'ERR') {
    echo "<script>alert('잘못된 접근입니다.')</script>";
    echo "<script>location.replace('/')</script>";
    exit;
  }

  $DATA = $ACCESS_TOKEN_RESPONSE['data'];

  $access_token = $DATA['access_token'];
  $refresh_token = $DATA['refresh_token'];
  $office_no = $DATA['office_no'];
  $user_no = $DATA['user_no'];

  //사용자 정보 가져오기-----------------------------
  $method = "GET";
  $header = array(
    "Authorization: Bearer " . $access_token,
    "Content-Type: application/json"
  );

  $url = "https://api.hiworks.com/user/v2/me";
  $body = "";

  $curl_result = curl_request($url, $method, $header, $body);

  $curl_json = json_decode($curl_result, true);

  $_SESSION['hiworks_ID'] = $curl_json['user_id'];

  //DB에서 하이웍스 로그인--------------------------------
  try {
    $config = parse_ini_file($root . '/.config/app.ini');

    $hiworksID = (string)$_SESSION['hiworks_ID'];
    $bodyArr["user_id"] = $hiworksID;
    $bodyArr["login_class"] = "hiworks";
    $body = json_encode($bodyArr, JSON_UNESCAPED_UNICODE);
    $header = array();
    array_push($header, "Content-Type: application/json");
    $access_token = $config['access_token'];
    array_push($header, "access_token: $access_token");
    $response = ClientAPI("user", "login", "", $header, $body);

    $result = json_decode($response, true);
    if ($result["result_code"] == "P000") { //로그인 ㄱㄱ
      $_SESSION["access_token"] = $result["access_token"];
      $_SESSION["refresh_token"] = $result["refresh_token"];
      $_SESSION["user_id"] = $result["user_id"];
      $_SESSION["user_seq_no"] = $result["user_seq_no"];
      $_SESSION["access_token_expire"] = $result["access_token_expire"];
      $_SESSION["refresh_token_expire"] = $result["refresh_token_expire"];
      $_SESSION["user_account_seq_no"] = $result["user_account_seq_no"];
      // var_dump($result);
      header("Location: " . $config['homepath']);
    } elseif ($result["result_code"] == "P901") { //아이디가 없다면?
      header("Location: /openIdConnect");
    } else { //에러 발생
      $error_code = $result["result_code"];
      $error_msg = $result["result_msg"];
      header("Location: /system/errorPage/serverError.php?error=" . $error_code . ' : ' . $error_msg);
    }
  } catch (exception $e) {
    echo "exception:" . $e->getMessage();
  }
}
?>
<!DOCTYPE html>
<html lang="ko">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Redirecting...</title>
  <style>
    p {
      font-size: 2rem;
      font-weight: bold;
      position: relative;
      animation: fade 1.5s ease infinite;
    }

    @keyframes fade {
      0% {
        opacity: 1;
      }

      50% {
        opacity: .5;
      }

      100% {
        opacity: 1;
      }
    }
  </style>
</head>

<body>
  <p>Redirecting...</p>
</body>

</html>