<?php
include($_SERVER["DOCUMENT_ROOT"] . "/system/include/phpHeader.php");
?>
<html>

<head>
    <?php
    include($root . "/system/include/head.php");
    ?>
    <script type="module" src="/system/js/common/common.js?v=<?php echo $config["version"]; ?>" defer></script>
    <script type="module" src="/system/js/custMemberApi.js?v=<?php echo $config["version"]; ?>" defer></script>
    <link rel="stylesheet" href="/system/css/custMemberApi.css?v=<?php echo $config["version"]; ?>">
</head>

<body>
    <?php
    include($root . "/system/include/loadingBox.php");
    ?>
    <div id="wrap" class="leftListClose subMenuClose">
        <div class="left">
            <menu>
                <?php
                include($root . "/system/include/menu.php");
                ?>
            </menu>
        </div>
        <div class="right">
            <header>
                <?php
                include($root . "/system/include/header.php");
                ?>
            </header>
            <div class="container">
                <nav class="subMenu"></nav>
                <main>
                    <?php
                    include($root . "/system/include/toolbar.php");
                    ?>
                    <section>
                        <h2>고객관리</h2>
                        <article id="custList">
                            <h3>Cust Member List</h3>
                            <br>
                            <div class="flexBox">
                                <div class="leftBox">
                                    <div class="searchOption">

                                    </div>
                                    <div class="tableTopDiv">
                                        <button id="addNewApi" class="btn_style_2 btn_color_point">
                                            <i class="fa-solid fa-plus"></i>
                                            고객 추가
                                        </button>
                                        <p class="totalCount">Total Count : <strong>0</strong></p>
                                    </div>
                                    <div class="tableDiv scrollable">
                                        <table>
                                            <colgroup>
                                                <col width="25%">
                                                <col width="25%">
                                                <col width="15%">
                                                <col width="15%">
                                                <col width="20%">
                                            </colgroup>
                                            <thead></thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <div class="tableBottomDiv">
                                        <div class="rowInViewDiv">
                                            <b>Show</b>
                                            <select name="rowInView" id="rowInView">
                                                <option value="10">10</option>
                                                <option value="30">30</option>
                                                <option value="50" selected>50</option>
                                                <option value="100">100</option>
                                                <option value="200">200</option>
                                            </select>
                                        </div>
                                        <div class="paging"></div>
                                    </div>
                                </div>
                                <div class="rightBox">
                                    <h4>고객 상세</h4>
                                    <br>
                                    <div class="tableDiv scrollable">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>왼쪽 리스트에서 고객을 클릭하세요</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tableBottomDiv"></div>
                                </div>
                            </div>
                        </article>
                    </section>
                </main>
                <aside>
                    <?php
                    include($root . "/system/include/aside.php");
                    ?>
                </aside>
            </div>
        </div>
        <?php
        include($root . "/system/include/modal.php");
        ?>
    </div>
    <script>
        function delThisTr() {
            let target = event.target;
            let td = target.parentNode.parentNode.querySelectorAll('td');
            td.forEach((el, idx) => {
                if (idx !== 3) {
                    el.querySelector('input').value = '';
                }
            });
        }
    </script>
</body>

</html>