<?php
include($_SERVER["DOCUMENT_ROOT"] . "/system/include/phpHeader.php");
?>

<html>

<head>
    <?php
    include($root . "/system/include/head.php");
    ?>
    <script type="module" src="/system/js/common/common.js?v=<?php echo $config["version"]; ?>" defer></script>
    <link rel="stylesheet" href="/css/dashboard.css?v=<?php echo $config["version"]; ?>">
    <script src="/js/dashboard.js?v=<?php echo $config["version"]; ?>" defer></script>
</head>

<body>
    <?php
    include($root . "/system/include/loadingBox.php");
    ?>
    <div id="wrap" class="leftListClose subMenuClose">
        <div class="left">
            <menu>
                <?php
                include($root . "/system/include/menu.php");
                ?>
            </menu>
        </div>
        <div class="right">
            <header>
                <?php
                include($root . "/system/include/header.php");
                ?>
            </header>
            <div class="container">
                <nav class="subMenu"></nav>
                <main>
                    <?php
                    include($root . "/system/include/toolbar.php");
                    ?>
                    <section>
                        <article>
                            <div class="gridArea">
                                <div class="gridItem apiStatus">
                                    <div class="title">
                                        <div class="leftTitle">
                                            <p>실시간 API 현황</p>
                                        </div>
                                        <div class="rightIcon">
                                            <a href="/system/manageApiStatus"><i class="fa-solid fa-link pageLink"></i></a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <div class="apiTimeGap">
                                            <h5>Response Time</h5>
                                            <p>평균 <strong>0</strong> ms</p>
                                        </div>
                                        <div class="arrowAnimation">
                                            <i class="fa-solid fa-chevron-right"></i>
                                            <i class="fa-solid fa-chevron-right"></i>
                                            <i class="fa-solid fa-chevron-right"></i>
                                        </div>
                                        <div class="apiReq">
                                            <i class="fa-solid fa-cloud-arrow-down"></i>
                                            <h5>정상 API</h5>
                                            <h5 class="apiReqNum">0건</h5>
                                        </div>
                                        <div class="apiErr">
                                            <i class="fa-solid fa-triangle-exclamation"></i>
                                            <i class="triangle fa-solid fa-triangle-exclamation"></i>
                                            <h5>API 에러</h5>
                                            <h5 class="apiErrNum">0건</h5>
                                        </div>
                                        <div class="checkAPIBox">
                                            <h5>마지막 검사 시간</h5>
                                            <p>0000-00-00 00:00:00</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="gridItem serverStatus">
                                    <div class="title">
                                        <div class="leftTitle">
                                            <p>서버 현황</p>
                                        </div>
                                        <div class="rightIcon">
                                            <i class="fa-solid fa-ellipsis"></i>
                                        </div>
                                    </div>
                                    <div class="content">
                                    </div>
                                </div>
                                <div class="gridItem">
                                    <div class="title">
                                        <div class="leftTitle">
                                            <p>방문자 통계</p>
                                        </div>
                                        <div class="rightIcon">
                                            <a href="/Pages/visit_statistics"><i class="fa-solid fa-link pageLink"></i></a>
                                        </div>
                                    </div>
                                    <div class="content" style="position: relative">
                                        <canvas id="visitors">
                                            This text is displayed if your browser does not support HTML5 Canvas.
                                        </canvas>
                                    </div>
                                </div>
                                <div class="gridItem">
                                    <div class="title">
                                        <div class="leftTitle">
                                            <p>API 요청 통계</p>
                                        </div>
                                        <div class="rightIcon">
                                            <a href="/Pages/api_statistics"><i class="fa-solid fa-link pageLink"></i></a>
                                        </div>
                                    </div>
                                    <div class="content" style="position: relative">
                                        <canvas id="apiStatistics">
                                            This text is displayed if your browser does not support HTML5 Canvas.
                                        </canvas>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </section>
                </main>
                <aside>
                    <?php
                    include($root . "/system/include/aside.php");
                    ?>
                </aside>
            </div>
        </div>
        <?php
        include($root . "/system/include/modal.php");
        ?>
    </div>
</body>

</html>