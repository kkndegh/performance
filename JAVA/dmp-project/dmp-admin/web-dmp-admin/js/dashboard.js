//반응형 css 조정
const section = document.querySelector('section');
const flexRow = document.querySelectorAll('.flexRow');
let section_ro = new ResizeObserver((entries) => {
	for (let entry of entries) {
		const cr = entry.contentRect; // this
		// console.log('Element:', entry.target);
		// console.log(`Element size: ${cr.width}px x ${cr.height}px`);
		// console.log(`Element padding: ${cr.top}px ; ${cr.left}px`);
		if (cr.width <= 1200) {
			flexRow.forEach((el) => {
				el.classList.add('section_lower_than_1200');
			});
		} else {
			flexRow.forEach((el) => {
				el.classList.remove('section_lower_than_1200');
			});
		}
	}
});
section_ro.observe(section);
