<?php
include($_SERVER["DOCUMENT_ROOT"] . "/system/include/phpHeader.php");

try {
    // 전송된 Body
    $body = file_get_contents('php://input');

    $reqJson = json_decode($body, true);

    $body = json_encode($reqJson, JSON_UNESCAPED_UNICODE);

    // Route 정보 추출
    $route = apiroute();


    $header = array();
    array_push($header, "Content-Type: application/json");

    if ($route["code"] == 'login') {
        $access_token = $config['access_token'];
        array_push($header, "access_token: $access_token");
    } elseif (isset($reqJson["headerData"]) == true) { //API Test의 경우 별도 처리
        $headerData = json_decode($reqJson["headerData"], true);
        foreach ($headerData as $key => $value) {
            array_push($header, "$key: $value");
        }
        array_push($header, "access_token: $access_token");
        // array_push($header, "REFERER: " . $route["referer"]);
        unset($reqJson["headerData"]);
        if (count($reqJson) == 0) {
            $reqJson = null;
        }
        $body = json_encode($reqJson, JSON_UNESCAPED_UNICODE);
    } else {
        //refresh token 요청이 오면 헤더에 refresh token 삽입
        if ($route["code"] == 'tokenIUD' and $reqJson["act_evt"] == 'req') {
            array_push($header, "refresh_token: " . $_SESSION["refresh_token"]);
        }
        //헤더에 토큰 삽입 (필수)
        $access_token = $_SESSION["access_token"];
        array_push($header, "access_token: " . $access_token);
        // array_push($header, "REFERER: " . $route["referer"]);
    }

    // 결과 수신
    $result = $route["param"] != "" ?
        ClientAPI($route["group"], $route["code"], $route["param"], $header, $body)
        : ClientAPI($route["group"], $route["code"], "", $header, $body);

    //로그인 요청이 오면 토큰, 아이디, 권한을 세션에 저장
    if ($route["code"] == "login") {
        if (isset(json_decode($result, true)["access_token"]) == true) {
            $_SESSION["access_token"] = json_decode($result, true)["access_token"];
            $_SESSION["refresh_token"] = json_decode($result, true)["refresh_token"];
            $_SESSION["user_id"] = json_decode($result, true)["user_id"];
            $_SESSION["user_account_name"] = json_decode($result, true)["user_account_name"];
            $_SESSION["user_seq_no"] = json_decode($result, true)["user_seq_no"];
            $_SESSION["user_account_seq_no"] = json_decode($result, true)["user_account_seq_no"];
            $_SESSION["access_token_expire"] = json_decode($result, true)["access_token_expire"];
            $_SESSION["refresh_token_expire"] = json_decode($result, true)["refresh_token_expire"];
        }
    }

    //Refresh Token 요청이 오면 세션에 새 토큰, 토큰 만료기간 저장
    if ($route["code"] == 'tokenIUD' and $reqJson["act_evt"] == 'req') {
        $_SESSION["access_token"] = json_decode($result, true)["access_token"];
        $_SESSION["access_token_expire"] = json_decode($result, true)["access_token_expire"];
    }
    echo $result;
    // var_dump($reqJson);
} catch (exception $e) {
    echo "exception:" . $e->getMessage();
}
