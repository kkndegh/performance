CREATE TABLE `dmpdb.t_api_his` (
  `his_seq_no` bigint(20) NOT NULL COMMENT='API 호출 이력 일련번호',
  `p_group` varchar(50) DEFAULT NULL COMMENT='',
  `p_code` varchar(50) DEFAULT NULL COMMENT='',
  `req_method` varchar(50) DEFAULT NULL COMMENT='요청 메소드',
  `req_url` text DEFAULT NULL COMMENT='요청 url',
  `req_header` text DEFAULT NULL COMMENT='요청 헤더',
  `req_body` text DEFAULT NULL COMMENT='요청 바디',
  `call_ts` datetime DEFAULT current_timestamp() COMMENT='호출일자',
  PRIMARY KEY (`his_seq_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='API 호출 이력';

CREATE TABLE `dmpdb.t_api_info` (
  `method` varchar(50) NOT NULL COMMENT 'API메소드',
  `group` varchar(50) NOT NULL COMMENT 'API그룹',
  `code` varchar(50) NOT NULL COMMENT 'API코드',
  `code_name` varchar(50) NOT NULL COMMENT 'API코드명',
  `call_procedure` varchar(50) NOT NULL COMMENT '호출프로시저',
  `content` text DEFAULT NULL COMMENT '내용',
  `create_ts` datetime DEFAULT current_timestamp() COMMENT='생성일자',
  `create_user_seq_no` bigint(20) DEFAULT NULL COMMENT='생성자 일련번호',
  PRIMARY KEY (`method`,`group`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='API 정보';

CREATE TABLE `dmpdb.t_auth_group` (
  `auth_group_seq_no` bigint(20) NOT NULL AUTO_INCREMENT COMMENT='권한 그룹 일련번호',
  `group_name` varchar(50) NOT NULL COMMENT='권한 그룹명',
  `group_content` text NOT NULL COMMENT='권한 그룹 내용',
  PRIMARY KEY (`auth_group_seq_no`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='권한 그룹 정보';

CREATE TABLE `dmpdb.t_authority` (
  `authority_id` varchar(50) NOT NULL COMMENT='권한 아이디',
  `authority_name` varchar(50) DEFAULT NULL COMMENT='권한 이름',
  `group` varchar(50) DEFAULT NULL COMMENT='',
  `code` varchar(50) DEFAULT NULL COMMENT='',
  PRIMARY KEY (`authority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='권한 정보';

CREATE TABLE `dmpdb.t_authority_his` (
  `his_seq_no` bigint(20) NOT NULL COMMENT='권한 호출 이력 일련번호',
  `p_group` varchar(50) DEFAULT NULL COMMENT='',
  `p_code` varchar(50) DEFAULT NULL COMMENT='',
  `authority_id` varchar(50) DEFAULT NULL COMMENT='권한 아이디',
  `user_seq_no` varchar(50) DEFAULT NULL COMMENT='사용자 일련번호',
  `reg_ts` datetime DEFAULT current_timestamp() COMMENT='등록일자',
  PRIMARY KEY (`his_seq_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='권한 호출 이력';

CREATE TABLE `dmpdb.t_manual` (
  `manual_seq_no` bigint(20) NOT NULL AUTO_INCREMENT COMMENT='메뉴얼 일련번호',
  `menu_group_id` varchar(20) DEFAULT NULL COMMENT='메뉴 그룹 아이디',
  `sub_menu_name` varchar(20) DEFAULT NULL COMMENT='서브메뉴명',
  `manual_name` varchar(100) NOT NULL COMMENT='메뉴얼명',
  `content` text DEFAULT NULL COMMENT='내용',
  `reg_ts` datetime DEFAULT current_timestamp() COMMENT='등록일자',
  `reg_user_seq_no` bigint(20) DEFAULT NULL COMMENT='등록자 일련번호',
  `mod_ts` datetime DEFAULT NULL COMMENT='수정일자',
  `mod_user_seq_no` bigint(20) DEFAULT NULL COMMENT='수정자 일련번호',
  PRIMARY KEY (`manual_seq_no`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='메뉴얼';

CREATE TABLE `dmpdb.t_user_group` (
  `user_group_seq_no` bigint(20) NOT NULL AUTO_INCREMENT COMMENT='사용자 그룹 일련번호',
  `group_name` varchar(50) NOT NULL COMMENT='사용자 그룹명',
  `group_content` text NOT NULL COMMENT='사용자 그룹 내용',
  PRIMARY KEY (`user_group_seq_no`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='사용자 그룹';

CREATE TABLE `dmpdb.t_menu` (
  `menu_seq_no` bigint(20) NOT NULL COMMENT='메뉴 일련번호',
  `menu_group_id` varchar(50) NOT NULL COMMENT='메뉴 그룹 아이디',
  `menu_content` text DEFAULT NULL COMMENT='메뉴 내용',
  `menu_class` varchar(100) NOT NULL COMMENT='메뉴 클래스',
  PRIMARY KEY (`menu_seq_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='메뉴 정보';

CREATE TABLE `dmpdb.t_sub_user` (
  `user_seq_no` bigint(20) NOT NULL COMMENT='사용자 일련번호',
  `sub_id` varchar(50) NOT NULL COMMENT='아이디',
  `sub_pw` varchar(50) NOT NULL COMMENT='비밀번호',
  `sub_use_yn` char(1) DEFAULT NULL COMMENT '서브 사용 여부',
  `reg_ts` datetime DEFAULT current_timestamp() COMMENT='등록일자',
  `reg_user_seq_no` bigint(20) DEFAULT NULL COMMENT='등록자 일련번호',
  `mod_ts` datetime DEFAULT NULL COMMENT='수정일자',
  `mod_user_seq_no` bigint(20) DEFAULT NULL COMMENT='수정자 일련번호',
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='서브 사용자 계정 정보';

CREATE TABLE `dmpdb.t_sub_menu` (
  `sub_menu_seq_no` bigint(20) NOT NULL AUTO_INCREMENT COMMENT='서브메뉴 일련번호',
  `menu_seq_no` bigint(20) DEFAULT NULL COMMENT='메뉴 일련번호',
  `sub_menu_name` varchar(50) DEFAULT NULL COMMENT='서브 메뉴명',
  `sub_menu_content` text DEFAULT NULL COMMENT='서브메뉴 내용',
  `sub_menu_class` varchar(100) DEFAULT NULL COMMENT='서브메뉴 클래스',
  `sub_menu_show_yn` char(1) DEFAULT NULL COMMENT '서브 메뉴 노출 여부',
  `sub_menu_url` varchar(100) DEFAULT NULL COMMENT='서브 메뉴 url',
  PRIMARY KEY (`sub_menu_seq_no`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='서브 메뉴 정보';

CREATE TABLE `dmpdb.t_token_user` (
  `user_token` varchar(100) NOT NULL COMMENT '인증토큰',
  `user_id` varchar(50) NOT NULL COMMENT '유저 아이디',
  `token_use_yn` char(1) DEFAULT 'Y' COMMENT '토큰사용유무(Y/N)',
  `create_ts` datetime DEFAULT current_timestamp() COMMENT '생성일자',
  `valid_ts` datetime DEFAULT (current_timestamp() + interval 10 minute) COMMENT '유효일자',
  PRIMARY KEY (`user_token`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='사용자 토큰 확인';

CREATE TABLE `dmpdb.t_token_user_his` (
  `user_token` varchar(100) NOT NULL COMMENT='인증토큰',
  `user_id` varchar(50) NOT NULL COMMENT='유저 아이디',
  `token_use_yn` char(1) DEFAULT 'N' COMMENT='토큰사용유무(Y/N)',
  `reg_ts` datetime DEFAULT current_timestamp() COMMENT='등록일자',
  `reg_user_seq_no` bigint(20) DEFAULT NULL COMMENT='등록자 일련번호',
  PRIMARY KEY (`user_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='사용자 토큰 확인 이력';

CREATE TABLE `dmpdb.t_user` (
  `user_seq_no` bigint(20) NOT NULL AUTO_INCREMENT COMMENT='사용자 일련번호',
  `id` varchar(50) NOT NULL COMMENT='아이디',
  `pw` varchar(50) NOT NULL COMMENT='비밀번호',
  `use_yn` char(1) DEFAULT NULL COMMENT '사용 여부',
  `del_yn` char(1) DEFAULT NULL COMMENT '삭제 여부',
  `reg_ts` datetime DEFAULT current_timestamp() COMMENT='등록일자',
  `reg_user_seq_no` bigint(20) DEFAULT NULL COMMENT='등록자 일련번호',
  `mod_ts` datetime DEFAULT NULL COMMENT='수정일자',
  `mod_user_seq_no` bigint(20) DEFAULT NULL COMMENT='수정자 일련번호',
  PRIMARY KEY (`user_seq_no`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='사용자 정보';

