/** mailsung */
package com.only1fs.dmp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.transaction.ChainedTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class ChainedTxConfiguration {
    @Bean(name="chainedTransactionManager")
    @Primary
    @Autowired
    public PlatformTransactionManager transactionManager( @Qualifier("dmpManagerMasterTransactionManager") PlatformTransactionManager dmpManagerMasterTransactionManager
                                                        , @Qualifier("dmpManagerSlaveTransactionManager") PlatformTransactionManager dmpManagerSlaveTransactionManager
                                                        )
    {
        return new ChainedTransactionManager( dmpManagerMasterTransactionManager
                                            , dmpManagerSlaveTransactionManager
                                            );
    }
}
