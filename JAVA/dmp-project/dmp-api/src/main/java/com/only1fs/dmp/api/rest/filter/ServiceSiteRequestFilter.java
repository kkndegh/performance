package com.only1fs.dmp.api.rest.filter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.only1fs.dmp.Constant;
import com.only1fs.dmp.exception.AppRuntimeException;

import static com.only1fs.dmp.exception.AppStatus.*;

/**
 *  /v1으로 시작하는 api에 대한 header 확인
 * @author mailsung
 *
 */
//@Slf4j
public class ServiceSiteRequestFilter implements Filter {

    private boolean checkHeader = false;
    private ObjectMapper objectMapper;
    
    public ServiceSiteRequestFilter(ObjectMapper objectMapper, boolean checkHeader) {
        super();
        this.objectMapper = objectMapper;
        this.checkHeader = checkHeader;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        boolean rundoFilter = true;
        AppRuntimeException error = null;
        HttpServletRequest requestToUse = (HttpServletRequest)request;
        HttpServletResponse responseToUse = (HttpServletResponse)response;
        if( !(request instanceof ContentCachingRequestWrapper) ) {
            requestToUse = new ContentCachingRequestWrapper((HttpServletRequest)request);
        }
        if(!(response instanceof ContentCachingResponseWrapper)){
            responseToUse = new ContentCachingResponseWrapper((HttpServletResponse)response);
        }
        
        if( checkHeader ) {
            HttpServletRequest req = (HttpServletRequest) request;
            String svcId = req.getHeader(Constant.HEADER_NAME_SVC_ID);
            String certKey = req.getHeader(Constant.HEADER_NAME_SVC_CERT_KEY);
    
            if(svcId == null) {
                error = new AppRuntimeException(UNAUTHORIZED, HttpStatus.UNAUTHORIZED);
                rundoFilter = false;
            }else if(certKey == null) {
                error = new AppRuntimeException(UNAUTHORIZED, HttpStatus.UNAUTHORIZED);
                rundoFilter = false;
            } else {
            }
        }

        try {
            if( rundoFilter ) {
                chain.doFilter(requestToUse, responseToUse);
                
            } else {
                ContentCachingResponseWrapper httpres = (ContentCachingResponseWrapper) responseToUse ;
                httpres.setStatus(HttpStatus.UNAUTHORIZED.value());
                httpres.setContentType("application/json");
                httpres.setCharacterEncoding("EUC-KR");
                
                Map<String,Object> ret = new HashMap<String,Object>();
                
                String code = error.getStatus().getCode();
                String reason = error.getStatus().getDesc();
                String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

                ret.put("code", code);
                ret.put("reason", reason);
                ret.put("time", time);
                
                objectMapper.writeValue(httpres.getWriter(), ret);
            }
        
        }finally {
            // log.debug("requestURL : {}", requestToUse.getRequestURL().toString());
            // log.debug("requestParam : {}", new String(((ContentCachingRequestWrapper)requestToUse).getContentAsByteArray()));
            // log.debug("responseBody : {}", new String(((ContentCachingResponseWrapper)responseToUse).getContentAsByteArray()));    
            ((ContentCachingResponseWrapper)responseToUse).copyBodyToResponse();
        }
    }

    @Override
    public void destroy() {
        
        
    }

}
