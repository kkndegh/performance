/** mailsung */
package com.only1fs.dmp.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public enum AppStatus {

    OK_00000                    ( "00000",   "OK",                          "성공" ),
    OK_00001                    ( "00001",   "OK",                          "제3자 정보제공 미동의" ),
    OK_00002                    ( "00002",   "OK",                          "개설일자 중복 오류" ),
    OK_00003                    ( "00003",   "OK",                          "등록 가능한 대출요청 초과" ),
    OK_00004                    ( "00004",   "OK",                          "데이터가 존재하지 않습니다" ),
    OK_00005                    ( "00005",   "OK",                          "등록 가능한 판매몰이 존재하지 않습니다" ),
    BAD_REQUEST_40000           ( "40000",   "Bad Request",                 "메소드 또는 컨텐츠 타입 불일치" ),
    BAD_REQUEST_40001           ( "40001",   "Bad Request",                 "파라메터 정보 부족" ),
    BAD_REQUEST_40002           ( "40002",   "Bad Request",                 "잘못된 파라메터 정보" ),
    BAD_REQUEST_40003           ( "40003",   "Bad Request",                 "조회 기간 오류" ),
    UNAUTHORIZED                ( "40100",   "Unauthorized",                "서비스아이디 또는 서비스 키가 잘못됐습니다." ),
    FORBIDDEN                   ( "40300",   "Forbidden",                   "등록이 필요한 IP 입니다." ),
    NOT_FOUND_40400             ( "40400",   "Not Found",                   "요청한 페이지(Resource)를 찾을 수 없습니다." ),
    NOT_FOUND_40401             ( "40401",   "Not Found",                   "등록된 Resource({Parametger}) 를 찾을 수 없습니다." ),
    ERROR                       ( "50000",   "INTERNAL_SERVER_ERROR",       "내부서버 오류" )
    ;

    private final String code;
    private final String name;
    private final String desc;
}
