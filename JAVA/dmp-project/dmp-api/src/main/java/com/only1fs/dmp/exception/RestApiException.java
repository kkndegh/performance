package com.only1fs.dmp.exception;

import org.springframework.http.HttpStatus;

public class RestApiException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    
    
    private HttpStatus statusCode;
    private String message;  
    
    public RestApiException(HttpStatus statusCode) {
        super("");
        this.statusCode = statusCode;
    }
    
    public RestApiException(HttpStatus statusCode, String message) {
        super(message);
        this.statusCode = statusCode;
        this.message = message;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }
    
    public String getMessage() {
        return message;
    }
    
    
}
