/** mailsung */
package com.only1fs.dmp.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@SuppressWarnings("serial")
@Getter
public class AppRuntimeException extends RuntimeException {
    
    private AppStatus status;
    private String msg;
    private HttpStatus httpStatus;
    private String replaceStr;

    public AppRuntimeException(AppStatus status) {
        this(status, "");
    }

    public AppRuntimeException(AppStatus status, String msg) {
        super(status.getName() + " - " + msg);
        this.status = status;
        this.msg = msg;
    }

    public AppRuntimeException(AppStatus status, HttpStatus httpStatus) {
        super(status.getName());
        this.status = status;
        this.httpStatus = httpStatus;
    }

    public AppRuntimeException(AppStatus status, HttpStatus httpStatus, String replaceStr) {
        super(status.getName());
        this.status = status;
        this.httpStatus = httpStatus;
        this.replaceStr = replaceStr;
    }
}
