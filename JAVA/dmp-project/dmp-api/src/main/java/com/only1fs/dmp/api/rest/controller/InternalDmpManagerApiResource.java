package com.only1fs.dmp.api.rest.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.only1fs.dmp.api.rest.service.DmpApiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;    
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * DMP API 프로시저 호출
 */
@RestController
@RequestMapping("/api/v1")
public class InternalDmpManagerApiResource {

    @Autowired
    private DmpApiService dmpApiService;

    /**
     * dmp get 접근
     * 
     * @param group String
     * @param code String
     * @param requestBody Map<String, String>
     * @param request HttpServletRequest
     * @return ResponseEntity<HashMap<String, Object>>
     */
    @GetMapping(value = "/{group}/{code}")
    public ResponseEntity<String> getDepth2(@PathVariable("group") String group, 
                                            @PathVariable("code") String code,
                                            @RequestHeader Map<String, String> requestHeader,
                                            HttpServletRequest request) {

        String rs = dmpApiService.callDmpProcedure(group, code, requestHeader, request);

        return new ResponseEntity<String>(rs, HttpStatus.OK);
    }

    /**
     * dmp post 접근
     * 
     * @param group String
     * @param code String
     * @param requestBody RequestBody Map<String, Object>
     * @param requestHeader RequestHeader Map<String, String>
     * @param request HttpServletRequest
     * @return ResponseEntity<HashMap<String, Object>>
     */
    @PostMapping(value = "/{group}/{code}")
    public ResponseEntity<String> postDepth2(@PathVariable("group") String group, 
                                            @PathVariable("code") String code, 
                                            @RequestBody Map<String, Object> requestBody,
                                            @RequestHeader Map<String, String> requestHeader,
                                            HttpServletRequest request) {

        String rs = dmpApiService.callDmpProcedure(group, code, requestBody, requestHeader, request);

        return new ResponseEntity<String>(rs, HttpStatus.OK);
    }

    /**
     * dmp put 접근
     * 
     * @param group String
     * @param code String
     * @param requestBody RequestBody Map<String, Object>
     * @param requestHeader RequestHeader Map<String, String>
     * @param request HttpServletRequest
     * @return ResponseEntity<HashMap<String, Object>>
     */
    @PutMapping(value = "/{group}/{code}")
    public ResponseEntity<String> putDepth2(@PathVariable("group") String group, 
                                            @PathVariable("code") String code, 
                                            @RequestBody Map<String, Object> requestBody,
                                            @RequestHeader Map<String, String> requestHeader,
                                            HttpServletRequest request) {
        
        String rs = dmpApiService.callDmpProcedure(group, code, requestBody, requestHeader, request);

        return new ResponseEntity<String>(rs, HttpStatus.OK);
    }

    /**
     * dmp delete 접근
     * 
     * @param group String
     * @param code String
     * @param requestBody RequestBody Map<String, Object>
     * @param requestHeader RequestHeader Map<String, String>
     * @param request HttpServletRequest
     * @return ResponseEntity<HashMap<String, Object>>
     */
    @DeleteMapping(value = "/{group}/{code}")
    public ResponseEntity<String> deleteDepth2(@PathVariable("group") String group, 
                                                @PathVariable("code") String code, 
                                                @RequestBody Map<String, Object> requestBody,
                                                @RequestHeader Map<String, String> requestHeader,
                                                HttpServletRequest request) {
        
        String rs = dmpApiService.callDmpProcedure(group, code, requestBody, requestHeader, request);
        
        return new ResponseEntity<String>(rs, HttpStatus.OK);
    }
}
