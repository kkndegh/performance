/** mailsung */
package com.only1fs.dmp;

import java.nio.charset.Charset;

public class Constant {
    public static final Charset DEFAULT_CHARSET = Charset.forName("KSC5601");

    public static final String HEADER_NAME_SVC_ID = "svcId";
    public static final String HEADER_NAME_SVC_CERT_KEY = "svcCertKey";

    public static final String API_SELLERBOT_SVC_ID = "sellerbotcash";

    public static final String PARAM_REQ_TITLE = "[요청 값]";
    public static final String PARAM_RES_TITLE = "[응답 값]";
    
}