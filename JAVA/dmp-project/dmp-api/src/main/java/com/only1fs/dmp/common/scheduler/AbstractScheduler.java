/** mailsung */
package com.only1fs.dmp.common.scheduler;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.function.Consumer;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;

import com.hazelcast.core.HazelcastInstance;
import com.only1fs.dmp.common.funciton.ExceptionRunnable;

@Slf4j
public abstract class AbstractScheduler {
    @Autowired
    private HazelcastInstance hazelcastInstance;
    @Autowired 
    private JobLauncher jobLauncher;

    protected void executeInternal(String lockName, Runnable runnable) {
        execute(lockName, 
                () -> runnable.run(), 
                (e) -> {
                    log.error("" + e);
                });
    }

    protected void executeBatch(String lockName, Job job, JobParameters params) {
        execute(lockName,
                () -> jobLauncher.run(job, params),
                (e) -> {
                    log.error("" + e);
                });
    }

    // exclusive!!! - cluster 내에서 동일한 스케쥴 잡이 동시에 실행되는 것을 방지
    private void execute(String lockName, ExceptionRunnable runnable, Consumer<Exception> exceptionHandler) {
        Lock lock = hazelcastInstance.getCPSubsystem().getLock(lockName);
        boolean locked = false;

        try {
            locked = lock.tryLock(500, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {}

        if (locked) {
            try {
                runnable.run();
            } catch (Exception e) {
                exceptionHandler.accept(e);
            } finally {
                lock.unlock();
            }
        }
    }

}
