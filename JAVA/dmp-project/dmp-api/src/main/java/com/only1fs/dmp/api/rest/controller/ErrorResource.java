package com.only1fs.dmp.api.rest.controller;

import javax.servlet.http.HttpServletRequest;

import static com.only1fs.dmp.exception.AppStatus.*;
import com.only1fs.dmp.exception.AppRuntimeException;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ErrorResource implements ErrorController {

    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping("/error")
    public void handleError(HttpServletRequest request) {
        throw new AppRuntimeException(NOT_FOUND_40400, HttpStatus.NOT_FOUND);
    }
}
