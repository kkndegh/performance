/** mailsung */
package com.only1fs.dmp.common.util;

import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

public class StringUtil {
    private static Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

    public static boolean isNumeric(String s) {
        if (s == null) {
            return false; 
        }
        return pattern.matcher(s).matches();
    }

    public static boolean isEmpty(String s) {
        return !StringUtils.hasLength(s);
    }
}