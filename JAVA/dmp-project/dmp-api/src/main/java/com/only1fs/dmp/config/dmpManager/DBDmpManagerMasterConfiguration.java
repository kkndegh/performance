/** mailsung */
package com.only1fs.dmp.config.dmpManager;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

import com.only1fs.dmp.common.annotation.dmpManager.DmpManagerMasterMapper;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@MapperScan(value = "com.only1fs.dmp.mybatis.mapper.dao", annotationClass = DmpManagerMasterMapper.class, sqlSessionFactoryRef = "dmpManagerMasterSqlSessionFactory")
@EnableTransactionManagement
public class DBDmpManagerMasterConfiguration {
    @Bean(name = "dmpManagerMasterDataSource", destroyMethod = "close")
    @ConfigurationProperties(prefix = "spring.datasource.dmpmanager.master.hikari")
    public DataSource dmpManagerMasterDataSource() {
        return DataSourceBuilder.create().type(HikariDataSource.class).build();
    }

    @Bean(name = "dmpManagerMasterTransactionManager")
    public DataSourceTransactionManager dmpManagerMasterTransactionManager(@Qualifier("dmpManagerMasterDataSource") DataSource dmpManagerMasterDataSource) {
        return new DataSourceTransactionManager(dmpManagerMasterDataSource);
    }

    @Bean(name = "dmpManagerMasterSqlSessionFactory")
    public SqlSessionFactory dmpManagerMasterSqlSessionFactory(@Qualifier("dmpManagerMasterDataSource") DataSource dmpManagerMasterDataSource, ApplicationContext applicationContext) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dmpManagerMasterDataSource);
        sqlSessionFactoryBean.setConfigLocation(applicationContext.getResource("classpath:config/mybatis/mybatis-config.xml"));
        sqlSessionFactoryBean.setMapperLocations(applicationContext.getResources("classpath:config/mybatis/dmpManager/mapper-master/*.xml"));

        return sqlSessionFactoryBean.getObject();
    }

    @Bean(name = "dmpManagerMasterSqlSessionTemplate")
    public SqlSessionTemplate dmpManagerMasterSqlSessionTemplate(SqlSessionFactory dmpManagerMasterSqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(dmpManagerMasterSqlSessionFactory);
    }
}
