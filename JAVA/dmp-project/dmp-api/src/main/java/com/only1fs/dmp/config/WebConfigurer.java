package com.only1fs.dmp.config;

import com.only1fs.dmp.api.rest.filter.ServiceSiteRequestFilter;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConfigurer {
    
    @Value("${application.external.checkHeader:true}") private boolean checkHeader = true;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @Bean
    public FilterRegistrationBean<ServiceSiteRequestFilter> serviceSiteFilter(ServiceSiteRequestFilter filter){
        FilterRegistrationBean<ServiceSiteRequestFilter> registFilter = new FilterRegistrationBean<>();
        registFilter.setFilter(filter);
        registFilter.addUrlPatterns("/api/v1/*");
        return registFilter;
    }
    
    @Bean
    ServiceSiteRequestFilter serviceSiteRequestFilter() {
        return new ServiceSiteRequestFilter(objectMapper, checkHeader);
    }
}
