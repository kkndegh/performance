/** mailsung */
package com.only1fs.dmp.config.dmpManager;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

import com.only1fs.dmp.common.annotation.dmpManager.DmpManagerSlaveMapper;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@MapperScan(value = "com.only1fs.dmp.mybatis.mapper.dao", annotationClass = DmpManagerSlaveMapper.class, sqlSessionFactoryRef = "dmpManagerSlaveSqlSessionFactory")
@EnableTransactionManagement
public class DBDmpManagerSlaveConfiguration {
    @Bean(name = "dmpManagerSlaveDataSource", destroyMethod = "close")
    // @Primary
    @ConfigurationProperties(prefix = "spring.datasource.dmpmanager.slave.hikari")
    public DataSource dmpManagerSlaveDataSource() {
        HikariDataSource source = DataSourceBuilder.create().type(HikariDataSource.class).build();
        return source;
    }

    @Bean(name = "dmpManagerSlaveTransactionManager")
    public DataSourceTransactionManager dmpManagerSlaveTransactionManager(@Qualifier("dmpManagerSlaveDataSource") DataSource dmpManagerSlaveDataSource) {
        return new DataSourceTransactionManager(dmpManagerSlaveDataSource);
    }

    @Bean(name = "dmpManagerSlaveSqlSessionFactory")
    // @Primary
    public SqlSessionFactory dmpManagerSlaveSqlSessionFactory(@Qualifier("dmpManagerSlaveDataSource") DataSource dmpManagerSlaveDataSource, ApplicationContext applicationContext) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dmpManagerSlaveDataSource);
        sqlSessionFactoryBean.setConfigLocation(applicationContext.getResource("classpath:config/mybatis/mybatis-config.xml"));
        sqlSessionFactoryBean.setMapperLocations(applicationContext.getResources("classpath:config/mybatis/dmpManager/mapper-slave/*.xml"));

        return sqlSessionFactoryBean.getObject();
    }

    @Bean(name = "dmpManagerSlaveSqlSessionTemplate")
    // @Primary
    public SqlSessionTemplate dmpManagerSlaveSqlSessionTemplate(SqlSessionFactory dmpManagerSlaveSqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(dmpManagerSlaveSqlSessionFactory);
    }
}
