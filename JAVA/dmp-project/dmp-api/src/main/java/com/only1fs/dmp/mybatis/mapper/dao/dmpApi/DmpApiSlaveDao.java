package com.only1fs.dmp.mybatis.mapper.dao.dmpApi;

import com.only1fs.dmp.common.annotation.dmpManager.DmpManagerSlaveMapper;
import com.only1fs.dmp.dto.ProcedureParam;

@DmpManagerSlaveMapper
public interface DmpApiSlaveDao {

    /**
     * dmpdb DB스키마 프로시저 호출
     * 
     * @param requestBody
     * @return
     */
    public String callDmpPApiFilter(ProcedureParam paramMap);

}
