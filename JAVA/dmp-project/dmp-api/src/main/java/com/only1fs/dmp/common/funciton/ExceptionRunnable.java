/** mailsung */
package com.only1fs.dmp.common.funciton;

@FunctionalInterface
public interface  ExceptionRunnable {
    void run() throws Exception;
}
