#!/bin/bash

PROFILE=$1

TAG=$(cat dmp-api.tag)
YAML=/home/gitlab/yaml/dmp-api.yaml

sed -i 's/latest/'"$TAG"'/g' $YAML
sed -i 's/env_profile/'"$PROFILE"'/g' $YAML
sudo kubectl apply -f $YAML
