#!/bin/bash

PROFILE=$1

TAG=$(cat aos-scheduler.tag)
YAML=/home/gitlab/yaml/aos-scheduler.yaml

sed -i 's/latest/'"$TAG"'/g' $YAML
sed -i 's/env_profile/'"$PROFILE"'/g' $YAML
sudo kubectl apply -f $YAML
