/** mailsung */
package com.only.aos.scheduler.service;

import java.util.HashMap;
import java.util.function.Function;

import com.only.aos.scheduler.Constant;
import com.only.aos.scheduler.common.BaseValue;
import com.only.aos.scheduler.common.util.CommonUtil;
import com.only.aos.scheduler.common.util.HttpUtil;
import com.only.aos.scheduler.dto.HttpParam;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 스케줄러 동작 서비스
 * 
 * 1. 스케줄러 리스트 조회
 * 2.1 스케줄러 리스트 callback 호출
 * 2.2 스케줄러 타임 수정
 */
@Slf4j
@Service
public class AosSchedulerService extends BaseValue{

    @Autowired
    private CommonUtil commonUtil;

    @Autowired
    private HttpUtil httpUtil;

    public void aosApiCall() {
        log.debug("****************************** 스케줄러 Start ******************************");

        String scheListResStr = "";                 //스케줄러 리스트 응답값
        String scheNextResStr = "";                 //스케줄러 타임 수정 결과 응답값

        Function<String, String> profileChek;       //함수형 인터페이스 선언
        JSONParser jsonParser = new JSONParser();   //JSON Parser 선언

        try {
            /** 
             * 1. 스케줄러 리스트 조회 및 실행
            */
            String scheUrl = baseUrl + "/api/v1/sche/schedulerList?sche_play_yn=Y";
            HttpParam scheParam = new HttpParam(Constant.SCHE_TITLE, scheUrl, scheMethod, scheHeader, scheBody);    //통신파라미터 초기화

            log.debug(scheParam.toString());

            //스케줄러 -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> 스케줄러
            profileChek = (s) -> "local".equals(s) ? httpUtil.callApiHttp(scheParam) : httpUtil.callApiHttps(scheParam);
            scheListResStr = profileChek.apply(profiles);

            log.debug("[스케줄러 호출 응답 값] :: "+ scheListResStr);

            /** 
             * 2. 스케줄러 리스트 callback 호출
             */
            //jsonString -> object
            JSONObject scheListResObj = (JSONObject) jsonParser.parse(scheListResStr);
            String rstCd = scheListResObj.get("result_code").toString();

            //외부 통신
            if(rstCd.contains("P000")){
                int totalCount = Integer.parseInt(scheListResObj.get("total_count").toString());
                if(totalCount > 0){
                    //조회 데이터 내 스케줄러 리스트 jsonString -> JSONArray
                    JSONArray schedulerList = (JSONArray) scheListResObj.get("list");
                    for(int i=0; i<schedulerList.size(); i++){
                        /** 
                         * 2.1 스케줄러 리스트 callback 호출
                         */
                        //callback정보 조회
                        JSONObject schedulerObj = (JSONObject) jsonParser.parse(schedulerList.get(i).toString());
                        JSONObject schedulerCallObj = (JSONObject) jsonParser.parse(schedulerObj.get("callback").toString());

                        String url = commonUtil.nvl(schedulerCallObj.get("url"), "");
                        String header = commonUtil.nvl(schedulerCallObj.get("header"), "");
                        String body = commonUtil.nvl(schedulerCallObj.get("body"), "");
                        String method = commonUtil.nvl(schedulerCallObj.get("method"), "");

                        HttpParam callBackParam = new HttpParam("["+i+"]"+Constant.CALL_BACK_TITLE, url, method, header, body);  //통신파라미터 초기화
                        log.debug("["+i+"] AOS 외부 API 호출 시작");
                        log.debug(scheParam.toString());

                        //외부 api 호출
                        profileChek = (s) -> "dev".equals(s) ? httpUtil.callApiHttp(callBackParam) : httpUtil.callApiHttps(callBackParam);
                        String schedulerCallRes = profileChek.apply(profiles);

                        log.debug("==응답 값("+i+") :: "+ schedulerCallRes);
                        log.debug("["+i+"] AOS 외부 API 호출 끝");

                        /** 
                         * 2.2 스케줄러 타임 수정
                         */
                        JSONObject extResObj = (JSONObject) jsonParser.parse(schedulerCallRes);
                        String resultCode = (String) extResObj.get("result_code");
                        String resultMsg = (String) extResObj.get("result_msg");
                        String resultSysMsg = (String) extResObj.get("result_sys_msg");

                        if(resultCode.contains("P000")){
                            String scheNextUrl = baseUrl + "/api/v1/sche/schedulerNext";
                            String scheNextBody="";
                            String scheSeqNo = schedulerCallObj.get("sche_seq_no").toString();

                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put("sche_seq_no", scheSeqNo);
                            JSONObject bodyObj = new JSONObject(map);
                            scheNextBody = bodyObj.toString();

                            HttpParam nextParam = new HttpParam(Constant.SCHE_NEXT_TITLE, scheNextUrl, scheNextMethod, scheNextHeader, scheNextBody);    //통신파라미터 초기화
                            log.debug(scheParam.toString());

                            //스케줄러 -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> 스케줄러
                            profileChek = (s) -> "dev".equals(s) ? httpUtil.callApiHttp(nextParam) : httpUtil.callApiHttps(nextParam);
                            scheListResStr = profileChek.apply(profiles);

                            log.debug("["+i+"] 스케줄러 타임 수정 응답 값 :: "+ scheNextResStr);
                        }else{
                            log.debug("[" + resultCode + "] - " + resultMsg + " ( " + resultSysMsg + " )");
                        }
                    }
                }else{
                    log.debug("발송대상 데이터 없음.");
                }
            }
            log.debug("****************************** 스케줄러 End ******************************");
        } catch (Exception e) {
            log.error("스케줄러 에러발생 :: "+e.toString());
        }
    }
}
