package com.only.aos.scheduler.common.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import com.only.aos.scheduler.dto.HttpParam;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * HTTP 유틸
 * 
 * @since 2022.05.25
 * @author kh
 */
@Component
public class HttpUtil {

    @Autowired
    private CommonUtil commonUtil;

    /**
     * HTTP프로토콜 통신 메서드
     * 
     * @param HttpParam
     * @return String
     */
    public String callApiHttp(HttpParam hp) {
        HttpURLConnection conn = null;
        String rspStr = "";

        try {
            // URL 설정
            URL url = new URL(hp.getPUrl());

            conn = (HttpURLConnection) url.openConnection();

            // jsonString -> object
            JSONParser jsonParser = new JSONParser();
            JSONObject headerObj = (JSONObject) jsonParser.parse(hp.getPHeader());
            for (Object key : headerObj.keySet()) {
                String keyStr = (String)key;
                String keyvalue = headerObj.get(keyStr).toString();
                conn.setRequestProperty(keyStr, keyvalue);
            }

            // methodType의 경우 POST, GET, PUT, DELETE 가능
            conn.setRequestMethod(hp.getPMethod());
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Transfer-Encoding", "chunked");
            conn.setRequestProperty("Connection", "keep-alive");
            conn.setDoOutput(true);

            if (!hp.getPMethod().equals("GET")) {
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "UTF-8"));
                bw.write(hp.getPBody());
                bw.flush();
                bw.close();
            }

            // 보내고 결과값 받기
            int responseCode = conn.getResponseCode();
            if (responseCode == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                rspStr = sb.toString();
            }
        } catch (Exception e) {
            return commonUtil.resultObjMk(commonUtil.getClassName(),commonUtil.getMethodName(),"P999", e.toString());
        }
        return rspStr;
    }

    /**
     * HTTPS 프로토콜 통신 메서드
     * 
     * @param HttpParam
     * @return String
     */
    public String callApiHttps(HttpParam hp){
        HttpsURLConnection conn = null;
        String rspStr="";
        
        try {
            //URL 설정
            URL url = new URL(hp.getPUrl());
 
            conn = (HttpsURLConnection) url.openConnection();

            // jsonString -> object
            JSONParser jsonParser = new JSONParser();
            JSONObject headerObj = (JSONObject) jsonParser.parse(hp.getPHeader());
            for (Object key : headerObj.keySet()) {
                String keyStr = (String)key;
                String keyvalue = headerObj.get(keyStr).toString();
                conn.setRequestProperty(keyStr, keyvalue);
            }
            
            // methodType의 경우 POST, GET, PUT, DELETE 가능
            conn.setRequestMethod(hp.getPMethod());
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Transfer-Encoding", "chunked");
            conn.setRequestProperty("Connection", "keep-alive");
            conn.setDoOutput(true);
            
            if(!hp.getPMethod().equals("GET")){
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "UTF-8"));
                bw.write(hp.getPBody());
                bw.flush();
                bw.close();
            }
            
            // 보내고 결과값 받기
            int responseCode = conn.getResponseCode();
            if (responseCode == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                rspStr = sb.toString();
            } 
        } catch (Exception e) {
            return commonUtil.resultObjMk(commonUtil.getClassName(),commonUtil.getMethodName(),"P999", e.toString());
        }
        return rspStr;
    }
}