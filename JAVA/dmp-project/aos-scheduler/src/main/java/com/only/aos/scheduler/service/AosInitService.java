package com.only.aos.scheduler.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 스케줄러 초기 동작 서비스.
 * Spring 컨텍스트의 초기화가 완료된 후 동작.
 * 5초마다 스케줄러 리스트 호출 및 동작.
 * 
 * @author kh
 * @since 2022.07.11
 * 
 */
@Slf4j
@Component
public class AosInitService implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private AosSchedulerService aosSchedulerService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        while(true){
            try {
                long beforeTime = System.currentTimeMillis();

                new Thread(() -> {
                    aosSchedulerService.aosApiCall();
                }).start();

                //5초대기
                Thread.sleep(5000);

                long afterTime = System.currentTimeMillis(); 
                long secDiffTime = (afterTime - beforeTime)/1000;

                log.debug("동작 텀 시간 체크(m) : "+secDiffTime);
            } catch (Exception e) {
                log.error("스케줄러 초기화 에러 :: "+e.toString());
            }
        }
    }
    
}