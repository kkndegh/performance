/** mailsung */
package com.only.aos.scheduler;

import java.nio.charset.Charset;

public class Constant {
    public static final Charset DEFAULT_CHARSET = Charset.forName("EUC-KR");

    //HttpParam 셋팅시 Title 목록
    public static final String SCHE_TITLE = "[스케줄러 리스트 조회 Params]";
    public static final String SCHE_NEXT_TITLE = "[스케줄러 타임 수정 데이터 Params]";
    public static final String CALL_BACK_TITLE = "AOS 외부 API 요청 Params";
}