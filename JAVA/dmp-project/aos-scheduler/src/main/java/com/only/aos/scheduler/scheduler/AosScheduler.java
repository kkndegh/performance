/** mailsung */
package com.only.aos.scheduler.scheduler;

// import com.only.aos.scheduler.service.AosSchedulerService;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/*
 * 초기화시 무한루프로 스케줄러 호출함으로 스프링 스케줄러 사용X.
 */
@Component
public class AosScheduler {

    // @Autowired
    // private AosSchedulerService aosSchedulerService;

    // @Value("${scheduler.run.enabled:false}")
    // private boolean runScheduler;

    // @Scheduled(cron = "${scheduler.run.cron}")
    // public void execute() throws Exception {

    //     //if(!runScheduler) return;

    //     // String lockName = getClass().getSimpleName();
    //     // executeInternal(lockName, this::runReq);
    //     this.runReq();
    // } 
    
    // private void runReq() {
    //     aosSchedulerService.aosApiCall();
    // }

    // public void goScheduler(){
    //     while(true){
    //         try {
    //             aosSchedulerService.aosApiCall();
    //         } catch (Exception e) {
    //             
    //         }
    //     }
    // }
}