package com.only.aos.scheduler.common;

import org.springframework.beans.factory.annotation.Value;

/**
 * 공통 value
 */
public class BaseValue {
    /** 
     * 프로파일
    */
    @Value("${spring.profiles.active}")
    public String profiles;

    /** 
     * AOS URL
    */
    @Value("${aos.base.url}")
    public String baseUrl;

    /** 
     * 스케줄러 메소드 정보
    */
    @Value("${scheduler.info.method}")
    public String scheMethod;

    /** 
     * 스케줄러 헤더 정보
    */
    @Value("${scheduler.info.header}")
    public String scheHeader;

    /** 
     * 스케줄러 바디 정보
    */
    @Value("${scheduler.info.body}")
    public String scheBody;

    /** 
     * 스케쥴러 타임 수정 메소드 정보
    */
    @Value("${scheduler.next.method}")
    public String scheNextMethod;

    /** 
     * 스케쥴러 타임 수정 헤더 정보
    */
    @Value("${scheduler.next.header}")
    public String scheNextHeader;
}