package com.only1fs.dgw.kcb;

import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestIsNumeric {

    private Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

    public boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false; 
        }
        return pattern.matcher(strNum).matches();
    }

    @Test
    public void execute() throws Exception {
        log.debug("1{}", isNumeric("22"));
        log.debug("2{}", isNumeric("5.05"));
        log.debug("3{}", isNumeric("-200"));
        log.debug("4{}", isNumeric(null));
        log.debug("5{}", isNumeric("abc"));
    }
}