/** kenny */
package com.only1fs.dgw.kcb;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestWorkingDay {

    private static final DayOfWeek SAT = DayOfWeek.of(6);
    private static final DayOfWeek SUN = DayOfWeek.of(7);
    private static List<String> exDates = Arrays.asList("2021-02-11","2021-02-12", "2021-03-01", "2021-05-05", "2021-05-19", "2021-09-20", "2021-09-21", "2021-09-22");

    private static LocalDate getInqDate(LocalDate from) {
        LocalDate date = from;
        int retry = 30;
        do {
            if (!SAT.equals(date.getDayOfWeek()) && !SUN.equals(date.getDayOfWeek()) && !exDates.contains(date.toString()))
                break;
            date = date.minusDays(1);
        } while (retry-- > 0);
        log.debug("selected : {}", date);
        return date;
    }

    public static void main(String[] args) {
        TestWorkingDay.getInqDate(LocalDate.parse("2021-02-14"));
    }
}
