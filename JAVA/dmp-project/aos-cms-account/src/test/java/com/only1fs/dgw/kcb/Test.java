package com.only1fs.dgw.kcb;

import java.util.HashMap;

import com.eclipsesource.json.JsonObject;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Test {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {

        HashMap<String, Object> result = new HashMap<String, Object>();

        JsonObject headerParams = new JsonObject();
        headerParams.add("hash_hmac", "132da9b084aa7cb8bddc4d87a8e5979e37d3e0daa5debb256c0cd32ac6bf66d0"); // Hash된 인증정보
        headerParams.add("api_key", "UP-1655360178-3761"); // 고유 식별 Key
        result.put("reqHeader", headerParams.toString());

        JsonObject bodyParams = new JsonObject();
        bodyParams.add("send_type", "S"); // 발송형태(R:예약,S:즉시)
        bodyParams.add("to", "01047488869"); // 예약 ID(영문, 숫자, -, _) 40byte

        result.put("reqBody", bodyParams.toString());
        result.put("resData",
                "{\"rcode\":\"1000\",\"rdesc\":\"요청 처리 성공\",\"data\":{\"grp_id\":\"ZkzFQIQLYOMXNVv\",\"msg_id\":\"ZkzFQIQLYOMZAV3\"}}");

        String sendRst = (String) result.get("resData");
        // System.out.println("sendRst:::" + sendRst);
        String sendReqHeader = (String) result.get("reqHeader");
        // System.out.println("sendReqHeader:::" + sendReqHeader);
        String sendReqBody = (String) result.get("reqBody");
        // System.out.println("sendReqBody:::" + sendReqBody);

        try {
            JSONParser jsonParser1 = new JSONParser();
            Object obj = jsonParser1.parse(sendRst);

            JSONParser jsonParser2 = new JSONParser();
            Object header = jsonParser2.parse(sendReqHeader);

            JSONParser jsonParser3 = new JSONParser();
            Object body = jsonParser3.parse(sendReqBody);

            JSONObject jsonObj = (JSONObject) obj;
            jsonObj.put("header", header);
            jsonObj.put("body", body);

            System.out.println("jsonObj:::" + jsonObj);


        } catch (ParseException e) {
            e.printStackTrace();
        }

        // JSONObject json =  new JSONObject(result);

        // System.out.println( "JSON:::" + json);
    }
    
}