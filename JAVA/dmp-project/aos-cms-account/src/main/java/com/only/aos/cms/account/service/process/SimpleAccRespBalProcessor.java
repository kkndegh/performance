/**
 * @author mailsung
 */
package com.only.aos.cms.account.service.process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.only.aos.cms.account.service.vo.simple.SimpleAccRespBalVO;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("SimpleAccRespBalProcessor")
public class SimpleAccRespBalProcessor implements Processor {
    @Autowired 
    private ProducerTemplate producer;
    @Autowired 
    private ObjectMapper mapper;

    @Override
    public void process(Exchange ex) throws Exception {
        String jsonString = ex.getIn().getBody(String.class);
        if (jsonString == null) throw new Exception("Empty Body!");
        
        SimpleAccRespBalVO[][] dtos = mapper.readValue(jsonString, SimpleAccRespBalVO[][].class);
        if (dtos != null) {
            for (int i = 0, n = dtos.length ; i < n ; i++) {
                for (int j = 0, m = dtos[i].length ; j < m ; j++) {
                    producer.asyncRequestBody("direct:to-account-verify", mapper.writeValueAsString(dtos[i][j]));
                }
            }
        }

    }
}
