package com.only.aos.cms.account.service.vo.simple;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
public abstract class SimpleAccRespVO implements Cloneable, Serializable {
    private static final long serialVersionUID = 1L;
    private String errCode;
    private String errMessage;    
    private String aisReqSeqNo;

    public SimpleAccRespVO clone() throws CloneNotSupportedException {
        SimpleAccRespVO cloned = (SimpleAccRespVO)super.clone();
        return cloned;
    }
}
