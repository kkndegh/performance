package com.only.aos.cms.account.common.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {
    
    public static final String timezoneName = TimeZone.getDefault().getID();
    /** yyyy-MM */
    public static final DateTimeFormatter FORMAT_MONTH = DateTimeFormatter.ofPattern("yyyy-MM");
    /** yyyyMM */
    public static final DateTimeFormatter FORMAT_MONTH_NO_HI = DateTimeFormatter.ofPattern("yyyyMM");
    /** yyyyMMdd */
    public static final DateTimeFormatter NO_FORMAT_DAY   = DateTimeFormatter.ofPattern("yyyyMMdd");
    /** yyyy-MM-dd */
    public static final DateTimeFormatter FORMAT_DAY   = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    /** yyyy-MM-dd HH:mm */
    public static final DateTimeFormatter FORMAT_HOUR  = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    /** yyyy-MM-dd HH:mm:ss */
    public static final DateTimeFormatter FORMAT_FULL  = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    /** yyyyMMddHHmmss */
    public static final DateTimeFormatter NO_FORMAT_FULL  = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    
    public static final DateTimeFormatter FORMAT_TIME_FULL  = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
    
    /** yyyy년 MM월 dd일 */
    public static final DateTimeFormatter KR_FORMAT_YYYY_MM_DD = DateTimeFormatter.ofPattern("yyyy년 MM월 dd일");

    /** MM월 dd일 */
    public static final DateTimeFormatter KR_FORMAT_MM_DD = DateTimeFormatter.ofPattern("MM월 dd일");    
        
    private static SimpleDateFormat yyyymm = new SimpleDateFormat("yyyyMM");
    
    public static Date getYearMonth(String str) throws ParseException {
        return yyyymm.parse(str);
    }
    
    public static String getYearMonthString(Date date) throws ParseException {
        return yyyymm.format(date);
    }
    
    /**
     * yyyy-MM-dd HH:mm:ss 형태의 String을 LocalDateTime으로 변경 한다.
     * @param datetime
     * @return
     */
    public static LocalDateTime getDateTime(String datetime) {
        return LocalDateTime.parse(datetime, FORMAT_FULL);
    }
    
    /**
     * 설정한 포맷 형식으로 날짜를 변화 한다.
     * @param datetime
     * @return
     */
    public static String getDateTimeFormat(LocalDateTime datetime, DateTimeFormatter format) {
        return datetime.format(format);
    }
    
    /**
     * yyyy-MM-dd HH:mm:ss 형태의 String으로 변경 한다.
     * @param datetime
     * @return
     */
    public static String getDateTime(LocalDateTime datetime) {
        return datetime.format(FORMAT_FULL);
    }
    
    /**
     * yyyy-MM-dd HH:mm:ss 형태의 String을 LocalDateTime으로 변경 한다.
     * @param datetime
     * @return
     */
    public static LocalDateTime getDateTimeFull(String datetime) {
        return LocalDateTime.parse(datetime, FORMAT_TIME_FULL);
    }
    
    public static LocalDateTime getDateTimeFormat(String datetime, DateTimeFormatter format) {
        return LocalDateTime.parse(datetime, format);
    }
    

    /**
     * 오늘 날짜의 시작 시간을 리턴 한다.
     * @return
     */
    public static LocalDateTime getDateTimeFormStartOfDay() {
        LocalDate ld = LocalDate.now();
        return getDateTimeStartOfDay(ld);
        
    }
    
    public static LocalDateTime getDateTimeStartOfDay(LocalDate day) {
        return day.atStartOfDay();
        
    }
    
    /**
     * yyyy-MM-dd 형태로된 날짜 스트링을 사용하여 시작 시간을 설정하여 LocalDateTime을 리턴 한다.
     * @param day
     * @return
     */
    public static LocalDateTime getDateTimeFormStartOfDay(String day) {
        LocalDate ld = LocalDate.parse(day, FORMAT_DAY);
        return getDateTimeStartOfDay(ld);
    }

    public static LocalDateTime getDateTimeFormStartOfDayFormatType(String day, DateTimeFormatter FormatType) {
        LocalDate ld = LocalDate.parse(day, FormatType);
        return getDateTimeStartOfDay(ld);
    }

    /**
     * 오늘 날짜의 마지막 시간을 리턴한다.
     * @return
     */
    public static LocalDateTime getDateTimeFormEndOfDay() {
        LocalDate ld = LocalDate.now();
        return getDateTimeEndOfDay(ld);
    }
    
    public static LocalDateTime getDateTimeEndOfDay(LocalDate day) {
        return day.atTime(LocalTime.MAX);
    }
    
    /**
     * yyyy-MM-dd 형태로된 날짜 스트링을 사용하여 마지막 시간을 설정하여 LocalDateTime을 리턴 한다. 
     * @param day
     * @return
     */
    public static LocalDateTime getDateTimeFormEndOfDay(String day) {
        if(day == null) return null;
        LocalDate ld = LocalDate.parse(day, FORMAT_DAY);
        return getDateTimeEndOfDay(ld);
    }


    /**
     * yyyy-MM-dd HH:mm 형태
     * @param ldt 
     * @return
     */
    public static String getDateTimeMinuteString(LocalDateTime ldt) {
        if(ldt == null) return null;
        return ldt.format(FORMAT_HOUR);
    }
    
    /**
     * yyyy-MM 형태
     * @param ldt 
     * @return
     */
    public static String getDateTimeMonthString(LocalDateTime ldt) {
        if(ldt == null) return null;
        return ldt.format(FORMAT_MONTH);
    }
    /**
     * yyyy-MM-dd 형태의 String으로 부터 LocalDate를 반환 한다.
     * @param day
     * @return
     */
    public static LocalDate getDate(String day) {
        return LocalDate.parse(day, FORMAT_DAY);
    }
    /**
     * yyyy-MM-dd 형태
     * @param ld
     * @return
     */
    public static String getDateString(LocalDate ld) {
        if(ld == null) return null;
        return ld.format(FORMAT_DAY);
    }
    /**
     * yyyyMM 형태
     * @param dt
     * @return
     */
    public static String getYearAndMonth(LocalDate dt) {
        if(dt == null) return null;
        return dt.format(FORMAT_MONTH_NO_HI);
    }

    /**
     * yyyyMMdd 형태
     * @param dt
     * @return
     */
    public static String getDateTimeNoFormatDay(LocalDate dt) {
        return dt.format(NO_FORMAT_DAY);
    }

    /**
     * Format되지 않은 Date String을 Format 된 String으로 변경 함.
     * @param date
     * @return
     */
    public static String makeNonFormatToFormat(String date) {
        int size = date.length();
        StringBuilder sb = new StringBuilder();
        if( size >= 4) {
            sb.append( date.substring(0, 4)).append("-");    
        }
        if( size >= 6) {
            sb.append(date.substring(4, 6));    
        }
        if( size >= 8) {
            //일까지
            sb.append("-").append(date.substring(6, 8));
        }
        if( size >= 10) {
            //시까지
            sb.append(" ").append(date.substring(8, 10));
        }
        if (size >= 12) {
            //분까지
            sb.append(":").append(date.substring(10, 12));
        }
        if(size >= 14) {
            //초까지
            sb.append(":").append(date.substring(12,14));
        }
        
        return sb.toString();
    }
    
    /**
     * Format되지 않은 Date String을 Format 된 String으로 변경 함.
     * @param date
     * @return
     */
    public static String strDateToKoDateString(String date) {
        int size = date.length();
        StringBuilder sb = new StringBuilder();
        if( size >= 4) {
            sb.append( date.substring(2, 4)).append("년");    
        }
        if( size >= 6) {
            sb.append(" ").append(date.substring(4, 6)).append("월");    
        }
        
        return sb.toString();
    }
    
    /**
     * yyyy-MM-dd 형태로된 날짜 스트링을 사용하여 시작 시간을 설정하여 LocalDateTime을 리턴 한다.
     * @param day
     * @return
     */
    public static LocalDateTime getStrDateToLocalDateTime(String strDate, String format) {
        LocalDate ld = LocalDate.parse(strDate, DateTimeFormatter.ofPattern(format));
        return getDateTimeStartOfDay(ld);
    }
    

    /**
     * 문자열 날짜 형식을 변환 한다. 
     * @param strDate
     * @param format
     * @param toFormat
     * @return
     */
    public static String getFomatDateString(String strDate, String format, String toFormat) {
        try {
            LocalDate ld = LocalDate.parse(strDate, DateTimeFormatter.ofPattern(format));
            return getDateTimeStartOfDay(ld).format(DateTimeFormatter.ofPattern(toFormat));    
        } catch (Exception e) {
            return strDate;
        }
        
    }
    
    /**
     * 문자열 날짜 형식을 변환 한다.
     * @param strDate
     * @param format
     * @param toFormat
     * @return
     */
    public static String getFomatDateString(String strDate, DateTimeFormatter format, DateTimeFormatter toFormat) {
        try {
            LocalDate ld = LocalDate.parse(strDate, format);
            return getDateTimeStartOfDay(ld).format(toFormat);    
        } catch (Exception e) {
            return strDate;
        }
        
    }
    
    /**
     * 
     * @param strDate
     * @return
     */
    public static String getFomatDayToMonth(String strDate) {
        try {
            LocalDate ld = LocalDate.parse(strDate, NO_FORMAT_DAY);
            return getDateTimeStartOfDay(ld).format(FORMAT_MONTH_NO_HI);    
        } catch (Exception e) {
            return strDate;
        }
    }
    
    /**
     * 오늘 날짜의 시작 시간을 리턴 한다.
     * @return
     */
    public static String getLsatMonthKoString() {
        LocalDate ld = LocalDate.now();
        String yyyymm = getDateTimeStartOfDay(ld).minusMonths(1).format(FORMAT_MONTH_NO_HI);
        return strDateToKoDateString(yyyymm); 
    }

    /**
     * 특정 형태의 String으로 부터 LocalDate를 반환 한다.
     * @param date
     * @return
     */
    public static LocalDate getDate(String date, DateTimeFormatter format) {
        return LocalDate.parse(date, format);
    }

    public static String getDateTimeKrFormatYMD(LocalDate dt) {
        return dt.format(KR_FORMAT_YYYY_MM_DD);
    }

    public static String getDateTimeKrFormatMD(LocalDate dt) {
        return dt.format(KR_FORMAT_MM_DD);
    }    

    public static String getDateTimeNoFormatFull(LocalDateTime dt) {
        return dt.format(NO_FORMAT_FULL);
    }

    /**
     * format 형식에 따라 리턴
     * @param ldt 
     * @return
     */
    public static String getDateTimeString(LocalDateTime ldt, DateTimeFormatter format) {
        if(ldt == null) return null;
        return ldt.format(format);
    }

    /**
     * format 형식에 따라 리턴
     * @param ldt 
     * @return
     */
    public static String getDateTimeString(Timestamp dt, DateTimeFormatter format) {
        if(dt == null) return null;
        return dt.toLocalDateTime().toLocalDate().format(format);
    }

    public static String getDateTimeString(String scraYmd, DateTimeFormatter formatDay) {
        if(scraYmd == null) return null;
        return String.format(scraYmd, formatDay);
    }


}

