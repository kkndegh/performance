package com.only.aos.cms.account.service.vo;

import com.only.aos.cms.account.service.vo.common.AbstractAuditingEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown=true)
public class CustAcctVO extends AbstractAuditingEntity{
    
    // private String custAcctSeqNo;
    private String custSeqNo;
    // private String bankCertMSeqNo;
    // private String bankIdCryptVal;
    // private String bankPasswdCryptVal;
    // private String dpsiNm;
    private String acctNo;
    // private String acctPasswdCryptVal;
    // private String ctzBizNoCryptVal;
    // private String crc;
    // private String custAcctStsCd;
    // private String acctInqRltCd;
    // private String acctPrc;
    // private String acctPrcModTs;
    // private String traDt;
    // private String traTimes;
    // private String mngTgtYn;
    // private String recoYn;
    // private String recoTm;
    // private String delYn;
    // private String acctBizYn;
    // private String acctBizInspYn;

    private String bankCd;
}