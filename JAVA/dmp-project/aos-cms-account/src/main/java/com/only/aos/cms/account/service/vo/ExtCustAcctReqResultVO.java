package com.only.aos.cms.account.service.vo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 4.9.9    고객 계좌 거래내역 조회 요청 결과 통보
 *
 */
@JsonInclude(JsonInclude.Include.ALWAYS)
@Getter
@Setter
@ToString
public class ExtCustAcctReqResultVO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long ais_req_seq_no;
    private String req_sts_cd;
    private String req_rlt_cd;
    private String cust_id;
    private String acct_no;
    private String bank_cd;
}