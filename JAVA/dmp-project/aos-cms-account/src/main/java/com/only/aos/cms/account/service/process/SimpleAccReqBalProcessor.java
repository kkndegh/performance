/**
 * @author kenny
  */
package com.only.aos.cms.account.service.process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.only.aos.cms.account.security.CryptHelper;
import com.only.aos.cms.account.service.vo.TsCustAcctVrfyVO;
import com.only.aos.cms.account.service.vo.simple.SimpleAccReqVO;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Slf4j
@Component("SimpleAccReqBalProcessor")
public class SimpleAccReqBalProcessor implements Processor {
    @Autowired private ObjectMapper mapper;
    @Value("${ais.threads:2}") private int nThreads;

    @Override
    public void process(Exchange ex) throws Exception {
        @SuppressWarnings("unchecked")
        List<TsCustAcctVrfyVO> reqs = (List<TsCustAcctVrfyVO>)ex.getIn().getBody(List.class);
        if (reqs == null) throw new CamelException("Routing pipe is broken");

        ArrayList<ArrayList<SimpleAccReqVO>> outer = new ArrayList<ArrayList<SimpleAccReqVO>>();
        ArrayList<SimpleAccReqVO> inner = new ArrayList<SimpleAccReqVO>();

        int w = (int)Math.ceil((double)reqs.size() / (double)nThreads);
        Iterator<TsCustAcctVrfyVO> it = reqs.iterator();

        while (it.hasNext()) {
            SimpleAccReqVO dto = getDtoFromReq(it.next());
            if (dto != null)
                inner.add(dto);

            if (inner.size() >= w) {
                outer.add(inner);
                inner = new ArrayList<SimpleAccReqVO>();
            }
        }

        if (inner.size() > 0) outer.add(inner);
        if (outer.size() < 1) throw new Exception("Request is empty!");

        String jsonString = mapper.writeValueAsString(outer);
        log.info("jsonString = {}", jsonString);
        ex.getIn().setBody(jsonString);
    }

    private SimpleAccReqVO getDtoFromReq(TsCustAcctVrfyVO req) {
        Long aisReqSeqNo = req.getAcctVrfySeqNo();
        String bankCd = req.getBankCd();
        String bankId = CryptHelper.aes_decrypt(req.getBankId());
        String bankPw = CryptHelper.aes_decrypt(req.getBankPw());
        
        String acctNo = req.getAcctNo();
        if (acctNo != null) 
            acctNo = acctNo.replace("-", "");

        String acctPw = CryptHelper.aes_decrypt(req.getAcctPw());
        String ctzBizNo = CryptHelper.aes_decrypt(req.getCtzBizNo());
        if (ctzBizNo != null) 
            ctzBizNo = ctzBizNo.replace("-", "");

        if (StringUtils.isEmpty(bankCd) || StringUtils.isEmpty(acctNo) ) {
            return null;
        }

        SimpleAccReqVO dto = new SimpleAccReqVO();
        dto.setAisReqSeqNo(String.valueOf(aisReqSeqNo));
        dto.setBankCd(bankCd);
        dto.setBankId(bankId);
        dto.setBankPw(bankPw);
        dto.setAcctNo(acctNo);
        dto.setAcctPw(acctPw);
        dto.setCtzBizNo(ctzBizNo);

        return dto;
    }
}
