/** mailsung */
package com.only.aos.cms.account.scheduler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.only.aos.cms.account.common.scheduler.AbstractScheduler;
import com.only.aos.cms.account.service.AisQueryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Component
public class AisScheduler extends AbstractScheduler {

    @Autowired
    public ObjectMapper objectMapper;

    @Autowired 
    private AisQueryService aisQueryService;
    
    @Value("${scheduler.aisQuery.run:false}") 
    private boolean runScheduler;
    
    @Scheduled(fixedDelayString="${scheduler.aisQuery.fixedDelayString}") 
    public void schedulingForSendRequestToAis() {
        if (!runScheduler) 
            return;
          String lockName = getClass().getSimpleName();
        executeInternal(lockName, this::executeSendRequestToAis);
    }

    public void executeSendRequestToAis() {
        log.info("scheduled::ExecuteSendRequestToAis.");
        aisQueryService.sendRequestToAis();
    }
}