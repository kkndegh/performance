package com.only.aos.cms.account.service.vo;

import com.only.aos.cms.account.service.vo.common.AbstractAuditingEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown=true)
public class CustMVO extends AbstractAuditingEntity{
    
    private String custSeqNo;
    private String bizNm;
    private String bizNo;
    private String ceoNm;
    private String ceoNo;
    private String chrgNm;
    private String chrgNo;
    private String custId;
    private String custLockTs;
    private String custStsCd;
    private String delTs;
    private String delYn;
    private String freeTrialYn;
    private String infPathCd;
    private String infSiteCd;
    private String infSiteId;
    private String passwdCryptVal;
    private String passwdErrCnt;
    private String passwdMdfyDt;
    private String passwdMdfyTgtYn;
    
}