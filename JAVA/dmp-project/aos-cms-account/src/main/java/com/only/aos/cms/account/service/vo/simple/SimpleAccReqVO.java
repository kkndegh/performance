package com.only.aos.cms.account.service.vo.simple;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@ToString
public class SimpleAccReqVO {
  private String aisReqSeqNo;
  private String bankNm;
  private String bankCd;
  private String bankId;
  private String bankPw;
  private String acctNo;
  private String acctPw;
  private String ctzBizNo;
  private String crc;
  private String inqSttDt; 
  private String inqEndDt;
  private String custAcctSeqNo;
}
