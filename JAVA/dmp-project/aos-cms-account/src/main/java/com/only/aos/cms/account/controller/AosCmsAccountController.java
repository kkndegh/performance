package com.only.aos.cms.account.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * AOS CMS ACCOUNT 컨트롤러
 */
@RestController
@RequestMapping("/api/v1")
public class AosCmsAccountController {

    /**
     * 헬스체크
     * 
     * @param request HttpServletRequest
     * @return ResponseEntity<HashMap<String>>
     */
    @GetMapping(value = "/HealthCheck")
    public ResponseEntity<String> postAos(HttpServletRequest request) {
        String rs = "Health Check!!";
        return new ResponseEntity<String>(rs, HttpStatus.OK);
    }
}
