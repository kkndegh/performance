/** mailsung */
package com.only.aos.cms.account;

import java.nio.charset.Charset;

public class Constant {
    public static final Charset DEFAULT_CHARSET = Charset.forName("EUC-KR");

    //HttpParam 셋팅시 Title
    public static final String AIS_TITLE = "[Params]";
} 