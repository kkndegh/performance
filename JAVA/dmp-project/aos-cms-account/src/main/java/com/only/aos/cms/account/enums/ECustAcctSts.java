package com.only.aos.cms.account.enums;

/**
 * 고객 계좌 상태
 * @author bak
 *
 */
public enum ECustAcctSts {
    REG,
    MOD,
    RDY,
    INQ,
    NOR,
    ERR,
    INS,
    PAU
}
