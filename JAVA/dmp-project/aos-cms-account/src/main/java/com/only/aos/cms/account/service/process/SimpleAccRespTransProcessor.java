/**
 * @author mailsung
 */
package com.only.aos.cms.account.service.process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.only.aos.cms.account.service.vo.simple.SimpleAccRespTransVO;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("SimpleAccRespTransProcessor")
public class SimpleAccRespTransProcessor implements Processor {
    @Autowired private ObjectMapper mapper;
    @Autowired private ProducerTemplate producer;

    @Override
    public void process(Exchange ex) throws Exception {
        String jsonString = ex.getIn().getBody(String.class);
        if (jsonString == null) throw new Exception("Empty Body!");
        
        SimpleAccRespTransVO[][] dtos = mapper.readValue(jsonString, SimpleAccRespTransVO[][].class);
        if (dtos != null) {
            for (int i = 0, n = dtos.length ; i < n ; i++) {
                for (int j = 0, m = dtos[i].length ; j < m ; j++) {
                    producer.asyncRequestBody("direct:to-persistence", mapper.writeValueAsString(dtos[i][j]));
                }
            }
        }
    }
}
