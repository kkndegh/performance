package com.only.aos.cms.account.common.util;

import java.util.HashMap;

import com.only.aos.cms.account.service.dto.HttpParamDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * 공통 유틸
 * 
 * @since 2022.05.31
 * @author kh
 */
@Slf4j
@Component
public class CommonUtil {

    @Value("${api.https.boolean}")
    public boolean apiHttpsBoolean;

    @Autowired
    public HttpUtil httpUtil;
    
    /**
     * 에러발생 처리
     * 
     * @param resultCode String
     * @param resultMsg String
     * @return String
     */
    public String resultObjMk(String clazz, String method, String resultCode, String resultMsg){
        log.error("에러발생["+clazz+"-"+method+"] :: "+ resultMsg);
        HashMap<String, String> errObj = new HashMap<String, String>();

        errObj.put("result_code", resultCode);
        errObj.put("result_msg", resultMsg);
        
        return errObj.toString();
    }

    /**
    * 실행중인 클래스 이름을 취득
    *
    * @return String
    */
    public String getClassName() {
        return Thread.currentThread().getStackTrace()[2].getClassName();
    }

    /**
    * 실행중인 함수를 취득。
    *
    * @return String
    */
    public String getMethodName() {
        return Thread.currentThread().getStackTrace()[2].getMethodName();
    }

    /**
    * String의 Null이면 공백처리
    *
    * @param Object str
    * @param String trans
    * @return String
    */
    public String nvl(Object str, String trans) {
        return (str == null) ? trans : str.toString();
    }

    /**
    * API CALL
    * 
    * @param String title
    * @param String url
    * @param String method
    * @param String header
    * @param String body
    * @return String
    */
    public String callApi(String title, String url, String method, String header, String body) {

        //GET인경우 URL에 GET PARAMETER 설정
        if(method.equals("GET")){
            if(url.contains("?")){
                url += "&"+"bodys="+body;
            }else{
                url += "?"+"bodys="+body;
            }
        }

        HttpParamDto param = new HttpParamDto(title, url, method, header, body);    //통신파라미터 초기화
        log.debug(param.toString());

        //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
        if(!apiHttpsBoolean){
            return httpUtil.callApiHttp(param);
        }else{
            return httpUtil.callApiHttps(param);
        }
    }
}