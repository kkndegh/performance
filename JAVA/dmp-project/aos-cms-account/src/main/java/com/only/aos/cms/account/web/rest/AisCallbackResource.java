/**
 * @author kenneth
 */
package com.only.aos.cms.account.web.rest;

import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/v1/ais")
@Slf4j
public class AisCallbackResource {
    @Autowired private ProducerTemplate producer;

    @PostMapping("/speed/acct/result")
    public ResponseEntity<Void> cbSpeedAcctTraInqueryResult(@RequestHeader("X-AIS-REQ") String proc, @RequestBody String payload) {
        int n = payload.length();
        if (log.isDebugEnabled()) {
            StringBuffer sb = new StringBuffer();
            sb.append("\n(+)=======================================================================(+)\n");
            sb.append(payload);
            sb.append("\n(-)=======================================================================(-)\n");
            log.debug(sb.toString());
        } else {
            log.info("{}...", payload.substring(0, n > 100 ? 100 : n));
        }

        if (n > 0)
            producer.asyncRequestBodyAndHeader("direct:from-ais", payload, "X-AIS-REQ", proc);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PostMapping("/speed/acct/error")
    public ResponseEntity<Void> cbSpeedAcctTraInqueryError(@RequestHeader("X-EXCEPTION") String exce, @RequestBody String payload) {
        StringBuffer sb = new StringBuffer();
        sb.append("\n(+)=======================================================================(+)\n");
        sb.append(payload);
        sb.append("\n(-)=======================================================================(-)\n");
        log.info(sb.toString());

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
