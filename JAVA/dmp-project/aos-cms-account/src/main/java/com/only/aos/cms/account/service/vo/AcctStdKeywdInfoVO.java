package com.only.aos.cms.account.service.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.only.aos.cms.account.service.vo.common.AbstractAuditingEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown=true)
public class AcctStdKeywdInfoVO  extends AbstractAuditingEntity {

    private Long acctKeywdSeqNo;      // 계좌 키워드 일련번호

    private String keywd;             // 키워드

    private String keywdTypCd;        // 키워드 유형 코드

    private Long acctStdKeywdSeqNo;   // 계좌 표준 키워드 일련번호
}