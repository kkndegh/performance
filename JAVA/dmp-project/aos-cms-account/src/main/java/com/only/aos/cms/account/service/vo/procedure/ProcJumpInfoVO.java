package com.only.aos.cms.account.service.vo.procedure;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.only.aos.cms.account.service.vo.common.AbstractAuditingEntity;
import com.only.aos.cms.account.service.vo.common.CommonVO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 점프 목록
 */
@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcJumpInfoVO extends CommonVO implements Serializable  {
    private static final long serialVersionUID = 1L;

    private List<JumpInfoVO> list;
    private JumpInfoVO data;

    @Getter
    @Setter
    @ToString
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    @JsonIgnoreProperties(ignoreUnknown=true)
    public static class JumpInfoVO  extends AbstractAuditingEntity {

        private Long jumpSeqNo;        // 점프 일련번호

        private String jumpCateCd;    // 점프 카테고리 코드

        private String jumpItemNm;    // 점프 항목명

        private String jumpUrl;        // 점프 URL

        private Integer ord;        // 순서
    }
}