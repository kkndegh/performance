package com.only.aos.cms.account.service.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class HttpParamDto implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
    * title
    */
    private String pTitle;

    /**
    * url
    */
    private String pUrl;

    /**
    * 메소드
    */
    private String pMethod;

    /**
    * 헤더
    */
    private String pHeader;

    /**
    * 바디
    */
    private String pBody;

    /**
    * toString
    */
    public String toString(){
        return pTitle + " -> " + "[ Url = " + pUrl + " ]"+ ", [ Method = " + pMethod + " ]"+ ", [ Header = " + pHeader + " ]"+ ", [ Body = " + pBody + "]";
    }
}