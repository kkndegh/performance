package com.only.aos.cms.account.enums;

/**
 * 대사 상태 코드
 * @author YunaJang
 */
public enum ERecoSts {
    /** 대사대기 */
    RDY,
    /** 입금확인 */
    IDEP,
    /** 미확인 */
    UDEP,
    /** 수동확인 */
    MDEP,
    /** 대사제외 */
    EXCL
}