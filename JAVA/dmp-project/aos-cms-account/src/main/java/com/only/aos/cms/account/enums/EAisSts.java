package com.only.aos.cms.account.enums;

/**
 * @author kenneth
 * 2019.11.01
 */
public enum EAisSts {
    STP, // System stopped
    ANA, // Agent is not available
    ANF, // Agent is not funtional
    RUN, 
}