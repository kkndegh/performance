package com.only.aos.cms.account.service.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.only.aos.cms.account.service.vo.common.AbstractAuditingEntity;
import com.only.aos.cms.account.service.vo.procedure.ProcCustAcctInfoVO.CustAcctInfoVO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown=true)
public class AisReqInfoVO  extends AbstractAuditingEntity {

    private Long aisReqSeqNo;       // 요청 일련번호
    
    private Long aisSeqNo;          // AIS 일련번호
    
    private Long custAcctSeqNo;     // 고객 계좌 일련 번호
    
    private String inqSttDt;        // 조회 시작 일자
    
    private String inqEndDt;        // 조회 종료 일자
    
    private String runSttTm;        // 실행 시작 일시
    
    private String runEndTm;        // 실행 종료 일시
    
    private String reqStsCd;        // 요청 상태 코드

    private String reqRltCd;        // 요청 결과 코드
    
    private String traContTrsStsCd; // 거래 내역 전송 상태 코드
    
    private String priOrdCd;        // 우선순위코드
    
    private String reqTypCd;        // 요청 유형 코드
    
    private String subsNotiUrl;     //

    private CustAcctInfoVO custAcctInfo;   // 고객 계좌 정보
}