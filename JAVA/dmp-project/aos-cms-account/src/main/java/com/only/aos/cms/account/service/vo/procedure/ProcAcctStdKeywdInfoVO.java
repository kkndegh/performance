package com.only.aos.cms.account.service.vo.procedure;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.only.aos.cms.account.service.vo.common.AbstractAuditingEntity;
import com.only.aos.cms.account.service.vo.common.CommonVO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 계좌 표준 키워드
 */
@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcAcctStdKeywdInfoVO extends CommonVO implements Serializable  {
    private static final long serialVersionUID = 1L;

    private List<AcctStdKeywdInfoVO> list;
    private AcctStdKeywdInfoVO data;

    @Getter
    @Setter
    @ToString
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    @JsonIgnoreProperties(ignoreUnknown=true)
    public static class AcctStdKeywdInfoVO  extends AbstractAuditingEntity {

        private Long acctStdKeywdSeqNo;    // 계좌 표준 키워드 일련번호
        
        private String stdTypCd;        // 표준 유형 코드

        private String stdKeywd;        // 표준 키워드
    }
}