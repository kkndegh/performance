package com.only.aos.cms.account.security;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import com.only.aos.cms.account.common.util.EntityUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CryptHelper {
    
    @SuppressWarnings("unused")
    private final Logger logger = LoggerFactory.getLogger(CryptHelper.class);

    private final static String CD = "7Jio66as7JuQ66eM7IS4IQ";

    
    public static SecretKeySpec generateAESKey(final String key, final String encoding) {
        try {
            final byte[] finalKey = new byte[16];
            int i = 0;
            for(byte b : key.getBytes(encoding))
                finalKey[i++%16] ^= b;            
            return new SecretKeySpec(finalKey, "AES");
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public static String aes_encrypt(String data) {
        return aes_encrypt(data, CD);
    }

    public static String aes_decrypt(String data) {
        return aes_decrypt(data, CD);
    }

    /**
     * idx 값 이후에 ***로 변경 한다.
     * 
     * @param data
     * @param idx
     * @return
     */
    public static String aes_decrypt(String data, int idx) {
        String sss = aes_decrypt(data, CD);
        return EntityUtil.maskingUtil(sss, idx);
    }

    public static String aes_encrypt(String password, String strKey) {
        
        
        String result = null;
        if (password == null)
            return null;
        if (password.isEmpty())
            return password;
        try {
            SecretKey key = generateAESKey(strKey, "UTF-8");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);

            byte[] cleartext = password.getBytes("UTF-8");
            byte[] ciphertextBytes = cipher.doFinal(cleartext);
            
            result = new String(DatatypeConverter.printHexBinary(ciphertextBytes));
            
            // System.out.println("-------------------en_pass---------------------------");
            // System.out.println("password:"+password );
            // System.out.println("result:"+result );
            // System.out.println("------------------------------------------------");

            return result;

        } catch (Exception e) {

        }
        return null;
    }

    public static String aes_decrypt(String passwordhex, String strKey) {
        if (passwordhex == null)
            return null;
        if (passwordhex.isEmpty())
            return passwordhex;
        try {
            SecretKey key = generateAESKey(strKey, "UTF-8");
            Cipher decipher = Cipher.getInstance("AES");

            decipher.init(Cipher.DECRYPT_MODE, key);

            byte[] decodeHex = DatatypeConverter.parseHexBinary(passwordhex);

            byte[] ciphertextBytes = decipher.doFinal(decodeHex);

            return new String(ciphertextBytes, "UTF-8");

        } catch (Exception e) {

        }
        return null;
    }
}
