package com.only.aos.cms.account.web.rest;

import com.only.aos.cms.account.service.AisStatusService;
import com.only.aos.cms.account.service.vo.SystemStatusVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/v1/ais")
public class AisStatusResource {
    @Autowired private AisStatusService aisStatusService;

    @PostMapping("/status")
    public ResponseEntity<Void> status(@RequestBody SystemStatusVO dto) {

        if (log.isDebugEnabled()) {
            StringBuffer sb = new StringBuffer();
            sb.append("\n(+)=======================================================================(+)\n");
            sb.append(dto);
            sb.append("\n(-)=======================================================================(-)\n");
            log.debug(sb.toString());
        }

        if (dto != null)
            aisStatusService.updateStatus(dto);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
