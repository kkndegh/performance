/**
 * @author kenny
  */
package com.only.aos.cms.account.service.process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.only.aos.cms.account.service.AisQueryService;
import com.only.aos.cms.account.service.vo.simple.SimpleAccRespBalVO;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("AcctVerifyProcessor")
public class AcctVerifyProcessor implements Processor {
    @Autowired
    private AisQueryService aisQueryService;
    @Autowired 
    private ObjectMapper mapper;

    @Override
    public void process(Exchange ex) throws Exception {
        String jsonString = ex.getIn().getBody(String.class);
        if (jsonString == null)
            throw new Exception("Empty Body!");

        SimpleAccRespBalVO dto = mapper.readValue(jsonString, SimpleAccRespBalVO.class);
        if (dto != null) {
            log.info("==> {}", dto);
            aisQueryService.updateEndTm(dto);
        }
    }
}
