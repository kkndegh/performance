package com.only.aos.cms.account.service.vo.procedure;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.only.aos.cms.account.enums.ERecoSts;
import com.only.aos.cms.account.service.vo.common.AbstractAuditingEntity;
import com.only.aos.cms.account.service.vo.common.CommonVO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 계좌 거래 내역
 */
@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcAcctTraContInfoVO extends CommonVO implements Serializable {
    private static final long serialVersionUID = 1L;

    private List<AcctTraContInfoVO> list;
    private AcctTraContInfoVO data;

    @Getter
    @Setter
    @ToString
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    @JsonIgnoreProperties(ignoreUnknown=true)
    public static class AcctTraContInfoVO  extends AbstractAuditingEntity {
		
		private Long acctTraContSeqNo;	// 계좌 거내 내역 일련 번호

		private String traDt;			// 거래 일자

        private String traTimes;           // 거래 시각

        private String crcCd;              // 통화 코드

        private Long wdrPrc;               // 출금액

        private Long depoPrc;              // 입금액

		private Long traRemain;			// 거래후 잔액

		private String note1;			// 기재사항 1

		private String note2;			// 기재사항 2

		private String traTyp1;			// 거래 수단 1

		private String traTyp2;			// 거래 수단 2

		private String acctNo;			// 계좌 번호

		private String acctTraCont;		// 계좌 거래 내역

		private String summ;			// 적요

		private String traBranchNm;		// 거래 지점명

		private Long custAcctSeqNo;		// 고객 계좌 일련 번호

		private Long aisReqSeqNo;		// AIS 요청 일련번호

		private Long acctKeywdSeqNo;	// 계좌 키워드 일련번호

		@Enumerated(EnumType.STRING)
		private ERecoSts recoStsCd;		// 대사 상태 코드
    }
}