package com.only.aos.cms.account.config;

import org.springframework.stereotype.Component;

/**
 * Application constants.
 */
@Component
public final class Constants {
    public static final String SYSTEM_ACCOUNT = "system";
    public static final String CUSTOMER_AUDTITOR_NAME = "customer_id";
    public static final String HEADER_NAME_SVC_ID = "svc_id";
    public static final String HEADER_NAME_SVC_CERT_KEY = "svc_cert_key";

    public static final String SYSTEM_STATUS_ERROR = "SE000001";
}
