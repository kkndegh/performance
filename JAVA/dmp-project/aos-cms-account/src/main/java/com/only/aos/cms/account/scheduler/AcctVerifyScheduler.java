package com.only.aos.cms.account.scheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.only.aos.cms.account.common.scheduler.AbstractScheduler;
import com.only.aos.cms.account.service.AisQueryService;
import com.only.aos.cms.account.service.AisStatusService;
import com.only.aos.cms.account.service.vo.TsCustAcctVrfyVO;
import com.only.aos.cms.account.service.vo.procedure.AisAvailableVO;

import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
  
@Slf4j
@Component
public class AcctVerifyScheduler extends AbstractScheduler {
    @Autowired private AisStatusService statusService;
    @Autowired private ProducerTemplate producer;
    @Autowired private AisQueryService queryService;
    @Value("${ais.port:9080}") private int aisPort;
    @Value("${scheduler.acctVerify.run:false}") private boolean runScheduler;
    @Value("${scheduler.acctVerify.fetch:20}") private int maxFetch;
    @Value("${scheduler.acctVerify.query:2}") private int maxQuery;

    @Scheduled(cron="${scheduler.acctVerify.cron}") 
    public void schedulingForAcctVerify() {
        if (runScheduler) 
            return;
        String lockName = getClass().getSimpleName();
        executeInternal(lockName, this::executeAcctVerify);
    }

    public void executeAcctVerify() {
        log.info("scheduled::ExecuteAcctVerify.");
        try {
            List<TsCustAcctVrfyVO> list = queryService.selectAcctVerifyList(maxFetch);
            if (list == null || list.size() < 1) return;

            AisAvailableVO sys = statusService.getAisAvailableInfo();
            if (sys == null) {
                log.warn("We have not the available AISes.");
                return;
            }

            List<Long> seqs = new ArrayList<Long>();
            List<TsCustAcctVrfyVO> part = new ArrayList<TsCustAcctVrfyVO>();

            for (int i = 0, n = list.size() ; i < n ; ) {
                TsCustAcctVrfyVO req = list.get(i);

                part.add(req);
                seqs.add(req.getAcctVrfySeqNo());
                i++;

                if (i % maxQuery == 0)
                    sendQuery(sys, part, seqs);
            }

            if (part.size() > 0)
                sendQuery(sys, part, seqs);
        } catch (Exception e) {
            log.error("" + e);
        }
    }
    
    private void sendQuery(AisAvailableVO sys, List<TsCustAcctVrfyVO> part, List<Long> seqs) {
        try {
            queryService.updateSttTm(seqs);

            Map<String, Object> headers = new HashMap<String, Object>();
            headers.put("X-AIS-REQ", "SimpleAccReqBal");
            headers.put("X-AIS-REQ-SEQS", new ArrayList<Long>(seqs));
            headers.put("X-AIS-HOST", sys.getIp());
            headers.put("X-AIS-PORT", aisPort);
            producer.asyncRequestBodyAndHeaders("direct:to-ais", new ArrayList<TsCustAcctVrfyVO>(part), headers);

        } catch (Exception e) {
            log.error("" + e);
        } finally {
            part.clear();
            seqs.clear();
        }
    }
}
  