/**
 * @author kenny
 */
package com.only.aos.cms.account.service;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.IMap;
import com.only.aos.cms.account.Constant;
import com.only.aos.cms.account.common.util.CommonUtil;
import com.only.aos.cms.account.service.vo.SystemStatusVO;
import com.only.aos.cms.account.service.vo.procedure.AisAvailableVO;
import com.only.aos.cms.account.service.vo.procedure.ProcAisAvailableVO;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AisStatusService {

    @Autowired
    private CommonUtil commonUtil;

    @Autowired(required = false)
    @Qualifier("ais-status")
    private IMap<String, Object> aisStatus;
    // @Autowired private TrAisLRepository trAisLRepository;

    @Value("${aos.base.url}")
    public String aosBaseUrl;

    @Autowired
    public ObjectMapper objectMapper;

    @PostConstruct
    public void init() {
        if (aisStatus != null)
            aisStatus.put("prev-seq-no", 0L);
    }

    public void updateStatus(SystemStatusVO dto) {
        AisAvailableVO aisAvailableInfo = new AisAvailableVO();
        String hostAddr = dto.getHostAddr();

        // TODO availableBody 값 확인이 필요.
        // Optional<AisAvailableVO> opt = trAisLRepository.findByIp(hostAddr);
        //API 호출 정보
        String toUrl = aosBaseUrl + "/api/v1/ais/availableInfo";
        String toMethod = "POST";
        String toHeader = "{\"access_token\":\"qwerty\"}";
        String toBody = "{\"host_addr\":\"" + hostAddr + "\"}";
        String availableStr = ""; // 응답값

        //API 호출
        availableStr = commonUtil.callApi(Constant.AIS_TITLE, toUrl, toMethod, toHeader, toBody);
        log.debug(availableStr);
        
        try {
            ProcAisAvailableVO procAisAvailable = objectMapper.readValue(availableStr, ProcAisAvailableVO.class);

            if (procAisAvailable.getList().size() > 0) {
                for (int i = 0; i < procAisAvailable.getList().size(); i++) {
                    aisAvailableInfo = procAisAvailable.getList().get(i);
                }
            }

            if (ObjectUtils.isNotEmpty(aisAvailableInfo)) {
                aisAvailableInfo.setAisVer(dto.getVersion());
                aisAvailableInfo.setIp(hostAddr);
                aisAvailableInfo.setBrCnt((long)dto.getBrowserCount());
                aisAvailableInfo.setAisStsCd(dto.getStatus());
                aisAvailableInfo.setReqRdyNum((long)dto.getPendingCount());
                aisAvailableInfo.setReqRunNum((long)dto.getCompletedCount());
            }
            String aisAvailableInfoJsonStr = objectMapper.writeValueAsString(aisAvailableInfo);
        
            // TODO - 원본
            // trAisLRepository.save(aisAvailableInfo);
    
            //API 호출 정보
            toUrl = aosBaseUrl + "/api/v1/ais/availableInfoSave";
            toMethod = "POST";
            toHeader = "{\"access_token\":\"qwerty\"}";
            toBody = aisAvailableInfoJsonStr;
            availableStr = ""; // 응답값
    
            //API 호출
            availableStr = commonUtil.callApi(Constant.AIS_TITLE, toUrl, toMethod, toHeader, toBody);
            log.debug(availableStr);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized AisAvailableVO getAisAvailableInfo() {
        AisAvailableVO aisAvailableInfo = null;

        try {
            Long prevSeqNo = (Long)aisStatus.get("prev-seq-no");
            log.debug("==> prevSeqNo = {}", prevSeqNo);
            
            if (prevSeqNo == null) prevSeqNo = 0L;

            //API 호출 정보
            String toUrl = aosBaseUrl + "/api/v1/ais/availableInfo";
            String toMethod = "POST";
            String toHeader = "{\"access_token\":\"qwerty\"}";
            String toBody = "{}";
            String availableStr = ""; // 응답값

            //API 호출
            availableStr = commonUtil.callApi(Constant.AIS_TITLE, toUrl, toMethod, toHeader, toBody);
            log.debug(availableStr);

            ProcAisAvailableVO procAisAvailable = objectMapper.readValue(availableStr, ProcAisAvailableVO.class);

            if (procAisAvailable.getList().size() > 0) {
                for (int i = 0; i < procAisAvailable.getList().size(); i++) {
                    aisAvailableInfo = procAisAvailable.getList().get(i);
                }
            }

            if (ObjectUtils.isNotEmpty(aisAvailableInfo)) {
                log.info("Selected AIS : " + aisAvailableInfo);

                Long aisSeqNo = aisAvailableInfo.getAisSeqNo();

                aisStatus.put("prev-seq-no", aisSeqNo);
            }

        } catch (Exception e) {
            log.error("에러 :: "+e.toString());
        }
        return aisAvailableInfo;
    }
}

