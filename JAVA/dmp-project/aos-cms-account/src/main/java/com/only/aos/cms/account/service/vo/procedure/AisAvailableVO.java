package com.only.aos.cms.account.service.vo.procedure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.only.aos.cms.account.enums.EAisSts;
import com.only.aos.cms.account.service.vo.common.AbstractAuditingEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown=true)
public class AisAvailableVO extends AbstractAuditingEntity {

    private Long aisSeqNo;      // AIS 일련번호

    private String aisVer;      // AIS 버전

    private String ip;          // AIS 아이피

    private Long brCnt;         // 브라우저 수

    private Long aisPrcsCpa;    // AIS 처리 용량

    private EAisSts aisStsCd;    // AIS 상태 코드

    private Long reqRdyNum;     // 요청 대기 건수

    private Long reqRunNum;     // 요청 실행 건수
    
}