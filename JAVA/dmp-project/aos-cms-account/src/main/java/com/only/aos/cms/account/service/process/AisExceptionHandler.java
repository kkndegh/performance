/**
 * @author kenny
  */
package com.only.aos.cms.account.service.process;

import java.util.List;

import com.only.aos.cms.account.enums.ECustAcctSts;
import com.only.aos.cms.account.enums.EReqSts;
import com.only.aos.cms.account.service.AisQueryService;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import static com.only.aos.cms.account.config.Constants.SYSTEM_STATUS_ERROR;

@Slf4j
@Component("AisExceptionHandler")
public class AisExceptionHandler implements Processor {
   @Autowired private AisQueryService aisQueryService;

    @Override
    public void process(Exchange ex) throws Exception {
        String routeId = ex.getUnitOfWork().getRouteContext().getRoute().getId(); //log.debug("=> routeId : " + routeId);
        Exception e = ex.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class); log.warn("Exception : " + e.getMessage());
        
        switch (routeId) {
            case "to-ais": 
            {
                @SuppressWarnings("unchecked")
                List<Long> seqs = (List<Long>)ex.getIn().getHeader("X-AIS-REQ-SEQS", List.class);
                if (seqs != null && seqs.size() > 0)
                    aisQueryService.updateStatusEndTm(seqs, EReqSts.ERR, SYSTEM_STATUS_ERROR);

                @SuppressWarnings("unchecked")
                List<Long> custAccts = (List<Long>)ex.getIn().getHeader("X-AIS-REQ-CUST-ACCTS", List.class); 
                if (custAccts != null && custAccts.size() > 0)
                    aisQueryService.updateCustAccts(custAccts, ECustAcctSts.ERR, SYSTEM_STATUS_ERROR);
            }
            break;
            case "from-ais": {
            }
            break;
            case "to-persistence": {
            }
            break;
            default: break;
        }
    }
}