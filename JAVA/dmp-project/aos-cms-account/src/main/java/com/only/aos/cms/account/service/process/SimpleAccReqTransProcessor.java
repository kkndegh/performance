/**
 * @author mailsung
  */
package com.only.aos.cms.account.service.process;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.only.aos.cms.account.security.CryptHelper;
import com.only.aos.cms.account.service.vo.AisReqInfoVO;
import com.only.aos.cms.account.service.vo.simple.SimpleAccReqVO;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Slf4j
@Component("SimpleAccReqTransProcessor")
public class SimpleAccReqTransProcessor implements Processor {
    @Autowired private ObjectMapper mapper;
    @Value("${ais.threads:2}") private int nThreads;

    @Override
    public void process(Exchange ex) throws Exception {
        log.debug("==> I'm SimpleAccTransReqProcessor");
        @SuppressWarnings("unchecked")
        List<AisReqInfoVO> reqs = (List<AisReqInfoVO>)ex.getIn().getBody(List.class);
        // if (reqs == null) throw new CamelException("Routing pipe is broken");

        ArrayList<ArrayList<SimpleAccReqVO>> outer = new ArrayList<ArrayList<SimpleAccReqVO>>();
        ArrayList<SimpleAccReqVO> inner = new ArrayList<SimpleAccReqVO>();

        int w = (int)Math.ceil((double)reqs.size() / (double)nThreads);  //log.debug("-----> w = " + w);
        Iterator<AisReqInfoVO> it = reqs.iterator();

        while (it.hasNext()) {
            SimpleAccReqVO dto = getDtoFromReq(it.next());
            if (dto != null)
                inner.add(dto);

            if (inner.size() >= w) {
                // log.debug("new inner!");
                outer.add(inner);
                inner = new ArrayList<SimpleAccReqVO>();
            }
        }

        if (inner.size() > 0) outer.add(inner);
        if (outer.size() < 1) throw new Exception("Request is empty!");

        String jsonString = mapper.writeValueAsString(outer); //log.debug("**^^** jsonString = \n" + jsonString);

        // String jsonString = mapper.writeValueAsString("{\"test\" : \"1\"}");

        ex.getIn().setBody(jsonString);
    }

    private SimpleAccReqVO getDtoFromReq(AisReqInfoVO req) {
        Long aisReqSeqNo = req.getAisReqSeqNo();
        // String bankNm = req.getCustAcctInfo().getBankAuthM().getBankNm();
        // String bankCd = req.getCustAcctInfo().getBankAuthM().getBankCd();
        String bankNm = "신한은행";
        String bankCd = "shinhan";
        String bankId = CryptHelper.aes_decrypt(req.getCustAcctInfo().getBankIdCryptVal());
        String bankPw = CryptHelper.aes_decrypt(req.getCustAcctInfo().getBankPasswdCryptVal());
        
        String acctNo = req.getCustAcctInfo().getAcctNo();
        if (acctNo != null) 
            acctNo = acctNo.replace("-", "");

        String acctPw = CryptHelper.aes_decrypt(req.getCustAcctInfo().getAcctPasswdCryptVal());
        String ctzBizNo = CryptHelper.aes_decrypt(req.getCustAcctInfo().getCtzBizNoCryptVal());
        if (ctzBizNo != null) 
            ctzBizNo = ctzBizNo.replace("-", "");

        String crc = req.getCustAcctInfo().getCrc();
        String inqSttDt = req.getInqSttDt();
        String inqEndDt = req.getInqEndDt();
        String custAcctSeqNo = String.valueOf(req.getCustAcctSeqNo());

        // if (StringUtils.isEmpty(bankCd)
        //     || StringUtils.isEmpty(acctNo)
        //     || StringUtils.isEmpty(inqSttDt)
        //     || StringUtils.isEmpty(inqEndDt)
        // ) {
        //     return null;
        // }

        if (log.isDebugEnabled()) {
            StringBuffer sb = new StringBuffer();
            sb.append("aisReqSeqNo=").append(aisReqSeqNo).append(",");
            sb.append("bankNm=").append(bankNm).append(",");
            sb.append("bankCd=").append(bankCd).append(",");
            sb.append("bankId=").append(bankId).append(",");
            sb.append("acctNo=").append(acctNo).append(",");
            sb.append("ctzBizNo=").append(ctzBizNo).append(",");
            sb.append("crc=").append(crc).append(",");
            sb.append("inqSttDt=").append(inqSttDt).append(",");
            sb.append("inqEndDt=").append(inqEndDt).append(",");
            sb.append("custAcctSeqNo=").append(custAcctSeqNo);
            log.debug(sb.toString());
        }

        SimpleAccReqVO dto = new SimpleAccReqVO();
        dto.setAisReqSeqNo(String.valueOf(aisReqSeqNo));
        dto.setBankNm(bankNm);
        dto.setBankCd(bankCd);
        dto.setBankId(bankId);
        dto.setBankPw(bankPw);
        dto.setAcctNo(acctNo);
        dto.setAcctPw(acctPw);
        dto.setCtzBizNo(ctzBizNo);
        dto.setCrc(crc);
        dto.setInqSttDt(inqSttDt);
        dto.setInqEndDt(inqEndDt);
        dto.setCustAcctSeqNo(custAcctSeqNo);

        return dto;
    }
}
