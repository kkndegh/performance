package com.only.aos.cms.account.common.util;

import org.apache.commons.lang3.StringUtils;



public class EntityUtil{
  
    public static String maskingUtil(String text, int idx){
        if(text.length() > idx) {
            return text.substring(0, idx) + StringUtils.repeat("*", text.length() - idx);    
        } else {
            return text;
        }
        
    }
}