package com.only.aos.cms.account.service.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.only.aos.cms.account.service.vo.common.AbstractAuditingEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown=true)
public class TsCustAcctVrfyVO extends AbstractAuditingEntity{
    
    private String bankCd;
    private String bankId;
    private String bankPw;
    private String acctNo;
    private String acctPw;
    private String ctzBizNo;

    private Long acctVrfySeqNo;
    private String runSttTm;
    private String runEndTm;
    private String reqStsCd;
    private String reqRltCd;
}
