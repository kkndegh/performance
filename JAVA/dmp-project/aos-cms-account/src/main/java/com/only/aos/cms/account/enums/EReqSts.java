package com.only.aos.cms.account.enums;

/**
 * A050 정산계좌조회 요청 상태 코드
 * @author bak
 *
 */
public enum EReqSts {
    RDY, // 대기
    RUN, // 실행
    ERR, // 오류
    COM // 완료
}
