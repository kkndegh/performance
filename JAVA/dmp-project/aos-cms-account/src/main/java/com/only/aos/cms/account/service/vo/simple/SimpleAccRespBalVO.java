package com.only.aos.cms.account.service.vo.simple;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString(callSuper = true)
public class SimpleAccRespBalVO extends SimpleAccRespVO {
    private static final long serialVersionUID = 1L;
    private Balance result;

    @Getter
    @Setter
    @ToString
    public static class Balance implements Serializable {
        private static final long serialVersionUID = 1L;
        private String acctNm;
        private String acctNo;
        private String crc;
        private String balance;
        private String wdrAvlAmt;
        private String cmsWdrAvlAmt;
    }
}
