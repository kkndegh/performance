/**
 * @author kenny
  */
package com.only.aos.cms.account.service.process;

import java.util.HashMap;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.only.aos.cms.account.Constant;
import com.only.aos.cms.account.common.util.CommonUtil;
import com.only.aos.cms.account.service.AisQueryService;
import com.only.aos.cms.account.service.vo.ExtCustAcctReqResultVO;
import com.only.aos.cms.account.service.vo.procedure.ProcCustAcctVO;
import com.only.aos.cms.account.service.vo.simple.SimpleAccRespTransVO;
import com.only.aos.cms.account.service.vo.AisReqInfoVO;
import com.only.aos.cms.account.service.vo.CustAcctVO;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang3.ObjectUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("PersistenceProcessor")
public class PersistenceProcessor implements Processor {
    
    @Autowired
    private AisQueryService aisQueryService;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    public ObjectMapper objectMapper;

    @Autowired
    private CommonUtil commonUtil;

    @Value("${aos.base.url}") public String aosBaseUrl;

    @Override
    public void process(Exchange ex) throws Exception {
        // log.debug("I'm PersistenceProcessor");
        String jsonString = ex.getIn().getBody(String.class);
        if (jsonString == null)
            throw new Exception("Empty Body!");

        SimpleAccRespTransVO dto = mapper.readValue(jsonString, SimpleAccRespTransVO.class);
        if (dto != null) {
            aisQueryService.updateResult(dto);
            aisReqResultSendToGSM(ex, dto);
        }
    }

    /**
     * 4.9.9 고객 계좌 거래내역 조회 요청 결과 통보 (SMP -> Service)
     * 
     * @throws JsonProcessingException
     */
    private void aisReqResultSendToGSM(Exchange ex, SimpleAccRespTransVO dto) throws JsonProcessingException {
        // GSM으로 전달할 URL 조회
        AisReqInfoVO trAisReqL = aisQueryService.getAisReq(Long.parseLong(dto.getAisReqSeqNo()));

        if (ObjectUtils.isEmpty(trAisReqL)) {
            return;
        }

        String notiUrl = trAisReqL.getSubsNotiUrl();
        if(notiUrl == null || notiUrl.isEmpty())
            return;

        notiUrl = notiUrl.replace("http://", "http4://"); log.debug("*** notiUrl = {}", notiUrl);
        ex.getIn().setHeader("X-AIS-GSM-URL", notiUrl);

        ExtCustAcctReqResultVO extCustAcctReqResultDTO = new ExtCustAcctReqResultVO();
        extCustAcctReqResultDTO.setAis_req_seq_no(trAisReqL.getAisReqSeqNo());
        extCustAcctReqResultDTO.setReq_sts_cd(trAisReqL.getReqStsCd());
        extCustAcctReqResultDTO.setReq_rlt_cd(trAisReqL.getReqRltCd());

        // API 설계서 v2.0.8 반영
        //TODO - 원본
        //Optional<ReTcCustAcct> optReTcCustAcct = reTcCustAcctRepository.findById(trAisReqL.getCustAcctSeqNo());

        //API 호출 정보
        String rstStr = "";
        String toUrl = aosBaseUrl + "/api/v1/ais/custAcct";
        String toMethod = "POST";
        String toHeader = "{\"access_token\":\"qwerty\"}";
        String toBody = "{}";

        //호출 파라미터 셋팅
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("custAcctSeqNo", trAisReqL.getCustAcctSeqNo());

        JSONObject obj = new JSONObject(map);
        toBody = obj.toJSONString();

        //API 호출
        rstStr = commonUtil.callApi(Constant.AIS_TITLE, toUrl, toMethod, toHeader, toBody);
        log.debug(rstStr);

        ProcCustAcctVO procCustAcctVO = objectMapper.readValue(rstStr, ProcCustAcctVO.class);
        CustAcctVO custAcctVO = procCustAcctVO.getData();

        //TODO - 원본
        // if(optReTcCustAcct.isPresent()) {
        //     ReTcCustAcct reTcCustAcct = optReTcCustAcct.get();
        //     extCustAcctReqResultDTO.setAcct_no(reTcCustAcct.getAcctNo());

        //     Optional<TsCustM> tsCustM = tsCustMRepository.findById(reTcCustAcct.getCustSeqNo());
        //     extCustAcctReqResultDTO.setCust_id(tsCustM.get().getCustId());

        //     if(Objects.nonNull(reTcCustAcct.getBankAuthM()))
        //         extCustAcctReqResultDTO.setBank_cd(reTcCustAcct.getBankAuthM().getBankCd());
        // }

        extCustAcctReqResultDTO.setAcct_no(custAcctVO.getAcctNo());

        //TODO - ts_cust_m 테이블이 없어서 일시 보류
        //API 호출 정보
        // rstStr = "";
        // toUrl = aosBaseUrl + "/api/v1/ais/custM";
        // toMethod = "POST";
        // toHeader = "{\"access_token\":\"qwerty\"}";
        // toBody = "{}";

        // //호출 파라미터 셋팅
        // HashMap<String, Object> mapC = new HashMap<String, Object>();
        // mapC.put("custSeqNo", custAcctVO.getCustSeqNo());

        // JSONObject objC = new JSONObject(mapC);
        // toBody = objC.toJSONString();

        // //API 호출
        // rstStr = commonUtil.callApi(Constant.AIS_TITLE, toUrl, toMethod, toHeader, toBody);
        // log.debug(rstStr);

        // ProcCustMVO procCustMVO = objectMapper.readValue(rstStr, ProcCustMVO.class);
        // CustMVO custMVO = procCustMVO.getData();

        // extCustAcctReqResultDTO.setCust_id(custMVO.getCustId());

        if(Objects.nonNull(custAcctVO.getBankCd()))
            extCustAcctReqResultDTO.setBank_cd(custAcctVO.getBankCd());

        String jsonBody = mapper.writeValueAsString(extCustAcctReqResultDTO);
        ex.getIn().setBody(jsonBody);
    }
}
