package com.only.aos.cms.account.enums;

public enum ECustSts {
	/** 정상 */
	NR,
	/** 임시 */
	TP,
	/** 잠김 */
	LK,
	/** 만료 */
	EM,
	/** 정지 */
	ST,
	
}
