package com.only.aos.cms.account.enums;

public enum EInfPath {
	/** 인터파크 */			IPK,
	/** 위메프 */			WMP,

	/* 페이스북   */		FABO,
	/* 인스타그램 */		INGR,
	/* 유튜브     */		YOTU,
	/* 구글베너   */		GOBA,
	/* 뉴스       */		NEWS,

	/** 나우앤아카데미 */	NNA,
	/** 이셀러스 */			ESR,
	/** 다음 */				DAM,
	/** 네이버 */			NVR,
	/** 기타검색 */			ESE,
	/** 지인소개 */			AIN,
	/** 마립 */				MAR,
	/** S-MAP */			SMA,
	/** 소드원쇼핑몰론 */	SDO,
	/** 온리원페이 */		OOP,
	/** 기타 */				ETC,

	/** 카페24 */ 			CF24,
	/** 고도몰 */			GODM,
	/** 플레이오토 */		PLA,
	/** 이지어드민 */		EZA,

	/** 신한은행 */			SHIN,
	/** 제니스9 */			GENS,
	/** SC제일은행 */		SCB,
	/** 셀메이트 */			SMT,
	/** 현대카드 */			HDC,
	/** 세모장부 */			SMJB,
	/** 셀러봇캐시 */		SELC,
	/** KB국민은행 */		KBB
}
