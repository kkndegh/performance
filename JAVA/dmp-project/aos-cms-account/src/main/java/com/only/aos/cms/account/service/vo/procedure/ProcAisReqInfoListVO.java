package com.only.aos.cms.account.service.vo.procedure;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.only.aos.cms.account.service.vo.AisReqInfoVO;
import com.only.aos.cms.account.service.vo.common.CommonVO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 정산 계좌 요청 목록
 */
@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcAisReqInfoListVO extends CommonVO implements Serializable  {
    private static final long serialVersionUID = 1L;

    private List<AisReqInfoVO> list;
}