package com.only.aos.cms.account.service.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.only.aos.cms.account.enums.EAisSts;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class SystemStatusVO {
    private Long aisSeqNo;
    private String version;
    private String hostAddr;
    private EAisSts status;
    private int browserCount;
    private int pendingCount;
    private int completedCount;

    public Long getAisSeqNo()                            { return aisSeqNo;                      }
    public void setAisSeqNo(Long aisSeqNo)               { this.aisSeqNo = aisSeqNo;             }
    public String getVersion()                           { return version;                       }
    public void setVersion(String version)               { this.version = version;               }
    public String getHostAddr()                          { return hostAddr;                      }
    public void setHostAddr(String hostAddr)             { this.hostAddr = hostAddr;             }
    public EAisSts getStatus()                           { return status;                        }
    public void setStatus(EAisSts status)                { this.status = status;                 }
    public int getBrowserCount()                         { return browserCount;                  }
    public void setBrowserCount(int browserCount)        { this.browserCount = browserCount;     }
    public int getPendingCount()                         { return pendingCount;                  }
    public void setPendingCount(int pendingCount)        { this.pendingCount = pendingCount;     }
    public int getCompletedCount()                       { return completedCount;                }
    public void setCompletedCount(int completedCount)    { this.completedCount = completedCount; }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();

        // sb.append("aisSeqNo=").append(aisSeqNo).append(",");
        sb.append("version=").append(version).append(",");
        sb.append("hostAddr=").append(hostAddr).append(",");
        sb.append("status=").append(status).append(",");
        sb.append("browserCount=").append(browserCount).append(",");
        sb.append("pendingCount=").append(pendingCount).append(",");
        sb.append("completedCount=").append(completedCount);

        return sb.toString();
    }
}
