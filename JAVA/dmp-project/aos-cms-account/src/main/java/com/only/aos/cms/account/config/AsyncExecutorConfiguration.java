/** kenny */
package com.only.aos.cms.account.config;

import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

@Configuration
public class AsyncExecutorConfiguration {
    private final int asyncExecutorSize;

    public AsyncExecutorConfiguration(@Value("${spring.datasource.async-executor-size}") int asyncExecutorSize) {
        this.asyncExecutorSize = asyncExecutorSize;
    }
    
    @Bean
    public Scheduler asyncExcecutor() {
        return Schedulers.fromExecutor(Executors.newFixedThreadPool(asyncExecutorSize));
    }
}