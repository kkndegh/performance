package com.only.aos.cms.account.service.vo.procedure;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.only.aos.cms.account.enums.ECustAcctSts;
import com.only.aos.cms.account.enums.EYesOrNo;
import com.only.aos.cms.account.service.vo.common.AbstractAuditingEntity;
import com.only.aos.cms.account.service.vo.common.CommonVO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 고객 계좌
 */
@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcCustAcctInfoVO extends CommonVO implements Serializable  {
    private static final long serialVersionUID = 1L;

    private List<CustAcctInfoVO> list;
    private CustAcctInfoVO data;

    @Getter
    @Setter
    @ToString
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    @JsonIgnoreProperties(ignoreUnknown=true)
    public static class CustAcctInfoVO  extends AbstractAuditingEntity {

        private Long custAcctSeqNo;            // 고객 계좌 일련 번호
        
        private Long custSeqNo;                // 고객 일련번호
        
        private Long bankCertMSeqNo;           // 은행 인증정보 일련번호
        
        private String bankIdCryptVal;         // 은행 아이디 
        
        private String bankPasswdCryptVal;     // 은행 비밀번호 암호화 값
        
        private String dpsiNm;                 // 예금주
        
        private String acctNo;                 // 계좌번호

        private String acctPasswdCryptVal;     // 계좌번호 비밀번호
        
        private String ctzBizNoCryptVal;       // 주민 사업자 번호

        private String crc;                    // 통화
        
        private ECustAcctSts custAcctStsCd;    // 고객 계좌 상태 코드

        private String acctInqRltCd;           // 계좌 조회 결과 값

        private EYesOrNo delYn;                // 삭제 여부

        private EYesOrNo mngTgtYn;             // 관리 대상 여부

        private Long acctPrc;                  // 계좌 금액

        // TODO private LocalDateTime acctPrcModTs;    // 계좌 금액 수정 시점
        private String acctPrcModTs;           // 계좌 금액 수정 시점

        private String traDt;                  // 거래 일자

        private String traTimes;               // 거래 시각

        private EYesOrNo reco_yn;              // 대사(계좌) 여부

        private int recoTm;                    // 대사 시점

        private EYesOrNo acctBizYn;            // 계좌 사업자 여부

        private EYesOrNo acctBizInspYn;        // 계좌 사업자 검증 여부
    }
}