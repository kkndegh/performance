package com.only.aos.cms.account.service.vo.simple;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
public class SimpleAccRespTransVO extends SimpleAccRespVO {
    private static final long serialVersionUID = 1L;
    
    private String orderNo = "0";
    private ArrayList<Transaction> traCont;
    private String custAcctSeqNo;    

    @Getter
    @Setter
    @ToString(callSuper = true)
    public static class Transaction {
        private String traDt;
        private String traTimes;
        private String crcCd;
        private String wdrPrc;
        private String depoPrc;
        private String traRemain;
        private String note1;
        private String note2;
        private String traTyp1;
        private String traTyp2;
        private String acctNo;
        private String accTraCont;
        private String summ;
        private String traBranchNm;
    }
}
