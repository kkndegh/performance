package com.only.aos.cms.account.service.vo.common;

/**
 * Base abstract class for entities which will hold definitions for created, last modified by and created,
 * last modified by date.
 */

public abstract class AbstractAuditingEntity {

    private String regId;

    private String regTs = null;

    private String modId;

    private String modTs = null;

    public String getRegId() {
        return regId;
    }

    public void setRegId(String createdBy) {
        this.regId = createdBy;
    }

    public String getRegTs() {
        return regTs;
    }

    public void setRegTs(String createdDate) {
        this.regTs = createdDate;
    }

    public String getModId() {
        return modId;
    }

    public void setModId(String lastModifiedBy) {
        this.modId = lastModifiedBy;
    }

    public String getModTs() {
        return modTs;
    }

    public void setModTs(String lastModifiedDate) {
        this.modTs = lastModifiedDate;
    }
}
