package com.only.aos.cms.account.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.IMap;
import com.only.aos.cms.account.Constant;
import com.only.aos.cms.account.common.util.CommonUtil;
import com.only.aos.cms.account.enums.ECustAcctSts;
import com.only.aos.cms.account.enums.ERecoSts;
import com.only.aos.cms.account.enums.EReqSts;
import com.only.aos.cms.account.service.vo.AisReqInfoVO;
import com.only.aos.cms.account.service.vo.TsCustAcctVrfyVO;
import com.only.aos.cms.account.service.vo.procedure.ProcAcctKeywdInfoDataVO;
import com.only.aos.cms.account.service.vo.procedure.ProcAcctTraContInfoVO;
import com.only.aos.cms.account.service.vo.procedure.ProcAisReqInfoDataVO;
import com.only.aos.cms.account.service.vo.procedure.ProcAisReqInfoListVO;
import com.only.aos.cms.account.service.vo.procedure.ProcCustAcctVrfyVO;
import com.only.aos.cms.account.service.vo.procedure.ProcAcctTraContInfoVO.AcctTraContInfoVO;
import com.only.aos.cms.account.service.vo.procedure.AisAvailableVO;
import com.only.aos.cms.account.service.vo.simple.SimpleAccRespBalVO;
import com.only.aos.cms.account.service.vo.simple.SimpleAccRespTransVO;
import com.only.aos.cms.account.service.vo.simple.SimpleAccRespTransVO.Transaction;

import org.apache.camel.ProducerTemplate;
import org.apache.commons.lang3.ObjectUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AisQueryService {

    @Autowired
    public ObjectMapper objectMapper;

    @Autowired
    private CommonUtil commonUtil;

    @Autowired 
    private AisStatusService statusService;
    
    @Autowired 
    private ProducerTemplate producer;

    @Value("${aos.base.url}") public String aosBaseUrl;
    @Value("${ais.port:9080}") private int aisPort;
    @Value("${scheduler.aisQuery.fetch:20}") private int maxFetch;
    @Value("${scheduler.aisQuery.query:2}") private int maxQuery;
    @Resource(name="ais-query-filter") private IMap<Long, Boolean> aisQueryFilter;

    public void sendRequestToAis() {
        try {
            // TODO Pageable 쿼리에서 어떻게 사용하는지 확인이 필요.
            Pageable pageable = PageRequest.of(0, maxFetch, Sort.by("aisReqSeqNo"));
            
            String toAisUrl = aosBaseUrl + "/api/v1/ais/aisReqInfoList";
            String toAisMethod = "POST";
            String toAisHeader = "{\"access_token\":\"qwerty\"}";
            String toAisBody = "{}";
            String toAisStr = "";    //응답값

            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("req_sts_cd", EReqSts.RDY.toString());
            //map.put("req_object_type", "list");
            // map.put("pageable", pageable);
            JSONObject obj = new JSONObject(map);
            toAisBody = obj.toJSONString();

            //API 호출
            toAisStr = commonUtil.callApi(Constant.AIS_TITLE, toAisUrl, toAisMethod, toAisHeader, toAisBody);
            log.debug(toAisStr);

            ProcAisReqInfoListVO procAisAvailable = objectMapper.readValue(toAisStr, ProcAisReqInfoListVO.class);
            List<AisReqInfoVO> list = procAisAvailable.getList()
                                    .stream()
                                    .filter(x-> {
                                        Long key = x.getCustAcctSeqNo();
                                        if (aisQueryFilter.get(key) == null) {
                                            aisQueryFilter.put(key, Boolean.TRUE);
                                            return true;
                                    }
                                    return false;
                                })
                                .collect(Collectors.toList());

            if (list.size() < 1) {
                return;
            }

            AisAvailableVO sys = statusService.getAisAvailableInfo();
            if (sys == null) {
                log.warn("We have not the available AISes.");
                return;
            }

            List<Long> seqs = new ArrayList<Long>();
            LinkedHashSet<Long> lhs = new LinkedHashSet<Long>();
            List<AisReqInfoVO> part = new ArrayList<AisReqInfoVO>();

            for (int i = 0, n = list.size() ; i < n ; ) {
                AisReqInfoVO req = list.get(i);

                part.add(req);
                seqs.add(req.getAisReqSeqNo());
                lhs.add(req.getCustAcctSeqNo());
                i++;

                if (i % maxQuery == 0)
                    sendQuery(sys, part, seqs, lhs);
            }

            if (part.size() > 0)
                sendQuery(sys, part, seqs, lhs);

        } catch (Exception e) {
            e.printStackTrace();
        }
	}

    private void sendQuery(AisAvailableVO sys, List<AisReqInfoVO> part, List<Long> seqs, LinkedHashSet<Long> lhs) {
        this.updateStatusSttTm(seqs, EReqSts.RUN, sys.getAisSeqNo());
        this.updateCustAccts(new ArrayList<Long>(lhs), ECustAcctSts.INQ, null);

        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("X-AIS-REQ", "SimpleAccReqTrans");
        headers.put("X-AIS-REQ-SEQS", new ArrayList<Long>(seqs));
        headers.put("X-AIS-REQ-CUST-ACCTS", new ArrayList<Long>(lhs));
        headers.put("X-AIS-HOST", sys.getIp());
        headers.put("X-AIS-PORT", aisPort);
        producer.asyncRequestBodyAndHeaders("direct:to-ais", new ArrayList<AisReqInfoVO>(part), headers);

        part.clear();
        seqs.clear();
        lhs.clear();
    }

    public AisReqInfoVO getAisReq(Long aisReqSeqNo) {

        String toAisUrl = aosBaseUrl + "/api/v1/ais/aisReqInfoData";
        String toAisMethod = "POST";
        String toAisHeader = "{\"access_token\":\"qwerty\"}";
        String toAisBody = "{}";
        String toAisStr = "";    //응답값
        
        try {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("req_sts_cd", aisReqSeqNo);
            //map.put("req_object_type", "data");

            JSONObject obj = new JSONObject(map);
            toAisBody = obj.toJSONString();

            //API 호출
            toAisStr = commonUtil.callApi(Constant.AIS_TITLE, toAisUrl, toAisMethod, toAisHeader, toAisBody);
            log.debug(toAisStr);

            ProcAisReqInfoDataVO procAisReqInfo = objectMapper.readValue(toAisStr, ProcAisReqInfoDataVO.class);
            return procAisReqInfo.getData();

        } catch (Exception e) {
            log.error("Exception :: "+e.toString());
            return new AisReqInfoVO();
        } 
        
    }

    public void updateStatusSttTm(List<Long> seqs, EReqSts sts, Long aisSeqNo) {
        //API 호출 정보
        String rstStr = "";
        String toUrl = aosBaseUrl + "/api/v1/ais/updateStatusSttTm";
        String toMethod = "POST";
        String toHeader = "{\"access_token\":\"qwerty\"}";
        String toBody = "{}";

        //List를 문자열로 변경
        String seqsString="";
        int idx=0;
        for(Long seq : seqs){
            if(idx==0){
                seqsString += String.valueOf(seq);
            }else{
                seqsString += "," + String.valueOf(seq);
            }
            idx++;
        }

        //호출 파라미터 셋팅
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("seqs", seqsString);
        map.put("sts", sts.toString());
        map.put("aisSeqNo", aisSeqNo);

        JSONObject obj = new JSONObject(map);
        toBody = obj.toJSONString();

        //API 호출
        rstStr = commonUtil.callApi(Constant.AIS_TITLE, toUrl, toMethod, toHeader, toBody);
        log.debug(rstStr);
    }

    public void updateStatusEndTm(List<Long> seqs, EReqSts sts, String errCode) {
        //API 호출 정보
        String rstStr = "";
        String toUrl = aosBaseUrl + "/api/v1/ais/updateStatusEndTm";
        String toMethod = "POST";
        String toHeader = "{\"access_token\":\"qwerty\"}";
        String toBody = "{}";

        //List를 문자열로 변경
        String seqsString="";
        int idx=0;
        for(Long seq : seqs){
            if(idx==0){
                seqsString += String.valueOf(seq);
            }else{
                seqsString += "," + String.valueOf(seq);
            }
            idx++;
        }

        //호출 파라미터 셋팅
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("seqs", seqsString);
        map.put("sts", sts.toString());
        map.put("errCode", errCode);

        JSONObject obj = new JSONObject(map);
        toBody = obj.toJSONString();

        //API 호출
        rstStr = commonUtil.callApi(Constant.AIS_TITLE, toUrl, toMethod, toHeader, toBody);
        log.debug(rstStr);
    }

    public void updateCustAccts(List<Long> custAccts, ECustAcctSts sts, String errCode) {
        //API 호출 정보
        String rstStr = "";
        String toUrl = aosBaseUrl + "/api/v1/ais/updateCustAccts";
        String toMethod = "POST";
        String toHeader = "{\"access_token\":\"qwerty\"}";
        String toBody = "{}";

        //List를 문자열로 변경
        String custAcctString="";
        int idx=0;
        for(Long custAcct : custAccts){
            if(idx==0){
                custAcctString += String.valueOf(custAcct);
            }else{
                custAcctString += "," + String.valueOf(custAcct);
            }
            idx++;
        }

        //호출 파라미터 셋팅
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("custAccts", custAcctString);
        map.put("sts", sts.toString());
        map.put("errCode", errCode);

        JSONObject obj = new JSONObject(map);
        toBody = obj.toJSONString();

        //API 호출
        rstStr = commonUtil.callApi(Constant.AIS_TITLE, toUrl, toMethod, toHeader, toBody);
        log.debug(rstStr);
    }

    public List<TsCustAcctVrfyVO> selectAcctVerifyList(int fetch) throws Exception {
        String toUrl = aosBaseUrl + "/api/v1/ais/custAcctVrfy";
        String toMethod = "POST";
        String toHeader = "{\"access_token\":\"qwerty\"}";
        String toBody = "{}";
        String rstStr = "";    //응답값
        
        try {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("fetch", fetch);

            JSONObject obj = new JSONObject(map);
            toBody = obj.toJSONString();

            //API 호출
            rstStr = commonUtil.callApi(Constant.AIS_TITLE, toUrl, toMethod, toHeader, toBody);
            log.debug(rstStr);

            ProcCustAcctVrfyVO procCustAcctVrfyVO = objectMapper.readValue(rstStr, ProcCustAcctVrfyVO.class);
            
            return procCustAcctVrfyVO.getList();
        } catch (Exception e) {
            log.error("Exception :: "+e.toString());
            return null;
        }
    }
    
    public void updateSttTm(List<Long> seqs) throws Exception {
        List<TsCustAcctVrfyVO> list = seqs.stream().map(v->{
            TsCustAcctVrfyVO nv = new TsCustAcctVrfyVO();
            nv.setAcctVrfySeqNo(v);
            nv.setReqStsCd(EReqSts.RUN.toString());
            return nv;
        }).collect(Collectors.toList());

        if (list.size() > 0){
            //API 호출 정보
            String rstStr = "";
            String toUrl = aosBaseUrl + "/api/v1/ais/updateSttTm";
            String toMethod = "POST";
            String toHeader = "{\"access_token\":\"qwerty\"}";
            String toBody = "{}";

            //호출 파라미터 셋팅
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("params", list);

            JSONObject obj = new JSONObject(map);
            toBody = obj.toJSONString();

            //API 호출
            rstStr = commonUtil.callApi(Constant.AIS_TITLE, toUrl, toMethod, toHeader, toBody);
            log.debug(rstStr);
        }
            
    }

    // @Transactional
    public void updateResult(SimpleAccRespTransVO dto) {
        String errCode = dto.getErrCode();

        EReqSts sts = "00000000".equals(errCode) || "42110000".equals(errCode) ? EReqSts.RUN : EReqSts.ERR;

        Long custAcctSeqNo = Long.parseLong(dto.getCustAcctSeqNo());
        log.debug("custAcctSeqNo = {}", custAcctSeqNo);

        Long aisReqSeqNo = Long.parseLong(dto.getAisReqSeqNo());
        log.debug("aisReqSeqNo = {}", aisReqSeqNo);

        if (sts != EReqSts.ERR) {
            ArrayList<Transaction> trans = dto.getTraCont();
            if (trans != null) {
                if (dto.getOrderNo().equals("1"))
                    Collections.sort(trans, new DescendingComparator());

                // 기존 거래내역 레코드를 유지하도록 변경
                List<String> savedList = new ArrayList<String>();
                for (Transaction t : trans) {
                    String traDt = t.getTraDt();
                    String traTimes = t.getTraTimes();
                    String traRemain = t.getTraRemain();
                    String availableStr0 = "";

                    try {
                        //API 호출 정보
                        String toUrl = aosBaseUrl + "/api/v1/ais/acctTraContInfo";
                        String toMethod = "POST";
                        String toHeader = "{\"access_token\":\"qwerty\"}";
                        String toBody = "{}";

                        //호출 파라미터 셋팅
                        HashMap<String, Object> map = new HashMap<String, Object>();
                        map.put("cust_acct_seq_no", custAcctSeqNo);
                        map.put("tra_dt", traDt);
                        map.put("tra_times", traTimes);
                        map.put("tra_remain", traRemain);
                        map.put("req_object_type", "list");

                        JSONObject obj = new JSONObject(map);
                        toBody = obj.toJSONString();

                        //API 호출
                        availableStr0 = commonUtil.callApi(Constant.AIS_TITLE, toUrl, toMethod, toHeader, toBody);
                        log.debug(availableStr0);

                        ProcAcctTraContInfoVO procAcctTraContInfo = objectMapper.readValue(availableStr0, ProcAcctTraContInfoVO.class);
                        if (procAcctTraContInfo.getList().size() > 0) {
                            continue;
                        }
                    } catch (JsonMappingException e1) {
                        e1.printStackTrace();
                    } catch (JsonProcessingException e1) {
                        e1.printStackTrace();
                    }

                    AcctTraContInfoVO acctTraContInfoVO = new AcctTraContInfoVO();
                    acctTraContInfoVO.setAcctKeywdSeqNo(0L); // 임의로 0L값 지정 후 밑에 데이터가 있을 경우 치환

                    String content = toHalfChar(t.getAccTraCont());
                    String availableStr = ""; // 응답값
                    try {
                        //API 호출 정보
                        String toUrl = aosBaseUrl + "/api/v1/ais/acctKeywdInfo";
                        String toMethod = "POST";
                        String toHeader = "{\"access_token\":\"qwerty\"}";
                        String toBody = "{}";

                        //호출 파라미터 셋팅
                        HashMap<String, Object> map = new HashMap<String, Object>();
                        map.put("keywd", content);
                        map.put("req_object_type", "data");

                        JSONObject obj = new JSONObject(map);
                        toBody = obj.toJSONString();

                        //API 호출
                        availableStr = commonUtil.callApi(Constant.AIS_TITLE, toUrl, toMethod, toHeader, toBody);
                        log.debug(availableStr);

                        ProcAcctKeywdInfoDataVO procAcctKeywdInfo = objectMapper.readValue(availableStr, ProcAcctKeywdInfoDataVO.class);
                        
                        if (ObjectUtils.isNotEmpty(procAcctKeywdInfo.getData().getAcctKeywdSeqNo())) {
                            acctTraContInfoVO.setAcctKeywdSeqNo(procAcctKeywdInfo.getData().getAcctKeywdSeqNo());
                        }
                        
                    } catch (JsonMappingException e) {
                        e.printStackTrace();
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    

                    acctTraContInfoVO.setTraDt(traDt);
                    acctTraContInfoVO.setTraTimes(traTimes);
                    acctTraContInfoVO.setCrcCd(t.getCrcCd());
                    acctTraContInfoVO.setWdrPrc(Long.parseLong(t.getWdrPrc()));
                    acctTraContInfoVO.setDepoPrc(Long.parseLong(t.getDepoPrc()));
                    acctTraContInfoVO.setTraRemain(Long.parseLong(traRemain));
                    acctTraContInfoVO.setNote1(t.getNote1());
                    acctTraContInfoVO.setNote2(t.getNote2());
                    acctTraContInfoVO.setTraTyp1(t.getTraTyp1());
                    acctTraContInfoVO.setTraTyp2(t.getTraTyp2());
                    acctTraContInfoVO.setAcctNo(t.getAcctNo());
                    acctTraContInfoVO.setAcctTraCont(content);
                    acctTraContInfoVO.setSumm(t.getSumm());
                    acctTraContInfoVO.setTraBranchNm(t.getTraBranchNm());
                    acctTraContInfoVO.setCustAcctSeqNo(custAcctSeqNo);
                    acctTraContInfoVO.setAisReqSeqNo(aisReqSeqNo);
                    acctTraContInfoVO.setRecoStsCd(ERecoSts.RDY);

                    ObjectMapper mapper = new ObjectMapper();
                    try {
                        String voString = mapper.writeValueAsString(acctTraContInfoVO);
                        savedList.add(voString);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }

                if (savedList.size() > 0) {
                    //API 호출 정보
                    String toUrl = aosBaseUrl + "/api/v1/ais/acctTraContInfoSave";
                    String toMethod = "POST";
                    String toHeader = "{\"access_token\":\"qwerty\"}";
                    String toBody = "{}";
                    String rstStr = "";

                    //호출 파라미터 셋팅
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    ObjectMapper mapper = new ObjectMapper();
                    try {
                        String dtoString = mapper.writeValueAsString(dto);
                        
                        map.put("acc_resp_trans_dto", dtoString);
                        map.put("saved_list", savedList);
                        
                        JSONObject obj = new JSONObject(map);
                        toBody = obj.toJSONString();
                        
                        //API 호출
                        rstStr = commonUtil.callApi(Constant.AIS_TITLE, toUrl, toMethod, toHeader, toBody);
                        log.debug(rstStr);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private static String toHalfChar(String src) {
        StringBuffer sb = new StringBuffer();
        char c = 0;
        for (int i = 0, n = src.length() ; i < n ; i++) {
            c = src.charAt(i);
            if (c >= '！' && c <= '～') c -= 0xfee0;
            else if (c == ' ') c = 0x20;
            sb.append(c);
        }
        return sb.toString();
    }

    public static class DescendingComparator implements Comparator<Transaction> {
        @Override
        public int compare(Transaction o1, Transaction o2) {
            int compared = o2.getTraDt().compareTo(o1.getTraDt());
            return compared == 0 ? o2.getTraTimes().compareTo(o1.getTraTimes()) : compared;
        }
    }

    public void updateEndTm(SimpleAccRespBalVO dto) throws Exception {
        Long seqNo = Long.parseLong(dto.getAisReqSeqNo());
        String errCode = dto.getErrCode();
        EReqSts sts =  "00000000".equals(errCode) ? EReqSts.COM : EReqSts.ERR;

        // TODO - 원본
        // trCustAcctVrfyDAO.updateEndTm(seqNo, sts, errCode);

        //API 호출 정보
        String rstStr = "";
        String toUrl = aosBaseUrl + "/api/v1/ais/updateEndTm";
        String toMethod = "POST";
        String toHeader = "{\"access_token\":\"qwerty\"}";
        String toBody = "{}";

        //호출 파라미터 셋팅
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("seqNo", seqNo);
        map.put("sts", sts.toString());
        map.put("errCode", errCode);

        JSONObject obj = new JSONObject(map);
        toBody = obj.toJSONString();

        //API 호출
        rstStr = commonUtil.callApi(Constant.AIS_TITLE, toUrl, toMethod, toHeader, toBody);
        log.debug(rstStr);
    }
	
}
