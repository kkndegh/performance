package com.only.aos.cms.account.service.vo.procedure;

import com.only.aos.cms.account.service.vo.CustMVO;
import com.only.aos.cms.account.service.vo.common.CommonVO;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown=true)
public class ProcCustMVO extends CommonVO implements Serializable  {
    
    private static final long serialVersionUID = 1L;

    private CustMVO data;
}