/**
 * @author mailsung
 */
package com.only.aos.ais.common.scheduler;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;

import com.hazelcast.core.HazelcastInstance;

@Slf4j
public abstract class AbstractScheduler {
    @Autowired private HazelcastInstance hazelcastInstance;
    
    protected void executeInternal(String name, Runnable runnable) {
        Lock lock = hazelcastInstance.getLock(name);
        boolean locked = false;
        try {
            locked = lock.tryLock(100, TimeUnit.MILLISECONDS);
        } catch(InterruptedException e){}
        if (locked) {
            try {
                runnable.run();
            } catch (Exception e) {
                log.warn("" + e);
            } finally {
                lock.unlock();
            }
        }
    }
}
