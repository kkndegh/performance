/**
 * @author kenny
 */
package com.only.aos.ais.service.camel;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Route extends RouteBuilder {
    @Value("${route.max-pool-size:2}")
    private int maxPoolSize;
    @Value("${route.core-pool-size:2}")
    private int corePoolSize;
    @Value("${route.http4-connect-timeout:1000}")
    private int connectTimeout;
    @Value("${route.http4-socket-timeout:1000}")
    private int socketTimeout;

    @Override
    public void configure() {
        getContext().getExecutorServiceManager().getDefaultThreadPoolProfile().setMaxPoolSize(maxPoolSize);
        onException(Exception.class).process("AisExceptionHandler").handled(true);

        from("direct:to-ais").routeId("to-ais")
        .threads(corePoolSize)
        .setHeader(Exchange.CONTENT_TYPE, constant("application/json; charset=UTF-8"))
        .choice()
            .when(header("X-AIS-REQ").isEqualTo("SimpleAccReqTrans"))
                .process("SimpleAccReqTransProcessor")
                .toD("http4://${header.X-AIS-HOST}:${header.X-AIS-PORT}?httpMethod=POST&connectTimeout=" + connectTimeout + "&socketTimeout=" + socketTimeout)
                // .when(header("X-AIS-REQ").isEqualTo("SimpleAccReqBal"))
                // .process("SimpleAccReqBalProcessor")
                // .toD("http4://${header.X-AIS-HOST}:${header.X-AIS-PORT}?httpMethod=POST&connectTimeout=" + connectTimeout + "&socketTimeout=" + socketTimeout)
            .otherwise()
                .log(LoggingLevel.WARN, "Unknown Request : ${headers}")
        .end()
        ;
      
        // from("direct:from-ais")//.routeId("from-ais")
        // .threads(corePoolSize)
        // .choice()
        //     .when(header("X-AIS-REQ").isEqualTo("SimpleAccReqTrans"))
        //         .process("SimpleAccRespTransProcessor")
        //     .when(header("X-AIS-REQ").isEqualTo("SimpleAccReqBal"))
        //         .process("SimpleAccRespBalProcessor")
        //     .otherwise()
        //         .log(LoggingLevel.WARN, "Unknown Response : ${headers}")
        // .end()
        // ;
        
        // from("direct:to-persistence")//.routeId("to-persistence")
        // .threads(corePoolSize)
        // .process("PersistenceProcessor")
        // .choice()
        // .when(header("X-AIS-GSM-URL").isNotNull())
        //     .toD("${header.X-AIS-GSM-URL}?httpMethod=POST&connectTimeout=" + connectTimeout + "&socketTimeout=" + socketTimeout)
        // .end()
        // ;

        // from("direct:to-account-verify").process("AcctVerifyProcessor");
    }
}
