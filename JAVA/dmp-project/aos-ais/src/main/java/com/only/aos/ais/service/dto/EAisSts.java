/**
 * @author kenneth
 * 2019.11.01
 */
package com.only.aos.ais.service.dto;

public enum EAisSts {
    STP, // System stopped
    ANA, // Agent is not available
    ANF, // Agent is not funtional
    RUN, 
}