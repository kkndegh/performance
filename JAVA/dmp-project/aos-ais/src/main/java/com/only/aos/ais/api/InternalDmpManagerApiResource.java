package com.only.aos.ais.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//import com.only.aos.ntf.service.AosNtfService;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * AOS NTF API
 */
@RestController
@RequestMapping("/api/v1")
public class InternalDmpManagerApiResource {

    // @Autowired
    // private AosNtfService aosNtfService;

    /**
     * aos ntf post 접근
     * 
     * @param group String
     * @param code String
     * @param requestBody RequestBody Map<String, Object>
     * @param requestHeader RequestHeader Map<String, String>
     * @param request HttpServletRequest
     * @return ResponseEntity<HashMap<String, Object>>
     */
    @GetMapping(value = "/{group}/{code}")
    public ResponseEntity<String> postAosAis(@PathVariable("group") String group, 
                                            @PathVariable("code") String code, 
                                            @RequestHeader Map<String, String> requestHeader,
                                            HttpServletRequest request) {
        //String rs = aosNtfService.ntfApi(group, code, requestBody, request);
        System.out.println("@@@@@@@@@@@@@@");
        String rs = "";
        return new ResponseEntity<String>(rs, HttpStatus.OK);
    }
}
