package com.only.aos.ais.common.util;

import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;

/**
 * 공통 유틸
 * 
 * @since 2022.05.31
 * @author kh
 */
@Slf4j
public class CommonUtil {
    
    /**
     * 에러발생 처리
     * 
     * @param resultCode String
     * @param resultMsg String
     * @return String
     */
    public static String resultObjMk(String clazz, String method, String resultCode, String resultMsg){
        log.error("에러발생["+clazz+"-"+method+"] :: "+ resultMsg);
        HashMap<String, String> errObj = new HashMap<String, String>();

        errObj.put("result_code", resultCode);
        errObj.put("result_msg", resultMsg);
        
        return errObj.toString();
    }

    /**
    * 실행중인 클래스 이름을 취득
    *
    * @return String
    */
    public static String getClassName() {
        return Thread.currentThread().getStackTrace()[2].getClassName();
    }

    /**
    * 실행중인 함수를 취득。
    *
    * @return String
    */
    public static String getMethodName() {
        return Thread.currentThread().getStackTrace()[2].getMethodName();
    }
}