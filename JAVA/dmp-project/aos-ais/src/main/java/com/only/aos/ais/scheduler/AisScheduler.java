/** mailsung */
package com.only.aos.ais.scheduler;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.hazelcast.core.IMap;
import com.only.aos.ais.common.scheduler.AbstractScheduler;
import com.only.aos.ais.service.dto.SystemStatusDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
// import org.springframework.data.domain.Sort;


import lombok.extern.slf4j.Slf4j;

// import org.springframework.data.domain.PageRequest;
// import org.springframework.data.domain.Pageable;
// import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;

import org.apache.camel.ProducerTemplate;

@Slf4j
@Component
public class AisScheduler extends AbstractScheduler {

    //@Autowired private AisStatusService statusService;
    @Autowired private ProducerTemplate producer;
    //@Autowired private AisQueryService queryService;
    @Value("${ais.port:9080}") private int aisPort;
    @Value("${scheduler.aisQuery.run:false}") private boolean runScheduler;
    @Value("${scheduler.aisQuery.fetch:20}") private int maxFetch;
    @Value("${scheduler.aisQuery.query:2}") private int maxQuery;
    // @Resource(name="ais-query-filter") private IMap<Long, Boolean> aisQueryFilter;

    @Scheduled(cron = "${scheduler.run.email.send.cron}")
    public void schedulingForSendRequestToAis() {
        // if (!runScheduler) 
        //     return;
  		String lockName = getClass().getSimpleName();
		executeInternal(lockName, this::executeSendRequestToAis);
    }

	public void executeSendRequestToAis() {
        log.info("scheduled::ExecuteSendRequestToAis.");
        // Pageable pageable = PageRequest.of(0, maxFetch, Sort.by("aisReqSeqNo"));
        // List<ReTrAisReqL> list = queryService.getAisReqList(EReqSts.RDY, pageable)
        //                         .stream()
        //                         .filter(x-> {
        //                             Long key = x.getCustAcctSeqNo();
        //                             if (aisQueryFilter.get(key) == null) {
        //                                 aisQueryFilter.put(key, Boolean.TRUE);
        //                                 return true;
        //                             }
        //                             return false;
        //                         })
        //                         .collect(Collectors.toList());

        // if (list.size() < 1) {
        //     return;
        // }

        // SystemStatusDTO sys = statusService.getAvailableAis();
        // if (sys == null) {
        //     log.warn("We have not the available AISes.");
        //     return;
        // }

        // List<Long> seqs = new ArrayList<Long>();
        // LinkedHashSet<Long> lhs = new LinkedHashSet<Long>();
        // List<ReTrAisReqL> part = new ArrayList<ReTrAisReqL>();

        // for (int i = 0, n = list.size() ; i < n ; ) {
        //     ReTrAisReqL req = list.get(i);

        //     part.add(req);
        //     seqs.add(req.getAisReqSeqNo());
        //     lhs.add(req.getCustAcctSeqNo());
        //     i++;

        //     if (i % maxQuery == 0)
        //         sendQuery(sys, part, seqs, lhs);
        // }

        // if (part.size() > 0)
        //     sendQuery(sys, part, seqs, lhs);

        SystemStatusDTO sys = new SystemStatusDTO();
        sys.setHostAddr("192.168.111.17"); //TODO 호스트 정보 입력

        List<Long> seqs = new ArrayList<Long>();
        seqs.add((long) 0); //TODO seqs 정보 입력

        LinkedHashSet<Long> lhs = new LinkedHashSet<Long>();
        lhs.add((long) 0); //TODO lhs 정보 입력

        List<Map<String, String>> part = new ArrayList<Map<String, String>>();  //TODO part 정보 입력
        Map<String, String> map = new HashMap<String, String>();
        map.put("aisReqSeqNo", "1");
        map.put("custAcctSeqNo", "1");
        map.put("inqSttDt", "1");
        map.put("inqEndDt", "1");
        map.put("runSttTm", "1");
        map.put("runEndTm", "1");
        map.put("reqStsCd", "1");
        map.put("reqRltCd", "1");
        map.put("traContTrsStsCd", "1");
        map.put("priOrdCd", "1");
        map.put("reqTypCd", "1");
        map.put("subsNotiUrl", "1");
        
        sendQuery(sys, part, seqs, lhs);
    }
    
    private void sendQuery(SystemStatusDTO sys, List<Map<String, String>> part, List<Long> seqs, LinkedHashSet<Long> lhs) {
        // queryService.updateStatusSttTm(seqs, EReqSts.RUN, sys.getAisSeqNo());
        // queryService.updateCustAccts(new ArrayList<Long>(lhs), ECustAcctSts.INQ, null);

        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("X-AIS-REQ", "SimpleAccReqTrans");
        headers.put("X-AIS-REQ-SEQS", new ArrayList<Long>(seqs));
        headers.put("X-AIS-REQ-CUST-ACCTS", new ArrayList<Long>(lhs));
        headers.put("X-AIS-HOST", sys.getHostAddr());
        headers.put("X-AIS-PORT", aisPort);
        producer.asyncRequestBodyAndHeaders("direct:to-ais", new ArrayList<Map<String, String>>(part), headers);

        part.clear();
        seqs.clear();
        lhs.clear();
    }

}