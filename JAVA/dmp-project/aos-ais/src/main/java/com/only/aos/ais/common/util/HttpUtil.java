package com.only.aos.ais.common.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * HTTP 유틸
 * 
 * @since 2022.05.25
 * @author kh
 */
public class HttpUtil {

    /**
     * HTTP프로토콜 통신 메서드
     * 
     * @param methodType String
     * @param tUrl String
     * @param header String
     * @param body String
     * @return String
     */
    public static String callApiHttp(String methodType, String tUrl, String header, String body){
        
        HttpURLConnection conn = null;
        String rspStr="";
        
        try {
            //URL 설정
            URL url = new URL(tUrl);
 
            conn = (HttpURLConnection) url.openConnection();

            // jsonString -> object
            JSONParser jsonParser = new JSONParser();
            JSONObject headerObj = (JSONObject) jsonParser.parse(header);
            for (Object key : headerObj.keySet()) {
                String keyStr = (String)key;
                String keyvalue = headerObj.get(keyStr).toString();
                conn.setRequestProperty(keyStr, keyvalue);
            }
            
            // methodType의 경우 POST, GET, PUT, DELETE 가능
            conn.setRequestMethod(methodType);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Transfer-Encoding", "chunked");
            conn.setRequestProperty("Connection", "keep-alive");
            conn.setDoOutput(true);
            
            if (!methodType.equals("GET")) {
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "UTF-8"));
                bw.write(body);
                bw.flush();
                bw.close();
            }
            
            // 보내고 결과값 받기
            int responseCode = conn.getResponseCode();
            if (responseCode == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                rspStr = sb.toString();
            } 
        } catch (Exception e) {
            return CommonUtil.resultObjMk(CommonUtil.getClassName(),CommonUtil.getMethodName(),"P999", e.toString());
        }
        return rspStr;
    }

    /**
     * HTTPS 프로토콜 통신 메서드
     * 
     * @param methodType String
     * @param tUrl String
     * @param header String
     * @param body String
     * @return String
     */
    public static String callApiHttps(String methodType, String tUrl, String header, String body){
        
        HttpsURLConnection conn = null;
        String rspStr="";
        
        try {
            //URL 설정
            URL url = new URL(tUrl);
 
            conn = (HttpsURLConnection) url.openConnection();

            // jsonString -> object
            JSONParser jsonParser = new JSONParser();
            JSONObject headerObj = (JSONObject) jsonParser.parse(header);
            for (Object key : headerObj.keySet()) {
                String keyStr = (String)key;
                String keyvalue = headerObj.get(keyStr).toString();
                conn.setRequestProperty(keyStr, keyvalue);
            }
            
            // type의 경우 POST, GET, PUT, DELETE 가능
            conn.setRequestMethod(methodType);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Transfer-Encoding", "chunked");
            conn.setRequestProperty("Connection", "keep-alive");
            conn.setDoOutput(true);
            
            if (!methodType.equals("GET")) {
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "UTF-8"));
                bw.write(body);
                bw.flush();
                bw.close();
            }
            
            // 보내고 결과값 받기
            int responseCode = conn.getResponseCode();
            if (responseCode == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                rspStr = sb.toString();
            } 
        } catch (Exception e) {
            return CommonUtil.resultObjMk(CommonUtil.getClassName(),CommonUtil.getMethodName(),"P999", e.toString());
        }
        return rspStr;
    }
}