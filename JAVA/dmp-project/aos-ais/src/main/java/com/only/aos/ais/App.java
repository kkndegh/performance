/** kenny */
package com.only.aos.ais;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.Banner;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@EnableScheduling
@SpringBootApplication
public class App {
	public static void main(String[] args) {

		SpringApplication app = new SpringApplication(App.class);
		app.setBannerMode(Banner.Mode.OFF);

		Environment env = app.run(args).getEnvironment();
		String name = env.getProperty("spring.application.name");
		String host = env.getProperty("server.address");
		String port = env.getProperty("server.port");

		log.info("\n'{}' is running at {}:{} with profile {}", name, host, port, env.getActiveProfiles());

		Runtime.getRuntime().addShutdownHook(new Thread(name + "-shutdown-hook") {
            public void run() {
				log.info("=== {} got shutdown! ===", name);
            }
        });
	}
}
 