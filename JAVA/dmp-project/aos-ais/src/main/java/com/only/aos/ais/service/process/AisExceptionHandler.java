/**
 * @author kenny
  */
package com.only.aos.ais.service.process;

import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
// import onlyone.smp.persistent.domain.enums.ECustAcctSts;
// import onlyone.smp.persistent.domain.enums.EReqSts;
// import onlyone.smp.ais.service.AisQueryService;
// import static onlyone.smp.ais.config.Constants.SYSTEM_STATUS_ERROR;

@Slf4j
@Component("AisExceptionHandler")
public class AisExceptionHandler implements Processor {
   // @Autowired private AisQueryService service;

    @Override
    public void process(Exchange ex) throws Exception {
        String routeId = ex.getUnitOfWork().getRouteContext().getRoute().getId(); //log.debug("=> routeId : " + routeId);
        Exception e = ex.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class); log.warn("Exception : " + e.getMessage());
        
        switch (routeId) {
            case "to-ais": 
            //case "to-ais-json-body":
            {
                // @SuppressWarnings("unchecked")
                // List<Long> seqs = (List<Long>)ex.getIn().getHeader("X-AIS-REQ-SEQS", List.class);
                // if (seqs != null && seqs.size() > 0)
                //     service.updateStatusEndTm(seqs, EReqSts.ERR, SYSTEM_STATUS_ERROR);

                // @SuppressWarnings("unchecked")
                // List<Long> custAccts = (List<Long>)ex.getIn().getHeader("X-AIS-REQ-CUST-ACCTS", List.class); 
                // if (custAccts != null && custAccts.size() > 0)
                //     service.updateCustAccts(custAccts, ECustAcctSts.ERR, SYSTEM_STATUS_ERROR);
            }
            break;
            case "from-ais": {
            }
            break;
            case "to-persistence": {
            }
            break;
            default: break;
        }
    }
}