/**
 * @author kenny
  */
package com.only.aos.ais.service.process;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.camel.CamelException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;
// import onlyone.smp.persistent.domain.related.ReTrAisReqL;
// import onlyone.smp.security.CryptHelper;
// import onlyone.smp.service.dto.SimpleAccReqDTO;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Slf4j
@Component("SimpleAccReqTransProcessor")
public class SimpleAccReqTransProcessor implements Processor {
    @Autowired private ObjectMapper mapper;
    @Value("${ais.threads:2}") private int nThreads;

    @Override
    public void process(Exchange ex) throws Exception {
        // log.debug("==> I'm SimpleAccTransReqProcessor");
        // @SuppressWarnings("unchecked")
        // List<ReTrAisReqL> reqs = (List<ReTrAisReqL>)ex.getIn().getBody(List.class);
        // if (reqs == null) throw new CamelException("Routing pipe is broken");

        // ArrayList<ArrayList<SimpleAccReqDTO>> outer = new ArrayList<ArrayList<SimpleAccReqDTO>>();
        // ArrayList<SimpleAccReqDTO> inner = new ArrayList<SimpleAccReqDTO>();

        // int w = (int)Math.ceil((double)reqs.size() / (double)nThreads);  //log.debug("-----> w = " + w);
        // Iterator<ReTrAisReqL> it = reqs.iterator();

        // while (it.hasNext()) {
        //     SimpleAccReqDTO dto = getDtoFromReq(it.next());
        //     if (dto != null)
        //         inner.add(dto);

        //     if (inner.size() >= w) {
        //         // log.debug("new inner!");
        //         outer.add(inner);
        //         inner = new ArrayList<SimpleAccReqDTO>();
        //     }
        // }

        // if (inner.size() > 0) outer.add(inner);
        // if (outer.size() < 1) throw new Exception("Request is empty!");

        String jsonString = mapper.writeValueAsString("{\"test\" : \"1\"}"); //log.debug("**^^** jsonString = \n" + jsonString);
        ex.getIn().setBody(jsonString);
    }

}
