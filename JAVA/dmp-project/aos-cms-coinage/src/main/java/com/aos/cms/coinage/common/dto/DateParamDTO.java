package com.aos.cms.coinage.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
/**
 * 결제일자
 * 
 * @since 2022.09.26
 * @author kh
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class DateParamDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String trxDt;
    private String trxTm;
}
