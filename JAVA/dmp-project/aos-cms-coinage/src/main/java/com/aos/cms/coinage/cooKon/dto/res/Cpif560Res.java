package com.aos.cms.coinage.cooKon.dto.res;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 회원가입
 * 
 * @since 2022.09.26
 * @author kh
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Cpif560Res implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @JsonProperty("biz_no")
    private String bizNo;

    @JsonProperty("cp_memb_no")
    private String cpMembNo;
}