package com.aos.cms.coinage.hyoSung.socket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;

import com.sf.xc3.XC_v3;

import org.springframework.beans.factory.annotation.Value;

import lombok.extern.slf4j.Slf4j;

/**
 * 효성 BaseAction 구현체
 * 
 * @author kh
 * @since 2022.10.20
 */
@Slf4j
public class XC_JavaSocket implements BaseAction
{
    // 전송 메시지 타입 정의 : 샘플용
    // public static final int MSGTYPE_SESSION_KEY    = 3;    // 세션키 공유 메시지
    // public static final int MSGTYPE_CIPHER         = 4;    // 암호문 메시지
    public static final int MSGTYPE_SESSION_KEY    = 0x03;    // 세션키 공유 메시지
    public static final int MSGTYPE_CIPHER         = 0x04;    // 암호문 메시지

    public final static int TIMEOUT            = 60*60*1000; //수신 대기시간 

    @Value("${hyosung.con.conf}")
    private static String CONF;
    
    Socket cs = null;
    DataOutputStream outBound;
    DataInputStream inBound;
    
    XC_v3 xc = new XC_v3();
    
    public int close()
    {
        int ret=-1;
        try{
            inBound.close();
            outBound.close();
            cs.close();
            xc.close();
            ret=1;
        }catch (Exception e){}
        
        return ret;
    }

    public int connect(String ip, int port, String conf) {
        int ret =-999;
        try{
            cs = new Socket(ip, port);
                
            outBound = new DataOutputStream(cs.getOutputStream());
            inBound = new DataInputStream(cs.getInputStream());
            cs.setSoTimeout(TIMEOUT);
            
            ret = xc.init("", conf, 1);

            if ( ret < 0 ){
                log.debug("init returned [" + ret + "]");
                return ret;
            }
            
            //key Init
            byte [] output    = null;
            int output_len=-1;
            output = xc.keyinitBin();
            output_len = output.length;
            
            byte[] sendData = new byte[output_len + 3];

            byte[] data = null;
            try {
                cs.setSoTimeout(TIMEOUT);
                /**
                 * Parse Message Header
                 */
                
                // message type
                sendData[0] = MSGTYPE_SESSION_KEY;
                
                // message length
                sendData[1] = (byte)((byte)(output.length >> 8) & 0x000000FF);
                sendData[2] = (byte)((byte)output.length & 0x000000FF);
                            
                // generate message
                System.arraycopy(output, 0, sendData, 3, output.length);
                // send header & message
                outBound.write(sendData);
                
                int nData = 0;
                
                try {
                    //cs.setSoTimeout(TIMEOUT);
                    /**
                     *  Read Message Header
                     */
                    // Read Message Type
                    inBound.readByte();
                    
                    // Read Message Length
                    nData = (inBound.readUnsignedByte() << 8);
                    nData += inBound.readUnsignedByte();
                    
                    /** 
                     * Read Message
                     */
                    // Allocate Message Data Buffer
                    if (nData < 0) {
                        log.debug("FATAL : readFully read invalid data");
                        throw new Exception();
                    }
                    
                    data = new byte[nData];
                    // Read Message Data
                    inBound.readFully(data);

                    // XCDebug.printHex(data);
                }catch (EOFException e) {
                    throw new EOFException();
                }catch (IOException e) {
                    log.debug("Read Failed : " + e.getMessage());
                    throw new IOException();
                }
        
                //key Final
                ret = xc.keyFinal(data);
                if(ret<0){
                    log.debug("key final return : " + ret);
                    log.debug("");
                    log.debug("Key Final Fail :");
                    log.debug("");
                    return ret;
                }
            }catch (Exception e) {
                e.printStackTrace();
                log.debug("Exception occurred : (" + e.getMessage() + ")");
                xc.error(null);
            }
        }catch(Exception e){
            e.printStackTrace();
            return ret;
        }
        log.debug(">>>서버 접속 성공 \n접속 서버 정보 : "+ ip + ":" + port );
        log.debug("ret:" + ret + "\n");
        return ret;
    }
    
    public byte[] recvData() {
        byte[] data = null;
        int nData = 0;
        
        try {
            cs.setSoTimeout(TIMEOUT);
            /**
             *  Read Message Header
             */
            // Read Message Type
            inBound.readByte();
            
            // Read Message Length
            nData = (inBound.readUnsignedByte() << 8);
            nData += inBound.readUnsignedByte();
            /** 
             * Read Message
             */
            // Allocate Message Data Buffer
            if (nData < 0) {
                log.debug("FATAL : readFully read invalid data");
                throw new Exception();
            }
            log.debug("수신대상 암호문 크기 : " + nData);
            data = new byte[nData];
            // Read Message Data
            inBound.readFully(data);
            // XCDebug.printHex(data);
            data = xc.decode(data);
        }catch (Exception e) {
            log.debug("타임아웃!");
            log.debug("Read Failed : " + e.getMessage());
        }
        return data;
    }

    public boolean sendData(String data) {
        byte [] b_data = data.getBytes();
        try {
            b_data = xc.encode(b_data);
            int len1 = 0;
            int len2 = 0;
            byte[] sendData = new byte[b_data.length + 3];
       
            //cs.setSoTimeout(TIMEOUT);
            /**
             * Parse Message Header
             */
            
            // message type
            sendData[0] = MSGTYPE_CIPHER;
            
            // message length
            len1 = (b_data.length >> 8) & 0x000000FF;
            len2 = b_data.length & 0x000000FF;
            
            sendData[1] = (byte)len1;
            sendData[2] = (byte)len2;
                        
            // generate message
            System.arraycopy(b_data, 0, sendData, 3, b_data.length);
            // send header & message
            outBound.write(sendData);
            
            //XCDebug.printHex(sendData);
        }catch (Exception e) {
            log.debug("Write failed : " + e.getMessage());
            return false;
        }
        return true;
    }
}
