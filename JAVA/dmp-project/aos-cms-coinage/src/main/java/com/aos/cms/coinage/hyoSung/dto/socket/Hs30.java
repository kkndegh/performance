package com.aos.cms.coinage.hyoSung.dto.socket;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 30. 회원증빙 시작전문
 * 
 * @since 2022.10.17
 * @author kh
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Hs30 implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Hs30Dp1;
    private String Hs30Dp2;
    private String Hs30Dp3;
    private String Hs30Dp4;
    private String Hs30Dp5;
    private String Hs30Dp6;
    private String Hs30Dp7;
    private String Hs30Dp8;
    private String Hs30Dp9;
    private String Hs30Dp10;
    private String Hs30Dp11;
    private String Hs30Dp12;
    private String Hs30Dp13;
    private String Hs30Dp14;
    private String Hs30Dp15;
    private String Hs30Dp16;
    private String Hs30Dp17;
    private String Hs30Dp18;
    private String Hs30Dp19;
    private String Hs30Dp20;

}
