package com.aos.cms.coinage.cooKon.dto.res;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 계좌삭제
 * 
 * @since 2022.09.26
 * @author kh
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Cpif826Res implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @JsonProperty("cp_memb_no")
    private String cpMembNo;

    @JsonProperty("biz_no")
    private String bizNo;

    @JsonProperty("fnni_cd")
    private String fnniCd;

    @JsonProperty("cp_sys_acno")
    private String cpSysAcno;

    @JsonProperty("acct_info")
    private String acctInfo;

}