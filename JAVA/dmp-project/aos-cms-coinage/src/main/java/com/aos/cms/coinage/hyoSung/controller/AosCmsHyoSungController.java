package com.aos.cms.coinage.hyoSung.controller;

import com.aos.cms.coinage.hyoSung.dto.HsDTO;
import com.aos.cms.coinage.hyoSung.service.AosCmsHyoSungService;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

/**
 * AosCmsCoinage 컨트롤러
 * 
 * @author kh
 * @since 2022.09.13
 */
@RestController
@RequestMapping("/api/v1")
@EnableAsync
@RequiredArgsConstructor
public class AosCmsHyoSungController {

    private final AosCmsHyoSungService aosCmsHyoSungService;

    /**
     * 효성 집금 프로세스 API
     * 
     * @param hsReq HsDTO
     * @return ResponseEntity<String>
     * 
     * @TEST
     * http://localhost:9290/api/v1/cms/hyosung
     * 
     * //전체 플로워
     * {"user_id" : "sdsitess", "user_pw" : "testtest", "user_sub_id" : "swswtest", "user_sub_pw" : "swswswsw", "ap_id":"30002",
        "member_no" : "HGSM20221101953440"
        , "evidence_file_url" : "https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fk.kakaocdn.net%2Fdn%2FcHyZGc%2FbtqGdsax4PA%2F28U11KsfCaTKQ4OP3twksK%2Fimg.jpg"
        , "bank" : "39", "account_no" : "42950311562318", "req_no" : "870528",  "account_owner" : "홍길동"
        , "req_amount" : "200000"
        }
     *
     * //단일
     * {"step_no" : "25",
        "user_id" : "sdsitess", "user_pw" : "testtest", "user_sub_id" : "swswtest", "user_sub_pw" : "swswswsw", "ap_id":"30001",
        "member_no" : "HGSM20221101953443"
        , "bank" : "39", "account_no" : "42950311562519", "req_no" : "870530",  "account_owner" : "홍길동"
        }
     */
    @PostMapping(value = "/cms/hyosung")
    public ResponseEntity<String> procPaymentHyosung(@Valid @RequestBody HsDTO hsReq) {

        String rs = aosCmsHyoSungService.hyoSungCmsPaymentDoc(hsReq);

        return new ResponseEntity<String>(rs, HttpStatus.OK);
    }
}
