package com.aos.cms.coinage.cooKon.dto.res;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 계좌번호 검증
 * 
 * @since 2022.09.26
 * @author kh
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Cpif821Res implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @JsonProperty("cp_memb_no")
    private String cpMembNo;

    @JsonProperty("biz_no")
    private String bizNo;

    @JsonProperty("fnni_cd")
    private String fnniCd;

    @JsonProperty("acct_no")
    private String acctNo;

    @JsonProperty("verify_type")
    private String verifyType;

    @JsonProperty("acct_nm")
    private String acctNm;

    @JsonProperty("verify_tr_dt")
    private String verifyTrDt;

    @JsonProperty("verify_tr_no")
    private String verifyTrNo;

    @JsonProperty("verify_txt")
    private String verifyTxt;
}