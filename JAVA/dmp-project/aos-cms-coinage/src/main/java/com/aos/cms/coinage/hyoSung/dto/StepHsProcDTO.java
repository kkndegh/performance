package com.aos.cms.coinage.hyoSung.dto;

import java.io.Serializable;

import com.aos.cms.coinage.hyoSung.dto.res.HsCommonRes;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Step 간 통신 파라미터
 * 
 * @since 2022.10.13
 * @author kh
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class StepHsProcDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private HsDTO params;
    private SocketDTO socket;
    private HsCommonRes resultRes;
}