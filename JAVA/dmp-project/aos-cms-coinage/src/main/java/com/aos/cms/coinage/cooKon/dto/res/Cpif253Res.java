package com.aos.cms.coinage.cooKon.dto.res;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 거래내역조회
 * 
 * @since 2022.09.26
 * @author kh
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Cpif253Res implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @JsonProperty("cp_memb_no")
    private String cpMembNo;

    @JsonProperty("biz_no")
    private String bizNo;

    @JsonProperty("tr_kind")
    private String trKind;

    @JsonProperty("tr_from_dt")
    private String trFromDt;

    @JsonProperty("tr_to_dt")
    private String trToDt;

    @JsonProperty("page_no")
    private String pageNo;

    @JsonProperty("row_cnt")
    private String rowCnt;

    @JsonProperty("tr_tot_cnt")
    private String trTotCnt;

    @JsonProperty("tr_list_cnt")
    private String trListCnt;

    @JsonProperty("tr_list_info")
    private List<Cpif253ArrayRes> trListInfo;

    @Getter
    @Setter
    @ToString
    public static class Cpif253ArrayRes implements Serializable {

        private static final long serialVersionUID = 1L;
        
        @JsonProperty("tr_dtime")
        private String trDtime;
    
        @JsonProperty("tr_dt")
        private String trDt;
    
        @JsonProperty("tr_no")
        private String trNo;
    
        @JsonProperty("tr_type_nm")
        private String trTypeNm;
    
        @JsonProperty("tr_amt")
        private String trAmt;
    
        @JsonProperty("tr_fee")
        private String trFee;
    
        @JsonProperty("tr_fee_vat")
        private String trFeeVat;
    
        @JsonProperty("tr_fee_tot")
        private String trFeeTot;
    
        @JsonProperty("tr_stat_nm")
        private String trStatNm;
    
        @JsonProperty("tr_read_yn")
        private String trReadYn;
    
        @JsonProperty("fnni_cd")
        private String fnniCd;
    
        @JsonProperty("acct_info")
        private String acctInfo;
    
        @JsonProperty("tr_remark")
        private String trRemark;
    }
}