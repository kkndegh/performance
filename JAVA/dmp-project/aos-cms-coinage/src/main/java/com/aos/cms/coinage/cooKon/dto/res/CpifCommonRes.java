package com.aos.cms.coinage.cooKon.dto.res;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.Getter;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * API 공통
 * 
 * @since 2022.09.26
 * @author kh
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class CpifCommonRes implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @JsonProperty("VV")
    private String  vv;

    @JsonProperty("EV")
    private String ev;

    @JsonProperty("RC")
    private String rc;
    
    @JsonProperty("COMMON_HEAD")
    private CpifCommonHead commonHead;
    
    @JsonProperty("RS_DTIME")
    private String rsDtime;

    @JsonProperty("TNO")
    private String tno;
    
    @JsonProperty("VM")
    private String vm;
    
    @JsonProperty("EM")
    private String em;
    
    @JsonProperty("ID")
    private String id;
    
    @JsonProperty("RM")
    private String rm;

    @Getter
    @Setter
    @ToString
    public static class CpifCommonHead implements Serializable {
        private static final long serialVersionUID = 1L;

        @JsonProperty("MESSAGE")
        private String message;

        @JsonProperty("CODE")
        private String code;

        @JsonProperty("ERROR")
        private String error;
    }
}