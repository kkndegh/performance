package com.aos.cms.coinage;

import java.nio.charset.Charset;

public class Constant {
    public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

    //HttpParam 셋팅시 Title 목록
    public static final String CMS_LOG_TITLE = "[CMS REQ 로그 Params]";
} 