package com.aos.cms.coinage.hyoSung.util.convert;

import java.nio.charset.Charset;

import com.aos.cms.coinage.hyoSung.dto.socket.Hs15;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs20;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs30;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs35_E;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs40;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs45;

import org.springframework.stereotype.Component;

/**
 * 응답 전문 생성
 * 
 * @since 2022.10.21
 * @author kh
 */
@Component
public class ResDataConvert {

    //실시간 회원조회 개별부
    public Hs15 Hs15Res(String data, Charset charset) {
        Hs15 hs15Res = new Hs15();
        byte[] byteArray = data.getBytes(charset);

        // 공통정보부
        hs15Res.setHsHeaderDp1(getData(byteArray        , 0, 10, charset));     // 업체ID
        hs15Res.setHsHeaderDp2(getData(byteArray        , 10, 10, charset));    // 업체PW
        hs15Res.setHsHeaderDp3(getData(byteArray        , 20, 10, charset));    // SW-ID
        hs15Res.setHsHeaderDp4(getData(byteArray        , 30, 10, charset));    // SW-PW
        hs15Res.setHsHeaderDp5(getData(byteArray        , 40, 6, charset));     // 송신일자
        hs15Res.setHsHeaderDp6(getData(byteArray        , 46, 4, charset));     // 전문구분
        hs15Res.setHsHeaderDp7(getData(byteArray        , 50, 3, charset));     // 업무구분
        hs15Res.setHsHeaderDp8(getData(byteArray        , 53, 6, charset));     // 연번
        hs15Res.setHsHeaderDp9(getData(byteArray        , 59, 6, charset));     // 전송일자
        hs15Res.setHsHeaderDp10(getData(byteArray       , 65, 6, charset));     // 전송시간
        hs15Res.setHsHeaderDp11(getData(byteArray       , 71, 1, charset));     // 응답결과
        hs15Res.setHsHeaderDp12(getData(byteArray       , 72, 4, charset));     // 응답코드
        hs15Res.setHsHeaderDp13(getData(byteArray       , 76, 24, charset));    // FMS사용영역 

        // 요청정보부
        hs15Res.setHs15Dp1(getData(byteArray    , 100, 2, charset));    // 거래계좌은행코드
        hs15Res.setHs15Dp2(getData(byteArray    , 102, 15, charset));   // 거래계좌번호
        hs15Res.setHs15Dp3(getData(byteArray    , 117, 13, charset));   // 거래계좌 생년월일/사업자번호
        hs15Res.setHs15Dp4(getData(byteArray    , 130, 20, charset));   // 계좌예금주명
        hs15Res.setHs15Dp5(getData(byteArray    , 150, 20, charset));   // 회원번호
        hs15Res.setHs15Dp6(getData(byteArray    , 170, 1, charset));    // 처리내역
        hs15Res.setHs15Dp7(getData(byteArray    , 171, 1, charset));    // 처리구분
        hs15Res.setHs15Dp8(getData(byteArray    , 172, 20, charset));   // 전자동의Key
        hs15Res.setHs15Dp9(getData(byteArray    , 192, 13, charset));   // 납부자 연락처 
        hs15Res.setHs15Dp10(getData(byteArray   , 205, 8, charset));    // FMS번호
        hs15Res.setHs15Dp11(getData(byteArray   , 213, 87, charset));   // 미사용
        hs15Res.setHs15Dp12(getData(byteArray   , 300, 20, charset));   // 사용자영역

        return hs15Res;
    }

    //실시간 회원 등록/삭제 개별부
    public Hs20 Hs20Res(String data, Charset charset) {
        Hs20 hs20Res = new Hs20();
        byte[] byteArray = data.getBytes(charset);

        // 공통정보부
        hs20Res.setHsHeaderDp1(getData(byteArray        , 0, 10, charset));     // 업체ID
        hs20Res.setHsHeaderDp2(getData(byteArray        , 10, 10, charset));    // 업체PW
        hs20Res.setHsHeaderDp3(getData(byteArray        , 20, 10, charset));    // SW-ID
        hs20Res.setHsHeaderDp4(getData(byteArray        , 30, 10, charset));    // SW-PW
        hs20Res.setHsHeaderDp5(getData(byteArray        , 40, 6, charset));     // 송신일자
        hs20Res.setHsHeaderDp6(getData(byteArray        , 46, 4, charset));     // 전문구분
        hs20Res.setHsHeaderDp7(getData(byteArray        , 50, 3, charset));     // 업무구분
        hs20Res.setHsHeaderDp8(getData(byteArray        , 53, 6, charset));     // 연번
        hs20Res.setHsHeaderDp9(getData(byteArray        , 59, 6, charset));     // 전송일자
        hs20Res.setHsHeaderDp10(getData(byteArray       , 65, 6, charset));     // 전송시간
        hs20Res.setHsHeaderDp11(getData(byteArray       , 71, 1, charset));     // 응답결과
        hs20Res.setHsHeaderDp12(getData(byteArray       , 72, 4, charset));     // 응답코드
        hs20Res.setHsHeaderDp13(getData(byteArray       , 76, 24, charset));    // FMS사용영역 

        // 요청정보부
        hs20Res.setHs20Dp1(getData(byteArray    , 100, 2, charset));    // 거래계좌은행코드
        hs20Res.setHs20Dp2(getData(byteArray    , 102, 15, charset));   // 거래계좌번호
        hs20Res.setHs20Dp3(getData(byteArray    , 117, 13, charset));   // 거래계좌 생년월일/사업자번호
        hs20Res.setHs20Dp4(getData(byteArray    , 130, 20, charset));   // 계좌예금주명
        hs20Res.setHs20Dp5(getData(byteArray    , 150, 20, charset));   // 회원번호
        hs20Res.setHs20Dp6(getData(byteArray    , 170, 1, charset));    // 처리내역
        hs20Res.setHs20Dp7(getData(byteArray    , 171, 1, charset));    // 처리구분
        hs20Res.setHs20Dp8(getData(byteArray    , 172, 20, charset));   // 전자동의Key
        hs20Res.setHs20Dp9(getData(byteArray    , 192, 20, charset));   // 회원명
        hs20Res.setHs20Dp10(getData(byteArray   , 212, 2, charset));    // 약정일
        hs20Res.setHs20Dp11(getData(byteArray   , 214, 10, charset));   // 기본금액
        hs20Res.setHs20Dp12(getData(byteArray   , 224, 8, charset));    // 이체종료일
        hs20Res.setHs20Dp13(getData(byteArray   , 232, 13, charset));   // 회원연락처
        hs20Res.setHs20Dp14(getData(byteArray   , 245, 8, charset));    // FMS번호
        hs20Res.setHs20Dp15(getData(byteArray   , 253, 47, charset));   // 미사용
        hs20Res.setHs20Dp16(getData(byteArray   , 300, 20, charset));   // 사용자영역

        return hs20Res;
    }

    // 회원증빙 - 시작
    public Hs30 Hs30Res(String data, Charset charset) {
        Hs30 hs30Res = new Hs30();
        byte[] byteArray = data.getBytes(charset);

        // 요청정보부
        hs30Res.setHs30Dp1(getData(byteArray    , 0, 1, charset));     // Record구분
        hs30Res.setHs30Dp2(getData(byteArray    , 1, 10, charset));    // 업체ID
        hs30Res.setHs30Dp3(getData(byteArray    , 11, 10, charset));   // 업체PW
        hs30Res.setHs30Dp4(getData(byteArray    , 21, 10, charset));   // SW-ID
        hs30Res.setHs30Dp5(getData(byteArray    , 31, 10, charset));   // SW-PW
        hs30Res.setHs30Dp6(getData(byteArray    , 41, 8, charset));    // 송신일자
        hs30Res.setHs30Dp7(getData(byteArray    , 49, 4, charset));    // 전문구분
        hs30Res.setHs30Dp8(getData(byteArray    , 53, 3, charset));    // 업무구분
        hs30Res.setHs30Dp9(getData(byteArray    , 56, 1, charset));    // 회원결제수단
        hs30Res.setHs30Dp10(getData(byteArray   , 57, 20, charset));   // 회원번호
        hs30Res.setHs30Dp11(getData(byteArray   , 77, 2, charset));    // 증빙구분
        hs30Res.setHs30Dp12(getData(byteArray   , 79, 5, charset));    // 증빙데이터유형
        hs30Res.setHs30Dp13(getData(byteArray   , 84, 6, charset));    // 증빙데이터길이
        hs30Res.setHs30Dp14(getData(byteArray   , 90, 1, charset));    // 응답결과
        hs30Res.setHs30Dp15(getData(byteArray   , 91, 4, charset));    // 응답코드
        hs30Res.setHs30Dp16(getData(byteArray   , 95, 30, charset));   // 응답메시지
        hs30Res.setHs30Dp17(getData(byteArray   , 125, 45, charset));  // 미사용영역
        hs30Res.setHs30Dp18(getData(byteArray   , 170, 30, charset));  // 사용자정의
        hs30Res.setHs30Dp19(getData(byteArray   , 200, 1, charset));   // CR
        hs30Res.setHs30Dp20(getData(byteArray   , 201, 1, charset));   // LF

        return hs30Res;
    }

    // 회원증빙 - 종료
    public Hs35_E Hs35ERes(String data, Charset charset) {
        Hs35_E hs35ERes = new Hs35_E();
        byte[] byteArray = data.getBytes(charset);

        // 요청정보부
        hs35ERes.setHs35EDp1(getData(byteArray    , 0, 1, charset));          // Record구분
        hs35ERes.setHs35EDp2(getData(byteArray    , 1, 10, charset));         // 업체ID
        hs35ERes.setHs35EDp3(getData(byteArray    , 11, 1, charset));         // 회원결제수단
        hs35ERes.setHs35EDp4(getData(byteArray    , 12, 20, charset));        // 회원번호
        hs35ERes.setHs35EDp5(getData(byteArray    , 32, 1, charset));         // 처리결과
        hs35ERes.setHs35EDp6(getData(byteArray    , 33, 4, charset));         // 처리결과코드
        hs35ERes.setHs35EDp7(getData(byteArray    , 37, 30, charset));        // 처리결과메시지
        hs35ERes.setHs35EDp8(getData(byteArray    , 67, 133, charset));       // 미사용
        hs35ERes.setHs35EDp9(getData(byteArray    , 200, 1, charset));        // CR
        hs35ERes.setHs35EDp10(getData(byteArray   , 201, 1, charset));        // LF

        return hs35ERes;
    }


    //실시간 출금요청
    public Hs40 Hs40Res(String data, Charset charset) {
        Hs40 hs40Res = new Hs40();
        byte[] byteArray = data.getBytes(charset);

        // 공통정보부
        hs40Res.setHsHeaderDp1(getData(byteArray        , 0, 10, charset));     // 업체ID
        hs40Res.setHsHeaderDp2(getData(byteArray        , 10, 10, charset));    // 업체PW
        hs40Res.setHsHeaderDp3(getData(byteArray        , 20, 10, charset));    // SW-ID
        hs40Res.setHsHeaderDp4(getData(byteArray        , 30, 10, charset));    // SW-PW
        hs40Res.setHsHeaderDp5(getData(byteArray        , 40, 6, charset));     // 송신일자
        hs40Res.setHsHeaderDp6(getData(byteArray        , 46, 4, charset));     // 전문구분
        hs40Res.setHsHeaderDp7(getData(byteArray        , 50, 3, charset));     // 업무구분
        hs40Res.setHsHeaderDp8(getData(byteArray        , 53, 6, charset));     // 연번
        hs40Res.setHsHeaderDp9(getData(byteArray        , 59, 6, charset));     // 전송일자
        hs40Res.setHsHeaderDp10(getData(byteArray       , 65, 6, charset));     // 전송시간
        hs40Res.setHsHeaderDp11(getData(byteArray       , 71, 1, charset));     // 응답결과
        hs40Res.setHsHeaderDp12(getData(byteArray       , 72, 4, charset));     // 응답코드
        hs40Res.setHsHeaderDp13(getData(byteArray       , 76, 24, charset));    // FMS사용영역 

        // 요청정보부
        hs40Res.setHs40Dp1(getData(byteArray     , 100, 50, charset));         // 미사용
        hs40Res.setHs40Dp2(getData(byteArray     , 150, 20, charset));         // 회원번호
        hs40Res.setHs40Dp3(getData(byteArray     , 170, 15, charset));         // 출금요청금액
        hs40Res.setHs40Dp4(getData(byteArray     , 185, 2, charset));          // 납입회차
        hs40Res.setHs40Dp5(getData(byteArray     , 187, 1, charset));          // 기거래결과조회여부
        hs40Res.setHs40Dp6(getData(byteArray     , 188, 9, charset));          // 미사용
        hs40Res.setHs40Dp7(getData(byteArray     , 197, 10, charset));         // 일 출금한도
        hs40Res.setHs40Dp8(getData(byteArray     , 207, 10, charset));         // 월 출금한도
        hs40Res.setHs40Dp9(getData(byteArray     , 217, 15, charset));         // 미사용
        hs40Res.setHs40Dp10(getData(byteArray    , 232, 20, charset));         // 현금영수증 발행정보
        hs40Res.setHs40Dp11(getData(byteArray    , 252, 18, charset));         // FMS사용영역
        hs40Res.setHs40Dp12(getData(byteArray    , 270, 15, charset));         // 출금금액
        hs40Res.setHs40Dp13(getData(byteArray    , 285, 15, charset));         // 수수료
        hs40Res.setHs40Dp14(getData(byteArray    , 300, 20, charset));         // 사용자영역

        return hs40Res;
    }

    //실시간 출금결과확인
    public Hs45 Hs45Res(String data, Charset charset) {
        Hs45 hs45Res = new Hs45();
        byte[] byteArray = data.getBytes(charset);

        // 공통정보부
        hs45Res.setHsHeaderDp1(getData(byteArray        , 0, 10, charset));     // 업체ID
        hs45Res.setHsHeaderDp2(getData(byteArray        , 10, 10, charset));    // 업체PW
        hs45Res.setHsHeaderDp3(getData(byteArray        , 20, 10, charset));    // SW-ID
        hs45Res.setHsHeaderDp4(getData(byteArray        , 30, 10, charset));    // SW-PW
        hs45Res.setHsHeaderDp5(getData(byteArray        , 40, 6, charset));     // 송신일자
        hs45Res.setHsHeaderDp6(getData(byteArray        , 46, 4, charset));     // 전문구분
        hs45Res.setHsHeaderDp7(getData(byteArray        , 50, 3, charset));     // 업무구분
        hs45Res.setHsHeaderDp8(getData(byteArray        , 53, 6, charset));     // 연번
        hs45Res.setHsHeaderDp9(getData(byteArray        , 59, 6, charset));     // 전송일자
        hs45Res.setHsHeaderDp10(getData(byteArray       , 65, 6, charset));     // 전송시간
        hs45Res.setHsHeaderDp11(getData(byteArray       , 71, 1, charset));     // 응답결과
        hs45Res.setHsHeaderDp12(getData(byteArray       , 72, 4, charset));     // 응답코드
        hs45Res.setHsHeaderDp13(getData(byteArray       , 76, 24, charset));    // FMS사용영역 

        // 요청정보부
        hs45Res.setHs45Dp1(getData(byteArray     , 100, 6, charset));         // 원전문 전송일자
        hs45Res.setHs45Dp2(getData(byteArray     , 106, 6, charset));         // 원전문 연번 
        hs45Res.setHs45Dp3(getData(byteArray     , 112, 2, charset));         // 거래계좌은행코드
        hs45Res.setHs45Dp4(getData(byteArray     , 114, 15, charset));        // 거래계좌번호
        hs45Res.setHs45Dp5(getData(byteArray     , 129, 13, charset));        // 거래계좌 생년월일/사업자번호
        hs45Res.setHs45Dp6(getData(byteArray     , 142, 20, charset));        // 계좌주예금주명
        hs45Res.setHs45Dp7(getData(byteArray     , 162, 20, charset));        // 회원번호
        hs45Res.setHs45Dp8(getData(byteArray     , 182, 15, charset));        // 출금요청금액
        hs45Res.setHs45Dp9(getData(byteArray     , 197, 2, charset));         // 납입회차
        hs45Res.setHs45Dp10(getData(byteArray    , 199, 1, charset));         // 처리구분
        hs45Res.setHs45Dp11(getData(byteArray    , 200, 20, charset));        // 전자동의Key
        hs45Res.setHs45Dp12(getData(byteArray    , 220, 15, charset));        // 출금금액
        hs45Res.setHs45Dp13(getData(byteArray    , 235, 15, charset));        // 수수료
        hs45Res.setHs45Dp14(getData(byteArray    , 250, 70, charset));        // 미사용

        return hs45Res;
    }

    public String getData(byte[] byteArray, int offset, int length, Charset charset) {
        if (byteArray == null)
            return "";

        String str = null;
        str = new String(byteArray, offset, length, charset);
        return str.trim();
    }
}