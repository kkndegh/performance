package com.aos.cms.coinage.hyoSung.dto.socket;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 45. 실시간CMS(정기출금) 실시간 출금결과확인 개별부
 * 
 * @since 2022.10.17
 * @author kh
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Hs45 extends HsHeader implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Hs45Dp1;
    private String Hs45Dp2;
    private String Hs45Dp3;
    private String Hs45Dp4;
    private String Hs45Dp5;
    private String Hs45Dp6;
    private String Hs45Dp7;
    private String Hs45Dp8;
    private String Hs45Dp9;
    private String Hs45Dp10;
    private String Hs45Dp11;
    private String Hs45Dp12;
    private String Hs45Dp13;
    private String Hs45Dp14;
}
