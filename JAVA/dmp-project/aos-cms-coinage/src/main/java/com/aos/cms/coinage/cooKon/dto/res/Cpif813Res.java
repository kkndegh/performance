package com.aos.cms.coinage.cooKon.dto.res;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 시스템계좌조회
 * 
 * @since 2022.09.26
 * @author kh
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Cpif813Res implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @JsonProperty("ci ")
    private String ci ;

    @JsonProperty("memb_cd")
    private String membCd;

    @JsonProperty("fnni_cd")
    private String fnniCd;

    @JsonProperty("acct_no")
    private String acctNo;

    @JsonProperty("biz_no")
    private String bizNo;

    @JsonProperty("cp_sys_acno")
    private String cpSysAcno;
}