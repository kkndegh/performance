package com.aos.cms.coinage.hyoSung.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;

import java.awt.image.BufferedImage;
import java.nio.charset.Charset;

import org.apache.commons.lang.StringUtils;

import com.aos.cms.coinage.Constant;
import com.aos.cms.coinage.Validation;
import com.aos.cms.coinage.common.dto.DateParamDTO;
import com.aos.cms.coinage.common.dto.ValidDTO;
import com.aos.cms.coinage.hyoSung.dto.HsDTO;
import com.aos.cms.coinage.hyoSung.dto.StepHsProcDTO;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs15;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs20;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs30;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs35;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs35_E;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs40;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs45;
import com.aos.cms.coinage.hyoSung.dto.socket.HsHeader;
import com.aos.cms.coinage.hyoSung.dto.SocketDTO;
import com.aos.cms.coinage.hyoSung.dto.res.HsCommonRes;
import com.aos.cms.coinage.hyoSung.util.ImageByteShorts;
import com.aos.cms.coinage.hyoSung.util.convert.ReqDataConvert;
import com.aos.cms.coinage.hyoSung.util.convert.ResDataConvert;

/**
 * HyosungApi 서비스
 * 
 * @author kh
 * @since 2022.10.13
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class HyosungApiService{

    @Value("${hyosung.conf}")
    private String CONF;

    @Value("${hyosung.real.server_ip}")
    private String REAL_SERVER_IP;

    @Value("${hyosung.real.server_port}")
    private int REAL_SERVER_PORT;

    @Value("${hyosung.confirm.server_ip}")
    private String CONFIRM_SERVER_IP;

    @Value("${hyosung.confirm.server_port}")
    private int CONFIRM_SERVER_PORT;

    private final ResDataConvert resDataConvert;

    private final HyoSungConnect hyoSungConnect;

    private final ImageByteShorts imageByteShorts;
    
    // 15. 회원조회
    public StepHsProcDTO hs15(HsDTO params, DateParamDTO dt, SocketDTO socket){
        StepHsProcDTO rtnObj = null;
        HsCommonRes resultObj = null;

        try {
            //소켓정보 셋팅
            socket.setData(null);
            socket.setReturnData("");
            socket.setConnection(1);
            socket.setRecv(1);
            socket.setType(2);
            socket.setTransmitType("1000");
            socket.setPacketCode("500");
            socket.setTreat("");
            socket.setRefer1(0);

            //발송정보 추가 셋팅
            params.setPostDate("");
            params.setPostSerial("");

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepHsProcDTO(params, socket, resultObj);
                return rtnObj;
            }

            //파라미터 셋팅
            String paramStr = paramParmProc(params, dt, socket);
            socket.setData(paramStr);
            
            // 수신 부
            hyoSungConnect.socket_connect(REAL_SERVER_IP, REAL_SERVER_PORT, CONF);
            String resp = hyoSungConnect.socket_real_send(paramStr, params, socket);
            hyoSungConnect.socket_close();
            
            //결과 저장
            socket.setReturnData(resp);

            //데이터 부
            Hs15 hs15Res = resDataConvert.Hs15Res(resp, Constant.DEFAULT_CHARSET);

            String rc = hs15Res.getHsHeaderDp12();                  //응답코드
            String rResult = hs15Res.getHsHeaderDp11();             //응답결과
            String rAccountNo = hs15Res.getHs15Dp2();               //거래계좌
            
            if(rResult.equals("Y") && rAccountNo.equals(params.getAccountNo())){
                log.info("== 회원조회 확인 완료");
                log.info("== 출금요청 실행 ::>>>>>>>>>>>>>>>>");
                // -> 출금요청
                params.setStepNo("40");
            } else if(rResult.equals("Y") && !rAccountNo.equals(params.getAccountNo())){
                log.info("== 회원조회 확인 완료 :: " + rc);
                log.info("== 회원증빙 실행 ::>>>>>>>>>>>>>>>>");
                // -> 회원증빙
                params.setStepNo("30");
            }else{
                log.info("== 회원조회 확인 완료 :: " + rc);
                log.info("== 회원증빙 실행 ::>>>>>>>>>>>>>>>>");
                // -> 회원증빙
                params.setStepNo("30");
            }
        } catch (Exception e) {
            log.error("효성 SOCKET 연동 오류 [hs15]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepHsProcDTO(params, socket, resultObj);
        }
        return rtnObj;
    }

    // 20. 회원등록
    public StepHsProcDTO hs20(HsDTO params, DateParamDTO dt, SocketDTO socket){
        StepHsProcDTO rtnObj = null;
        HsCommonRes resultObj = null;

        try {
            //소켓정보 셋팅
            socket.setData(null);
            socket.setReturnData("");
            socket.setConnection(1);
            socket.setRecv(1);
            socket.setType(2);
            socket.setTransmitType("1000");
            socket.setPacketCode("400");
            socket.setTreat("N");
            socket.setRefer1(0);

            //발송정보 추가 셋팅
            params.setPostDate("");
            params.setPostSerial("");

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepHsProcDTO(params, socket, resultObj);
                return rtnObj;
            }

            //파라미터 셋팅
            String paramStr = paramParmProc(params, dt, socket);
            socket.setData(paramStr);

            // 수신 부
            hyoSungConnect.socket_connect(REAL_SERVER_IP, REAL_SERVER_PORT, CONF);
            String resp = hyoSungConnect.socket_real_send(paramStr, params, socket);
            hyoSungConnect.socket_close();

            //결과 저장
            socket.setReturnData(resp);

            //데이터 부
            Hs20 hs20Res = resDataConvert.Hs20Res(resp, Constant.DEFAULT_CHARSET);

            String rc = hs20Res.getHsHeaderDp12();                  //응답코드
            String rResult = hs20Res.getHsHeaderDp11();             //응답결과

            if(rResult.equals("Y")){
                log.info("== 회원등록 확인 완료");
                log.info("== 회원조회 실행 ::>>>>>>>>>>>>>>>>");
                // -> 회원조회
                params.setStepNo("15");
            } else{
                log.info("== 회원등록 실패 :: " + rc);
                params.setStepNo("920");
            }
        } catch (Exception e) {
            log.error("효성 SOCKET 연동 오류 [hs20]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepHsProcDTO(params, socket, resultObj);
        }
        return rtnObj;
    }

    // 25. 회원삭제
    public StepHsProcDTO hs25(HsDTO params, DateParamDTO dt, SocketDTO socket){
        StepHsProcDTO rtnObj = null;
        HsCommonRes resultObj = null;

        try {
            //소켓정보 셋팅
            socket.setData(null);
            socket.setReturnData("");
            socket.setConnection(1);
            socket.setRecv(1);
            socket.setType(2);
            socket.setTransmitType("1000");
            socket.setPacketCode("400");
            socket.setTreat("D");
            socket.setRefer1(0);
            params.setStepNo("20");  //회원등록과 동일로직 사용하여 20으로 변경. socket.treat 값으로 구분

            //발송정보 추가 셋팅
            params.setPostDate("");
            params.setPostSerial("");

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepHsProcDTO(params, socket, resultObj);
                return rtnObj;
            }

            //파라미터 셋팅
            String paramStr = paramParmProc(params, dt, socket);
            socket.setData(paramStr);

            // 수신 부
            hyoSungConnect.socket_connect(REAL_SERVER_IP, REAL_SERVER_PORT, CONF);
            String resp = hyoSungConnect.socket_real_send(paramStr, params, socket);
            hyoSungConnect.socket_close();

            //결과 저장
            socket.setReturnData(resp);

            //데이터 부
            Hs20 hs20Res = resDataConvert.Hs20Res(resp, Constant.DEFAULT_CHARSET);

            String rc = hs20Res.getHsHeaderDp12();                  //응답코드
            String rResult = hs20Res.getHsHeaderDp11();             //응답결과

            if(rResult.equals("Y")){
                log.info("== 회원삭제 확인 완료");
            } else{
                log.info("== 회원삭제 실패 :: " + rc);
                params.setStepNo("925");
            }

        } catch (Exception e) {
            log.error("효성 SOCKET 연동 오류 [hs25]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepHsProcDTO(params, socket, resultObj);
        }
        return rtnObj;
    }

    // 30. 회원증빙
    public StepHsProcDTO hs30(HsDTO params, DateParamDTO dt, SocketDTO socket){
        StepHsProcDTO rtnObj = null;
        HsCommonRes resultObj = null;

        try {
            //소켓정보 셋팅
            socket.setData(null);
            socket.setReturnData("");
            socket.setConnection(1);
            socket.setRecv(1);
            socket.setType(1);
            socket.setTransmitType("1000");
            socket.setPacketCode("100");
            socket.setTreat("");
            socket.setRefer1(0);

            //발송정보 추가 셋팅
            params.setPostDate("");
            params.setPostSerial("");

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepHsProcDTO(params, socket, resultObj);
                return rtnObj;
            }

            //증빙파일 사이즈 셋팅 ( 300000byte 이상인 경우 축소)
            BufferedImage image = imageByteShorts.getUrlToImage(params.getEvidenceFileUrl());
            int volum = imageByteShorts.getImageVolum(image);

            if(volum > 300000){
                //이미지 용량 축소
                image = imageByteShorts.getShortImage(image);
                volum = imageByteShorts.toByteArray(image, "jpg").length;
            }
            params.setEvidenceFileSize(String.valueOf(volum));
            params.setEncEvidenceFile(imageByteShorts.imageToBase64(imageByteShorts.toByteArray(image, "jpg")));

            //파라미터 셋팅
            String paramStr = paramParmProc(params, dt, socket);
            socket.setData(paramStr);

            // 수신 부
            hyoSungConnect.socket_connect(CONFIRM_SERVER_IP, CONFIRM_SERVER_PORT, CONF);
            String resp = hyoSungConnect.socket_confirm_send(paramStr, params, socket);

            //결과 저장
            socket.setReturnData(resp);

            //데이터 부
            Hs30 hs30Res = resDataConvert.Hs30Res(resp, Constant.DEFAULT_CHARSET);

            String rc = hs30Res.getHs30Dp15();                      //응답코드
            String rResult = hs30Res.getHs30Dp14();                 //응답결과
            
            if(rResult.equals("Y")){
                log.info("== 회원증빙 확인 완료");
                log.info("== 회원증빙데이터 확인 실행 ::>>>>>>>>>>>>>>>>");
                params.setStepNo("35");
            } else{
                hyoSungConnect.socket_close();
                log.info("== 회원증빙 실패 :: " + rc);
                params.setStepNo("930");
            }
        } catch (Exception e) {
            log.error("효성 SOCKET 연동 오류 [hs30]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepHsProcDTO(params, socket, resultObj);
        }
        return rtnObj;
    }

    // 35. 회원증빙데이터
    public StepHsProcDTO hs35(HsDTO params, DateParamDTO dt, SocketDTO socket){
        StepHsProcDTO rtnObj = null;
        HsCommonRes resultObj = null;

        try {
            //소켓정보 셋팅
            socket.setData(null);
            socket.setReturnData("");
            socket.setConnection(0);
            socket.setRecv(0);
            socket.setType(1);
            socket.setTransmitType("1000");
            socket.setPacketCode("900");
            socket.setTreat("");
            socket.setRefer1(socket.getCnt());

            //발송정보 추가 셋팅
            params.setPostDate("");
            params.setPostSerial("");

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepHsProcDTO(params, socket, resultObj);
                return rtnObj;
            }

            //파라미터 셋팅
            String paramStr = paramParmProc(params, dt, socket);
            socket.setData(paramStr);
            
            //최종데이터 여부 확인
            if(paramStr.substring(32 , 33).equals("Y")){
                // 수신 부
                String resp = hyoSungConnect.socket_confirm_send(paramStr, params, socket);
                hyoSungConnect.socket_close();

                //결과 저장
                socket.setReturnData(resp);

                //데이터 부
                Hs35_E hs35ERes = resDataConvert.Hs35ERes(resp, Constant.DEFAULT_CHARSET);

                String rc = hs35ERes.getHs35EDp6();                      //응답코드
                String rResult = hs35ERes.getHs35EDp7();                 //응답결과

                //최종데이터면 다음Step으로, 아니면 이어서 통신
                if(hs35ERes.getHs35EDp5().equals("Y")){
                    log.info("== 회원증빙데이터 확인 완료 [파일 수 :: " + (socket.getCnt()) + "]");
                    log.info("== 회원등록 실행 ::>>>>>>>>>>>>>>>>");
                    params.setStepNo("20");
                }else{
                    log.info("== 회원증빙데이터 전송 실패 :: " + rc +"[" + rResult + "]");
                    params.setStepNo("935");
                }
            }else{
                //다음데이터 전송
                hyoSungConnect.socket_confirm_send(paramStr, params, socket);

                log.info("== 회원증빙데이터 전송 [socket_cnt :: " + (socket.getCnt()) + "]");
                log.info("== 다음 데이터 전송 ::>>>>>>>>>>>>>>>>");
                socket.setCnt(socket.getCnt()+1);
            }
        } catch (Exception e) {
            log.error("효성 SOCKET 연동 오류 [hs35]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepHsProcDTO(params, socket, resultObj);
        }
        return rtnObj;
    }

    // 40. 출금
    public StepHsProcDTO hs40(HsDTO params, DateParamDTO dt, SocketDTO socket){
        StepHsProcDTO rtnObj = null;
        HsCommonRes resultObj = null;

        try {
            //소켓정보 셋팅
            socket.setData(null);
            socket.setReturnData("");
            socket.setConnection(1);
            socket.setRecv(1);
            socket.setType(1);
            socket.setTransmitType("1000");
            socket.setPacketCode("120");
            socket.setTreat("");
            socket.setRefer1(0);

            //발송정보 추가 셋팅
            params.setPostDate("");
            params.setPostSerial("");

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepHsProcDTO(params, socket, resultObj);
                return rtnObj;
            }

            //파라미터 셋팅
            String paramStr = paramParmProc(params, dt, socket);
            socket.setData(paramStr);

            // 수신 부
            hyoSungConnect.socket_connect(REAL_SERVER_IP, REAL_SERVER_PORT, CONF);
            String resp = hyoSungConnect.socket_real_send(paramStr, params, socket);
            hyoSungConnect.socket_close();

            //결과 저장
            socket.setReturnData(resp);

            //데이터 부
            Hs40 hs40Res = resDataConvert.Hs40Res(resp, Constant.DEFAULT_CHARSET);

            if(StringUtils.isEmpty(socket.getReturnData())){
                params.setPostDate(paramStr.substring(40,6));
                params.setPostSerial(paramStr.substring(53,6));
            }else{
                params.setPostDate(hs40Res.getHsHeaderDp9());
                params.setPostSerial(hs40Res.getHsHeaderDp8());
            }

            String rc = hs40Res.getHsHeaderDp12();                  //응답코드
            String rResult = hs40Res.getHsHeaderDp11();             //응답결과

            if(rResult.equals("Y")){
                log.info("== 출금 확인 완료");
                log.info("== 출금확인 실행 ::>>>>>>>>>>>>>>>>");
                params.setStepNo("45");
            } else{
                log.info("== 출금 실패 :: " + rc);
                params.setStepNo("940");
            }
        } catch (Exception e) {
            log.error("효성 SOCKET 연동 오류 [hs40]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepHsProcDTO(params, socket, resultObj);
        }
        return rtnObj;
    }

    // 45. 출금결과확인
    public StepHsProcDTO hs45(HsDTO params, DateParamDTO dt, SocketDTO socket){
        StepHsProcDTO rtnObj = null;
        HsCommonRes resultObj = null;

        try {
            //소켓정보 셋팅
            socket.setData(null);
            socket.setReturnData("");
            socket.setConnection(1);
            socket.setRecv(1);
            socket.setType(2);
            socket.setTransmitType("1000");
            socket.setPacketCode("200");
            socket.setTreat("");
            socket.setRefer1(0);

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepHsProcDTO(params, socket, resultObj);
                return rtnObj;
            }

            //파라미터 셋팅
            String paramStr = paramParmProc(params, dt, socket);
            socket.setData(paramStr);

            // 수신 부
            hyoSungConnect.socket_connect(REAL_SERVER_IP, REAL_SERVER_PORT, CONF);
            String resp = hyoSungConnect.socket_real_send(paramStr, params, socket);
            hyoSungConnect.socket_close();

            //결과 저장
            socket.setReturnData(resp);

            //데이터 부
            Hs45 hs45Res = resDataConvert.Hs45Res(resp, Constant.DEFAULT_CHARSET);

            String rc = hs45Res.getHsHeaderDp12();                  //응답코드
            String rResult = hs45Res.getHsHeaderDp11();             //응답결과
            
            if(rResult.equals("Y")){
                log.info("== 출금결과 확인 완료");

                // -> 프로세스 완료
                params.setStepNo("999");

                /*********************************************
                 * CMS 최종 로그 저장 
                 *********************************************/
                JSONObject resObj = new JSONObject();
                JSONObject reqObj = new JSONObject();
                reqObj.put("type", "hyosung");
                reqObj.put("type1", params.getStepNo());
                reqObj.put("type2", "1100");
                reqObj.put("ap_id", params.getApId());
                reqObj.put("packet_message", "");

                hyoSungConnect.saveLog(resObj);
            } else{
                log.info("== 출금결과 확인 실패 :: " + rc);
                params.setStepNo("945");
            }

        } catch (Exception e) {
            log.error("효성 SOCKET 연동 오류 [hs45]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepHsProcDTO(params, socket, resultObj);
        }
        return rtnObj;
    }

    /**
     * 효성 통신 파라미터 셋팅
     * 
     * @param hsDTO HsDTO
     * @param dt DateParamDTO
     * @param socket SocketDTO
     * @return String
     */
    private String paramParmProc(HsDTO hsDTO, DateParamDTO dt, SocketDTO socket){
        String paramStr="";

        try {
            HsHeader hsHeaderReq = new HsHeader();
            hsHeaderReq.setHsHeaderDp1(hsDTO.getUserID());           //업체ID
            hsHeaderReq.setHsHeaderDp2(hsDTO.getUserPW());           //업체PW
            hsHeaderReq.setHsHeaderDp3(hsDTO.getUserSubID());        //SW-ID
            hsHeaderReq.setHsHeaderDp4(hsDTO.getUserSubPW());        //SW-PW
            hsHeaderReq.setHsHeaderDp5(dt.getTrxDt().substring(2));  //송신일자
            hsHeaderReq.setHsHeaderDp6(socket.getTransmitType());    //전문구분
            hsHeaderReq.setHsHeaderDp7(socket.getPacketCode());      //업무구분
            hsHeaderReq.setHsHeaderDp8(dt.getTrxTm());               //연번
            hsHeaderReq.setHsHeaderDp9("");                          //전송일자
            hsHeaderReq.setHsHeaderDp10("");                         //전송시간
            hsHeaderReq.setHsHeaderDp11("");                         //응답결과
            hsHeaderReq.setHsHeaderDp12("");                         //응답코드
            hsHeaderReq.setHsHeaderDp13("");                         //FMS사용영역 

            switch(hsDTO.getStepNo()){
                case "15" : //회원조회
                    Hs15 hs15Req = new Hs15();
                    hs15Req.setHs15Dp1("");                     //거래계좌은행코드
                    hs15Req.setHs15Dp2("");                     //거래계좌번호
                    hs15Req.setHs15Dp3("");                     //거래계좌 생년월일/사업자번호
                    hs15Req.setHs15Dp4("");                     //계좌예금주명
                    hs15Req.setHs15Dp5(hsDTO.getMemberNo());    //회원번호
                    hs15Req.setHs15Dp6("");                     //처리내역
                    hs15Req.setHs15Dp7("");                     //처리구분
                    hs15Req.setHs15Dp8("");                     //전자동의Key
                    hs15Req.setHs15Dp9("");                     //납부자 연락처 
                    hs15Req.setHs15Dp10("");                    //FMS번호
                    hs15Req.setHs15Dp11("");                    //미사용
                    hs15Req.setHs15Dp12("");                    //사용자영역

                    ReqDataConvert hs15 = ReqDataConvert.builder()
                        .hsHeaderReq(hsHeaderReq)
                        .hs15Req(hs15Req)
                        .type("15")
                        .build();
                    paramStr = hs15.getString(Charset.forName("EUC-KR"));

                    log.info("== 15.회원조회전문 생성완료");
                    break;
                case "20" : //회원등록, 삭제
                    Hs20 hs20Req = new Hs20();
                    hs20Req.setHs20Dp1(hsDTO.getBank());            //거래계좌은행코드
                    hs20Req.setHs20Dp2(hsDTO.getAccountNo());       //거래계좌번호
                    hs20Req.setHs20Dp3(hsDTO.getReqNo());           //거래계좌 생년월일/사업자번호
                    hs20Req.setHs20Dp4(hsDTO.getAccountOwner());    //계좌예금주명
                    hs20Req.setHs20Dp5(hsDTO.getMemberNo());        //회원번호
                    hs20Req.setHs20Dp6(socket.getTreat());          //처리내역 (신규 : N , 삭제 : D)
                    hs20Req.setHs20Dp7("N");                        //처리구분
                    hs20Req.setHs20Dp8("");                         //전자동의Key
                    hs20Req.setHs20Dp9(hsDTO.getAccountOwner());    //회원명
                    hs20Req.setHs20Dp10("");                        //약정일
                    hs20Req.setHs20Dp11("");                        //기본금액
                    hs20Req.setHs20Dp12("");                        //이체종료일
                    hs20Req.setHs20Dp13("");                        //회원연락처
                    hs20Req.setHs20Dp14("");                        //FMS번호
                    hs20Req.setHs20Dp15("");                        //미사용
                    hs20Req.setHs20Dp16("");                        //사용자영역

                    ReqDataConvert hs20 = ReqDataConvert.builder()
                        .hsHeaderReq(hsHeaderReq)
                        .hs20Req(hs20Req)
                        .type("20")
                        .build();
                    paramStr = hs20.getString(Charset.forName("EUC-KR"));

                    if(socket.getTreat().equals("N")){
                        log.info("== 20.회원등록전문 생성완료");
                    }else{
                        log.info("== 25.회원삭제전문 생성완료");
                    }
                    break;
                case "30" : //회원증빙
                    Hs30 hs30Req = new Hs30();
                    hs30Req.setHs30Dp1("S");                        //Record구분
                    hs30Req.setHs30Dp2(hsDTO.getUserID());          //업체ID
                    hs30Req.setHs30Dp3(hsDTO.getUserPW());          //업체PW
                    hs30Req.setHs30Dp4(hsDTO.getUserSubID());       //SW-ID
                    hs30Req.setHs30Dp5(hsDTO.getUserSubPW());       //SW-PW
                    hs30Req.setHs30Dp6(dt.getTrxDt());              //송신일자
                    hs30Req.setHs30Dp7(socket.getTransmitType());   //전문구분
                    hs30Req.setHs30Dp8(socket.getPacketCode());     //업무구분
                    hs30Req.setHs30Dp9("R");                        //회원결제수단
                    hs30Req.setHs30Dp10(hsDTO.getMemberNo());       //회원번호
                    hs30Req.setHs30Dp11("01");                      //증빙구분
                    hs30Req.setHs30Dp12("JPG");                     //증빙데이터유형

                    String filesize = "000000";
                    if(StringUtils.isNotEmpty(hsDTO.getEvidenceFileSize())){
                        filesize += hsDTO.getEvidenceFileSize();
                        filesize = filesize.substring(filesize.length() - 6, filesize.length());
                    }

                    hs30Req.setHs30Dp13(filesize);                  //증빙데이터길이
                    hs30Req.setHs30Dp14("");                        //응답결과
                    hs30Req.setHs30Dp15("");                        //응답코드
                    hs30Req.setHs30Dp16("");                        //응답메시지
                    hs30Req.setHs30Dp17("");                        //미사용영역
                    hs30Req.setHs30Dp18(" ");                       //사용자정의
                    hs30Req.setHs30Dp19("\r");                      //CR
                    hs30Req.setHs30Dp20("\n");                      //LF

                    ReqDataConvert hs30 = ReqDataConvert.builder()
                        .hs30Req(hs30Req)
                        .type("30")
                        .build();
                    paramStr = hs30.getString(Charset.forName("EUC-KR"));

                    log.info("== 30.회원증빙전문 생성완료");
                    break;
                case "35" : //회원증빙데이터
                    Hs35 hs35Req = new Hs35();
                    hs35Req.setHs35Dp1("D");                        //Record구분
                    hs35Req.setHs35Dp2(hsDTO.getUserID());          //업체ID
                    hs35Req.setHs35Dp3("R");                        //회원결제수단
                    hs35Req.setHs35Dp4(hsDTO.getMemberNo());        //회원번호

                    //증빙데이터 셋팅
                    int start_no = 60000 * (socket.getCnt() - 1);
                    int end_no = 0;
                    String encodedEvidenceFile  = hsDTO.getEncEvidenceFile();

                    if ((60000 * socket.getCnt()) > encodedEvidenceFile.length()){
                        end_no = encodedEvidenceFile.length();
                    }else{
                        end_no = 60000 * (socket.getCnt());
                    }

                    String evidenceFile = encodedEvidenceFile.substring(start_no, end_no);
                    if ((60000 * socket.getCnt()) > encodedEvidenceFile.length()){
                        hs35Req.setHs35Dp5("Y");                    //최종데이터여부
                    }else{
                        hs35Req.setHs35Dp5("N");                    //최종데이터여부
                    }
                    
                    String number = "00" + String.valueOf(socket.getCnt());
                    number = number.substring(number.length() - 2, number.length());

                    hs35Req.setHs35Dp6(number);                     //데이터전송연번
                    hs35Req.setHs35Dp7("");                         //미사용
                    hs35Req.setHs35Dp8(evidenceFile);               //증빙데이터
                    hs35Req.setHs35Dp9("\r");                       //CR
                    hs35Req.setHs35Dp10("\n");                      //LF

                    ReqDataConvert hs35 = ReqDataConvert.builder()
                        .hs35Req(hs35Req)
                        .type("35")
                        .build();
                    paramStr = hs35.getString(Charset.forName("EUC-KR"));

                    log.info("== 35.회원증빙데이터전문 생성완료");
                    break;
                case "40" : //출금요청
                    Hs40 hs40Req = new Hs40();
                    hs40Req.setHs40Dp1("");                         //미사용
                    hs40Req.setHs40Dp2(hsDTO.getMemberNo());        //회원번호

                    String money = "000000000000000" + hsDTO.getReqAmount();
                    money = money.substring(money.length() - 15, money.length());

                    hs40Req.setHs40Dp3(money);                      //출금요청금액
                    hs40Req.setHs40Dp4("");                         //납입회차
                    hs40Req.setHs40Dp5("N");                        //기거래결과조회여부
                    hs40Req.setHs40Dp6("");                         //미사용
                    hs40Req.setHs40Dp7("");                         //일 출금한도
                    hs40Req.setHs40Dp8("");                         //월 출금한도
                    hs40Req.setHs40Dp9("");                         //미사용
                    hs40Req.setHs40Dp10("");                        //현금영수증 발행정보
                    hs40Req.setHs40Dp11("");                        //FMS사용영역
                    hs40Req.setHs40Dp12("");                        //출금금액
                    hs40Req.setHs40Dp13("");                        //수수료
                    hs40Req.setHs40Dp14("");                        //사용자영역
                    
                    ReqDataConvert hs40 = ReqDataConvert.builder()
                        .hsHeaderReq(hsHeaderReq)
                        .hs40Req(hs40Req)
                        .type("40")
                        .build();
                    paramStr = hs40.getString(Charset.forName("EUC-KR"));

                    log.info("== 40.출금요청전문 생성완료");
                    break;
                case "45" : //출금결과확인
                    Hs45 hs45Req = new Hs45();
                    hs45Req.setHs45Dp1(hsDTO.getPostDate());        //원전문 전송일자
                    hs45Req.setHs45Dp2(hsDTO.getPostSerial());      //원전문 연번
                    hs45Req.setHs45Dp3("");                         //거래계좌은행코드
                    hs45Req.setHs45Dp4("");                         //거래계좌번호
                    hs45Req.setHs45Dp5("N");                        //거래계좌 생년월일/사업자번호
                    hs45Req.setHs45Dp6("");                         //계좌주예금주명
                    hs45Req.setHs45Dp7("");                         //회원번호
                    hs45Req.setHs45Dp8("");                         //출금요청금액
                    hs45Req.setHs45Dp9("");                         //납입회차
                    hs45Req.setHs45Dp10("");                        //처리구분
                    hs45Req.setHs45Dp11("");                        //전자동의Key
                    hs45Req.setHs45Dp12("");                        //출금금액
                    hs45Req.setHs45Dp13("");                        //수수료
                    hs45Req.setHs45Dp14("");                        //미사용
                    
                    ReqDataConvert hs45 = ReqDataConvert.builder()
                        .hsHeaderReq(hsHeaderReq)
                        .hs45Req(hs45Req)
                        .type("45")
                        .build();
                    paramStr = hs45.getString(Charset.forName("EUC-KR"));

                    log.info("== 45.출금확인전문 생성완료");
                    break;
            }
        } catch (Exception e) {
            log.error("전문 셋팅 오류발생 :: " + e.toString());
        }
        return paramStr;
    }

    /**
     * 효성 전문값 Validation
     * 
     * @param params HsDTO
     * @return ValidDTO
     */
    private ValidDTO paramParmValidation(HsDTO params){
        ValidDTO validDTO = new ValidDTO(false, "체크완료");
        String stepNo = params.getStepNo();

        try {
            if(stepNo.equals("30") || stepNo.equals("35")){
                //공통부
                if(StringUtils.isEmpty(params.getUserID())){
                    validDTO.setChk(true);
                    validDTO.setMsg(Validation.HS_COMMON_1);
                }
                if(StringUtils.isEmpty(params.getUserPW())){
                    validDTO.setChk(true);
                    validDTO.setMsg(Validation.HS_COMMON_2);
                }
                if(StringUtils.isEmpty(params.getUserSubID())){
                    validDTO.setChk(true);
                    validDTO.setMsg(Validation.HS_COMMON_3);
                }
                if(StringUtils.isEmpty(params.getUserSubPW())){
                    validDTO.setChk(true);
                    validDTO.setMsg(Validation.HS_COMMON_4);
                }
            }

            switch(stepNo){
                case "15" : //회원조회
                    if(StringUtils.isEmpty(params.getMemberNo())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_15_1);
                    }
                    break;
                case "20" : //회원가입
                    if(StringUtils.isEmpty(params.getBank())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_20_1);
                    }
                    if(StringUtils.isEmpty(params.getAccountNo())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_20_2);
                    }
                    if(StringUtils.isEmpty(params.getReqNo())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_20_3);
                    }
                    if(StringUtils.isEmpty(params.getAccountOwner())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_20_4);
                    }
                    if(StringUtils.isEmpty(params.getMemberNo())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_20_5);
                    }
                    break;
                case "25" : //회원삭제
                if(StringUtils.isEmpty(params.getBank())){
                    validDTO.setChk(true);
                    validDTO.setMsg(Validation.HS_25_1);
                    }
                    if(StringUtils.isEmpty(params.getAccountNo())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_25_2);
                    }
                    if(StringUtils.isEmpty(params.getReqNo())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_25_3);
                    }
                    if(StringUtils.isEmpty(params.getAccountOwner())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_25_4);
                    }
                    if(StringUtils.isEmpty(params.getMemberNo())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_25_5);
                    }
                    break;
                case "30" : //회원증빙
                    if(StringUtils.isEmpty(params.getUserID())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_30_1);
                    }
                    if(StringUtils.isEmpty(params.getUserPW())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_30_2);
                    }
                    if(StringUtils.isEmpty(params.getUserSubID())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_30_3);
                    }
                    if(StringUtils.isEmpty(params.getUserSubPW())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_30_4);
                    }
                    if(StringUtils.isEmpty(params.getMemberNo())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_30_5);
                    }
                    if(StringUtils.isEmpty(params.getEvidenceFileUrl())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_30_6);
                    }
                    break;
                case "35" : //증빙데이터
                    if(StringUtils.isEmpty(params.getUserID())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_35_1);
                    }
                    if(StringUtils.isEmpty(params.getMemberNo())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_35_2);
                    }
                    if(StringUtils.isEmpty(params.getEvidenceFileUrl())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_35_3);
                    }
                    break;
                case "40" : //출금
                    if(StringUtils.isEmpty(params.getMemberNo())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_40_1);
                    }
                    if(StringUtils.isEmpty(params.getReqAmount())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_40_2);
                    }
                    break;
                case "45" : //거래내역조회
                    if(StringUtils.isEmpty(params.getPostDate())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_45_1);
                    }
                    if(StringUtils.isEmpty(params.getPostSerial())){
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.HS_45_2);
                    }
                    break;
            }
        } catch (Exception e) {
            log.error("전문 Validation 오류발생 :: " + e.toString());
        }
        return validDTO;
    }
}