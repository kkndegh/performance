package com.aos.cms.coinage.hyoSung.dto.socket;

import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;

/**
 * 0.실시간CMS(정기출금) 실시간공통부
 * 
 * @since 2022.10.13
 * @author sung
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HsHeader {
    private String hsHeaderDp1;
    private String hsHeaderDp2;
    private String hsHeaderDp3;
    private String hsHeaderDp4;
    private String hsHeaderDp5;
    private String hsHeaderDp6;
    private String hsHeaderDp7;
    private String hsHeaderDp8;
    private String hsHeaderDp9;
    private String hsHeaderDp10;
    private String hsHeaderDp11;
    private String hsHeaderDp12;
    private String hsHeaderDp13;
}
