package com.aos.cms.coinage.hyoSung.socket;

/**
 * 효성 BaseAction 인터페이스
 * 
 * @author kh
 * @since 2022.10.20
 */
public interface BaseAction {
    public int connect(String ip, int port, String conf);
    public boolean sendData(String data);
    public byte [] recvData();
    public int close();
}
