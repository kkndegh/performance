package com.aos.cms.coinage.hyoSung.dto.socket;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 35. 회원증빙등록 종료전문
 * 
 * @since 2022.10.17
 * @author kh
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Hs35_E implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Hs35EDp1;
    private String Hs35EDp2;
    private String Hs35EDp3;
    private String Hs35EDp4;
    private String Hs35EDp5;
    private String Hs35EDp6;
    private String Hs35EDp7;
    private String Hs35EDp8;
    private String Hs35EDp9;
    private String Hs35EDp10;
}
