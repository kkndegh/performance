package com.aos.cms.coinage.cooKon.dto.res;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 결제요청
 * 
 * @since 2022.09.26
 * @author kh
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Cpif830Res implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @JsonProperty("tr_dt")
    private String trDt;

    @JsonProperty("tr_tm")
    private String trTm;

    @JsonProperty("tr_no")
    private String trNo;

    @JsonProperty("trx_tp")
    private String trxTp;

    @JsonProperty("order_tr_dt")
    private String orderTrDt;

    @JsonProperty("order_tr_no")
    private String orderTrNo;

    @JsonProperty("prdt_nm")
    private String prdtNm;

    @JsonProperty("prdt_amt")
    private String prdtAmt;

    @JsonProperty("res_msg")
    private String resMsg;

    @JsonProperty("cash_aprv_no")
    private String cashAprvNo;

    @JsonProperty("fnni_cd")
    private String fnniCd;

    @JsonProperty("cp_sys_acno")
    private String cpSysAcno;

    @JsonProperty("acct_no")
    private String acctNo;

    @JsonProperty("res_cd")
    private String resCd;

    @JsonProperty("ci")
    private String ci;

    @JsonProperty("memb_cd")
    private String membCd;

    @JsonProperty("biz_no")
    private String bizNo;

    @JsonProperty("bnk_cd")
    private String bnkCd;
}