package com.aos.cms.coinage.cooKon.dto.res;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 회원상태 조회
 * 
 * @since 2022.09.26
 * @author kh
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Cpif828Res implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @JsonProperty("svc_reg_yn")
    private String svcRegYn;

    @JsonProperty("biz_no")
    private String bizNo;

    @JsonProperty("memb_stat_cd")
    private String membStatCd;

    @JsonProperty("cp_memb_no")
    private String cpMembNo;

    @JsonProperty("acct_reg_yn")
    private String acctRegYn;

    @JsonProperty("bank_cd")
    private String bankCd;

    @JsonProperty("ph_no")
    private String phNo;

    @JsonProperty("acct_seq")
    private String acctSeq;

    @JsonProperty("acct_info")
    private String acctInfo;
}