package com.aos.cms.coinage.hyoSung.dto.socket;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 40. 실시간CMS(정기출금) 실시간 출금요청 개별부
 * 
 * @since 2022.10.17
 * @author kh
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Hs40 extends HsHeader implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Hs40Dp1;
    private String Hs40Dp2;
    private String Hs40Dp3;
    private String Hs40Dp4;
    private String Hs40Dp5;
    private String Hs40Dp6;
    private String Hs40Dp7;
    private String Hs40Dp8;
    private String Hs40Dp9;
    private String Hs40Dp10;
    private String Hs40Dp11;
    private String Hs40Dp12;
    private String Hs40Dp13;
    private String Hs40Dp14;
}
