package com.aos.cms.coinage.cooKon.dto;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 요청 파라미터
 * 
 * @since 2022.09.26
 * @author kh
 */
@Data
@NoArgsConstructor
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CpifDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String cookonId;
    private String cryptKey;
    private String stepNo;
    private String apId;
    private String certfilepath;
    private String uKey;
    private String test;

    // 회원조회 [CPIF_AFFL_828]
    private String ci;
    private String membCd;
    private String bizNo;

    // 회원가입 [CPIF_AFFL_560] 
    private String membNm;
    private String phNo;
    private String birthDate;
    private String gender;
    private String teleCorp;
    private String nationalYn;
    private String appId;
    private String appVer;
    private String dvcOsNm;
    private String dvcOsVer;
    private String dvcMdl;
    private String dvcId;

    // 법인회원가입 [CPIF_AFFL_560] 
    private String compNm;
    private String compEmail;

    // 계좌검증 [CPIF_AFFL_821]
    private String cpMembNo;
    private String fnniCd;
    private String acctNo;
    private String verifyType;
    private String verifyTxt;

    // 시스템계좌번호조회 [CPIF_AFFL_813]

    // 계좌등록 [CPIF_AFFL_825]
    private String verifyTrDt;
    private String verifyTrNo;
    private String verifyNo;
    private String certKind;
    private String certFileEts;
    private String arsTrDt;
    private String arsTrNo;
    private String dsDtime;
    private String dsDataSize;
    private String dsData;
    private String hbAgreeDttm;

    //출금 [CPIF_AFFL_830]
    private String orderTrDt;
    private String orderTrNo;
    private String cpSysAcno;
    private String plainAcctNo;
    private String prdtNm;
    private String prdtAmt;
    private String mngOrgCd;
    private String trNo;

    //거래내역조회 [CPIF_AFFL_253]
    private String trKind;
    private String trFromDt;
    private String trToDt;
    private String pageNo;
    private String rowCnt;

    //회원삭제

    //계좌삭제

}