package com.aos.cms.coinage.config.Exception;

import lombok.Getter;

public enum ErrorCode {

     NOT_NULL("ERROR_CODE_0001","필수값이 누락되었습니다.")
     , EMPTY_VALUE("ERROR_CODE_0002", "필수값이 누락되었습니다.")
    ;

    @Getter
    private String code;

    @Getter
    private String description;

    ErrorCode(String code, String description) {
        this.code = code;
        this.description = description;
    }

}
