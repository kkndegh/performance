package com.aos.cms.coinage.common.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class ValidDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private boolean chk;
    private String msg;

}