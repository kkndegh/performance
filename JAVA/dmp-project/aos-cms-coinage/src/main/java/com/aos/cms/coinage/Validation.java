package com.aos.cms.coinage;

/**
 * validation constants
 * 
 * @author kh
 * @since 2022.10.26
 */
public class Validation {

    /**
     * 쿠콘 validation
     */
    //회원조회
    public static final String V_828_1 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_828]:: " + "ci값 또는 memb_cd 값 중 하나는 필수 입니다.";
    public static final String V_828_2 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_828]:: " + "biz_no값 또는 memb_cd 값 중 하나는 필수 입니다.";
    
    //회원가입
    public static final String V_560_1 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_560]:: " + "회원명은 필수 입니다.";
    public static final String V_560_2 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_560]:: " + "휴대폰번호는 필수 입니다.";
    public static final String V_560_3 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_560]:: " + "ci는 필수 입니다.";
    public static final String V_560_4 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_560]:: " + "생년월일은 필수 입니다.";
    public static final String V_560_5 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_560]:: " + "성별은 필수 입니다.";
    public static final String V_560_6 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_560]:: " + "통신사는 필수 입니다.";
    public static final String V_560_7 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_560]:: " + "외국인 구분은 필수 입니다.";

    public static final String V_561_1 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_561]:: " + "사업자 번호는 필수 입니다.";
    public static final String V_561_2 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_561]:: " + "사업장명은 필수 입니다.";
    public static final String V_561_3 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_561]:: " + "대표이메일은 필수 입니다.";
    public static final String V_561_4 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_561]:: " + "대표자명은 필수 입니다.";
    public static final String V_561_5 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_561]:: " + "휴대폰번호는 필수 입니다.";
    public static final String V_561_6 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_561]:: " + "통신사는 필수 입니다.";
    public static final String V_561_7 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_561]:: " + "생년월일은 구분은 필수 입니다.";
    public static final String V_561_8 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_561]:: " + "성별은 구분은 필수 입니다.";
    public static final String V_561_9 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_561]:: " + "내외국인구분은 필수 입니다.";

    //시스템계좌번호 조회
    public static final String V_813_1 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_813]:: " + "ci값 또는 memb_cd 값 중 하나는 필수 입니다.";
    public static final String V_813_2 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_813]:: " + "biz_no값 또는 memb_cd 값 중 하나는 필수 입니다.";
    public static final String V_813_3 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_813]:: " + "은행코드는 필수 입니다.";
    public static final String V_813_4 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_813]:: " + "계좌번호는 필수 입니다.";
    public static final String V_813_5 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_813]:: " + "사업자번호는 필수 입니다.";

    //계좌번호 검증
    public static final String V_821_1 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_821]:: " + "체크페이 회원코드는 필수 입니다.";
    public static final String V_821_2 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_821]:: " + "은행코드는 필수 입니다.";
    public static final String V_821_3 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_821]:: " + "계좌번호는 필수 입니다.";
    public static final String V_821_4 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_821]:: " + "계좌 검증 구분은 필수 입니다.";

    //계좌등록
    public static final String V_825_1 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_825]:: " + "체크페이 회원코드는 필수 입니다.";
    public static final String V_825_2 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_825]:: " + "은행코드는 필수 입니다.";
    public static final String V_825_3 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_825]:: " + "계좌번호는 필수 입니다.";
    public static final String V_825_4 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_825]:: " + "검증 거래 일자는 필수 입니다.";
    public static final String V_825_5 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_825]:: " + "검증 거래 번호는 필수 입니다.";
    public static final String V_825_6 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_825]:: " + "인증 종류는 필수 입니다.";
    public static final String V_825_7 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_825]:: " + "인증파일확장자는 필수 입니다.";
    public static final String V_825_8 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_825]:: " + "증빙일시는 필수 입니다.";
    public static final String V_825_9 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_825]:: " + "증빙데이터 크기는 필수 입니다.";
    public static final String V_825_10 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_825]:: " + "증빙 데이터는 필수 입니다.";

    //출금
    public static final String V_830_1 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_830]:: " + "주문일자는 필수 입니다.";
    public static final String V_830_2 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_830]:: " + "주문일련번호는 필수 입니다.";
    public static final String V_830_3 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_830]:: " + "ci값 또는 memb_cd 값 중 하나는 필수 입니다.";
    public static final String V_830_4 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_830]:: " + "biz_noi값 또는 memb_cd 값 중 하나는 필수 입니다.";
    public static final String V_830_5 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_830]:: " + "은행코드는 필수 입니다.";
    public static final String V_830_6 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_830]:: " + "체크페이 시스템 계좌번호는 필수 입니다.";
    public static final String V_830_7 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_830]:: " + "상품명은 필수 입니다.";
    public static final String V_830_8 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_830]:: " + "상품금액은 필수 입니다.";

    //거래내역조회
    public static final String V_253_1 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_253]:: " + "체크페이 회원코드는 필수 입니다.";
    public static final String V_253_2 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_253]:: " + "거래 유형은 필수 입니다.";
    public static final String V_253_3 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_253]:: " + "조회 시작일자는 필수 입니다.";
    public static final String V_253_4 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_253]:: " + "조회 종료 일자는 필수 입니다.";
    public static final String V_253_5 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_253]:: " + "페이지 번호는 필수 입니다.";
    public static final String V_253_6 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_253]:: " + "페이지당 ROW 수는 필수 입니다.";

    //회원삭제
    public static final String V_810_1 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_810]:: " + "회원코드(사업자번호)는 필수 입니다.";

    //계좌삭제
    public static final String V_826_1 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_826]:: " + "체크페이 회원코드는 필수 입니다.";
    public static final String V_826_2 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_826]:: " + "은행코드는 필수 입니다.";
    public static final String V_826_3 = "쿠콘 API 파라미터 오류 [CPIF_AFFL_826]:: " + "체크페이 시스템 계좌번호는 필수 입니다.";

    /**
     * 쿠콘 validation
     */
    //실시간공통부
    public static final String HS_COMMON_1 = "효성 전문 값 오류 [공통부]:: " + "업체ID는 필수 입니다.";
    public static final String HS_COMMON_2 = "효성 전문 값 오류 [공통부]:: " + "업체PW는 필수 입니다.";
    public static final String HS_COMMON_3 = "효성 전문 값 오류 [공통부]:: " + "SW-ID는 필수 입니다.";
    public static final String HS_COMMON_4 = "효성 전문 값 오류 [공통부]:: " + "SW-PW는 필수 입니다.";
    public static final String HS_COMMON_5 = "효성 전문 값 오류 [공통부]:: " + "송신일자는 필수 입니다.";
    public static final String HS_COMMON_6 = "효성 전문 값 오류 [공통부]:: " + "전문구분은 필수 입니다.";
    public static final String HS_COMMON_7 = "효성 전문 값 오류 [공통부]:: " + "업무구분은 필수 입니다.";
    public static final String HS_COMMON_8 = "효성 전문 값 오류 [공통부]:: " + "연번은 필수 입니다.";

    //회원조회
    public static final String HS_15_1 = "효성 전문 값 오류 [hs15]:: " + "회원번호는 필수 입니다.";

    //회원등록
    public static final String HS_20_1 = "효성 전문 값 오류 [hs20]:: " + "거래계좌은행코드는 필수 입니다.";
    public static final String HS_20_2 = "효성 전문 값 오류 [hs20]:: " + "거래계좌번호는 필수 입니다.";
    public static final String HS_20_3 = "효성 전문 값 오류 [hs20]:: " + "거래계좌 생년월일/사업자번호는 필수 입니다.";
    public static final String HS_20_4 = "효성 전문 값 오류 [hs20]:: " + "계좌예금주명은 필수 입니다.";
    public static final String HS_20_5 = "효성 전문 값 오류 [hs20]:: " + "회원번호는 필수 입니다.";

    //회원삭제
    public static final String HS_25_1 = "효성 전문 값 오류 [hs25]:: " + "거래계좌은행코드는 필수 입니다.";
    public static final String HS_25_2 = "효성 전문 값 오류 [hs25]:: " + "거래계좌번호는 필수 입니다.";
    public static final String HS_25_3 = "효성 전문 값 오류 [hs25]:: " + "거래계좌 생년월일/사업자번호는 필수 입니다.";
    public static final String HS_25_4 = "효성 전문 값 오류 [hs25]:: " + "계좌예금주명은 필수 입니다.";
    public static final String HS_25_5 = "효성 전문 값 오류 [hs25]:: " + "회원번호는 필수 입니다.";

    //회원 증빙
    public static final String HS_30_1 = "효성 전문 값 오류 [hs30]:: " + "업체ID는 필수 입니다.";
    public static final String HS_30_2 = "효성 전문 값 오류 [hs30]:: " + "업체PW는 필수 입니다.";
    public static final String HS_30_3 = "효성 전문 값 오류 [hs30]:: " + "SW-ID는 필수 입니다.";
    public static final String HS_30_4 = "효성 전문 값 오류 [hs30]:: " + "SW-PW는 필수 입니다.";
    public static final String HS_30_5 = "효성 전문 값 오류 [hs30]:: " + "회원번호는 필수 입니다.";
    public static final String HS_30_6 = "효성 전문 값 오류 [hs30]:: " + "증빙데이터는 필수 입니다.";

    //회원 증빙데이터
    public static final String HS_35_1 = "효성 전문 값 오류 [hs35]:: " + "업체ID는 필수 입니다.";
    public static final String HS_35_2 = "효성 전문 값 오류 [hs35]:: " + "회원번호는 필수 입니다.";
    public static final String HS_35_3 = "효성 전문 값 오류 [hs35]:: " + "증빙데이터는 필수 입니다.";

    //출금
    public static final String HS_40_1 = "효성 전문 값 오류 [hs40]:: " + "회원번호는 필수 입니다.";
    public static final String HS_40_2 = "효성 전문 값 오류 [hs40]:: " + "출금요청금액은 필수 입니다.";

    //출금확인
    public static final String HS_45_1 = "효성 전문 값 오류 [hs45]:: " + "원전문 전송일자는 필수 입니다.";
    public static final String HS_45_2 = "효성 전문 값 오류 [hs45]:: " + "원전문 연번은 필수 입니다.";
}