package com.aos.cms.coinage.cooKon.dto;

import java.io.Serializable;

import com.aos.cms.coinage.cooKon.dto.res.CpifCommonRes;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Step 간 통신 파라미터
 * 
 * @since 2022.10.13
 * @author kh
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class StepCooKonProcDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private CpifDTO params;

    private CpifCommonRes resultRes;
}