package com.aos.cms.coinage.hyoSung.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 요청 파라미터
 * 
 * @since 2022.10.13
 * @author sung
 */
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class HsDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String test = "";
    private String stepNo = "";
    private String apId = "";
    
    // 공통
    private String userID = "";
    private String userPW = "";
    private String userSubID = "";
    private String userSubPW = "";

    // 회원조회
    private String memberNo = "";

    // 회원등록
    private String bank = "";
    private String accountNo = "";
    private String reqNo = "";
    private String accountOwner = "";

    // 회원증빙
    private String evidenceFileUrl = "";
    private String evidenceFileSize = "";

    // 회원증빙 데이터
    private String encEvidenceFile = "";

    // 실시간 출금
    private String reqAmount = "";
    
    //출금결과조회
    private String postDate = "";
    private String postSerial = "";
}
