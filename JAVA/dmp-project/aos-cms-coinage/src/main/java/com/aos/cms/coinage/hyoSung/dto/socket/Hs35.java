package com.aos.cms.coinage.hyoSung.dto.socket;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 35. 회원증빙등록 DATA전문
 * 
 * @since 2022.10.17
 * @author kh
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Hs35 implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Hs35Dp1;
    private String Hs35Dp2;
    private String Hs35Dp3;
    private String Hs35Dp4;
    private String Hs35Dp5;
    private String Hs35Dp6;
    private String Hs35Dp7;
    private String Hs35Dp8;
    private String Hs35Dp9;
    private String Hs35Dp10;
}
