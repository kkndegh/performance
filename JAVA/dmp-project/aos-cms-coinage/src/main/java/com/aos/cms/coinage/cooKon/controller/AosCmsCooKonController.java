package com.aos.cms.coinage.cooKon.controller;

import com.aos.cms.coinage.cooKon.service.AosCmsCooKonService;
import com.aos.cms.coinage.cooKon.dto.CpifDTO;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

/**
 * AosCmsCoinage 컨트롤러
 * 
 * @author kh
 * @since 2022.09.13
 */
@RestController
@RequestMapping("/api/v1")
@EnableAsync
@RequiredArgsConstructor
public class AosCmsCooKonController {

    private final AosCmsCooKonService aosCmsCooKonService;

    /**
     * 쿠콘 집금 프로세스 API
     * 
     * @param cpifReq CpifDTO
     * @return ResponseEntity<String>
     * 
     * @TEST
     * http://localhost:9290/api/v1/cms/payment
     * 
     * "test":"Y" 는 테스트용 cookonId, cryptKey 사용하기 위해 사용.
     * //전체 플로워
     * //개인
     * {"test":"Y", "ap_id":"19000", "ci" : "123114A12G2xo5654dg412sAbvaaTEuqZiMcg==", "memb_nm" : "홍길동", "ph_no" : "01055554444", "birth_date" : "19800102", "gender" : "M", "tele_corp" : "01", "national_yn" : "Y", "fnni_cd" : "002", "acct_no" : "1234123412341234", "verify_type" : "0",
        "cert_kind" : "WRT", "cert_file_ets" : "jpg", "ds_dtime" : "20220922120000", "ds_data_size" : "50", "ds_data" : "aaaaaaaa",
        "prdt_nm" : "테스트 상품", "prdt_amt" : "1000000",
        "tr_kind" : "ALL", "tr_from_dt" : "20221001", "tr_to_dt" : "20221030", "page_no" : "1", "row_cnt" : "50"
        }

     *  //법인
     *  {"test":"Y", "ap_id":"17008", "biz_no" : "1005327445","comp_nm" : "온리원", "comp_email":"onlyone12@onlyone.com", "memb_nm" : "홍길동", "ph_no" : "01055554444", "birth_date" : "19800102", "gender" : "M", "tele_corp" : "01", "national_yn" : "Y", "fnni_cd" : "002", "acct_no" : "1234123412341234", "verify_type" : "0",
        "cert_kind" : "WRT", "cert_file_ets" : "jpg", "ds_dtime" : "20220922120000", "ds_data_size" : "50", "ds_data" : "aaaaaaaa",
        "prdt_nm" : "테스트 상품", "prdt_amt" : "1000000",
        "tr_kind" : "ALL", "tr_from_dt" : "20221001", "tr_to_dt" : "20221030", "page_no" : "1", "row_cnt" : "50"
        }

     *  //단일 호출
     *  {"test":"Y", "step_no" : "12" ,"ap_id":"17008","cp_memb_no" : "CB20221000000145"}
     */
    @PostMapping(value = "/cms/cookon")
    public ResponseEntity<String> procPaymentCookon(@Valid @RequestBody CpifDTO cpifReq) {

        String rs = aosCmsCooKonService.cooKonPaymentApi(cpifReq);

        return new ResponseEntity<String>(rs, HttpStatus.OK);
    }
}
