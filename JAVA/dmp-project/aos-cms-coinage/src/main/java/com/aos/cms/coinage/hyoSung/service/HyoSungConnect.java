package com.aos.cms.coinage.hyoSung.service;

import org.springframework.stereotype.Component;

import com.aos.cms.coinage.hyoSung.dto.HsDTO;
import com.aos.cms.coinage.hyoSung.dto.SocketDTO;
import com.aos.cms.coinage.hyoSung.socket.BaseAction;
import com.aos.cms.coinage.hyoSung.socket.XC_JavaSocket;

import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;

import com.aos.cms.coinage.Constant;
import com.aos.cms.coinage.common.BaseValue;
import com.aos.cms.coinage.common.dto.HttpParam;

/**
 * 효성 Soket Component
 * 
 * @author kh
 * @since 2022.10.26
 */
@Slf4j
@Component
public class HyoSungConnect extends BaseValue{

    private static BaseAction conn;

    /**
     * 효성 Soket Connect
     * 
     * @param paramStr String
     * @param params HsDTO
     * @return String
     */
    public void socket_connect(String SERVER_IP, int SERVER_PORT, String CONF){
        conn = new XC_JavaSocket();
        
         //서버접속
         if(conn.connect(SERVER_IP, SERVER_PORT, CONF) < 0){
            log.info("[[효성 Socket 연결실패]]");
        }else{
            log.info("[[효성 Socket 연결성공]]");
        }
    }

    /**
     * 효성 Soket Real Connect
     * 
     * @param paramStr String
     * @param params HsDTO
     * @return String
     */
    public String socket_real_send(String paramStr, HsDTO params, SocketDTO socket){
        String fullText = paramStr;
        String sdata = "";

        log.info("====================================================");
        log.info("[전문 정보]");
        
        log.info("_전문 정상길이 : 320");
        log.info("_전문 실제길이 : " + fullText.getBytes().length);

        /*********************************************
         * CMS REQ 로그 저장 
         *********************************************/
        JSONObject reqObj = new JSONObject();
        reqObj.put("type", "hyosung");
        reqObj.put("type1", params.getStepNo());
        reqObj.put("type2", "1000");
        reqObj.put("ap_id", params.getApId());
        reqObj.put("packet_message", fullText);
        
        saveLog(reqObj);
        
        //전문 송신
        conn.sendData(fullText);
        log.info("[[송신정보]] :: " + fullText);

        //전문 수신
        sdata = new String(conn.recvData());
        byte[] tgr = sdata.getBytes();
        log.info("[[수신정보]] :: " + sdata);

        /*********************************************
         * CMS RES 로그 저장 
         *********************************************/
        JSONObject resObj = new JSONObject();
        resObj.put("type", "hyosung");
        resObj.put("type1", params.getStepNo());
        resObj.put("type2", "1100");
        resObj.put("ap_id", params.getApId());
        resObj.put("packet_message", sdata);

        saveLog(resObj);
        
        try {
            log.info("_처리결과코드 : " + new String(tgr, 72, 4));
        } catch (Exception e) {
            log.info("_처리결과코드 : " + e.toString());
        }
        log.info("====================================================");
        return sdata;
    }

    /**
     * 효성 Soket Confirm Connect
     * 
     * @param paramStr String
     * @param params HsDTO
     * @return String
     */
    public String socket_confirm_send(String paramStr, HsDTO params, SocketDTO socket){
        String fullText = paramStr;
        String sdata = "";

        log.info("====================================================");
        log.info("[전문 정보]");

        if(params.getStepNo().equals("30")){
            log.info("_전문 정상길이 : 202");
        }else if(params.getStepNo().equals("35")){
            log.info("_전문 정상길이 : 60102");
        }

        log.info("_전문 실제길이 : " + fullText.getBytes().length);

        /*********************************************
         * CMS REQ 로그 저장 
         *********************************************/
        JSONObject reqObj = new JSONObject();
        reqObj.put("type", "hyosung");
        reqObj.put("type1", params.getStepNo());
        reqObj.put("type2", "1000");
        reqObj.put("ap_id", params.getApId());
        reqObj.put("packet_message", fullText.replaceAll("\n", " ").replaceAll("\r", " "));
        
        saveLog(reqObj);
        
        //전문 송신
        conn.sendData(fullText);
        log.info("[[송신정보]] :: " + fullText);
        
        if(params.getStepNo().equals("30")){
            try {
                //전문 수신
                sdata = new String(conn.recvData());
                byte[] tgr = sdata.getBytes();
                log.info("[[수신정보]] :: " + sdata);

                /*********************************************
                 * CMS RES 로그 저장 
                 *********************************************/
                JSONObject resObj = new JSONObject();
                resObj.put("type", "hyosung");
                resObj.put("type1", params.getStepNo());
                resObj.put("type2", "1100");
                resObj.put("ap_id", params.getApId());
                resObj.put("packet_message", sdata.replaceAll("\n", " ").replaceAll("\r", " "));

                saveLog(resObj);

                log.info("_처리결과코드 : " + new String(tgr, 91, 4));
            } catch (Exception e) {
                log.info("_처리결과코드 : " + e.toString());
            }
        }else if(params.getStepNo().equals("35")){
            if(paramStr.substring(32 , 33).equals("Y")){
                try {
                    //전문 수신
                    sdata = new String(conn.recvData());
                    byte[] tgr = sdata.getBytes();
                    log.info("[[수신정보]] :: " + sdata);

                    /*********************************************
                     * CMS RES 로그 저장 
                     *********************************************/
                    JSONObject resObj = new JSONObject();
                    resObj.put("type", "hyosung");
                    resObj.put("type1", params.getStepNo());
                    resObj.put("type2", "1100");
                    resObj.put("ap_id", params.getApId());
                    resObj.put("packet_message", sdata.replaceAll("\n", " ").replaceAll("\r", " "));

                    saveLog(resObj);

                    log.info("_처리결과코드 : " + new String(tgr, 32, 1));
                } catch (Exception e) {
                    log.info("_처리결과코드 : " + e.toString());
                }
            }
        }
        log.info("====================================================");
        return sdata;
    }

    /**
     * 효성 Soket Connect
     * 
     * @param paramStr String
     * @param params HsDTO
     * @return String
     */
    public void socket_close(){
        //소켓종료
        int ret = conn.close();
        if(ret>0){
            log.info("_연결종료");
        }else{
            log.info("_연결종료 실패 : " + ret);
        }
        log.info("====================================================");
    }

    /**
     * 효성 통신 LOG 저장
     * 
     * @param obj JSONObject
     */
    public void saveLog(JSONObject obj){
        String cmsLogInfoStr = "";    //CMS 로그 정보

            /*********************************************
             * 효성 로그 저장 
             *********************************************/
            String cmsLogInfoUrl = aosBaseUrl + "/api/v1/cmsLog/packetSaveHs";
            String cmsLogInfoMethod = "POST";
            String cmsLogInfoHeader = "{\"access_token\":\"qwerty\"}";
            String cmsLogInfoBody = obj.toString();

            log.debug("\n[[CMS 로그 저장 데이터 셋팅]]");
            log.debug("cmsLogInfoUrl :: "+cmsLogInfoUrl);
            log.debug("cmsLogInfoMethod :: "+cmsLogInfoMethod);
            log.debug("cmsLogInfoHeader :: "+cmsLogInfoHeader);
            log.debug("cmsLogInfoBody :: "+cmsLogInfoBody);

            HttpParam cmsLogInfoParam = 
                new HttpParam(Constant.CMS_LOG_TITLE, cmsLogInfoUrl, cmsLogInfoMethod, cmsLogInfoHeader, cmsLogInfoBody);    //통신파라미터 초기화

            //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
            cmsLogInfoStr = httpUtil.callApiHttps(cmsLogInfoParam);

            log.info("로그 저장 결과 :: " + cmsLogInfoStr + "\n");
    }
}