package com.aos.cms.coinage.cooKon.service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import com.aos.cms.coinage.common.dto.DateParamDTO;
import com.aos.cms.coinage.cooKon.dto.CpifDTO;
import com.aos.cms.coinage.cooKon.dto.StepCooKonProcDTO;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;

/**
 * AosCmsCoinage 서비스
 * 
 * @author kh
 * @since 2022.09.13
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class AosCmsCooKonService {

    private final CooKonApiService cooKonApiService;

    /**
     * 쿠콘 집금 Process
     * 
     * @param params CpifDTO
     * @return String
     */
    public String cooKonPaymentApi(CpifDTO params){
        JSONObject rstObj = new JSONObject();

        try {
            if(StringUtils.isNotEmpty(params.getStepNo())){
                String rst = stepCookonProc(params);
                
                rstObj.put("result", "SUCCESS");
                rstObj.put("msg", "쿠콘 Proc 실행완료");
                rstObj.put("response", rst);
            }else{
                new Thread(() -> {
                    params.setStepNo("1");
                    stepCookonProcs(params);
                }).start();

                rstObj.put("result", "SUCCESS");
                rstObj.put("msg", "쿠콘 Thread Proc 실행완료");
            }
        } catch (Exception e) {
            rstObj.put("result", "FAIL");
            rstObj.put("msg", "쿠콘 Proc 실패");

            log.error("쿠콘 API Proc 에러발생 :: " + e.toString());
        }
        return rstObj.toString();
    }

    /* 
    * 쿠콘 프로세스
    *
    * [[stepNo]]
    * 1 : 회원조회
    * 2 : 회원가입
    * 4 : 계좌검증
    * 6 : 계좌등록
    * 7 : 출금
    * 8 : 거래내역조회
    * 13 : 시스템계좌번호조회
    * */
    private void stepCookonProcs(CpifDTO params){
        StepCooKonProcDTO stepObj = new StepCooKonProcDTO(params, null);
        boolean status = true;  //루프 종료 플레그
        boolean timeBoolean = true; //반복 초 제한 플레그(재시도 시 nowTime.getSecond()에 걸리도록 설정)
        
        LocalDateTime nowTime = LocalDateTime.now();
        LocalDateTime nextTime = LocalDateTime.now();

        //결제요청 시간 저장
        String dt = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        String tm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
        DateParamDTO dateParam = new DateParamDTO(dt, tm);

        while(status){
            try {
                nowTime = LocalDateTime.now();
                if (nowTime.isAfter(nextTime)){
                    if (timeBoolean ? (nowTime.getSecond() == 0 || nowTime.getSecond() == 20 || nowTime.getSecond() == 40) : true){
                        try {
                            if(params.getStepNo().equals("1")){
                                //1. 회원조회 [CPIF_AFFL_828]
                                log.info("1. 회원조회 [CPIF_AFFL_828]");
                                stepObj = cooKonApiService.cpifAffl828(params, dateParam);
                                //반복 초 제한 플레그 설정
                                timeBoolean = false;
                            } else if(params.getStepNo().equals("2")){
                                //2. 회원가입 [CPIF_AFFL_560]
                                log.info("2. 회원가입 [CPIF_AFFL_560]");
                                stepObj = cooKonApiService.cpifAffl560(params, dateParam);
                            } else if(params.getStepNo().equals("4")){
                                //4. 계좌검증 [CPIF_AFFL_821]
                                log.info("4. 계좌검증 [CPIF_AFFL_821]");
                                stepObj = cooKonApiService.cpifAffl821(params, dateParam);
                            } else if(params.getStepNo().equals("13")){
                                //13. 시스템계좌번호조회 [CPIF_AFFL_813]
                                log.info("13. 시스템계좌번호조회 [CPIF_AFFL_813]");
                                stepObj = cooKonApiService.cpifAffl813(params, dateParam);
                            } else if(params.getStepNo().equals("6")){
                                //6. 계좌등록 [CPIF_AFFL_825]
                                log.info("6. 계좌등록 [CPIF_AFFL_825]");
                                stepObj = cooKonApiService.cpifAffl825(params, dateParam);
                            } else if(params.getStepNo().equals("7")){
                                //7. 출금 [CPIF_AFFL_830]
                                log.info("7. 출금 [CPIF_AFFL_830]");
                                stepObj = cooKonApiService.cpifAffl830(params, dateParam);
                            } else if(params.getStepNo().equals("8")){
                                //8. 거래내역조회 [CPIF_AFFL_253]
                                log.info("8. 거래내역조회 [CPIF_AFFL_253]");
                                stepObj = cooKonApiService.cpifAffl253(params, dateParam);
                                
                                if(params.getStepNo().equals("999")){
                                    log.info("==처리완료. 루프 종료.");
                                    log.info("== 처리완료. [SUCCESS]");
                                    status = false;
                                }
                            } else if(Integer.parseInt(params.getStepNo()) >= 900 && Integer.parseInt(params.getStepNo()) < 920){
                                log.error("==Exception 발생으로 인한 proc 종료");
                                break;
                            }
                        } catch (Exception e) {
                            log.error("stepCookonProcs 에러발생 :: " + e.toString());
                        }
                    }
                }
                //1초대기
                Thread.sleep(1000);
            } catch (Exception e) {
                log.error("쿠콘 API연동 에러 :: " + e.toString());
            }
        }
    }

    /* 
    * 쿠콘 단일 프로세스
    *
    * [[stepNo]]
    * 1 : 회원조회
    * 2 : 회원가입
    * 4 : 계좌검증
    * 6 : 계좌등록
    * 7 : 출금
    * 8 : 거래내역조회
    * 12 : 회원삭제
    * 13 : 시스템계좌번호조회
    * 16 : 계좌삭제
    * */
    private String stepCookonProc(CpifDTO params){
        JSONObject rst = null;
        StepCooKonProcDTO stepObj = new StepCooKonProcDTO(params, null);

        Date date = new Date();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss");

        //결제요청 시간 저장
        DateParamDTO dateParam = new DateParamDTO(sdf1.format(date), sdf2.format(date));

        try {
            log.info("요청 STEP_NO [" + params.getStepNo() + "] 실행");

            if(params.getStepNo().equals("1")){
                //1. 회원조회 [CPIF_AFFL_828]
                log.info("1. 회원조회 [CPIF_AFFL_828]");
                stepObj = cooKonApiService.cpifAffl828(params, dateParam);
            } else if(params.getStepNo().equals("2")){
                //2. 회원가입 [CPIF_AFFL_560]
                log.info("2. 회원가입 [CPIF_AFFL_560]");
                stepObj = cooKonApiService.cpifAffl560(params, dateParam);
            } else if(params.getStepNo().equals("4")){
                //4. 계좌검증 [CPIF_AFFL_821]
                log.info("4. 계좌검증 [CPIF_AFFL_821]");
                stepObj = cooKonApiService.cpifAffl821(params, dateParam);
            } else if(params.getStepNo().equals("13")){
                //13. 시스템계좌번호조회 [CPIF_AFFL_813]
                log.info("13. 시스템계좌번호조회 [CPIF_AFFL_813]");
                stepObj = cooKonApiService.cpifAffl813(params, dateParam);
            } else if(params.getStepNo().equals("6")){
                //6. 계좌등록 [CPIF_AFFL_825]
                log.info("6. 계좌등록 [CPIF_AFFL_825]");
                stepObj = cooKonApiService.cpifAffl825(params, dateParam);
            } else if(params.getStepNo().equals("7")){
                //7. 출금 [CPIF_AFFL_830]
                log.info("7. 출금 [CPIF_AFFL_830]");
                stepObj = cooKonApiService.cpifAffl830(params, dateParam);
            } else if(params.getStepNo().equals("8")){
                //8. 거래내역조회 [CPIF_AFFL_253]
                log.info("8. 거래내역조회 [CPIF_AFFL_253]");
                stepObj = cooKonApiService.cpifAffl253(params, dateParam);
            } else if(params.getStepNo().equals("12")){
                //12. 회원삭제 [CPIF_AFFL_810]
                log.info("12. 회원삭제 [CPIF_AFFL_810]");
                stepObj = cooKonApiService.cpifAffl810(params, dateParam);
            } else if(params.getStepNo().equals("16")){
                //16. 계좌삭제 [CPIF_AFFL_826]
                log.info("16. 계좌삭제 [CPIF_AFFL_826]");
                stepObj = cooKonApiService.cpifAffl826(params, dateParam);
            }
        } catch (Exception e) {
            log.error("stepCookonProc 에러발생 :: " + e.toString());
        } finally{
            rst = JSONObject.fromObject(stepObj.getResultRes());
        }
        return rst.toString();
    }
}
