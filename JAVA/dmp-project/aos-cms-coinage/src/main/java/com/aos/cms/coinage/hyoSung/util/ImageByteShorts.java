package com.aos.cms.coinage.hyoSung.util;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

import java.net.URL;

import java.io.*;

import org.apache.commons.codec.binary.Base64;

import java.awt.AlphaComposite;
import java.awt.RenderingHints;

/**
 * 이미지 유틸
 * 
 * @author kh
 * @since 2022.10.28
 */
@Component
@Slf4j
public class ImageByteShorts {
    
    //URL로 이미지 얻어오기
    public BufferedImage getUrlToImage(String imageUrl){
        BufferedImage originalImage = null;

        try {
            // URL로 부터 이미지 읽기
            URL url = new URL(imageUrl);
            originalImage = ImageIO.read(url);
            log.debug("==이미지 생성 완료");
        } catch (Exception e) {
            log.error("이미지 유틸 초기화 오류 :: " + e.toString());
        }

        return originalImage;
    }

    //이미지 사이즈 조회
    public int getImageVolum(BufferedImage image){
        int volum = 0;

        try {
            byte[] bytes = toByteArray(image, "jpg");
            volum = bytes.length;
            log.debug("==이미지 사이즈 조회 완료 :: " + volum);
        } catch (Exception e) {
            log.error("이미지 사이즈 조회 오류 :: " + e.toString());
        }
        return volum;
    }

    //이미지 용량 체크 및 축소
    public BufferedImage getShortImage(BufferedImage originalImage){
        log.debug("######[[이미지 리사이징]]######");
        BufferedImage rtn = null;
        //double ratio;
        int ratio = 100;

        try {
            byte[] imageBytes = toByteArray(originalImage, "jpg");
            log.debug("원본 이미지 용량 :: " + imageBytes.length);
            
            //이미지 용량 500000 이상인 경우 리사이징
            if(imageBytes.length > 500000){
                while(true){
                    // 크기 조정
                    int height = originalImage.getHeight();
                    int width = originalImage.getWidth();

                    // ratio = 200/(double)width;

                    // width = (int)(width * ratio); 
                    // height = (int)(height * ratio);

                    width = width - ratio; 
                    height = height - ratio;
                    
                    int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
                    BufferedImage resizeImage = resizeImageWithHint(originalImage, type, width, height);

                    byte[] resizeImageBytes = toByteArray(resizeImage, "png");
                    if(resizeImageBytes.length < 500000){
                        log.debug("리사이징 이미지 용량 :: " + resizeImageBytes.length);
                        log.debug("리사이징 이미지 base64 용량 :: " + imageToBase64(resizeImageBytes).length());
                        log.debug("################################################");
                        rtn = resizeImage;

                        break;
                    }else{
                        // originalImage = resizeImage;
                        ratio += 5;
                    }
                }
            }
        } catch (Exception e) {
            log.error("이미지 용량 축소 및 인코딩 오류 :: " + e.toString());
        }
        return rtn;
    }

    // 이미지를 바이트로 변환
    public byte[] toByteArray(BufferedImage bi, String format) {
        byte[] bytes = null;

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bi, format, baos);
            bytes = baos.toByteArray();
        } catch (Exception e) {
            log.error("이미지를 바이트로 변환 오류 :: " + e.toString());
        }
        return bytes;
    }

    //이미지 String을 Base64 인코딩
    public String imageToBase64(byte[] fileContent) {
        String encodedString = null;

        try {
            encodedString = new String(Base64.encodeBase64(fileContent));
        } catch (Exception e) {
            log.debug(e.toString());
        }
        return encodedString;
    }

    //이미지 리사이징
    private BufferedImage resizeImageWithHint(BufferedImage originalImage, int type, int width, int height) {
        BufferedImage resizedImage = new BufferedImage(width, height, type);

        Graphics2D graphics2d = resizedImage.createGraphics();
        graphics2d.drawImage(originalImage, 0, 0, width, height, null);
        graphics2d.dispose();
        graphics2d.setComposite(AlphaComposite.Src);
        // 보간 관련
        graphics2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        // 렌더링 관련
         graphics2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
        // 안티엘리어싱 여부
         graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        return resizedImage;
    }
}