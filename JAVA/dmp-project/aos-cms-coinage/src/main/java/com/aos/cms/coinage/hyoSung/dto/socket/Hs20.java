package com.aos.cms.coinage.hyoSung.dto.socket;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 20. 실시간CMS(정기출금) 실시간 회원등록 개별부
 * 
 * @since 2022.10.13
 * @author sung
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Hs20 extends HsHeader implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Hs20Dp1;
    private String Hs20Dp2;
    private String Hs20Dp3;
    private String Hs20Dp4;
    private String Hs20Dp5;
    private String Hs20Dp6;
    private String Hs20Dp7;
    private String Hs20Dp8;
    private String Hs20Dp9;
    private String Hs20Dp10;
    private String Hs20Dp11;
    private String Hs20Dp12;
    private String Hs20Dp13;
    private String Hs20Dp14;
    private String Hs20Dp15;
    private String Hs20Dp16;

}
