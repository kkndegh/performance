package com.aos.cms.coinage.hyoSung.dto.socket;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 15. 실시간CMS(정기출금) 실시간 회원조회 개별부
 * 
 * @since 2022.10.13
 * @author sung
 */
@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Hs15 extends HsHeader implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Hs15Dp1;
    private String Hs15Dp2;
    private String Hs15Dp3;
    private String Hs15Dp4;
    private String Hs15Dp5;
    private String Hs15Dp6;
    private String Hs15Dp7;
    private String Hs15Dp8;
    private String Hs15Dp9;
    private String Hs15Dp10;
    private String Hs15Dp11;
    private String Hs15Dp12;
}
