package com.aos.cms.coinage.cooKon.service;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLDecoder;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import net.sf.json.JSONObject;

import com.aos.cms.coinage.cooKon.dto.res.Cpif253Res;
import com.aos.cms.coinage.cooKon.dto.res.Cpif560Res;
import com.aos.cms.coinage.cooKon.dto.res.Cpif810Res;
import com.aos.cms.coinage.cooKon.dto.res.Cpif813Res;
import com.aos.cms.coinage.cooKon.dto.res.Cpif821Res;
import com.aos.cms.coinage.cooKon.dto.res.Cpif825Res;
import com.aos.cms.coinage.cooKon.dto.res.Cpif826Res;
import com.aos.cms.coinage.cooKon.dto.res.Cpif828Res;
import com.aos.cms.coinage.cooKon.dto.res.Cpif830Res;
import com.aos.cms.coinage.cooKon.dto.res.CpifCommonRes;
import com.aos.cms.coinage.Constant;
import com.aos.cms.coinage.Validation;
import com.aos.cms.coinage.common.BaseValue;
import com.aos.cms.coinage.common.dto.DateParamDTO;
import com.aos.cms.coinage.common.dto.HttpParam;
import com.aos.cms.coinage.common.dto.ValidDTO;
import com.aos.cms.coinage.cooKon.dto.CpifDTO;
import com.aos.cms.coinage.cooKon.dto.StepCooKonProcDTO;
import com.checkpay.util.SecurityUtil;

/**
 * CooKonApi 서비스
 * 
 * @author kh
 * @since 2022.09.23
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class CooKonApiService extends BaseValue{
    
    //URL
    @Value("${cookon.api.url}")
    private String cookonUrl;

    // 1. 회원조회
    public StepCooKonProcDTO cpifAffl828(CpifDTO params, DateParamDTO dp){
        StepCooKonProcDTO rtnObj = null;
        CpifCommonRes resultObj = null;

        try {
            String serviceCode = "CPIF_AFFL_828";

            //파라미터 셋팅
            String paramStr = paramParmProc(params);

            ValidDTO vd = paramParmValidation(params, paramStr);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepCooKonProcDTO(params, resultObj);
                return rtnObj;
            }

            // 수신 부
            String resp = cookonApiSConnect(paramStr, serviceCode, dp, params);
            JSONObject respDataObj = JSONObject.fromObject(resp);

            String respData = respDataObj.get("respData").toString();   //응답 전체 데이터
            String rc = respDataObj.get("rRC").toString();              //응답코드
            String rm = respDataObj.get("rRM").toString();              //응답 메시지
            String dEV = respDataObj.get("dEV").toString();             //EV값 디코딩
            String rCode = respDataObj.get("code").toString();          //인,디코딩 확인 코드
            String rMsg = respDataObj.get("msg").toString();            //인,디코딩 확인 메시지

            //결과 저장
            resultObj = objectMapper.readValue(respData, CpifCommonRes.class);
            
            //데이터 부
            Cpif828Res cpif828Obj = objectMapper.readValue(dEV, Cpif828Res.class);

            // rCode (1000 : 정상, 그외 : 오류)
            // rc (0000 : 정상 , G011 : 미가입자, 그외 : 비정상)
            // membStatCd ( 정상회원 : N, 일시정지 : S, 휴면회원 : D, 불가회원 : B)
            // 통신결과 확인
            if(StringUtils.isNotEmpty(rCode) && rCode.equals("1000")){
                if(rc.equals("G011")){
                    log.info("== 회원가입 여부 확인 완료 :: " + rm);
                    log.info("== 회원가입 실행 ::>>>>>>>>>>>>>>>>");
                    // -> 회원가입
                    params.setStepNo("2");
                } else if(rc.equals("0000") && cpif828Obj.getMembStatCd().equals("N")){
                    log.info("== 회원가입 여부 확인 완료.");
                    log.info("== 계좌조회 실행 ::>>>>>>>>>>>>>>>>");
                    // -> 계좌조회
                    params.setStepNo("13");
                    params.setCpMembNo(cpif828Obj.getCpMembNo());
                    params.setMembCd(cpif828Obj.getCpMembNo());
                } else if(cpif828Obj.getSvcRegYn().equals("N")){
                    log.info("== 회원가입 여부 확인 완료. 서비스 미가입.");
                    log.info("== 회원가입 실행 ::>>>>>>>>>>>>>>>>");
                    // -> 회원가입
                    params.setStepNo("2");
                } 
                else{
                    log.info("== 회원가입 여부 확인 완료 :: " + rm); 
                    params.setStepNo("901");
                }
            } else{
                log.info("== 회원가입 여부 확인 Proc 에러발생 :: " + rMsg);
                log.info("[" + rCode + "] " + rMsg);
                log.info("[data] " + respData);

                params.setStepNo("901");
            }
        } catch (Exception e) {
            log.error("쿠콘 API 연동 오류 [CPIF_AFFL_828]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepCooKonProcDTO(params, resultObj);
        }
        return rtnObj;
    }

    // 2. 회원가입
    public StepCooKonProcDTO cpifAffl560(CpifDTO params, DateParamDTO dp) {
        StepCooKonProcDTO rtnObj = null;
        CpifCommonRes resultObj = null;

        try {
            String serviceCode = "";

            // 560 : 개인 , 561 : 법인
            if(StringUtils.isNotEmpty(params.getBizNo())){
                serviceCode = "CPIF_AFFL_561";
            }else{
                serviceCode = "CPIF_AFFL_560";
            }

            //파라미터 셋팅
            String paramStr = paramParmProc(params);

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params, paramStr);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepCooKonProcDTO(params, resultObj);
                return rtnObj;
            }

            // 수신 부
            String resp = cookonApiSConnect(paramStr, serviceCode, dp, params);
            JSONObject respDataObj = JSONObject.fromObject(resp);

            String respData = respDataObj.get("respData").toString();   //응답 전체 데이터
            String rc = respDataObj.get("rRC").toString();              //응답코드
            String rm = respDataObj.get("rRM").toString();              //응답 메시지
            String dEV = respDataObj.get("dEV").toString();             //EV값 디코딩
            String rCode = respDataObj.get("code").toString();          //인,디코딩 확인 코드
            String rMsg = respDataObj.get("msg").toString();            //인,디코딩 확인 메시지
            
            //결과 저장
            resultObj = objectMapper.readValue(respData, CpifCommonRes.class);

            //데이터 부
            Cpif560Res cpif560Res = objectMapper.readValue(dEV, Cpif560Res.class);
            
            // rCode (1000 : 정상, 그외 : 오류)
            // rc (0000 : 정상 , G011 : 미가입자, 그외 : 비정상)
            // membStatCd ( 정상회원 : N, 일시정지 : S, 휴면회원 : D, 불가회원 : B)
            // 통신결과 확인
            if(StringUtils.isNotEmpty(rCode) && rCode.equals("1000")){
                if(rc.equals("0000")){
                    log.info("== 회원가입 완료");
                    log.info("== 회원가입 여부 확인 실행 ::>>>>>>>>>>>>>>>>");
                    // -> 회원조회
                    params.setStepNo("1");
                    params.setCpMembNo(cpif560Res.getCpMembNo());
                    params.setMembCd(cpif560Res.getCpMembNo());
                }else{
                    log.info("== 회원가입 실패 :: " + rm);
                    params.setStepNo("902");
                }
            }else{
                log.info("== 회원가입 Proc 에러발생 :: " + rMsg);
                log.info("[" + rCode + "] " + rMsg);
                log.info("[data] " + respData);

                params.setStepNo("902");
            }
        } catch (Exception e) {
            log.error("쿠콘 API 연동 오류 [CPIF_AFFL_560]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepCooKonProcDTO(params, resultObj);
        }
        return rtnObj;
    }

    // 13. 시스템계좌번호조회
    public StepCooKonProcDTO cpifAffl813(CpifDTO params, DateParamDTO dp) {
        StepCooKonProcDTO rtnObj = null;
        CpifCommonRes resultObj = null;
        
        try {
            String serviceCode = "CPIF_AFFL_813";

            //파라미터 셋팅
            String paramStr = paramParmProc(params);

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params, paramStr);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepCooKonProcDTO(params, resultObj);
                return rtnObj;
            }
 
            // 수신 부
            String resp = cookonApiSConnect(paramStr, serviceCode, dp, params);
            JSONObject respDataObj = JSONObject.fromObject(resp);

            String respData = respDataObj.get("respData").toString();   //응답 전체 데이터
            String rc = respDataObj.get("rRC").toString();              //응답코드
            String rm = respDataObj.get("rRM").toString();              //응답 메시지
            String dEV = respDataObj.get("dEV").toString();             //EV값 디코딩
            String rCode = respDataObj.get("code").toString();          //인,디코딩 확인 코드
            String rMsg = respDataObj.get("msg").toString();            //인,디코딩 확인 메시지
            
            //결과 저장
            resultObj = objectMapper.readValue(respData, CpifCommonRes.class);
            
            //데이터 부
            Cpif813Res cpif813Res = objectMapper.readValue(dEV, Cpif813Res.class);

            // 통신결과 확인
            if(StringUtils.isNotEmpty(rCode) && rCode.equals("1000")){
                if(rCode.equals("1000") && StringUtils.equals(params.getAcctNo(), cpif813Res.getAcctNo())){
                    if(rc.equals("0000")){
                        log.info("== 시스템계좌번호조회 완료");
                        log.info("== 출금 실행 ::>>>>>>>>>>>>>>>>");
                        // -> 출금
                        params.setStepNo("7");
                        params.setCpSysAcno(cpif813Res.getCpSysAcno());
                    }else{
                        if(StringUtils.isNotEmpty(params.getBizNo())){
                            log.info("== 시스템계좌번호조회 완료 :: " + rm);
                            log.info("== 계좌등록 실행 ::>>>>>>>>>>>>>>>>");
                            // -> 계좌등록
                            params.setStepNo("6");
                        }else{
                            log.info("== 시스템계좌번호조회 완료 :: " + rm);
                            log.info("== 계좌검증 실행 ::>>>>>>>>>>>>>>>>");
                            // -> 계좌검증
                            params.setStepNo("4");
                        }
                    }
                }
            } else{
                log.info("== 시스템계좌조회 Proc 에러발생 :: " + rMsg);
                log.info("[" + rCode + "] " + rMsg);
                log.info("[data] " + respData);

                params.setStepNo("903");
            }
        } catch (Exception e) {
            log.error("쿠콘 API 연동 오류 [CPIF_AFFL_813]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepCooKonProcDTO(params, resultObj);
        }
        return rtnObj;
    }

    // 4. 계좌검증 
    public StepCooKonProcDTO cpifAffl821(CpifDTO params, DateParamDTO dp) {
        StepCooKonProcDTO rtnObj = null;
        CpifCommonRes resultObj = null;

        try {
            String serviceCode = "CPIF_AFFL_821";

            //파라미터 셋팅
            String paramStr = paramParmProc(params);

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params, paramStr);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepCooKonProcDTO(params, resultObj);
                return rtnObj;
            }

            // 수신 부
            String resp = cookonApiSConnect(paramStr, serviceCode, dp, params);
            JSONObject respDataObj = JSONObject.fromObject(resp);

            String respData = respDataObj.get("respData").toString();   //응답 전체 데이터
            String rc = respDataObj.get("rRC").toString();              //응답코드
            String rm = respDataObj.get("rRM").toString();              //응답 메시지
            String dEV = respDataObj.get("dEV").toString();             //EV값 디코딩
            String rCode = respDataObj.get("code").toString();          //인,디코딩 확인 코드
            String rMsg = respDataObj.get("msg").toString();            //인,디코딩 확인 메시지
            
            //결과 저장
            resultObj = objectMapper.readValue(respData, CpifCommonRes.class);

            //데이터 부
            Cpif821Res cpif821Res = objectMapper.readValue(dEV, Cpif821Res.class);
            
            // 통신결과 확인
            if(StringUtils.isNotEmpty(rCode) && rCode.equals("1000")){
                if(rc.equals("0000")){
                    log.info("== 계좌검증 완료");
                    log.info("== 계좌등록 실행 ::>>>>>>>>>>>>>>>>");
                    // -> 계좌등록
                    params.setStepNo("6");

                    params.setVerifyTrDt(cpif821Res.getVerifyTrDt());
                    params.setVerifyTrNo(cpif821Res.getVerifyTrNo());
                }else{
                    log.info("== 계좌검증 완료 :: " + rm);
                    params.setStepNo("904");
                }
            } else{
                log.info("== 시스템계좌조회 Proc 에러발생 :: " + rMsg);
                log.info("[" + rCode + "] " + rMsg);
                log.info("[data] " + respData);

                params.setStepNo("904");
            }
        } catch (Exception e) {
            log.error("쿠콘 API 연동 오류 [CPIF_AFFL_821]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepCooKonProcDTO(params, resultObj);
        }
        return rtnObj;
    }

    // 6. 계좌등록
    public StepCooKonProcDTO cpifAffl825(CpifDTO params, DateParamDTO dp) {
        StepCooKonProcDTO rtnObj = null;
        CpifCommonRes resultObj = null;

        try {
            String serviceCode = "CPIF_AFFL_825";

            //파라미터 셋팅
            String paramStr = paramParmProc(params);

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params, paramStr);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepCooKonProcDTO(params, resultObj);
                return rtnObj;
            }

            // 수신 부
            String resp = cookonApiSConnect(paramStr, serviceCode, dp, params);
            JSONObject respDataObj = JSONObject.fromObject(resp);

            String respData = respDataObj.get("respData").toString();   //응답 전체 데이터
            String rc = respDataObj.get("rRC").toString();              //응답코드
            String rm = respDataObj.get("rRM").toString();              //응답 메시지
            String dEV = respDataObj.get("dEV").toString();             //EV값 디코딩
            String rCode = respDataObj.get("code").toString();          //인,디코딩 확인 코드
            String rMsg = respDataObj.get("msg").toString();            //인,디코딩 확인 메시지
            
            //결과 저장
            resultObj = objectMapper.readValue(respData, CpifCommonRes.class);

            //데이터 부
            Cpif825Res cpif825Res = objectMapper.readValue(dEV, Cpif825Res.class);
            
            if(StringUtils.isNotEmpty(rCode) && rCode.equals("1000")){
                if(rc.equals("0000") && cpif825Res.getAcctInfo().equals("X-" + params.getAcctNo().substring(params.getAcctNo().length() - 4))){
                    log.info("== 계좌등록 완료");
                    log.info("== 출금 실행 ::>>>>>>>>>>>>>>>>");
                    // -> 출금
                    params.setStepNo("7");
                    params.setCpSysAcno(cpif825Res.getCpSysAcno());
                }else{
                    log.info("== 계좌등록 Proc 에러발생 :: " + rm);
                    params.setStepNo("906");
                }
            }else{
                log.info("== 계좌등록 Proc 에러발생 :: " + rMsg);
                log.info("[" + rCode + "] " + rMsg);
                log.info("[data] " + respData);

                params.setStepNo("906");
            }
        } catch (Exception e) {
            log.error("쿠콘 API 연동 오류 [CPIF_AFFL_825]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepCooKonProcDTO(params, resultObj);
        }
        return rtnObj;
    }

    // 7. 출금
    public StepCooKonProcDTO cpifAffl830(CpifDTO params, DateParamDTO dp) {
        StepCooKonProcDTO rtnObj = null;
        CpifCommonRes resultObj = null;

        try {
            String serviceCode = "CPIF_AFFL_830";

            params.setOrderTrDt(dp.getTrxDt());
            params.setOrderTrNo(dp.getTrxTm());

            //파라미터 셋팅
            String paramStr = paramParmProc(params);

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params, paramStr);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepCooKonProcDTO(params, resultObj);
                return rtnObj;
            }

            // 수신 부
            String resp = cookonApiSConnect(paramStr, serviceCode, dp, params);
            JSONObject respDataObj = JSONObject.fromObject(resp);
 
            String respData = respDataObj.get("respData").toString();   //응답 전체 데이터
            String rc = respDataObj.get("rRC").toString();              //응답코드
            String rm = respDataObj.get("rRM").toString();              //응답 메시지
            String dEV = respDataObj.get("dEV").toString();             //EV값 디코딩
            String rCode = respDataObj.get("code").toString();          //인,디코딩 확인 코드
            String rMsg = respDataObj.get("msg").toString();            //인,디코딩 확인 메시지
            
            //결과 저장
            resultObj = objectMapper.readValue(respData, CpifCommonRes.class);

            //데이터 부
            Cpif830Res cpif830Res = objectMapper.readValue(dEV, Cpif830Res.class);

            // 통신결과 확인
            if(StringUtils.isNotEmpty(rCode) && rCode.equals("1000")){
                if(rc.equals("0000") && cpif830Res.getResCd().equals("070001")){
                    log.info("== 출금 완료");
                    log.info("== 거래내역 조회 실행 ::>>>>>>>>>>>>>>>>");
                    // -> 거래내역조회
                    params.setStepNo("8");
                    //params.setTrNo(cpif830Res.getTrNo());
                    
                    String ukey = dp.getTrxDt() + cpif830Res.getTrNo();
                    params.setUKey(ukey);
                }else{
                    log.info("== 출금 Proc 에러발생 :: " + rm);
                    params.setStepNo("907");
                }
            }else{
                log.info("== 출금 Proc 에러발생 :: " + rMsg);
                log.info("[" + rCode + "] " + rMsg);
                log.info("[data] " + respData);

                params.setStepNo("907");
            }
        } catch (Exception e) {
            log.error("쿠콘 API 연동 오류 [CPIF_AFFL_830]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepCooKonProcDTO(params, resultObj);
        }
        return rtnObj;
    }

    // 8. 거래내역조회
    public StepCooKonProcDTO cpifAffl253(CpifDTO params, DateParamDTO dp) {
        StepCooKonProcDTO rtnObj = null;
        CpifCommonRes resultObj = null;

        try {
            String serviceCode = "CPIF_AFFL_253";

            //파라미터 셋팅
            String paramStr = paramParmProc(params);

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params, paramStr);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepCooKonProcDTO(params, resultObj);
                return rtnObj;
            }

            // 수신 부
            String resp = cookonApiSConnect(paramStr, serviceCode, dp, params);
            JSONObject respDataObj = JSONObject.fromObject(resp);

            String respData = respDataObj.get("respData").toString();   //응답 전체 데이터
            String rc = respDataObj.get("rRC").toString();              //응답코드
            String rm = respDataObj.get("rRM").toString();              //응답 메시지
            String dEV = respDataObj.get("dEV").toString();             //EV값 디코딩
            String rCode = respDataObj.get("code").toString();          //인,디코딩 확인 코드
            String rMsg = respDataObj.get("msg").toString();            //인,디코딩 확인 메시지

            //결과 저장
            resultObj = objectMapper.readValue(respData, CpifCommonRes.class);

            //데이터 부
            Cpif253Res cpif253Res = objectMapper.readValue(dEV, Cpif253Res.class);

            // 통신결과 확인
            if(StringUtils.isNotEmpty(rCode) && rCode.equals("1000")){
                boolean chk = false;
                // String ukey = dp.getTrxDt() + params.getTrNo();
                String ukey = params.getUKey();

                if(cpif253Res.getTrListInfo().size() > 0){
                    for (int i = 0; i < cpif253Res.getTrListInfo().size(); i++){
                        if (rc.equals("0000") && StringUtils.equals(ukey, cpif253Res.getTrListInfo().get(i).getTrDt() + cpif253Res.getTrListInfo().get(i).getTrNo()) && cpif253Res.getTrListInfo().get(i).getTrStatNm().equals("070001")){
                            log.info("== 거래내역조회 완료");

                            // -> 프로세스 완료
                            params.setStepNo("999");

                            /*********************************************
                             * CMS 최종 로그 저장 
                             *********************************************/
                            JSONObject resObj = new JSONObject();
                            if(StringUtils.isNotEmpty(params.getUKey())){
                                resObj.put("ukey", params.getUKey());
                            }
                            resObj.put("type", "coinage");
                            resObj.put("type1", params.getStepNo());
                            resObj.put("type2", "0210");
                            resObj.put("ap_id", params.getApId());
                            resObj.put("message", "");
                            resObj.put("packet_message", "");

                            saveLog(resObj);
                            break;
                        }else if (rc.equals("0000") && StringUtils.equals(ukey, cpif253Res.getTrListInfo().get(i).getTrDt() + cpif253Res.getTrListInfo().get(i).getTrNo()) && !cpif253Res.getTrListInfo().get(i).getTrStatNm().equals("070001")){
                            log.info("== 거래내역조회 Proc 에러발생 :: " + rm);
                            chk = true;
                        }
                    }
                }else{
                    log.info("== 거래내역조회 Proc 에러발생 :: " + "거래 계좌 확인 불가");
                    chk = true;
                }
                
                if (!params.getStepNo().equals("90") && chk){
                    params.setStepNo("908");
                }
            }else{
                log.info("== 거래내역조회 Proc 에러발생 :: " + rMsg);
                log.info("[" + rCode + "] " + rMsg);
                log.info("[data] " + respData);

                params.setStepNo("908");
            }
        } catch (Exception e) {
            log.error("쿠콘 API 연동 오류 [CPIF_AFFL_253]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepCooKonProcDTO(params, resultObj);
        }
        return rtnObj;
    }

    // 12. 회원삭제
    public StepCooKonProcDTO cpifAffl810(CpifDTO params, DateParamDTO dp){
        StepCooKonProcDTO rtnObj = null;
        CpifCommonRes resultObj = null;

        try {
            String serviceCode = "CPIF_AFFL_810";

            //파라미터 셋팅
            String paramStr = paramParmProc(params);

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params, paramStr);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepCooKonProcDTO(params, resultObj);
                return rtnObj;
            }

            // 수신 부
            String resp = cookonApiSConnect(paramStr, serviceCode, dp, params);
            JSONObject respDataObj = JSONObject.fromObject(resp);

            String respData = respDataObj.get("respData").toString();   //응답 전체 데이터
            String rc = respDataObj.get("rRC").toString();              //응답코드
            String rm = respDataObj.get("rRM").toString();              //응답 메시지
            String dEV = respDataObj.get("dEV").toString();             //EV값 디코딩
            String rCode = respDataObj.get("code").toString();          //인,디코딩 확인 코드
            String rMsg = respDataObj.get("msg").toString();            //인,디코딩 확인 메시지

            //결과 저장
            resultObj = objectMapper.readValue(respData, CpifCommonRes.class);

            //데이터 부
            Cpif810Res cpif810Res = objectMapper.readValue(dEV, Cpif810Res.class);

            // 통신결과 확인
            if(StringUtils.isNotEmpty(rCode) && rCode.equals("1000")){
                if(rc.equals("0000")){
                    log.info("== 회원삭제 완료");
                    //stepNo = "90";
                }else{
                    log.info("== 회원삭제 실패 :: " + rm);
                    //stepNo = "99";
                }
            }else{
                log.info("[" + rCode + "] " + rMsg);
                log.info("[data] " + respData);

                params.setStepNo("912");
            }
        } catch (Exception e) {
            log.error("쿠콘 API 연동 오류 [CPIF_AFFL_810]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepCooKonProcDTO(params, resultObj);
        }
        return rtnObj;
    }

    // 16. 계좌삭제
    public StepCooKonProcDTO cpifAffl826(CpifDTO params, DateParamDTO dp) {
        StepCooKonProcDTO rtnObj = null;
        CpifCommonRes resultObj = null;

        try {
            String serviceCode = "CPIF_AFFL_826";

            //파라미터 셋팅
            String paramStr = paramParmProc(params);

            //벨리데이션 체크
            ValidDTO vd = paramParmValidation(params, paramStr);
            if(vd.isChk()){
                log.info(vd.getMsg());
                params.setStepNo("900");
                rtnObj = new StepCooKonProcDTO(params, resultObj);
                return rtnObj;
            }

            // 수신 부
            String resp = cookonApiSConnect(paramStr, serviceCode, dp, params);
            JSONObject respDataObj = JSONObject.fromObject(resp);

            String respData = respDataObj.get("respData").toString();   //응답 전체 데이터
            String rc = respDataObj.get("rRC").toString();              //응답코드
            String rm = respDataObj.get("rRM").toString();              //응답 메시지
            String dEV = respDataObj.get("dEV").toString();             //EV값 디코딩
            String rCode = respDataObj.get("code").toString();          //인,디코딩 확인 코드
            String rMsg = respDataObj.get("msg").toString();            //인,디코딩 확인 메시지

            //결과 저장
            resultObj = objectMapper.readValue(respData, CpifCommonRes.class);

            //데이터 부
            Cpif826Res cpif826Res = objectMapper.readValue(dEV, Cpif826Res.class);

            // 통신결과 확인
            if(StringUtils.isNotEmpty(rCode) && rCode.equals("1000")){
                if(rc.equals("0000")){
                    log.info("== 계좌삭제 완료");
                }else{
                    log.info("== 계좌삭제 실패 :: " + rm);
                }
            }else{
                log.info("[" + rCode + "] " + rMsg);
                log.info("[data] " + respData);

                params.setStepNo("916");
            }
        } catch (Exception e) {
            log.error("쿠콘 API 연동 오류 [CPIF_AFFL_826]:: " + e.toString());
            params.setStepNo("900");
        } finally{
            rtnObj = new StepCooKonProcDTO(params, resultObj);
        }
        return rtnObj;
    }

    /**
     * 쿠콘 API Connect
     * 
     * @param paramStr String
     * @param serviceCode String
     * @return JSONObject
     */
    private String cookonApiSConnect(String paramStr, String serviceCode, DateParamDTO dp, CpifDTO params){
        JSONObject rstobj = new JSONObject();
        String stepNo = params.getStepNo();

        //기관 ID
        String cookonId = params.getCookonId();
        //암호화 키
        String cryptKey = params.getCryptKey();

        if(params.getTest().equals("Y")){
            cookonId = "03000001";
            cryptKey = "checkpay&*#Ji$2iKyi8#dO8#dsI2NdV";
        }
        
        try {
            String trx_dt = dp.getTrxDt(); //거래일자 
            String trx_tm = dp.getTrxTm(); //거래시각 

            String EV = "";
            String VV = "";
            try {
                EV = SecurityUtil.EncryptAesBase64(trx_dt + trx_tm + paramStr, cryptKey, true);
                VV = SecurityUtil.getHmacSha256(paramStr, cryptKey, true);

                rstobj.put("code", "1000");
                rstobj.put("msg", "SUCCESS");
            } catch (Exception e) {
                //인,디코딩 관련 성공여부 코드
                rstobj.put("code", "9999");
                rstobj.put("msg", e.toString());
                return rstobj.toString();
            }
            
            log.info("\n\n\n");
            log.info("################발송정보################");
            log.info("[cookonId] :: " + cookonId);
            log.info("[cryptKey] :: " + cryptKey);
            log.info("[paramStr] :: " + paramStr);
            log.info("[serviceCode] :: " + serviceCode);
            
            String url = cookonUrl + serviceCode + ".jct"; 
            String data = "ID=" + cookonId + "&RQ_DTIME=" + trx_dt + trx_tm + "&TNO=" + trx_dt + trx_tm + "&EV=" + EV + "&VV=" + VV + "&EM=AES" + "&VM=HmacSHA256";

            log.info("[send url] :: " + url);
            log.info("[send data] :: " + data);

            /*********************************************
             * CMS REQ 로그 저장 
             *********************************************/
            JSONObject messageObj = new JSONObject();
            messageObj.put("ap_id", params.getApId());
            messageObj.put("state", stepNo);
            messageObj.put("certfilepath", params.getCertfilepath());
            messageObj.put("ID", cookonId);
            messageObj.put("RQ_DTIME", trx_dt + trx_tm);
            messageObj.put("TNO", trx_dt + trx_tm);
            messageObj.put("EV", EV);
            messageObj.put("VV", VV);
            messageObj.put("EM", "AES");
            messageObj.put("VM", "HmaxSHA256");

            JSONObject reqObj = new JSONObject();
            reqObj.put("type", "coinage");
            reqObj.put("type1", serviceCode.split("_")[2]);
            reqObj.put("type2", "0200");
            reqObj.put("ap_id", params.getApId());
            reqObj.put("message", messageObj);
            reqObj.put("packet_message", EV);
            
            saveLog(reqObj);
            
            HttpsURLConnection con = null;
            BufferedWriter bwriter = null;
            DataInputStream in = null;
            ByteArrayOutputStream bout = null;

            try { 
                URL req = new URL(url); 
                con = (HttpsURLConnection)req.openConnection(); 
                con.setConnectTimeout(3 * 1000); // 3 초 
                con.setReadTimeout(10 * 1000); // 10 초
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestMethod("POST"); 
                con.setRequestProperty("Content-Type", " application/x-www-form-urlencoded");
            
                bwriter = new BufferedWriter(new OutputStreamWriter(con.getOutputStream(), "UTF-8")); 
                bwriter.write(data);
                bwriter.flush(); 
            
                in = new DataInputStream(con.getInputStream()); 
                bout = new ByteArrayOutputStream(); 
 
                while (true) { 
                    byte[] buf = new byte[2048]; 
                    int n = in.read(buf); 
                    if (n == -1)
                        break; 
                    bout.write(buf, 0, n); 
                } 
                bout.flush();

                log.info("\n\n\n");
                log.info("################발송 결과################");
                log.info("{ResponseCode :: " + con.getResponseCode() + " ResponseMessage :: " + con.getResponseMessage() + "}");

            } catch(Exception e) { 
                log.error("쿠콘 통신 에러 발생 :: " + e.toString());
            } finally {
                try {
                    if ( bwriter != null ) bwriter.close(); 
                    if ( in != null ) in.close(); 
                    if ( bout != null ) bout.close(); 
                    if ( con != null ) con.disconnect(); 
                } catch(Exception e) { 
                    log.error("쿠콘 통신 커넥션 종료 에러 발생 :: " + e.toString());
                }
            }

            String respData = new String(bout.toByteArray(),"UTF-8");
            log.info("[respData] :: " + respData);
            JSONObject rtn = JSONObject.fromObject(respData);

            String rEV = rtn.getString("EV");
            String rVV = rtn.getString("VV");
            String rRC = rtn.getString("RC");
            String rRM = rtn.getString("RM");
            String rsDtime = rtn.getString("RS_DTIME");

            log.info("결과 코드 :: "+rRC);
            log.info("결과 메시지 :: "+rRM);

            if ( !SecurityUtil.VerifyMac(cryptKey, URLDecoder.decode(rEV, "UTF-8"), URLDecoder.decode(rVV, "UTF-8"), "", true) ) { //데이터검증
                log.info("<br/><br/>암호화 검증 Invalid");
            }
            
            String dEV = "";
            try {
                dEV = SecurityUtil.DecryptAesBase64(URLDecoder.decode(rEV, "UTF-8"), cryptKey, true);
                rstobj.put("code", "1000");
                rstobj.put("msg", "SUCCESS");
            } catch (Exception e) {
                //인,디코딩 관련 성공여부 코드
                rstobj.put("code", "9999");
                rstobj.put("msg", e.toString());
                return rstobj.toString();
            }
            
            JSONObject decEV = JSONObject.fromObject(dEV.substring(14));
            log.info("[decEVjsonString] :: "+ decEV);

            rstobj.put("respData", respData);
            rstobj.put("rRC", rRC);
            rstobj.put("rRM", rRM);
            rstobj.put("dEV", dEV.substring(14));

            /*********************************************
             * CMS RES 로그 저장 
             *********************************************/
            messageObj = new JSONObject();
            messageObj.put("ap_id", params.getApId());
            messageObj.put("state", stepNo);
            messageObj.put("certfilepath", params.getCertfilepath());
            if(StringUtils.isNotEmpty(params.getUKey())){
                messageObj.put("ukey", params.getUKey());
            }
            messageObj.put("ID", cookonId);
            messageObj.put("RS_DTIME", rsDtime);
            messageObj.put("TNO", trx_dt + trx_tm);
            messageObj.put("EV", rEV);
            messageObj.put("VV", rVV);
            messageObj.put("EM", "AES");
            messageObj.put("VM", "HmaxSHA256");
            messageObj.put("RC", rRC);
            messageObj.put("RM", rRM);

            JSONObject resObj = new JSONObject();
            if(StringUtils.isNotEmpty(params.getUKey())){
                resObj.put("ukey", params.getUKey());
            }
            resObj.put("type", "coinage");
            resObj.put("type1", serviceCode.split("_")[2]);
            resObj.put("type2", "0210");
            resObj.put("ap_id", params.getApId());
            resObj.put("message", messageObj);
            resObj.put("packet_message", rEV);

            saveLog(resObj);

        } catch (Exception e) {
            log.error("쿠콘 API 연동 오류 :: "+e.toString());
        }
        return rstobj.toString();
    }

    /**
     * 쿠콘 통신 파라미터 셋팅
     * 
     * @param cpifReq CpifDTO
     * @param stepNo String
     * @return String
     */
    private String paramParmProc(CpifDTO cpifReq){
        JSONObject paramJObj = new JSONObject();
        String stepNo = cpifReq.getStepNo();

        try {
            switch(stepNo){
                case "1" : //회원조회
                    paramJObj.put("ci", cpifReq.getCi());
                    paramJObj.put("memb_cd", cpifReq.getMembCd());
                    paramJObj.put("biz_no", cpifReq.getBizNo());
                    break;
                case "2" : //회원가입
                    if(StringUtils.isEmpty(cpifReq.getBizNo())){    //개인
                        paramJObj.put("ci", cpifReq.getCi());
                        paramJObj.put("app_id", cpifReq.getAppId());
                        paramJObj.put("app_ver", cpifReq.getAppVer());
                        paramJObj.put("dvc_os_nm", cpifReq.getDvcOsNm());
                        paramJObj.put("dvc_os_ver", cpifReq.getDvcOsVer());
                        paramJObj.put("dvc_mdl", cpifReq.getDvcMdl());
                        paramJObj.put("dvc_id", cpifReq.getDvcId());
                    }else{                                          //법인
                        paramJObj.put("biz_no", cpifReq.getBizNo());
                        paramJObj.put("comp_nm", cpifReq.getCompNm());
                        paramJObj.put("comp_email", cpifReq.getCompEmail());
                    }
                    paramJObj.put("memb_nm", cpifReq.getMembNm());
                    paramJObj.put("ph_no", cpifReq.getPhNo());
                    paramJObj.put("birth_date", cpifReq.getBirthDate());
                    paramJObj.put("gender", cpifReq.getGender());
                    paramJObj.put("tele_corp", cpifReq.getTeleCorp());
                    paramJObj.put("national_yn", cpifReq.getNationalYn());
                    break;
                case "13" : //시스템계좌번호조회
                    paramJObj.put("ci", cpifReq.getCi());
                    paramJObj.put("memb_nm", cpifReq.getMembNm());
                    paramJObj.put("fnni_cd", cpifReq.getFnniCd());
                    paramJObj.put("acct_no", cpifReq.getAcctNo());
                    paramJObj.put("biz_no", cpifReq.getBizNo());
                    break;
                case "4" : //계좌검증
                    paramJObj.put("cp_memb_no", cpifReq.getCpMembNo());
                    paramJObj.put("fnni_cd", cpifReq.getFnniCd());
                    paramJObj.put("acct_no", cpifReq.getAcctNo());
                    paramJObj.put("verify_type", cpifReq.getVerifyType());
                    paramJObj.put("verify_txt", cpifReq.getVerifyTxt());
                    break;
                case "6" : //계좌등록
                    paramJObj.put("cp_memb_no", cpifReq.getCpMembNo());
                    paramJObj.put("biz_no", cpifReq.getBizNo());
                    paramJObj.put("fnni_cd", cpifReq.getFnniCd());
                    paramJObj.put("acct_no", cpifReq.getAcctNo());
                    paramJObj.put("verify_tr_dt", cpifReq.getVerifyTrDt());
                    paramJObj.put("verify_tr_no", cpifReq.getVerifyTrNo());
                    paramJObj.put("verify_no", cpifReq.getVerifyNo());
                    paramJObj.put("cert_kind", cpifReq.getCertKind());
                    paramJObj.put("cert_file_ets", cpifReq.getCertFileEts());
                    paramJObj.put("ars_tr_dt", cpifReq.getArsTrDt());
                    paramJObj.put("ars_tr_no", cpifReq.getArsTrNo());
                    paramJObj.put("ds_dtime", cpifReq.getDsDtime());
                    paramJObj.put("ds_data_size", cpifReq.getDsDataSize());
                    paramJObj.put("ds_data", cpifReq.getDsData());
                    paramJObj.put("hb_agree_dttm", cpifReq.getHbAgreeDttm());
                    break;
                case "7" : //출금
                    paramJObj.put("order_tr_dt", cpifReq.getOrderTrDt());
                    paramJObj.put("order_tr_no", cpifReq.getOrderTrNo());
                    paramJObj.put("ci", cpifReq.getCi());
                    paramJObj.put("memb_cd", cpifReq.getMembCd());
                    paramJObj.put("biz_no", cpifReq.getBizNo());
                    paramJObj.put("fnni_cd", cpifReq.getFnniCd());
                    paramJObj.put("cp_sys_acno", cpifReq.getCpSysAcno());
                    paramJObj.put("plain_acct_no", cpifReq.getPlainAcctNo());
                    paramJObj.put("prdt_nm", cpifReq.getPrdtNm());
                    paramJObj.put("prdt_amt", cpifReq.getPrdtAmt());
                    paramJObj.put("mng_org_cd", cpifReq.getMngOrgCd());
                    break;
                case "8" : //거래내역조회
                    paramJObj.put("cp_memb_no", cpifReq.getCpMembNo());
                    paramJObj.put("biz_no", cpifReq.getBizNo());
                    paramJObj.put("tr_kind", cpifReq.getTrKind());
                    paramJObj.put("tr_from_dt", cpifReq.getTrFromDt());
                    paramJObj.put("tr_to_dt", cpifReq.getTrToDt());
                    paramJObj.put("page_no", cpifReq.getPageNo());
                    paramJObj.put("row_cnt", cpifReq.getRowCnt());
                    break;
                case "12" : //회원삭제
                    paramJObj.put("cp_memb_no", cpifReq.getCpMembNo());
                    paramJObj.put("biz_no", cpifReq.getBizNo());
                    break;
                case "16" : //계좌삭제
                    paramJObj.put("cp_memb_no", cpifReq.getCpMembNo());
                    paramJObj.put("biz_no", cpifReq.getBizNo());
                    paramJObj.put("fnni_cd", cpifReq.getFnniCd());
                    paramJObj.put("cp_sys_acno", cpifReq.getCpSysAcno());
                    break;
            }
        } catch (Exception e) {
            log.error("파라미터 셋팅 오류발생 :: " + e.toString());
        }
        return paramJObj.toString();
    }

    /**
     * 쿠콘 통신 파라미터 Validation
     * 
     * @param params CpifDTO
     * @param paramStr String
     * @return ValidDTO
     */
    private ValidDTO paramParmValidation(CpifDTO params, String paramStr){
        ValidDTO validDTO = new ValidDTO(false, "체크완료");
        JSONObject paramObj = JSONObject.fromObject(paramStr);
        String stepNo = params.getStepNo();

        try {
            if(!params.getTest().equals("Y")){
                if(StringUtils.isEmpty(params.getCookonId()) && StringUtils.isEmpty(params.getCryptKey())){
                    validDTO.setChk(true);
                    validDTO.setMsg("쿠콘 API 파라미터 오류 :: " + "cookon_id값 crypt_key값은 필수 입니다.");
                }
            }

            switch(stepNo){
                case "1" : //회원조회
                    if(!paramObj.has("biz_no")){
                        if(!paramObj.has("ci") && !paramObj.has("memb_cd")){
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_828_1);
                        }
                    }else{
                        if(!paramObj.has("biz_no") && !paramObj.has("memb_cd")){
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_828_2);
                        }
                    }
                    break;
                case "2" : //회원가입
                    if(!paramObj.has("biz_no")){                     //개인
                        if(!paramObj.has("memb_nm")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_560_1);
                        }
                        if(!paramObj.has("ph_no")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_560_2);
                        }
                        if(!paramObj.has("ci")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_560_3);
                        }
                        if(!paramObj.has("birth_date")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_560_4);
                        }
                        if(!paramObj.has("gender")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_560_5);
                        }
                        if(!paramObj.has("tele_corp")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_560_6);
                        }
                        if(!paramObj.has("national_yn")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_560_7);
                        }
                    }else{                                          //법인
                        if(!paramObj.has("biz_no")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_561_1);
                        }
                        if(!paramObj.has("comp_nm")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_561_2);
                        }
                        if(!paramObj.has("comp_email")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_561_3);
                        }
                        if(!paramObj.has("memb_nm")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_561_4);
                        }
                        if(!paramObj.has("ph_no")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_561_5);
                        }
                        if(!paramObj.has("tele_corp")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_561_6);
                        }
                        if(!paramObj.has("birth_date")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_561_7);
                        }
                        if(!paramObj.has("gender")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_561_8);
                        }
                        if(!paramObj.has("national_yn")){ 
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_561_9);
                        }
                    }
                    break;
                case "13" : //시스템계좌번호 조회
                    if(!paramObj.has("biz_no")){
                        if(!paramObj.has("ci") && !paramObj.has("memb_cd")){
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_813_1);
                        }
                    }else{
                        if(!paramObj.has("biz_no") && !paramObj.has("memb_cd")){
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_813_2);
                        }
                    }
                    if(!paramObj.has("fnni_cd")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_813_3);
                    }
                    if(!paramObj.has("acct_no")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_813_4);
                    }
                    break;
                case "4" : //계좌검증
                    if(!paramObj.has("cp_memb_no")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_821_1);
                    }
                    if(!paramObj.has("fnni_cd")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_821_2);
                    }
                    if(!paramObj.has("acct_no")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_821_3);
                    }
                    if(!paramObj.has("verify_type")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_821_4);
                    }
                    break;
                case "6" : //계좌등록
                    if(!paramObj.has("cp_memb_no")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_825_1);
                    }
                    if(!paramObj.has("fnni_cd")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_825_2);
                    }
                    if(!paramObj.has("acct_no")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_825_3);
                    }
                    if(!paramObj.has("verify_tr_dt")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_825_4);
                    }
                    if(!paramObj.has("verify_tr_no")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_825_5);
                    }
                    if(!paramObj.has("cert_kind")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_825_6);
                    }
                    if(!paramObj.has("cert_file_ets")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_825_7);
                    }
                    if(!paramObj.has("ds_dtime")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_825_8);
                    }
                    if(!paramObj.has("ds_data_size")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_825_9);
                    }
                    if(!paramObj.has("ds_data")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_825_10);
                    }
                    break;
                case "7" : //출금
                    if(!paramObj.has("order_tr_dt")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_830_1);
                    }
                    if(!paramObj.has("order_tr_no")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_830_2);
                    }
                    if(!paramObj.has("biz_no")){
                        if(!paramObj.has("ci") && !paramObj.has("memb_cd")){
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_830_3);
                        }
                    }else{
                        if(!paramObj.has("biz_no") && !paramObj.has("memb_cd")){
                            validDTO.setChk(true);
                            validDTO.setMsg(Validation.V_830_4);
                        }
                    }
                    if(!paramObj.has("fnni_cd")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_830_5);
                    }
                    if(!paramObj.has("cp_sys_acno")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_830_6);
                    }
                    if(!paramObj.has("prdt_nm")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_830_7);
                    }
                    if(!paramObj.has("prdt_amt")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_830_8);
                    }
                    break;
                case "8" : //거래내역조회
                    if(!paramObj.has("cp_memb_no")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_253_1);
                    }
                    if(!paramObj.has("tr_kind")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_253_2);
                    }
                    if(!paramObj.has("tr_from_dt")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_253_3);
                    }
                    if(!paramObj.has("tr_to_dt")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_253_4);
                    }
                    if(!paramObj.has("page_no")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_253_5);
                    }
                    if(!paramObj.has("row_cnt")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_253_6);
                    }
                    break;
                case "12" : //회원삭제
                    if(!paramObj.has("cp_memb_no")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_810_1);
                    }
                    break;
                case "16" : //계좌삭제
                    if(!paramObj.has("cp_memb_no")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_826_1);
                    }
                    if(!paramObj.has("fnni_cd")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_826_1);
                    }
                    if(!paramObj.has("cp_sys_acno")){ 
                        validDTO.setChk(true);
                        validDTO.setMsg(Validation.V_826_1);
                    }
                    break;
            }
        } catch (Exception e) {
            log.error("파라미터 Validation 오류발생 :: " + e.toString());
        }

        return validDTO;
    }

    /**
     * 쿠콘 통신 LOG 저장
     * 
     * @param obj JSONObject
     */
    private void saveLog(JSONObject obj){
        String cmsLogInfoStr = "";    //CMS 로그 정보

            /*********************************************
             * CMS 로그 저장 
             *********************************************/
            String cmsLogInfoUrl = aosBaseUrl + "/api/v1/cmsLog/packetSaveCk";
            String cmsLogInfoMethod = "POST";
            String cmsLogInfoHeader = "{\"access_token\":\"qwerty\"}";
            String cmsLogInfoBody = obj.toString();

            log.debug("\n[[CMS 로그 저장 데이터 셋팅]]");
            log.debug("cmsLogInfoUrl :: "+cmsLogInfoUrl);
            log.debug("cmsLogInfoMethod :: "+cmsLogInfoMethod);
            log.debug("cmsLogInfoHeader :: "+cmsLogInfoHeader);
            log.debug("cmsLogInfoBody :: "+cmsLogInfoBody);

            HttpParam cmsLogInfoParam = 
                new HttpParam(Constant.CMS_LOG_TITLE, cmsLogInfoUrl, cmsLogInfoMethod, cmsLogInfoHeader, cmsLogInfoBody);    //통신파라미터 초기화
            //log.debug(cmsLogInfoParam.toString());

            //ntf -> AOS -> AOS_FILTER -> DB -> AOS_FILTER -> AOS -> ntf
            cmsLogInfoStr = httpUtil.callApiHttps(cmsLogInfoParam);

            log.info("로그 저장 결과 :: " + cmsLogInfoStr + "\n");
    }
}