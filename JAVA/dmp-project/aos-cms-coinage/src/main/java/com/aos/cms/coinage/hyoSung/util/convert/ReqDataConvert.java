package com.aos.cms.coinage.hyoSung.util.convert;

import java.io.Serializable;
import java.nio.charset.Charset;

import com.aos.cms.coinage.hyoSung.dto.socket.Hs15;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs20;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs30;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs35;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs40;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs45;
import com.aos.cms.coinage.hyoSung.dto.socket.HsHeader;

import org.springframework.util.ObjectUtils;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.Builder;
import lombok.Data;

/**
 * 요청 전문 생성
 * 
 * @since 2022.10.13
 * @author sung
 */
@Data
@Builder
public class ReqDataConvert implements Packet, Serializable {

    private static final long serialVersionUID = 1L;
    private String type;

    private HsHeader hsHeaderReq;      // 0.실시간CMS(정기출금) 실시간공통부
    private Hs15 hs15Req;              // 15. 실시간CMS(정기출금) 실시간 회원조회 개별부
    private Hs20 hs20Req;              // 20. 실시간CMS(정기출금) 실시간 회원등록 개별부
    private Hs30 hs30Req;              // 30. 실시간CMS(정기출금) 실시간 회원증빙 개별부
    private Hs35 hs35Req;              // 35. 실시간CMS(정기출금) 실시간 회원증빙데이터 개별부
    private Hs40 hs40Req;              // 40. 실시간CMS(정기출금) 실시간 출금요청 개별부
    private Hs45 hs45Req;              // 45. 실시간CMS(정기출금) 실시간 출금결과확인 개별부

    @Override
    public ByteBuf getByteBuf(Charset charset) {
        int LENGTH = 0;
        ByteBuf bb = Unpooled.buffer(LENGTH);
        
        if ("15".equals(type)) {
            bb = Unpooled.buffer(320);

            //0.실시간CMS(정기출금) 실시간공통부
            this.getByteBufDefaultPacket(bb, charset);

            //15. 실시간CMS(정기출금) 실시간 회원조회 개별부
            this.getByteBufMemberCheckPacket(bb, charset);
        } else if ("20".equals(type)) {
            bb = Unpooled.buffer(320);

            //0.실시간CMS(정기출금) 실시간공통부
            this.getByteBufDefaultPacket(bb, charset);

            //20. 실시간CMS(정기출금) 실시간 회원등록 개별부
            this.getByteBufMemberInsertPacket(bb, charset);
        } else if ("30".equals(type)) {
            bb = Unpooled.buffer(202);

            //30. 실시간CMS(정기출금) 실시간 회원증빙 개별부
            this.getByteBufMemberCertInsertPacket(bb, charset);
        } else if ("35".equals(type)) {
            bb = Unpooled.buffer(60102);

            //35. 실시간CMS(정기출금) 실시간 회원증빙데이터 개별부
            this.getMemberCertDataPacket(bb, charset);
        } else if ("40".equals(type)) {
            bb = Unpooled.buffer(320);

            //0.실시간CMS(정기출금) 실시간공통부
            this.getByteBufDefaultPacket(bb, charset);

            //40. 실시간CMS(정기출금) 실시간 출금요청 개별부
            this.getWithdrawPacket(bb, charset);
        }  else if ("45".equals(type)) {
            bb = Unpooled.buffer(320);

            //0.실시간CMS(정기출금) 실시간공통부
            this.getByteBufDefaultPacket(bb, charset);

            //45. 실시간CMS(정기출금) 실시간 출금결과확인 개별부
            this.getResultPacket(bb, charset);
        }

        return bb;
    }

    // 0.실시간CMS(정기출금) 실시간공통부
    public void getByteBufDefaultPacket(ByteBuf bb, Charset charset) {
        getByteBuf(bb, "%-10s",       hsHeaderReq.getHsHeaderDp1()             , charset, 0, 10);
        getByteBuf(bb, "%10s",        hsHeaderReq.getHsHeaderDp2()             , charset, 0, 10);
        getByteBuf(bb, "%-10s",       hsHeaderReq.getHsHeaderDp3()             , charset, 0, 10);
        getByteBuf(bb, "%-10s",       hsHeaderReq.getHsHeaderDp4()             , charset, 0, 10);
        getByteBuf(bb, "%-6s",        hsHeaderReq.getHsHeaderDp5()             , charset, 0, 6);
        getByteBuf(bb, "%-4s",        hsHeaderReq.getHsHeaderDp6()             , charset, 0, 4);
        getByteBuf(bb, "%-3s",        hsHeaderReq.getHsHeaderDp7()             , charset, 0, 3);
        getByteBuf(bb, "%-6s",        hsHeaderReq.getHsHeaderDp8()             , charset, 0, 6);
        getByteBuf(bb, "%-6s",        hsHeaderReq.getHsHeaderDp9()             , charset, 0, 6);
        getByteBuf(bb, "%-6s",        hsHeaderReq.getHsHeaderDp10()            , charset, 0, 6);
        getByteBuf(bb, "%-1s",        hsHeaderReq.getHsHeaderDp11()            , charset, 0, 1);
        getByteBuf(bb, "%-4s",        hsHeaderReq.getHsHeaderDp12()            , charset, 0, 4);
        getByteBuf(bb, "%-24s",       hsHeaderReq.getHsHeaderDp13()            , charset, 0, 24);
    }

    //15. 실시간CMS(정기출금) 실시간 회원조회 개별부
    private void getByteBufMemberCheckPacket(ByteBuf bb, Charset charset) {
        getByteBuf(bb, "%-2s",         hs15Req.getHs15Dp1()            , charset, 0, 2);
        getByteBuf(bb, "%-15s",        hs15Req.getHs15Dp2()            , charset, 0, 15);
        getByteBuf(bb, "%-13s",        hs15Req.getHs15Dp3()            , charset, 0, 13);
        getByteBuf(bb, "%-20s",        hs15Req.getHs15Dp4()            , charset, 0, 20);
        getByteBuf(bb, "%-20s",        hs15Req.getHs15Dp5()            , charset, 0, 20);
        getByteBuf(bb, "%-1s",         hs15Req.getHs15Dp6()            , charset, 0, 1);
        getByteBuf(bb, "%-1s",         hs15Req.getHs15Dp7()            , charset, 0, 1);
        getByteBuf(bb, "%-20s",        hs15Req.getHs15Dp8()            , charset, 0, 20);
        getByteBuf(bb, "%-13s",        hs15Req.getHs15Dp9()            , charset, 0, 13);
        getByteBuf(bb, "%-8s",         hs15Req.getHs15Dp10()           , charset, 0, 8);
        getByteBuf(bb, "%-87s",        hs15Req.getHs15Dp11()           , charset, 0, 87);
        getByteBuf(bb, "%-20s",        hs15Req.getHs15Dp12()           , charset, 0, 20);
    }

    //20. 실시간CMS(정기출금) 실시간 회원등록 개별부
    private void getByteBufMemberInsertPacket(ByteBuf bb, Charset charset) {
        getByteBuf(bb, "%-2s",        hs20Req.getHs20Dp1()            , charset, 0, 2);
        getByteBuf(bb, "%-15s",       hs20Req.getHs20Dp2()            , charset, 0, 15);
        getByteBuf(bb, "%-13s",       hs20Req.getHs20Dp3()            , charset, 0, 13);
        getByteBuf(bb, "%-20s",       hs20Req.getHs20Dp4()            , charset, 0, 20);
        getByteBuf(bb, "%-20s",       hs20Req.getHs20Dp5()            , charset, 0, 20);
        getByteBuf(bb, "%-1s",        hs20Req.getHs20Dp6()            , charset, 0, 1);
        getByteBuf(bb, "%-1s",        hs20Req.getHs20Dp7()            , charset, 0, 1);
        getByteBuf(bb, "%-20s",       hs20Req.getHs20Dp8()            , charset, 0, 20);
        getByteBuf(bb, "%-20s",       hs20Req.getHs20Dp9()            , charset, 0, 20);
        getByteBuf(bb, "%-2s",        hs20Req.getHs20Dp10()           , charset, 0, 2);
        getByteBuf(bb, "%-10s",       hs20Req.getHs20Dp11()           , charset, 0, 10);
        getByteBuf(bb, "%-8s",        hs20Req.getHs20Dp12()           , charset, 0, 8);
        getByteBuf(bb, "%-13s",       hs20Req.getHs20Dp13()           , charset, 0, 13);
        getByteBuf(bb, "%-8s",        hs20Req.getHs20Dp14()           , charset, 0, 8);
        getByteBuf(bb, "%-47s",       hs20Req.getHs20Dp15()           , charset, 0, 47);
        getByteBuf(bb, "%-20s",       hs20Req.getHs20Dp16()           , charset, 0, 20);
    }

    //30. 실시간CMS(정기출금) 실시간 회원증빙 개별부
    private void getByteBufMemberCertInsertPacket(ByteBuf bb, Charset charset) {
        getByteBuf(bb, "%-1s",        hs30Req.getHs30Dp1()            , charset, 0, 1);
        getByteBuf(bb, "%-10s",       hs30Req.getHs30Dp2()            , charset, 0, 10);
        getByteBuf(bb, "%-10s",       hs30Req.getHs30Dp3()            , charset, 0, 10);
        getByteBuf(bb, "%-10s",       hs30Req.getHs30Dp4()            , charset, 0, 10);
        getByteBuf(bb, "%-10s",       hs30Req.getHs30Dp5()            , charset, 0, 10);
        getByteBuf(bb, "%-8s",        hs30Req.getHs30Dp6()            , charset, 0, 8);
        getByteBuf(bb, "%-4s",        hs30Req.getHs30Dp7()            , charset, 0, 4);
        getByteBuf(bb, "%-3s",        hs30Req.getHs30Dp8()            , charset, 0, 3);
        getByteBuf(bb, "%-1s",        hs30Req.getHs30Dp9()            , charset, 0, 1);
        getByteBuf(bb, "%-20s",       hs30Req.getHs30Dp10()           , charset, 0, 20);
        getByteBuf(bb, "%-2s",        hs30Req.getHs30Dp11()           , charset, 0, 2);
        getByteBuf(bb, "%-5s",        hs30Req.getHs30Dp12()           , charset, 0, 5);
        getByteBuf(bb, "%-6s",        hs30Req.getHs30Dp13()           , charset, 0, 6);
        getByteBuf(bb, "%-1s",        hs30Req.getHs30Dp14()           , charset, 0, 1);
        getByteBuf(bb, "%-4s",        hs30Req.getHs30Dp15()           , charset, 0, 4);
        getByteBuf(bb, "%-30s",       hs30Req.getHs30Dp16()           , charset, 0, 30);
        getByteBuf(bb, "%-45s",       hs30Req.getHs30Dp17()           , charset, 0, 45);
        getByteBuf(bb, "%-30s",       hs30Req.getHs30Dp18()           , charset, 0, 30);
        getByteBuf(bb, "%-1s",        hs30Req.getHs30Dp19()           , charset, 0, 1);
        getByteBuf(bb, "%-1s",        hs30Req.getHs30Dp20()           , charset, 0, 1);
    }

    //35. 실시간CMS(정기출금) 실시간 회원증빙데이터 개별부
    private void getMemberCertDataPacket(ByteBuf bb, Charset charset) {
        getByteBuf(bb, "%-1s",        hs35Req.getHs35Dp1()            , charset, 0, 1);
        getByteBuf(bb, "%-10s",       hs35Req.getHs35Dp2()            , charset, 0, 10);
        getByteBuf(bb, "%-1s",        hs35Req.getHs35Dp3()            , charset, 0, 1);
        getByteBuf(bb, "%-20s",       hs35Req.getHs35Dp4()            , charset, 0, 20);
        getByteBuf(bb, "%-1s",        hs35Req.getHs35Dp5()            , charset, 0, 1);
        getByteBuf(bb, "%-2s",        hs35Req.getHs35Dp6()            , charset, 0, 2);
        getByteBuf(bb, "%-65s",       hs35Req.getHs35Dp7()            , charset, 0, 65);
        getByteBuf(bb, "%-60000s",    hs35Req.getHs35Dp8()            , charset, 0, 60000);
        getByteBuf(bb, "%-1s",        hs35Req.getHs35Dp9()            , charset, 0, 1);
        getByteBuf(bb, "%-1s",        hs35Req.getHs35Dp10()           , charset, 0, 1);
    }

    //40. 실시간CMS(정기출금) 실시간 출금요청 개별부
    private void getWithdrawPacket(ByteBuf bb, Charset charset) {
        getByteBuf(bb, "%-50s",       hs40Req.getHs40Dp1()            , charset, 0, 50);
        getByteBuf(bb, "%-20s",       hs40Req.getHs40Dp2()            , charset, 0, 20);
        getByteBuf(bb, "%-15s",       hs40Req.getHs40Dp3()            , charset, 0, 15);
        getByteBuf(bb, "%-2s",        hs40Req.getHs40Dp4()            , charset, 0, 2);
        getByteBuf(bb, "%-1s",        hs40Req.getHs40Dp5()            , charset, 0, 1);
        getByteBuf(bb, "%-9s",        hs40Req.getHs40Dp6()            , charset, 0, 9);
        getByteBuf(bb, "%-10s",       hs40Req.getHs40Dp7()            , charset, 0, 10);
        getByteBuf(bb, "%-10s",       hs40Req.getHs40Dp8()            , charset, 0, 10);
        getByteBuf(bb, "%-15s",       hs40Req.getHs40Dp9()            , charset, 0, 15);
        getByteBuf(bb, "%-20s",       hs40Req.getHs40Dp10()           , charset, 0, 20);
        getByteBuf(bb, "%-18s",       hs40Req.getHs40Dp11()           , charset, 0, 18);
        getByteBuf(bb, "%-15s",       hs40Req.getHs40Dp12()           , charset, 0, 15);
        getByteBuf(bb, "%-15s",       hs40Req.getHs40Dp13()           , charset, 0, 15);
        getByteBuf(bb, "%-20s",       hs40Req.getHs40Dp14()           , charset, 0, 20);
    }

    //45. 실시간CMS(정기출금) 실시간 출금결과확인 개별부
    private void getResultPacket(ByteBuf bb, Charset charset) {
        getByteBuf(bb, "%-6s",       hs45Req.getHs45Dp1()            , charset, 0, 6);
        getByteBuf(bb, "%-6s",       hs45Req.getHs45Dp2()            , charset, 0, 6);
        getByteBuf(bb, "%-2s",       hs45Req.getHs45Dp3()            , charset, 0, 2);
        getByteBuf(bb, "%-15s",      hs45Req.getHs45Dp4()            , charset, 0, 15);
        getByteBuf(bb, "%-13s",      hs45Req.getHs45Dp5()            , charset, 0, 13);
        getByteBuf(bb, "%-20s",      hs45Req.getHs45Dp6()            , charset, 0, 20);
        getByteBuf(bb, "%-20s",      hs45Req.getHs45Dp7()            , charset, 0, 20);
        getByteBuf(bb, "%-15s",      hs45Req.getHs45Dp8()            , charset, 0, 15);
        getByteBuf(bb, "%-2s",       hs45Req.getHs45Dp9()            , charset, 0, 2);
        getByteBuf(bb, "%-1s",       hs45Req.getHs45Dp10()           , charset, 0, 1);
        getByteBuf(bb, "%-20s",      hs45Req.getHs45Dp11()           , charset, 0, 20);
        getByteBuf(bb, "%-15s",      hs45Req.getHs45Dp12()           , charset, 0, 15);
        getByteBuf(bb, "%-15s",      hs45Req.getHs45Dp13()           , charset, 0, 15);
        getByteBuf(bb, "%-70s",      hs45Req.getHs45Dp14()           , charset, 0, 70);
    }
    
    public void getByteBuf(ByteBuf bb, String format, Object args, Charset charset, int srcIndex, int length) {
        if (ObjectUtils.isEmpty(charset)) {
            bb.writeBytes(String.format(format,    args).getBytes(Charset.forName("EUC-KR")), srcIndex, length);
        } else {
            bb.writeBytes(String.format(format,    args).getBytes(charset), srcIndex, length);
        }
    }

    @Override
    public byte[] getBytes(Charset charset) {
        return getByteBuf(charset).array();
    }

    @Override
    public String getString(Charset charset) {
        return getByteBuf(charset).toString(charset);
    }
}
