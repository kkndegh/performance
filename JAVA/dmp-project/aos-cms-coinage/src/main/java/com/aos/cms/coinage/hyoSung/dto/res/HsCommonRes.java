package com.aos.cms.coinage.hyoSung.dto.res;


import lombok.Data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.NoArgsConstructor;


/**
 * API 공통
 * 
 * @since 2022.09.26
 * @author kh
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class HsCommonRes implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String code;
    private String msg;
}