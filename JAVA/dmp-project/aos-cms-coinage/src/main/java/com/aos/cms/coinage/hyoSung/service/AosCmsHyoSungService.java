package com.aos.cms.coinage.hyoSung.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.aos.cms.coinage.common.dto.DateParamDTO;
import com.aos.cms.coinage.hyoSung.dto.HsDTO;
import com.aos.cms.coinage.hyoSung.dto.StepHsProcDTO;
import com.aos.cms.coinage.hyoSung.dto.SocketDTO;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;

/**
 * AosCmsCoinage 서비스
 * 
 * @author kh
 * @since 2022.09.13
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class AosCmsHyoSungService {

    private final HyosungApiService hyosungApiService;

    /**
     * 효성 집금 Process
     * 
     * @param params HsDTO
     * @return String
     */
    public String hyoSungCmsPaymentDoc(HsDTO params){
        JSONObject rstObj = new JSONObject();

        try {
            if(StringUtils.isNotEmpty(params.getStepNo())){
                String rst = stepHyosungProc(params);
                
                rstObj.put("result", "SUCCESS");
                rstObj.put("msg", "효성 Proc 실행완료");
                rstObj.put("response", rst);
            }else{
                new Thread(() -> {
                    params.setStepNo("15");
                    stepHyosungProcs(params);
                }).start();
                
                rstObj.put("result", "SUCCESS");
                rstObj.put("msg", "효성 Thread Proc 실행완료");
            }
        } catch (Exception e) {
            rstObj.put("result", "FAIL");
            rstObj.put("msg", "효성 Proc 실패");

            log.error("효성 Proc 에러발생 :: " + e.toString());
        }
        
        return rstObj.toString();
    }

    /* 
    * 효성 프로세스
    *
    * [[stepNo]]
    * 15 : 회원조회
    * 20 : 회원등록
    * 25 : 회원삭제
    * 30 : 회원증빙
    * 35 : 회원증빙데이터
    * 40 : 출금요청
    * 45 : 출금결과확인
    * 999 : 정상완료
    * 900 ~ 945 : 기타오류
    * */
    private void stepHyosungProcs(HsDTO params){
        StepHsProcDTO stepObj = new StepHsProcDTO(params, null, null);
        //boolean status = true;  //루프 종료 플레그
        boolean timeBoolean = true; //반복 초 제한 플레그(재시도 시 nowTime.getSecond()에 걸리도록 설정)
        boolean thread_state = true;

        LocalDateTime nowTime = LocalDateTime.now();
        
        //결제요청 시간 저장
        String dt = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        String tm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));

        DateParamDTO dateParam = new DateParamDTO(dt, tm);

        SocketDTO socket = new SocketDTO();
        socket.setCnt(1);

        while (thread_state){
            try {
                nowTime = LocalDateTime.now();
                if (timeBoolean ? (nowTime.getSecond() % 5 == 0) : true){
                    try {
                        if(params.getStepNo().equals("15")){
                            //15. 회원조회
                            log.info("15. 회원조회");
                            stepObj = hyosungApiService.hs15(params, dateParam, socket);
                        } else if(params.getStepNo().equals("20")){
                            //20. 회원등록
                            log.info("20. 회원등록");
                            stepObj = hyosungApiService.hs20(params, dateParam, socket);
                        } else if(params.getStepNo().equals("30")){
                            //30. 회원증빙
                            log.info("30. 회원증빙");
                            stepObj = hyosungApiService.hs30(params, dateParam, socket);
                        } else if(params.getStepNo().equals("35")){
                            //35. 회원증빙데이터
                            log.info("35. 회원증빙데이터");
                            stepObj = hyosungApiService.hs35(params, dateParam, socket);
                        } else if(params.getStepNo().equals("40")){
                            //40. 출금요청
                            log.info("40. 출금요청");
                            stepObj = hyosungApiService.hs40(params, dateParam, socket);
                        } else if(params.getStepNo().equals("45")){
                            //45. 출금결과확인
                            log.info("45. 출금결과확인");
                            stepObj = hyosungApiService.hs45(params, dateParam, socket);
        
                            if(params.getStepNo().equals("999")){
                                log.info("==처리완료. 루프 종료.");
                                log.info("== 처리완료. [SUCCESS]");
                                thread_state = false;
                            }
                        } else if(Integer.parseInt(params.getStepNo()) >= 900 && Integer.parseInt(params.getStepNo()) < 950){
                            log.error("==Exception 발생으로 인한 proc 종료");
                            break;
                        }

                        //1초대기
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        log.error("stepHyosungProcs 에러발생 :: " + e.toString());
                    }
                }
            } catch (Exception e) {
                log.error("효성 API연동 에러 :: " + e.toString());
            }
        }
    }

    /* 
    * 효성 단일 프로세스
    *
    * [[stepNo]]
    * 15 : 회원조회
    * 20 : 회원등록
    * 25 : 회원삭제
    * 30 : 회원증빙
    * 35 : 회원증빙데이터
    * 40 : 출금요청
    * 45 : 출금결과확인
    * 999 : 정상완료
    * 900 ~ 945 : 기타오류
    * */
    private String stepHyosungProc(HsDTO params){
        JSONObject rst = null;
        StepHsProcDTO stepObj = new StepHsProcDTO(params, null, null);
        
        //결제요청 시간 저장
        String dt = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        String tm = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HHmmss"));
        DateParamDTO dateParam = new DateParamDTO(dt, tm);

        SocketDTO socket = new SocketDTO();
        socket.setCnt(1);

        try {
            if(params.getStepNo().equals("15")){
                //15. 회원조회
                log.info("15. 회원조회");
                stepObj = hyosungApiService.hs15(params, dateParam, socket);
            } else if(params.getStepNo().equals("20")){
                //20. 회원등록
                log.info("20. 회원등록");
                stepObj = hyosungApiService.hs20(params, dateParam, socket);
            } else if(params.getStepNo().equals("25")){
                //25. 회원삭제
                log.info("25. 회원삭제");
                stepObj = hyosungApiService.hs25(params, dateParam, socket);
            } else if(params.getStepNo().equals("30")){
                //30. 회원증빙
                log.info("30. 회원증빙");
                stepObj = hyosungApiService.hs30(params, dateParam, socket);
            } else if(params.getStepNo().equals("35")){
                //35. 회원증빙데이터
                log.info("35. 회원증빙데이터");
                stepObj = hyosungApiService.hs35(params, dateParam, socket);
            } else if(params.getStepNo().equals("40")){
                //40. 출금요청
                log.info("40. 출금요청");
                stepObj = hyosungApiService.hs40(params, dateParam, socket);
            } else if(params.getStepNo().equals("45")){
                //45. 출금결과확인
                log.info("45. 출금결과확인");
                stepObj = hyosungApiService.hs45(params, dateParam, socket);
            }
        } catch (Exception e) {
            log.error("stepHyosungProcs 에러발생 :: " + e.toString());
        } finally{
            rst = JSONObject.fromObject(stepObj.getResultRes());
        }
        return rst.toString();
    }
}
