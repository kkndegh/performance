package com.aos.cms.coinage.hyoSung.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * socket 정보
 * 
 * @since 2022.10.17
 * @author kh
 */
@Data
public class SocketDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String data;
    private String returnData;
    private int connection;
    private int recv;
    private int type;
    private String transmitType;
    private String packetCode;
    private String treat;
    private int refer1;
    private int cnt;
}
