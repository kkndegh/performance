package com.aos.cms.coinage;

import java.io.*;

import javax.imageio.ImageIO;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.net.URL;

import org.apache.commons.codec.binary.Base64;

import java.awt.AlphaComposite;
import java.awt.RenderingHints;

public class ImageToByteTest2 {

    static String outputFilePath = "C:\\works\\image\\";

    public static void main(String[] args) {
        // imageToBase64();
        // imageResize();

        // downImg("https://dcimg4.dcinside.co.kr/viewimage.php?id=21bac431ecdc2b9960bac1&no=24b0d769e1d32ca73ceb8ffa11d028317c5d1470b55e4cc42e6f4e2b917b6bfcff0e75e72842895820bcedcc22c3e40f00c498a392ad709dd754a14c6b7df4e0362898ec51e99f238b88b633e5d1e7022ec6cae468cab4b7acf23bab2f2af60b666579497e20a7616684dacc");
        // httpImageDown("https://www.sellerbot.co.kr/assets/images/intro/intro_logo.png");
        try {
            //makeImage("https://www.sellerbot.co.kr/assets/images/intro/intro_logo.png");
            //makeImage("https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fk.kakaocdn.net%2Fdn%2FcHyZGc%2FbtqGdsax4PA%2F28U11KsfCaTKQ4OP3twksK%2Fimg.jpg");
            

            // ByteArrayOutputStream baos = new ByteArrayOutputStream();
            // BufferedImage img = ImageIO.read(new URL("https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fk.kakaocdn.net%2Fdn%2FcHyZGc%2FbtqGdsax4PA%2F28U11KsfCaTKQ4OP3twksK%2Fimg.jpg"));
            // ImageIO.write(img, "jpg", baos);
            // byte[] bytes = baos.toByteArray();

            makeImage("https://dcimg4.dcinside.co.kr/viewimage.php?id=21bac431ecdc2b9960bac1&no=24b0d769e1d32ca73ceb8ffa11d028317c5d1470b55e4cc42e6f4e2b917b6bfcff0e75e72842895820bcedcc22c3e40f00c498a392ad709dd754a14c6b7df4e0362898ec51e99f238b88b633e5d1e7022ec6cae468cab4b7acf23bab2f2af60b666579497e20a7616684dacc");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void makeImage(String imageUrl) throws IOException {
        // URL로 부터 이미지 읽기
        URL url = new URL(imageUrl);
        BufferedImage originalImage = ImageIO.read(url);
        
        byte[] originalImageBytes = toByteArray(originalImage, "jpg");
        System.out.println("원본 이미지 용량 :: " + originalImageBytes.length);
        ImageIO.write(originalImage, "jpg", new File(outputFilePath + "originalImage.jpg"));
        System.out.println("\n\n\n\n");

        int idx = 1;
        int fer = 100;
        if(originalImageBytes.length > 300000){
            while(true){
                // 크기 조정
                int height = originalImage.getHeight();
                int width = originalImage.getWidth();

                //float fer = 400/width;

                // width = (int)(width / fer); 
                // height = (int)(height / fer);

                width = width - fer; 
                height = height - fer;
                
                int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
                BufferedImage resizeImage = resizeImageWithHint(originalImage, type, width, height);

                byte[] resizeImageByte = toByteArray(resizeImage, "jpg");
                System.out.println("리사이징 이미지 용량[" + idx + "] :: " + resizeImageByte.length);

                if(resizeImageByte.length < 300000){
                    System.out.println("리사이징 이미지 base64 용량 :: " + imageToBase64(resizeImageByte).length());
                    ImageIO.write(resizeImage, "jpg", new File(outputFilePath + "resizeImage.jpg"));
                    break;
                }else{
                    // originalImage = resizeImage;
                    fer += 5;
                    idx++;
                }
            }
        }
        //System.out.println("base64 = " + imageToBase64(bytes2));
        
        //System.out.println("name= " + inputFile.getName());
        // 첫번째 인자 : 대상 이미지 파일
        // 두번째 인자 : 변경 타입
        // 세번째 인자 : 파일경로와 이름
        //ImageIO.write(resizeImage, "png", new File(outputFilePath + "changeImg.jpg"));
    }

    public static byte[] toByteArray(BufferedImage bi, String format)
        throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bi, format, baos);
        byte[] bytes = baos.toByteArray();
        return bytes;

    }

    public static String imageToBase64(byte[] fileContent) {
        String encodedString = null;

        try {
            encodedString = new String(Base64.encodeBase64(fileContent));
        } catch (Exception e) {
            System.out.println(e.toString());
        } 
        return encodedString;
    }

 
    private static BufferedImage resizeImageWithHint(BufferedImage originalImage, int type, int width, int height) {
        BufferedImage resizedImage = new BufferedImage(width, height, type);
        Graphics2D graphics2d = resizedImage.createGraphics();
        graphics2d.drawImage(originalImage, 0, 0, width, height, null);
        graphics2d.dispose();
        graphics2d.setComposite(AlphaComposite.Src);
        // //보간 관련
        graphics2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        // //렌더링 관련
         graphics2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
        // //안티엘리어싱 여부
         graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        return resizedImage;
    }
}