package com.aos.cms.coinage;

import com.aos.cms.coinage.hyoSung.dto.socket.Hs30;
import com.aos.cms.coinage.hyoSung.dto.socket.Hs35;

import java.nio.charset.Charset;

public class ConvertTest {
    
    public static void main(String[] ar) {
        String str = "Sgtxlogis  logis007  gtxlogis  logis007  202209011000100RHGSM20200609217089  01JPG  284525                                                                                                                ";
        String r_str = "Sgtxlogis  logis007  gtxlogis  logis007  202208011100100RHGSM20170613574397  01JPG  257762YS000정상                                                                                                      ";
        String str2 = "Dgtxlogis  RHGSM20200609217089  N06                                                                 1KQGBB6GgCO3I8v3zSXONq+uaaYnQ5X8waFhZjlz+tAEkGfJGaitv9Z+FWMYGB0qC3+/+FAFiobhwF2jqetSPu2nb1qKOIk7n/I0AMify3+boakeIyNuDDGKdKqEfMQp9aZCVTOXHPagCMArMAT0ardQbGM24DIznOanoAhuei/Wo1hdlBBGD71NMhYDaM4NRESovUgD";
        Hs30 hs30Res = Hs30Res(str, Constant.DEFAULT_CHARSET);
        Hs30 r_hs30Res = Hs30Res(r_str, Constant.DEFAULT_CHARSET);
        System.out.println("str :: " + hs30Res.toString());
        System.out.println("r_str :: " + r_hs30Res.toString());

        Hs35 hs35Res = Hs35Res(str2, Constant.DEFAULT_CHARSET);
        System.out.println(hs35Res.toString());
    }

    public static Hs30 Hs30Res(String data, Charset charset) {
        Hs30 hs30Res = new Hs30();
        byte[] byteArray = data.getBytes(charset);

        // 요청정보부
        hs30Res.setHs30Dp1(getData(byteArray    , 0, 1, charset));     // Record구분
        hs30Res.setHs30Dp2(getData(byteArray    , 1, 10, charset));    // 업체ID
        hs30Res.setHs30Dp3(getData(byteArray    , 11, 10, charset));   // 업체PW
        hs30Res.setHs30Dp4(getData(byteArray    , 21, 10, charset));   // SW-ID
        hs30Res.setHs30Dp5(getData(byteArray    , 31, 10, charset));   // SW-PW
        hs30Res.setHs30Dp6(getData(byteArray    , 41, 8, charset));    // 송신일자
        hs30Res.setHs30Dp7(getData(byteArray    , 49, 4, charset));    // 전문구분
        hs30Res.setHs30Dp8(getData(byteArray    , 53, 3, charset));    // 업무구분
        hs30Res.setHs30Dp9(getData(byteArray    , 56, 1, charset));    // 회원결제수단
        hs30Res.setHs30Dp10(getData(byteArray   , 57, 20, charset));   // 회원번호
        hs30Res.setHs30Dp11(getData(byteArray   , 77, 2, charset));    // 증빙구분
        hs30Res.setHs30Dp12(getData(byteArray   , 79, 5, charset));    // 증빙데이터유형
        hs30Res.setHs30Dp13(getData(byteArray   , 84, 6, charset));    // 증빙데이터길이
        hs30Res.setHs30Dp14(getData(byteArray   , 90, 1, charset));    // 응답결과
        hs30Res.setHs30Dp15(getData(byteArray   , 91, 4, charset));    // 응답코드
        hs30Res.setHs30Dp16(getData(byteArray   , 95, 30, charset));   // 응답메시지
        hs30Res.setHs30Dp17(getData(byteArray   , 125, 45, charset));  // 미사용영역
        hs30Res.setHs30Dp18(getData(byteArray   , 170, 30, charset));  // 사용자정의
        hs30Res.setHs30Dp19(getData(byteArray   , 200, 1, charset));   // CR
        hs30Res.setHs30Dp20(getData(byteArray   , 201, 1, charset));   // LF

        return hs30Res;
    }

    //실시간 회원증빙데이터
    public static Hs35 Hs35Res(String data, Charset charset) {
        Hs35 hs35Res = new Hs35();
        byte[] byteArray = data.getBytes(charset);

        // 요청정보부
        hs35Res.setHs35Dp1(getData(byteArray    , 0, 1, charset));          // Record구분
        hs35Res.setHs35Dp2(getData(byteArray    , 1, 10, charset));         // 업체ID
        hs35Res.setHs35Dp3(getData(byteArray    , 11, 1, charset));         // 회원결제수단
        hs35Res.setHs35Dp4(getData(byteArray    , 12, 20, charset));        // 회원번호
        hs35Res.setHs35Dp5(getData(byteArray    , 32, 1, charset));         // 최종데이터여부
        hs35Res.setHs35Dp6(getData(byteArray    , 33, 2, charset));         // 데이터전송연번
        hs35Res.setHs35Dp7(getData(byteArray    , 35, 65, charset));        // 미사용
        hs35Res.setHs35Dp8(getData(byteArray    , 100, 200, charset));    // 증빙데이터
        // hs35Res.setHs35Dp9(getData(byteArray    , 60100, 1, charset));      // CR
        // hs35Res.setHs35Dp10(getData(byteArray   , 60101, 1, charset));      // LF

        return hs35Res;
    }

    public static String getData(byte[] byteArray, int offset, int length, Charset charset) {
        if (byteArray == null)
            return "";

        String str = null;
        str = new String(byteArray, offset, length, charset);
        return str.trim();
    }
}