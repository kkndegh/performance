package com.aos.cms.coinage.hyosung;

import com.aos.cms.coinage.hyosung.util.UtilMethod;

public class REAL_CMS_Sample_Telegrams {

	//실시간 공통부
	public static String REALCMS_COMMON_TEXT (String textType, String workType, String procNo) {
		String commonText = 
				"sdsitess  "																					//★필수(업체ID, 10byte)
				+ "testtest  "																				//★필수(업체PW, 10byte) 
				+ "swswtest  "																				//★필수(SW_ID, 10byte) 
				+ "swswswsw  "																			//★필수(SW-PW, 10byte) 
				+ UtilMethod.getNowDateYYMMDD()											//★필수(송신일자, 6byte)
				+ textType																					//★필수(전문구분, 4byte) 
				+ workType																				//★필수(업무구분, 4byte) 
				+ procNo																					//★필수(연번,6byte)
				+ "      "																						//(전송일자, 6byte) ☆응답값
				+ "      "																						//(전송시간, 6byte) ☆응답값
				+ " "																							//(응답결과, 1byte) ☆응답값
				+ "    "																						//(응답코드, 4byte) ☆응답값
				+"                        ";																	//(FMS사용영역, 24byte) ☆응답값
		return commonText;
	}
	
	/**
	 * 정기출금 업무
	 * 참고문서 FMS-TE-0035(SDSI-실시간CMS(정기출금))
	 */
	public static String REALCMS_MEM_APPLY_1000_400_WORK_TEXT =
			"81"																								//★필수(거래은행코드, 2byte) 
			+"20420204047676 "																		//★필수(거래계좌번호, 15byte) 
			+"871011       "																				//★필수(거래계좌 생년월일/사업자번호, 13byte) 
			+"손진혁              "																		//★필수(계좌예금주명, 20byte) 
			+"sonsonsonsonson-1523"																//★필수(회원번호, 20byte) 
			+"N"																								//★필수(처리내역, 1byte) 신규:N, 삭제:D 
			+"N"																								//★필수(처리구분, 1byte) 일반:N, 전자동의:A 
			+"                    "																			//(전자동의Key, 20byte)
			+"손진혁              "																		//★필수(회원명, 20byte) 
			+"  "																								//(약정일, 2byte)
			+"          "																						//(기본금액, 10byte)
			+"        "																						//(이체종료일, 8byte)
			+"             "																					//(회원연락처, 13byte)
			+"        "																						//(FMS번호, 8byte)
			+"                                               "													//(미사용, 47byte)
			+"                    ";																			//(사용자영역, 20byte)
	
	public static String REALCMS_MEM_SEARCH_1000_500_WORK_TEXT =
			"11"																								//★필수(거래은행코드, 2byte) 
			+ "3510909856013  "																		//★필수(거래계좌번호, 15byte) 
			+ "850705       "																				//★필수(거래계좌 생년월일/사업자번호, 13byte) 
			+ "이준성              "																		//★필수(계좌예금주명(20byte) 
			+ "10180061            "																		//★필수(회원번호, 20byte) 
			+ " "																								//(처리내역, 1byte) 
			+ " "																								//(처리구분, 1byte) 일반:N, 전자동의:A 
			+ "                    "																			//(전자동의Key, 20byte)
			+ "             "																					//(납부자연락처, 13byte)
			+ "        "																						//(FMS번호, 8byte)
			+ "                                                                                       "		//(미사용, 87byte)
			+ "                    ";																			//(사용자영역, 20byte)
	
	public static String REALCMS_PAY_APPLY_1000_120_WORK_TEXT = 
			"                                                  "												//(미사용, 50byte)
			+ "sonsonsonsonson-1523"																//★필수(회원번호, 20byte) 
			+ "000000000050000"																	//★필수(출금요청금액, 15byte) 
			+ "15"																							//(납입회차, 2byte)
			+ "N"																							//(기거래결과조회여부, 1byte)
			+ "         "																						//(미사용, 9byte)
			+ "          "																						//(일 출금한도, 10byte)
			+ "          "																						//(월 출금한도, 10byte)
			+ "               "																				//(미사용, 15byte)
			+ "                    "																			//(현금영수증 발행정보, 20byte)
			+ "                  "																				//(FMS사용영역, 18byte)
			+ "               "																				//(출금금액, 15byte)
			+ "               "																				//(수수료, 15byte)
			+ "                    ";																			//(사용자영역, 20byte)
	
	public static String REALCMS_ONE_PAY_SEARCH_1000_200 = 
			"190825"																						//★필수(원전문 전송일자, 6byte) 
			+ "000001"																						//★필수(원전문 연번, 6byte) 
			+ "  "																								//(거래계좌은행코드, 2byte) 
			+ "               "																				//(거래계좌번호, 15byte)
			+ "             "																					//(거래계좌 생년월일/사업자번호, 13byte)
			+ "                    "																			//(계좌주예금주명, 20byte)
			+ "                    "																			//(회원번호, 20byte)
			+ "               "																				//(출금요청금액, 15byte)
			+ "  "																								//(납입회차, 2byte)
			+ " "																								//(처리구분 일반 : N, 전자동의 : A, 1byte)
			+ "                    "																			//(전자동의Key, 20byte)
			+ "               "																				//(출금금액, 15byte)
			+ "               "																				//(수수료, 15byte)
			+ "                                                                      ";						//(미사용, 70byte)
	
	/**
	 * 실시간 계좌조회 업무
	 * 참고문서 FMS-TE-0028(SDSI-실시간 계좌조회)
	 */
	public static String REALCMS_ONE_ACCT_SEARCH_1000_300 = 
			"04"																								//★필수(거래은행코드, 2byte) 
			+ "20420204047676 "																		//★필수(거래계좌번호, 15byte) 
			+ "871024       "																				//★필수(거래계좌 생년월일/사업자번호, 13byte) 
			+ "손진혁              "																		//★필수(계좌예금주명, 20byte) 
			+ "                    "																			//(사용자영역, 20byte)
			+ "               "																				//(수수료, 15byte)
			+ "                                                  "												//(예비영역1, 50byte) 
			+ "                                             "													//(예비영역2, 45byte)
			+ "                                        ";														//(미사용, 40byte)
	
	/**
	 * 즉시출금 업무
	 * 참고문서 FMS-TE-0029(SDSI-실시간CMS)즉시출금
	 */
	
	public static String REALCMS_MEM_APPLY_1000_600_WORK_TEXT =
			"88"																								//★필수(거래은행코드, 2byte) 
			+ "100015121327   "																			//★필수(거래계좌번호, 15byte) 
			+ "950525       "																				//★필수거래계좌 생년월일/사업자번호, 13byte) 
			+ "손진혁              "																		//★필수계좌예금주명,20byte) 
			+ "                                                  "
			+ "                                                  "
			+ "                                                  "												//(미사용, 150byte)
			+ "                    ";																			//(사용자영역, 20byte)
	
	public static String REALCMS_PAY_APPLY_1000_100_WORK_TEXT =
			"81"																								//거래계좌은행코드(2byte) ★필수값
			+ "20420204047675 "																		//거래계좌번호(15byte) ★필수값
			+ "871024       "																				//거래계좌 생년월일/사업자번호(13byte) ★필수값
			+ "손진혁              "																		//계좌주예금주명(20byte) ★필수값
			+ "sonsonson-9765      "																	//회원번호(20byte) ★필수값
			+ "000000000020000"																	//출금요청금액(15byte) ★필수값
			+ "11"																							//납입회차(2byte)
			+ "          "																						//미사용(10byte)
			+ "          "																						//일 출금한도(15byte)
			+ "          "																						//월 출금한도(15byte)
			+ "  "																								//이동통신사(2byte)
			+ "             "																					//납부자 연락처(13byte)
			+ "                    "																			//현금영수증 발행정보(20byte)
			+ "P                 "																			//FMS사용영역(18byte)
			+ "               "																				//출금금액(15byte)
			+ "               "																				//수수료(15byte)
			+ "                    ";																			//출금금액(20byte)

	/**
	 * 즉시출금 배치실시간출금업무
	 * 참고문서 FMS-TE-0029(SDSI-실시간CMS)즉시출금
	 */
	
	public static String REALCMS_BATCH_APPLY_S = 
			"S"																								//★필수(Record구분, 1byte) 
			+ "sdsitest  " 																					//★필수(이용고객ID, 10byte)
			+ "testtest  " 																					//★필수(이용고객PW, 10byte)
			+ "swswtest  " 																				//★필수(프로그램업체ID, 10byte)
			+ "swswswsw  " 																				//★필수(프로그램업체PW, 10byte)
			+ "RCP" 																						//★필수(DATA구분, 3byte)
			+ "A" 																							//★필수(작업구분, 1byte)
			+ "20190829"																					//★필수(처리일자, 8byte) 
			+ "000495"																					//★필수(연번, 6byte)
			+ "                                                                                                                                      " //(미사용, 134byte)
			+ " " 																								//(처리결과, 1byte)
			+ "    " 																							//(결과코드, 4byte)
			+ "                              " 																//(결과메세지, 30byte)
			+ "                    " 																			//(사용자정의, 20byte)
			+ "\r\n";
	
	public static String REALCMS_BATCH_APPLY_R = 
			"S"																								//★필수(Record구분, 1byte) 
			+ "sdsitest  " 																					//★필수(이용고객ID, 10byte)
			+ "testtest  " 																					//★필수(이용고객PW, 10byte)
			+ "swswtest  " 																				//★필수(프로그램업체ID, 10byte)
			+ "swswswsw  " 																				//★필수(프로그램업체PW, 10byte)
			+ "RCP" 																						//★필수(DATA구분, 3byte)
			+ "R" 																							//★필수(작업구분, 1byte)
			+ "20190829"																					//★필수(처리일자, 8byte) 
			+ "000495"																					//★필수(연번, 6byte)
			+ "                                                                                                                                      " //(미사용, 134byte)
			+ " " 																								//(처리결과, 1byte)
			+ "    " 																							//(결과코드, 4byte)
			+ "                              " 																//(결과메세지, 30byte)
			+ "                    " 																			//(사용자정의, 20byte)
			+ "\r\n";
	
	public static String REALCMS_BATCH_APPLY_H = 
			"H"																								//★필수(Record구분, 1byte) 
			+ "sdsitest  " 																					//★필수(이용고객ID, 10byte)
			+ "000002"																					//★필수(신청건수, 6byte) 
			+ "000000000030000" 																	//★필수(신청건수, 15byte)
			+ "                                                            " 
			+ "                                                            " 
			+ "                                                             "									//(미사용, 181byte) 
			+ " "																								//(처리결과, 1byte) 
			+ "    " 																							//(결과코드, 4byte)
			+ "                              " 																//(사용자정의, 30byte)
			+ "\r\n";
	
	public static String[] REALCMS_BATCH_APPLY_D = 
		{ 
	    "D"																									//★필수(Record구분, 1byte) 
		+ "sdsitest  "																						//★필수(이용고객ID, 10byte) 
	    + "N" 																								//★필수(처리내역, 1byte)
		+ "000495" 																						//★필수(연번, 6byte)
	    + "81" 																								//★필수(출금은행코드, 2byte)
		+ "20420204047675 "																			//★필수(출금계좌번호, 15byte)
	    + "871024       " 																					//★필수(예금주생년월일/사업자번호, 13byte)
		+ "손진혁              "																			//★필수(계좌주예금주명, 20byte) 
	    + "sonsonson-9765      " 																		//★필수(회원번호, 20byte)
		+ "000000000010000" 																		//★필수(출금요청금액, 15byte)
	    + "11"																								//(납입회차, 2byte) 
		+ "                    " 																				//(전자동의Key, 20byte)
	    + "SK"																								//(이동통신사, 2byte)
		+ "01088648849  " 																				//(납부자연락처, 13byte)
	    + "                    "																				//(현금영수증발행정보, 20byte) 
		+ "        " 																							//(FMS정의, 8byte)
	    + "               " 																					//(출금금액, 15byte)
		+ "               "																					//(수수료, 15byte)
		+ "                         " 																			//(미사용, 25byte)
		+ " " 																									//(처리결과, 1byte)
		+ "    " 																								//(결과코드, 4byte)
		+ "          " 																						//(일출금한도, 10byte)
		+ "          " 																						//(월출금한도, 10byte)
		+ "\r\n",
	    "Dsdsitest  N0004950420420204047675 871024       손진혁              sonsonson-9775      00000000002000011                    SK01088648849                                                                                                              \r\n",
		};	
	
	public static String REALCMS_BATCH_APPLY_T = 
			"T" 																								//★필수(Record구분, 1byte)
			+ "sdsitest  " 																					//★필수(이용고객ID, 10byte)
			+ "      "																							//(신청건수, 6byte) 
			+ "               " 																				//(신청금액, 15byte)
			+ "      "																							//(정상건수, 6byte) 
			+ "               " 																				//(정상금액, 15byte)
			+ "      " 																						//(출금건수, 6byte)
			+ "               " 																				//(출금금액, 15byte)
			+ "                                                                                                                                           " //(미사용, 139byte)
			+ " " 																								//(처리결과, 1byte)
			+ "    " 																							//(결과코드, 4byte)
			+ "                              " 																//(사용자정의, 30byte)
			+ "\r\n";
	
	
}
