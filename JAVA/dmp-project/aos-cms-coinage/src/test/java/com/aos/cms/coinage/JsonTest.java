package com.aos.cms.coinage;

// import com.aos.cms.coinage.service.AosCmsCoinageService;

import org.junit.jupiter.api.Test;
// import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
// import com.fasterxml.jackson.databind.ObjectMapper;

import net.sf.json.JSONObject;

@SpringBootTest
public class JsonTest {

    @Test
    public void test1(){
        JSONObject pparam = new JSONObject();

        JSONObject param = new JSONObject();
        param.put("ci", "asdjaslkdj21j3120mlqkwdj1092dj12dj12odjdsdfdsfwefwf234234234lkdj1");
        param.put("memb_cd", "CP201702123548252");
        param.put("order_tr_dt", "20160711");
        param.put("order_tr_no", "151415");
        param.put("res_suc_url", "http://test.co.kr/succurl");
        param.put("res_err_url", "http://test.co.kr/errurl");
        param.put("prdt_nm", "[테스트]OO 상품");
        param.put("prdt_amt", "4500000");

        pparam.put("serviceCode", "CPIF_AFFL_830.jct");
        pparam.put("data", param);

        System.out.println(pparam.toString());
        System.out.println(pparam.get("serviceCode"));
        System.out.println(pparam.get("data"));
    }

}