package com.aos.cms.coinage;

import java.io.Serializable;

import lombok.Data;

/**
 * 20. 실시간CMS(정기출금) 실시간 회원등록 개별부
 * 
 * @since 2022.10.13
 * @author sung
 */
@Data
public class MemberInsertDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String memberInsertDp1 = "";
    private String memberInsertDp2 = "";
    private String memberInsertDp3 = "";
    private String memberInsertDp4 = "";
    private String memberInsertDp5 = "";
    private String memberInsertDp6 = "";
    private String memberInsertDp7 = "";
    private String memberInsertDp8 = "";
    private String memberInsertDp9 = "";
    private String memberInsertDp10 = "";
    private String memberInsertDp11 = "";
    private String memberInsertDp12 = "";
    private String memberInsertDp13 = "";
    private String memberInsertDp14 = "";
    private String memberInsertDp15 = "";
    private String memberInsertDp16 = "";
}
