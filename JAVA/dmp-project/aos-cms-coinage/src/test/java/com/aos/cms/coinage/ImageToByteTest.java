package com.aos.cms.coinage;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class ImageToByteTest {
    
    static String inputFilePath = "C:\\works\\image\\";
    static String outputFilePath = "C:\\works\\image\\";

    public static void main(String[] args) {

        String test =  "aaaaabbbbbccccc";

        System.out.println(test.substring(0, 5));
        System.out.println(test.substring(5, 10));
        //imageToBase64();
        //imageResize();

        //downImg("https://dcimg4.dcinside.co.kr/viewimage.php?id=21bac431ecdc2b9960bac1&no=24b0d769e1d32ca73ceb8ffa11d028317c5d1470b55e4cc42e6f4e2b917b6bfcff0e75e72842895820bcedcc22c3e40f00c498a392ad709dd754a14c6b7df4e0362898ec51e99f238b88b633e5d1e7022ec6cae468cab4b7acf23bab2f2af60b666579497e20a7616684dacc");
        //httpImageDown("https://www.sellerbot.co.kr/assets/images/intro/intro_logo.png");
        imageConvertShortsByte("https://www.sellerbot.co.kr/assets/images/intro/intro_logo.png");
    }

    private static void imageConvertShortsByte(String imageUrl){
        BufferedImage image = null;

        try {
	        // URL로 부터 이미지 읽기
	        URL url = new URL(imageUrl);
	        image = ImageIO.read(url);

            System.out.println("넓이 :: " + image.getWidth(null));
            System.out.println("높이 :: " + image.getWidth(null));

            int width = image.getWidth(null);
            int height = image.getHeight(null);

            float fer = 400 / (float)width;

            //2. Graphics2D 로 리사이징    
            BufferedImage resizedImage = new BufferedImage((int)(width * fer), (int)(height * fer), BufferedImage.TYPE_INT_RGB);
            Graphics2D graphics2D = resizedImage.createGraphics();
            graphics2D.drawImage(image, 0, 0, (int)(width * fer), (int)(height * fer), null);
            graphics2D.dispose();

            //3. BufferedImage 로 File 생성
            File outputfile = new File(outputFilePath + "test.jpg");
            try {
                if (outputfile.createNewFile()) {
                    
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    
                    String type = "jpg";
                    ImageIO.write(resizedImage, type, bos);

                    System.out.println("넓이@ :: " + resizedImage.getWidth(null));
                    System.out.println("높이@ :: " + resizedImage.getWidth(null));
                    
                    InputStream inputStream = new ByteArrayInputStream(bos.toByteArray());
                    
                    Files.copy(inputStream, outputfile.toPath(), StandardCopyOption.REPLACE_EXISTING);

                    //return outputfile;
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
            
            // BufferedImage inputImage = ImageIO.read(url);  // 받은 이미지 읽기

            // BufferedImage outputImage = new BufferedImage(width, height, inputImage.getType());
            // // 입력받은 리사이즈 길이와 높이 

            // Graphics2D graphics2D = outputImage.createGraphics(); 
            // graphics2D.drawImage(inputImage, 0, 0, width, height, null); // 그리기
            // graphics2D.dispose(); // 자원해제
            
            // ImageIO.write(outputImage, "jpg", new File(outputFilePath+"test.jpg"));
	    } catch (IOException e) {
            System.out.println("에러발생 :: " + e.toString());
	    }
    }

    private static void imageConvertShortsByte2(String imageUrl){
        InputStream in = null;
        ByteArrayOutputStream out = null;
        URL url = null;

        try {
            //URL로 이미지 가져오기
            url = new URL(imageUrl);
            in = new BufferedInputStream(url.openStream());
            out = new ByteArrayOutputStream();
            
            byte[] buf = new byte[1024];
            int n = 0;
            
            while (-1!=(n=in.read(buf))) {
                out.write(buf, 0, n);
            }
            
            byte[] response = out.toByteArray();

            System.out.println("바이트 response :: " + response.length);

            String encodedString = new String(Base64.encodeBase64(response));

            System.out.println("인코딩 :: " + encodedString);
        } catch (Exception e) {
            System.out.println("에러발생 :: " + e.toString());
        } finally{
            try {
                out.close();
                in.close();
            } catch (IOException e) {
                System.out.println("I/O 에러발생 :: " + e.toString());
            }
        }
    }

    public static void imageToBase64() {
        try {
             // load file
            File inputFile = new File(inputFilePath + "test.jpg");

            byte[] fileContent = FileUtils.readFileToByteArray(inputFile);
            String encodedString = new String(Base64.encodeBase64(fileContent));

            // create output file
            File outputFile = new File(outputFilePath + "test2.jpg");

            // decode the string and write to file
            byte[] decodedBytes = Base64.decodeBase64(encodedString);
            FileUtils.writeByteArrayToFile(outputFile, decodedBytes);

        } catch (Exception e) {
            System.out.println(e.toString());
        } 
    }

    /* 파일 정보와 리사이즈 값 정하는 메소드 */
    public static void imageResize(){
        try {
            File file = new File(inputFilePath);  //리사이즈할 파일 경로
            InputStream inputStream = new FileInputStream(file);
            Image img = new ImageIcon(file.toString()).getImage(); // 파일 정보 추출
            
            System.out.println("사진의 가로길이 : " + img.getWidth(null)); // 파일의 가로
            System.out.println("사진의 세로길이 : " + img.getHeight(null)); // 파일의 세로
            /* 파일의 길이 혹은 세로길이에 따라 if(분기)를 통해서 응용할 수 있습니다.
            * '예를 들어 파일의 가로 해상도가 1000이 넘을 경우 1000으로 리사이즈 한다. 같은 분기' */
            int width = 1280; // 리사이즈할 가로 길이
            int height = 720; // 리사이즈할 세로 길이
            
            BufferedImage resizedImage = resize(inputStream ,width , height );
            // 리사이즈 실행 메소드에 값을 넘겨준다.
            ImageIO.write(resizedImage, "jpg", new File(outputFilePath));
            // 리사이즈된 파일, 포맷, 저장할 파일경로
        } catch (Exception e) {
            System.out.println("오류 :: " + e.toString());
        }
    }

	/* 리사이즈 실행 메소드 */
    public static BufferedImage resize(InputStream inputStream, int width, int height) 
    		throws IOException {
    	
        BufferedImage inputImage = ImageIO.read(inputStream);  // 받은 이미지 읽기

        BufferedImage outputImage = new BufferedImage(width, height, inputImage.getType());
        // 입력받은 리사이즈 길이와 높이 

        Graphics2D graphics2D = outputImage.createGraphics(); 
        graphics2D.drawImage(inputImage, 0, 0, width, height, null); // 그리기
        graphics2D.dispose(); // 자원해제

        return outputImage;
    }

    public static void downImg(String addr){
        String address = addr; // 주소 입력
           
        try {
            InputStream is = null;
            URL url = new URL(address);
            FileOutputStream fos = new FileOutputStream(inputFilePath + "down.jpg");
            URLConnection urlConnection = url.openConnection();
            is = urlConnection.getInputStream();
            byte[] buffer = new byte[1024];
            int readBytes;

            while ((readBytes = is.read(buffer)) != -1) {

                fos.write(buffer, 0, readBytes);
            }
            fos.close();
            System.out.println("파일 다운완료");
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void httpImageDown(String addr){
        try {
            //http client 생성
            CloseableHttpClient httpClient = HttpClients.createDefault();
    
            //get 메서드와 URL 설정
            HttpGet httpGet = new HttpGet(addr);
    
            //agent 정보 설정
            httpGet.addHeader("User-Agent", "Mozila/5.0");
            
            //get 요청
            CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
            
            // System.out.println("::GET Response Status::");
            
            // //response의 status 코드 출력
            // System.out.println(httpResponse.getStatusLine().getStatusCode());
    
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    httpResponse.getEntity().getContent()));
    
            String inputLine;
            StringBuffer response = new StringBuffer();
    
            while ((inputLine = reader.readLine()) != null) {
                response.append(inputLine);
            }
            
            reader.close();
    
            //Print result
            System.out.println(response.toString());
            httpClient.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }

    }
}