package com.aos.cms.coinage.hyosung;

/**
 * 즉시출금 회원신청(건별) 1000/600
 * 참고문서 FMS-TE-0029(SDSI-실시간CMS(정기출금))
 */

public class REALCMS_MEMBER_APPLY_1000_600 {

	public static String REAL_CMS_IP = "121.134.74.90";	// 또는 121.134.74.70
	public static int REAL_CMS_TEST_PORT = 14000;
	public static int REAL_CMS_REAL_PORT = 24000;

	public static void main(String[] ar) {
		
		BaseAction conn = new XC_JavaSocket();
		
		 //서버접속
		if(conn.connect(REAL_CMS_IP, REAL_CMS_TEST_PORT)<0){
			System.out.println("연결실패");
			return ;
		}else{
			System.out.println("연결성공");
		}
		
		//전문작성
		String commonText = REAL_CMS_Sample_Telegrams.REALCMS_COMMON_TEXT("1000", "600", "000612");
		System.out.println("공통부 정상길이 : 100");
		System.out.println("공통부 실제길이 : " + commonText.toString().getBytes().length);
		System.out.println("");
		
		String workText = REAL_CMS_Sample_Telegrams.REALCMS_MEM_APPLY_1000_600_WORK_TEXT;
		System.out.println("개별부 정상길이 : 220");
		System.out.println("개별부 실제길이 : " + workText.toString().getBytes().length);
		System.out.println("");
		
		String fullText = commonText.toString() + workText.toString();
		System.out.println("전문 정상길이 : 320");
		System.out.println("전문 실제길이 : " + fullText.getBytes().length);
		System.out.println("전문[" + fullText + "]");
		System.out.println("");
		
		//전문송신
		conn.sendData(fullText);
		System.out.println("송신 " +fullText);

		//전문 수신
		String sdata = new String(conn.recvData());
		byte[] tgr = sdata.getBytes();
		System.out.println("수신 "+ sdata);

		try {
			System.out.println("처리결과코드 : " + new String(tgr, 72, 4));
		} catch (Exception e) {
			System.out.println("처리결과코드 : " + e.toString());
		}

		// 소켓종료
		int ret = conn.close();
		if (ret > 0) {
			System.out.println("연결종료");
		} else {
			System.out.println("연결종료 실패 : " + ret);
		}
	}
}
