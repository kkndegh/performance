package com.aos.cms.coinage;

import java.nio.charset.Charset;

import com.aos.cms.coinage.hyoSung.dto.socket.Hs15;
import com.aos.cms.coinage.hyoSung.dto.socket.HsHeader;
import com.aos.cms.coinage.hyoSung.util.convert.ReqDataConvert;

public class DocConvertTest {

    // @Autowired
    // private AosCmsCoinageService aosCmsCoinageService;

    public static void main(String[] args) {
        HsHeader commonDefaultDTO = new HsHeader();
        commonDefaultDTO.setHsHeaderDp1("1111111111");
        commonDefaultDTO.setHsHeaderDp2("2222222222");
        commonDefaultDTO.setHsHeaderDp3("3333333333");
        commonDefaultDTO.setHsHeaderDp4("4444444444");
        commonDefaultDTO.setHsHeaderDp5("555555");
        commonDefaultDTO.setHsHeaderDp6("6666");
        commonDefaultDTO.setHsHeaderDp7("777");
        commonDefaultDTO.setHsHeaderDp8("888888");
        commonDefaultDTO.setHsHeaderDp9("999999");
        commonDefaultDTO.setHsHeaderDp10("000000");
        commonDefaultDTO.setHsHeaderDp11("a");
        commonDefaultDTO.setHsHeaderDp12("bbbb");
        commonDefaultDTO.setHsHeaderDp13("cccccccccccccccccccccccc");


        Hs15 memberCheckDTO = new Hs15();
        memberCheckDTO.setHs15Dp1("5fdfsfd");

        MemberInsertDTO memberInsertDTO = new MemberInsertDTO();
        memberInsertDTO.setMemberInsertDp1("4342");

        ReqDataConvert data = ReqDataConvert.builder()
                                .hsHeaderReq(commonDefaultDTO) // 0.실시간CMS(정기출금) 실시간공통부
                                .hs15Req(memberCheckDTO) // 15. 실시간CMS(정기출금) 실시간 회원조회 개별부
                                //.memberInsertDTO(memberInsertDTO) // 20. 실시간CMS(정기출금) 실시간 회원등록 개별부
                                .type("15")
                                .build();

        String convertStr = data.getString(Charset.forName("EUC-KR"));
        System.out.println(convertStr);
    }

}