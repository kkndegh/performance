package com.aos.cms.coinage.socket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Base64;

import lombok.extern.slf4j.Slf4j;

import com.aos.cms.coinage.AesEcbUtils;

@Slf4j
public class Client {
    private final String aesEncryptionKey = "encryptKey!@#";

    public static void main(String[] args) {
        String ip = "localhost";
        int port = 25050;
        new Client(ip, port);
    }

    public Client(String ip, int port) {
        Socket socket = null;
        try {

            // 서버에 요청 보내기
            socket = new Socket(ip, port);
            log.info(socket.getInetAddress().getHostAddress() + "에 연결됨");

            send(socket);

            // 응답 출력
            log.info("[[소켓 암호화 메시지 수신]]");
            String resData = receive(socket);

            log.info("[[수신 전문" + "(" + resData.getBytes("EUC-KR").length + " byte)]] :: " + resData);

            //SEED 복호화 작업
            try {
                AesEcbUtils aesEcbUtils = new AesEcbUtils(aesEncryptionKey);
                String decryptedReqData = new String(aesEcbUtils.decrypt(Base64.getDecoder().decode(resData)));
                log.info("[[메시지 복호화" + "(" + decryptedReqData.getBytes("EUC-KR").length + " byte)]] :: " + decryptedReqData);
            } catch (Exception e) {
                log.error("복호화 에러 :: " + e.getMessage());
            }

            // 소켓 닫기 (연결 끊기)
            if (!socket.isClosed()) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            log.info("소켓 통신 종료");
        } catch (IOException e) {
            log.error("소켓 통신 오류 :: " + e.getMessage());
        }
    }

    public void send(Socket socket) {
        String reqData = "04002020020415553500000000044002001300    003349000000103      00328                                65500036097218                                티몬티몬                      23                    2001                0000017156680000017156680000000000008                                                                     023            1                   KRW0000000000000                         ";

        try {
            log.info("[[발신 전문]] :: " + reqData);
            AesEcbUtils aesEcbUtils = new AesEcbUtils(aesEncryptionKey);
            String encryptedReqData = Base64.getEncoder().encodeToString(aesEcbUtils.encrypt(reqData.getBytes("EUC-KR")));

            // String decryptedMessage = new String(aesEcbUtils.decrypt(Base64.getDecoder().decode(encryptedReqData)));
            // System.out.println(decryptedMessage);

            // 메세지 전달
            log.info("[[메시지 암호화]]");
            log.info("encryptedReqData :: " + encryptedReqData + "\n\n");

            //생성한 person 객체를 byte array로 변환
            byte[] data = encryptedReqData.getBytes("EUC-KR");

            // byte[] data = toByteArray(str);
            //서버로 내보내기 위한 출력 스트림 뚫음
            OutputStream os = socket.getOutputStream();

            //출력 스트림에 데이터 씀
            os.write(data);

            //보냄
            os.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String receive(Socket socket){
        String resultData = null;

        try {
            //수신 버퍼의 최대 사이즈 지정
            int maxBufferSize = 8000;

            //버퍼 생성
            byte[] recvBuffer = new byte[maxBufferSize];

            //서버로부터 받기 위한 입력 스트림 뚫음
            InputStream is = socket.getInputStream();

            //버퍼(recvBuffer) 인자로 넣어서 받음. 반환 값은 받아온 size
            int nReadSize = is.read(recvBuffer);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos.write(recvBuffer, 0, nReadSize);

            byte[] body = baos.toByteArray();

            resultData = new String(body, Charset.forName("EUC-KR"));
        } catch (Exception e) {
            log.info("socket 수신 에러 :: " + e.toString());
        }
        return resultData;
    }

    
    // private final String aesEncryptionKey = "encryptKey!@#";

    // private Socket socket;
    // private BufferedReader br;
    // private PrintWriter pw;
    
    // public Client(String ip, int port) {
    //     try {
    //         String reqData = "04002020022815205200000000021402001300    088349000000188      08816                                56212693702974                                e커머스사업본                 26                    06003               0000043323550000043323550000000000004                                                                     026            1                   KRW0000000000000                         ";
    //         String encryptedReqData = "";

    //         // 서버에 요청 보내기
    //         socket = new Socket(ip, port);
    //         log.info(socket.getInetAddress().getHostAddress() + "에 연결됨");
            
    //         // 메시지 받기
    //         br = new BufferedReader(new InputStreamReader(socket.getInputStream(), "EUC-KR"));
    //         pw = new PrintWriter(socket.getOutputStream());

    //         try {
    //             AesEcbUtils aesEcbUtils = new AesEcbUtils(aesEncryptionKey);
    //             encryptedReqData = Base64.getEncoder().encodeToString(aesEcbUtils.encrypt(reqData.getBytes("EUC-KR")));

    //             String decryptedMessage = new String(aesEcbUtils.decrypt(Base64.getDecoder().decode(encryptedReqData)));
	// 		System.out.println(decryptedMessage);
            
    //         } catch (Exception e) {
    //             log.error("암호화 에러 :: " + e.getMessage());
    //         }

            

    //         // 메세지 전달
    //         log.info("[[소켓 암호화 메시지 전송]]");
    //         log.info("reqData :: " + encryptedReqData + "\n\n");
    //         pw.println(encryptedReqData);
    //         pw.flush();
            
    //         String resData = "";
    //         String decryptedResData = "";

    //         // 응답 출력
    //         log.info("[[소켓 암호화 메시지 수신]]");
    //         resData = br.readLine();
    //         log.info("resData :: " + resData + "\n");

    //         try {
    //             AesEcbUtils aesEcbUtils = new AesEcbUtils(aesEncryptionKey);
    //             decryptedResData = new String(aesEcbUtils.decrypt(Base64.getDecoder().decode(resData)));
    //         } catch (Exception e) {
    //             log.error("복호화 에러 :: " + e.getMessage());
    //         }

    //         log.info("decryptedResData :: " + decryptedResData);
    //     } catch (IOException e) {
    //         log.error("소켓 통신 오류 :: " + e.getMessage());
    //     } finally {
    //         // 소켓 닫기 (연결 끊기)
    //         try {
    //             if(socket != null) { socket.close(); }
    //             if(br != null) { br.close(); }
    //             if(pw != null) { pw.close(); }
    //             log.error("소켓 통신 종료");
    //         } catch (IOException e) {
    //             log.error("소켓 커넥션 객체 오류 :: " + e.getMessage());
    //         }
    //     }
    // }
}