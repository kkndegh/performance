/** mailsung */
package com.only1fs.aos.exception;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.spring.web.advice.ProblemHandling;

@ControllerAdvice
public class ExceptionHandling implements ProblemHandling {
    @ExceptionHandler
    public ResponseEntity<Problem> handleAppException(AppRuntimeException e, NativeWebRequest request) {
        AppStatus st = e.getStatus();
        String replaceStr = e.getReplaceStr();

        Problem problem = null;
        if (StringUtils.isEmpty(replaceStr)) {
            problem = Problem.builder()
            .with("code", st.getCode())
            .with("reason", st.getDesc())
            .with("time", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
            .build();
        } else {
            problem = Problem.builder()
            .with("code", st.getCode())
            .with("reason", st.getDesc().replace("{Parametger}", replaceStr))
            .with("time", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
            .build();
        }
        
        return new ResponseEntity<Problem>(problem, e.getHttpStatus());
    }
}
