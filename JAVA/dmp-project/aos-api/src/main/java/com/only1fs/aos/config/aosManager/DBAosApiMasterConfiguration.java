/** mailsung */
package com.only1fs.aos.config.aosManager;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

import com.only1fs.aos.common.annotation.aosManager.AosManagerMasterMapper;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@MapperScan(value = "com.only1fs.aos.mybatis.mapper.dao", annotationClass = AosManagerMasterMapper.class, sqlSessionFactoryRef = "aosApiMasterSqlSessionFactory")
@EnableTransactionManagement
public class DBAosApiMasterConfiguration {
    @Bean(name = "aosApiMasterDataSource", destroyMethod = "close")
    @ConfigurationProperties(prefix = "spring.datasource.aosapi.master.hikari")
    public DataSource aosApiMasterDataSource() {
        return DataSourceBuilder.create().type(HikariDataSource.class).build();
    }

    @Bean(name = "aosApiMasterTransactionManager")
    public DataSourceTransactionManager aosApiMasterTransactionManager(@Qualifier("aosApiMasterDataSource") DataSource aosApiMasterDataSource) {
        return new DataSourceTransactionManager(aosApiMasterDataSource);
    }

    @Bean(name = "aosApiMasterSqlSessionFactory")
    public SqlSessionFactory aosApiMasterSqlSessionFactory(@Qualifier("aosApiMasterDataSource") DataSource aosApiMasterDataSource, ApplicationContext applicationContext) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(aosApiMasterDataSource);
        sqlSessionFactoryBean.setConfigLocation(applicationContext.getResource("classpath:config/mybatis/mybatis-config.xml"));
        sqlSessionFactoryBean.setMapperLocations(applicationContext.getResources("classpath:config/mybatis/aosApi/mapper-master/*.xml"));

        return sqlSessionFactoryBean.getObject();
    }

    @Bean(name = "aosApiMasterSqlSessionTemplate")
    public SqlSessionTemplate aosApiMasterSqlSessionTemplate(SqlSessionFactory aosApiMasterSqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(aosApiMasterSqlSessionFactory);
    }
}
