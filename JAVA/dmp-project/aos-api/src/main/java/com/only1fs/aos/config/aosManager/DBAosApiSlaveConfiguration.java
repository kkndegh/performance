/** mailsung */
package com.only1fs.aos.config.aosManager;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

import com.only1fs.aos.common.annotation.aosManager.AosManagerSlaveMapper;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@MapperScan(value = "com.only1fs.aos.mybatis.mapper.dao", annotationClass = AosManagerSlaveMapper.class, sqlSessionFactoryRef = "aosApiSlaveSqlSessionFactory")
@EnableTransactionManagement
public class DBAosApiSlaveConfiguration {
    @Bean(name = "aosApiSlaveDataSource", destroyMethod = "close")
    // @Primary
    @ConfigurationProperties(prefix = "spring.datasource.aosapi.slave.hikari")
    public DataSource aosApiSlaveDataSource() {
        HikariDataSource source = DataSourceBuilder.create().type(HikariDataSource.class).build();
        return source;
    }

    @Bean(name = "aosApiSlaveTransactionManager")
    public DataSourceTransactionManager aosApiSlaveTransactionManager(@Qualifier("aosApiSlaveDataSource") DataSource aosApiSlaveDataSource) {
        return new DataSourceTransactionManager(aosApiSlaveDataSource);
    }

    @Bean(name = "aosApiSlaveSqlSessionFactory")
    // @Primary
    public SqlSessionFactory aosApiSlaveSqlSessionFactory(@Qualifier("aosApiSlaveDataSource") DataSource aosApiSlaveDataSource, ApplicationContext applicationContext) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(aosApiSlaveDataSource);
        sqlSessionFactoryBean.setConfigLocation(applicationContext.getResource("classpath:config/mybatis/mybatis-config.xml"));
        sqlSessionFactoryBean.setMapperLocations(applicationContext.getResources("classpath:config/mybatis/aosApi/mapper-slave/*.xml"));

        return sqlSessionFactoryBean.getObject();
    }

    @Bean(name = "aosApiSlaveSqlSessionTemplate")
    // @Primary
    public SqlSessionTemplate aosApiSlaveSqlSessionTemplate(SqlSessionFactory aosApiSlaveSqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(aosApiSlaveSqlSessionFactory);
    }
}
