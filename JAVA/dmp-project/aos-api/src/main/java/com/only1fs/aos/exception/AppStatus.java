/** mailsung */
package com.only1fs.aos.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public enum AppStatus {

    OK_00000                    ( "00000",   "OK",                          "성공" ),
    UNAUTHORIZED                ( "40100",   "Unauthorized",                "서비스아이디 또는 서비스 키가 잘못됐습니다." ),
    FORBIDDEN                   ( "40300",   "Forbidden",                   "등록이 필요한 IP 입니다." ),
    NOT_FOUND_40400             ( "40400",   "Not Found",                   "요청한 페이지(Resource)를 찾을 수 없습니다." ),
    NOT_FOUND_40401             ( "40401",   "Not Found",                   "등록된 Resource({Parametger}) 를 찾을 수 없습니다." ),
    ERROR                       ( "50000",   "INTERNAL_SERVER_ERROR",       "내부서버 오류" )
    ;

    private final String code;
    private final String name;
    private final String desc;
}
