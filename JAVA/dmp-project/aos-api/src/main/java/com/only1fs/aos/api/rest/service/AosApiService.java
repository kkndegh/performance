package com.only1fs.aos.api.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Function;
import com.only1fs.aos.mybatis.mapper.dao.aosApi.AosApiSlaveDao;
import com.only1fs.aos.Constant;
import com.only1fs.aos.common.util.CommonUtil;
import com.only1fs.aos.common.util.HttpUtil;
import com.only1fs.aos.dto.HttpParam;
import com.only1fs.aos.dto.ProcedureParam;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * 프로시저 호출 서비스
 * 
 * @since 2022.05.25
 * @author kh
 */
@Slf4j
@Service
public class AosApiService {
    @Autowired
    private AosApiSlaveDao aosApiSlaveDao;

    @Autowired
    private CommonUtil commonUtil;

    @Autowired
    private HttpUtil httpUtil;

    @Value("${spring.profiles.active}")
    private String profiles;

    /**
     * aosdb DB시키마 프로시저 호출
     * 
     * @param group String
     * @param code String
     * @param requestHeader Map<String, String>
     * @param request HttpServletRequest
     * @return String
     */
    public String callGetAosProcedure(String group, 
                                    String code,
                                    Map<String, String> requestHeader,
                                    HttpServletRequest request) {
        
        String returnStr="";                    //API필터 응답값
        HashMap<String, Object> returnExtStr;   //외부통신 응답값
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            for (Entry<String, String> elem : requestHeader.entrySet()) {
                //파싱시 value속 상따움표가 들어있으면 프로시저 내에서 value값 추출 오류가 날 수 있어 쌍따움표 제거
                map.put(elem.getKey(), elem.getValue().replaceAll("\"", "").replaceAll("\\\"", ""));
            }
            String header = new ObjectMapper().writeValueAsString(map);
            StringBuffer requestURL = request.getRequestURL();
            String queryString = request.getQueryString();
            String body = queryString != null ? commonUtil.getParamTojson(queryString) : "";
            String requestMethod = request.getMethod();
            String reqUrl = request.getQueryString() == null ? requestURL.toString() : requestURL.append('?').append(queryString).toString();

            /** 
             * 1. Procedure 호출
             */
            log.debug("****************************** AOS 필터 프로시저 호출 ******************************");
            ProcedureParam paramMap = 
                new ProcedureParam(Constant.PARAM_REQ_TITLE, group, code, requestMethod, reqUrl, header, body, "", "");
            log.debug(paramMap.toString());

            returnStr = aosApiSlaveDao.callAosPApiFilter(paramMap);
            log.debug(Constant.PARAM_RES_TITLE + "::" + returnStr);

            /** 
             * 2. 외부 호출 검증 및 호출
             */
            if(StringUtils.isNotEmpty(returnStr)){
                //조건 :: returnStr = callYn == 'Y' ? returnExtStr.get("resultData") : returnStr
                returnExtStr = apiValidAndCall(returnStr);
                if(returnExtStr.get("callYn").toString().contains("Y")){
                    returnStr = returnExtStr.get("resultData").toString();
                }
            }

            return returnStr;
        } catch(Exception e) {
            return commonUtil.resultObjMk(commonUtil.getClassName(), commonUtil.getMethodName(), "P999", e.toString());
        }
    }

    /**
     * aosdb DB시키마 프로시저 호출
     * 
     * @param group String
     * @param code String
     * @param requestBody Map<String, Object>
     * @param requestHeader Map<String, String>
     * @param request HttpServletRequest
     * @return String
     */
    public String callPostAosProcedure(String group, 
                                    String code, 
                                    Map<String, Object> requestBody, 
                                    Map<String, String> requestHeader,
                                    HttpServletRequest request) {
        
        String returnStr="";                    //API필터 응답값
        HashMap<String, Object> returnExtStr;   //외부통신 응답값
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            for (Entry<String, String> elem : requestHeader.entrySet()) {
                //파싱시 value속 상따움표가 들어있으면 프로시저 내에서 value값 추출 오류가 날 수 있어 쌍따움표 제거
                map.put(elem.getKey(), elem.getValue().replaceAll("\"", "").replaceAll("\\\"", ""));
            }

            String header = new ObjectMapper().writeValueAsString(map);
            String body = new ObjectMapper().writeValueAsString(requestBody);
            StringBuffer requestURL = request.getRequestURL();
            String queryString = request.getQueryString();
            String requestMethod = request.getMethod();
            String reqUrl = request.getQueryString() == null ? requestURL.toString() : requestURL.append('?').append(queryString).toString();

            /** 
             * 1. Procedure 호출
             */
            log.debug("****************************** AOS 필터 프로시저 호출 ******************************");
            ProcedureParam paramMap = new ProcedureParam(Constant.PARAM_REQ_TITLE, group, code, requestMethod, reqUrl, header, body, "", "");
            log.debug(paramMap.toString());

            returnStr = aosApiSlaveDao.callAosPApiFilter(paramMap);
            log.debug(Constant.PARAM_RES_TITLE + "::" + returnStr);

            /** 
             * 2. 외부 호출 검증 및 호출
             */
            if(StringUtils.isNotEmpty(returnStr)){
                //조건 :: returnStr = callYn == 'Y' ? returnExtStr.get("resultData") : returnStr
                returnExtStr = apiValidAndCall(returnStr);
                if(returnExtStr.get("callYn").toString().contains("Y")){
                    returnStr = returnExtStr.get("resultData").toString();
                }
            }

            return returnStr;
        } catch(Exception e) {
            return commonUtil.resultObjMk(commonUtil.getClassName(),commonUtil.getMethodName(),"P999", e.toString());
        }
    }

    /**
     * 외부 API호출 프로세스
     * 
     * @param data String
     * @return HashMap<String, Object>
     */
    private HashMap<String, Object> apiValidAndCall(String data){
        HashMap<String, Object> result = new HashMap<String, Object>();     // 결과 응답 MAP
        String callYn = "N";                                                // 외부 API호출 여부

        try {
            //jsonString -> object
            JSONParser jsonParser = new JSONParser();
            JSONObject resObj = (JSONObject) jsonParser.parse(data);

            //외부 통신
            //조건 :: result_code = "P000" && resObj.get("call") != null
            String rstCd = resObj.get("result_code").toString();
            if(rstCd.contains("P000") ){
                if(resObj.toString().indexOf("\"call\":") != -1){
                    log.debug("[[AOS 외부 API 호출]] 시작"); 

                    callYn = "Y";
                    JSONParser jsonParserCall = new JSONParser();
                    JSONObject resObjCallStr = (JSONObject) jsonParserCall.parse(resObj.get("call").toString());
                    
                    String url = commonUtil.nvl(resObjCallStr.get("url"), "");
                    String header = commonUtil.nvl(resObjCallStr.get("header"), "");
                    String body = commonUtil.nvl(resObjCallStr.get("body"), "");
                    String method = commonUtil.nvl(resObjCallStr.get("method"), "");

                    HttpParam params = new HttpParam(Constant.PARAM_REQ_TITLE, url, method, header, body);    //통신파라미터 초기화
                    log.debug(params.toString());

                    //외부 api 호출
                    Function<String, String> profileChek = (s) -> "dev".equals(s) ? httpUtil.callApiHttp(params) : httpUtil.callApiHttps(params);
                    String extRes = profileChek.apply(profiles);
                    result.put("resultData", extRes);

                    log.debug(Constant.PARAM_RES_TITLE + "::" + extRes);
                    log.debug("[[AOS 외부 API 호출]] 끝");
                }
            }
        } catch (Exception e) {
            log.error("에러발생[apiValidAndCall] :: "+ e.toString());
        } finally{
            result.put("callYn", callYn);
        }
        return result;
    }
}
