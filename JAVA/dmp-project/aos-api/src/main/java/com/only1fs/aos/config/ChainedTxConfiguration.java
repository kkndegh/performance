/** mailsung */
package com.only1fs.aos.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.transaction.ChainedTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class ChainedTxConfiguration {
    @Bean(name="chainedTransactionManager")
    @Primary
    @Autowired
    public PlatformTransactionManager transactionManager(@Qualifier("aosApiMasterTransactionManager") PlatformTransactionManager aosApiMasterTransactionManager, 
                                                        @Qualifier("aosApiSlaveTransactionManager") PlatformTransactionManager aosApiSlaveTransactionManager
                                                        )
    {
        return new ChainedTransactionManager(aosApiMasterTransactionManager, aosApiSlaveTransactionManager);
    }
}
