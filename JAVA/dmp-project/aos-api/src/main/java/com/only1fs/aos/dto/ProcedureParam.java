package com.only1fs.aos.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class ProcedureParam implements Serializable{

    private static final long serialVersionUID = 1L;

    private String title;
    private String group;
    private String code;
    private String method;
    private String url;
    private String header; 
    private String body;
    private String resuldeCode;
    private String systeMmessage;

    /**
    * toString
    */
    public String toString(){
        return title + " -> " + "[ group = " + group + " ]"+ ", [ code = " + code + " ]" + ", [ method = " + method + " ]"
        + ", [ url = " + url + " ]"+ ", [ header = " + header + " ]" + ", [ body = " + body + " ]"
        + ", [ resuldeCode = " + resuldeCode + " ]"+ ", [ systeMmessage = " + systeMmessage + "]";
    }
    
}