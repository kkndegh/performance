package com.only1fs.aos.mybatis.mapper.dao.aosApi;

import com.only1fs.aos.common.annotation.aosManager.AosManagerSlaveMapper;
import com.only1fs.aos.dto.ProcedureParam;

@AosManagerSlaveMapper
public interface AosApiSlaveDao {

    /**
     * aosdb DB스키마 프로시저 호출
     * 
     * @param requestBody
     * @return
     */
    public String callAosPApiFilter(ProcedureParam paramMap);

}
