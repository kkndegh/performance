-- 2022.06.28 추가 : 문자 발송 템플릿 (TODO 2022-07-11 다른 프로젝트에서 관리로 인해 삭제 예정)
-- t_mobile_msg_template_master, t_mobile_msg_template

-- 2022.06.29 추가 : 카카오톡 발송 템플릿 (TODO 2022-07-11 다른 프로젝트에서 관리로 인해 삭제 예정)
-- t_kkont_template_master , t_kkont_template

-- 2022.06.29 추가 : 메일 발송 템플릿 (TODO 2022-07-11 다른 프로젝트에서 관리로 인해 삭제 예정)
-- t_mail_template_master , t_mail_template

-- 메일 발송데이터 저장 테이블
-- 테이블 명 변경 t_mail_info => t_mail_send_info
CREATE TABLE `t_mail_send_info` (
  `mail_no` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '메일시퀀스',
  `mail_subject` varchar(50) DEFAULT NULL COMMENT '메일 제목',
  `mail_context` text DEFAULT NULL COMMENT '메일 내용',
  `snd_email_addr` varchar(50) DEFAULT NULL COMMENT '수신 이메일주소',
  `rlt_code` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '결과 코드',
  `rlt_msg` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '결과 메세지',
  `send_req_ts` datetime DEFAULT NULL COMMENT '발신 요청 시점',
  `cust_seq_no` bigint(20) DEFAULT NULL COMMENT '회원 일련 번호',
  `send_cnt` bigint(20) DEFAULT 0 COMMENT '재발송 횟수',
  `tmplt_no` bigint(20) NOT NULL COMMENT '템플릿 시퀀스',
  `tmplt_group` varchar(10) NOT NULL COMMENT '템플릿 그룹',
  `tmplt_code` varchar(10) NOT NULL COMMENT '템플릿 코드',
  `tmplt_version` bigint(5) DEFAULT NULL COMMENT '템플릿 버전',
  `api_external_cert_key` varchar(1000) DEFAULT NULL COMMENT 'API키(JSON)',
  `reg_id` varchar(20) DEFAULT NULL COMMENT '작성자',
  `reg_dt` datetime DEFAULT current_timestamp() COMMENT '저장일시',
  PRIMARY KEY (`mail_no`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='메일발송관리';

-- 알림톡 발송데이터 저장 테이블
CREATE TABLE `aosdb`.`t_kkont_send_info` (
  `kkont_no` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '알림톡 일련번호',
  `kkont_tpl_id` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '알림톡 템플릿 아이디',
  `kkont_context` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT '알림톡 내용',
  `kkont_btn_context` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT '알림톡 버튼 내용',
  `snd_tgt_cust_no` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '발송 대상 고객 연락처',
  `rlt_code` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '결과 코드',
  `rlt_msg` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '결과 메세지',
  `send_req_ts` datetime DEFAULT NULL COMMENT '발신 요청 시점',
  `recv_ts` datetime DEFAULT NULL COMMENT '수신 시점',
  `cust_seq_no` bigint(20) DEFAULT NULL COMMENT '회원 일련 번호',
  `send_cnt` bigint(20) DEFAULT 0 COMMENT '재발송 횟수',
  `tmplt_no` bigint(20) NOT NULL COMMENT '템플릿 시퀀스',
  `tmplt_group` varchar(10) NOT NULL COMMENT '템플릿 그룹',
  `tmplt_code` varchar(10) NOT NULL COMMENT '템플릿 코드',
  `tmplt_version` bigint(5) DEFAULT NULL COMMENT '템플릿 버전',
  `api_external_cert_key` varchar(1000) DEFAULT NULL COMMENT 'API키(JSON)',
  `reg_id` varchar(20) DEFAULT NULL COMMENT '작성자',
  `reg_dt` datetime DEFAULT current_timestamp() COMMENT '저장일시',
  PRIMARY KEY (`kkont_no`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='알림톡 발송 관리';

-- 문자 발송데이터 저장 테이블
CREATE TABLE `t_mobile_msg_send_info` (
  `msg_no` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '문자 시퀀스',
  `send_type` char(1) DEFAULT NULL COMMENT '발송형태(R:예약,S:즉시)',
  `to_num` varchar(15) DEFAULT NULL COMMENT '수신자번호',
  `from_num` varchar(15) DEFAULT NULL COMMENT '발송전화번호',
  `msg_type` char(1) DEFAULT NULL COMMENT '메시지종류 (S,M,L)',
  `msg_subject` varchar(20) DEFAULT NULL COMMENT '메시지제목',
  `msg_context` varchar(2000) DEFAULT NULL COMMENT '메시지내용',
  `val` varchar(20) DEFAULT NULL COMMENT '메시지제한',
  `image` varchar(100) DEFAULT NULL COMMENT '이미지 경로',
  `cmpg_id` varchar(20) DEFAULT NULL COMMENT '예약 ID',
  `datetime` varchar(14) DEFAULT NULL COMMENT '예약시간(YYYYMMDDHH24MI)',
  `rlt_code` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '결과 코드',
  `rlt_msg` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '결과 메세지',
  `cust_seq_no` bigint(20) DEFAULT NULL COMMENT '회원 일련 번호',
  `send_cnt` bigint(20) DEFAULT 0 COMMENT '재발송 횟수',
  `tmplt_no` bigint(20) NOT NULL COMMENT '템플릿 시퀀스',
  `tmplt_group` varchar(10) NOT NULL COMMENT '템플릿 그룹',
  `tmplt_code` varchar(10) NOT NULL COMMENT '템플릿 코드',
  `tmplt_version` bigint(5) DEFAULT NULL COMMENT '템플릿 버전',
  `api_external_cert_key` varchar(1000) DEFAULT NULL COMMENT 'API키(JSON)',
  `reg_id` varchar(20) DEFAULT NULL COMMENT '작성자',
  `reg_dt` datetime DEFAULT current_timestamp() COMMENT '저장일시',
  PRIMARY KEY (`msg_no`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='메시지 발송 관리';