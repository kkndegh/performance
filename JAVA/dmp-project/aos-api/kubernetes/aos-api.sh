#!/bin/bash

PROFILE=$1

TAG=$(cat aos-api.tag)
YAML=/home/gitlab/yaml/aos-api.yaml

sed -i 's/latest/'"$TAG"'/g' $YAML
sed -i 's/env_profile/'"$PROFILE"'/g' $YAML
sudo kubectl apply -f $YAML