CREATE DEFINER=`kkndegh`@`%` PROCEDURE `aosdb`.`proc_aos_mail_add`(
	IN `pi_method` VARCHAR(50),
	IN `pi_group` VARCHAR(50),
	IN `pi_code` VARCHAR(50),
	IN `pi_header` TEXT,
	IN `pi_body` TEXT,
 	OUT `po_json` TEXT
)
BEGIN
	
	
	/* 
	 * ex)
	 * call aosdb.proc_aos_mail_add ('POST', 'MAIL', 'JOIN', '', '{"addr": "kkndegh@only1fs.com", "subject":  "메일 테스트", "replaceData": {"id": "admin", "authCode": "1234"}}', ?);
	 */
	
	DECLARE rtn_json TEXT DEFAULT '{}';
	DECLARE rtn_string TEXT;

	DECLARE v_str_tmplt_code VARCHAR(5);
	DECLARE v_txt_tmplt_context text;

	DECLARE v_str_addr VARCHAR(50);
	DECLARE v_str_title_key VARCHAR(50);
	DECLARE v_str_subject VARCHAR(50);
	DECLARE v_str_is_html VARCHAR(50);

	/*
	 * 1. 메일 내용 저장
	 */
	
	SET v_txt_tmplt_context = JSON_UNQUOTE(JSON_EXTRACT(pi_body, '$."v_txt_tmplt_context"'));
	SET v_str_tmplt_code = JSON_UNQUOTE(JSON_EXTRACT(pi_body, '$."v_str_tmplt_code"'));
	SET v_str_addr = JSON_UNQUOTE(JSON_EXTRACT(pi_body, '$."addr"'));
	SET v_str_title_key = JSON_UNQUOTE(JSON_EXTRACT(pi_body, '$."title_key"'));
	SET v_str_subject = JSON_UNQUOTE(JSON_EXTRACT(pi_body, '$."subject"'));
	
	/* 1. 메일 내용 저장 */
	INSERT INTO t_mail_send_info
	( addr, tmplt_master_code, title_key, subject, context)
		select 
			v_str_addr as addr,
			v_str_tmplt_code as tmplt_master_code,
			'email.auth.title' as title_key,
			v_str_subject as subject,
			v_txt_tmplt_context AS context
		from DUAL;
	
	SET rtn_json = JSON_INSERT(rtn_json, '$.httpCode', '200');
	SET rtn_json = JSON_INSERT(rtn_json, '$.result_code', 'P000');
	SET rtn_json = JSON_INSERT(rtn_json, '$.result_msg', '정상');
	
 	SET po_json = rtn_json;
END