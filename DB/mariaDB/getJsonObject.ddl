CREATE DEFINER=`kkndegh`@`%` PROCEDURE `aosdb`.`proc_aos_get_msg`(
	IN `pi_method` VARCHAR(50),
	IN `pi_group` VARCHAR(50),
	IN `pi_code` VARCHAR(50),
	IN `pi_header` TEXT,
	IN `pi_body` TEXT,
	OUT `po_json` TEXT)
BEGIN
	DECLARE rtn_json TEXT DEFAULT '{}';
	DECLARE rtn_string LONGTEXT DEFAULT '[]';
	DECLARE rtn_key_string TEXT DEFAULT '';
		
	SELECT 
		JSON_ARRAYAGG(
			JSON_OBJECT(
				'msg_no', nvl(msg_no,'')
				,'send_type', nvl(send_type,'')
				,'to_num', nvl(to_num,'')
				,'from_num', nvl(from_num,'')
				,'msg_type', nvl(msg_type,'')
				,'msg_subject', nvl(msg_subject,'')
				,'msg_context', nvl(msg_context,'')
				,'val', nvl(val,'')
				,'image', nvl(image,'')
				,'cmpg_id', nvl(cmpg_id,'')
				,'datetime', nvl(datetime,'')
				,'api_external_cert_key' , nvl(api_external_cert_key,'')
				,'reg_id', nvl(reg_id,'')
				,'send_cnt', nvl(send_cnt,''))
		) AS result
		INTO rtn_string
	FROM t_mobile_msg_send_info 
	WHERE rlt_code = 'R000'
		AND send_cnt < 3
		AND datetime < now();
	

	SELECT 
		external_key
		INTO rtn_key_string
	FROM t_external_api_key
	WHERE api_code = pi_group;

	SET rtn_json = JSON_INSERT(rtn_json, '$.result_code', 'P000');
	SET rtn_json = JSON_INSERT(rtn_json, '$.result_msg', '정상');
	SET rtn_json = JSON_INSERT(rtn_json, '$.external_key', rtn_key_string);
	SET rtn_json = JSON_MERGE(rtn_json, CONCAT('{"list":',ifnull(rtn_string, '[]'),'}'));
	


-- 	SELECT rtn_json AS result;
 	SET po_json = rtn_json;
END