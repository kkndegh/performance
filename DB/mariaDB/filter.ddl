CREATE DEFINER=`000706fbtnnals`@`%` PROCEDURE `aosdb`.`proc_api_filter`(
	IN `pi_method` VARCHAR(50),
	IN `pi_group` VARCHAR(50),
	IN `pi_code` VARCHAR(50),
	IN `pi_url` TEXT,
	IN `pi_header` TEXT,
	IN `pi_body` TEXT,
	IN `pi_result_code` VARCHAR(4),
	IN `pi_sys_msg` TEXT
)
BEGIN

	/*
		
		call proc_api_filter ('POST','member','login','https://','{"access_token":"asdf"}','{"account_id":"aa","account_pw":"1234"}', null, null);
		call proc_api_filter ('POST','scrap','req','https://','{"access_token":"asdf"}','{"mall_cd":"001"}', null, null);
		
	*/
	
	DECLARE bookmark VARCHAR(50);
	DECLARE v_str_qry TEXT;
	DECLARE rtn_json JSON DEFAULT '{}';
	
	DECLARE v_str_result_code VARCHAR(4);
	DECLARE v_str_result_sys_msg text;
	
	DECLARE v_str_procname VARCHAR(50);
	DECLARE v_int_api_req_seq_no BIGINT;
	DECLARE v_str_access_token TEXT;
	
	DECLARE v_int_member_seq_no BIGINT;
	DECLARE v_int_user_seq_no BIGINT;

	DECLARE v_int_auth_group_seq_no BIGINT;
	DECLARE v_int_menu_auth_group_seq_no BIGINT;
	
	DECLARE v_str_vaild_token CHAR(1);
	DECLARE v_str_vaild_user CHAR(1);
	DECLARE v_str_vaild_member CHAR(1);
	DECLARE v_int_api_group_seq_no bigint;
	
	DECLARE v_str_call_procedure_req VARCHAR(50);
	DECLARE v_str_call_procedure VARCHAR(50);
	DECLARE v_int_api_seq_no BIGINT;
	DECLARE v_str_service VARCHAR(50);
	DECLARE v_str_service_method VARCHAR(50);
	DECLARE v_str_service_url VARCHAR(200);
	DECLARE v_str_service_header text;
	DECLARE v_str_service_body text;
	DECLARE v_str_log_yn CHAR(1);
	DECLARE v_str_test_user_yn CHAR(1);

	DECLARE v_int_menu_seq_no BIGINT;
	DECLARE v_int_menu_auth_chk BIGINT;

	
	-- Exception 오류 처리
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
	
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
		 @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		 
		ROLLBACK;
		 
		SET rtn_json = '{}';

		/*		
		select
			COUNT(*) 
			INTO @his_chk
		FROM t_api_req_his
		WHERE api_req_seq_no = v_int_api_req_seq_no;
		
		SELECT IFNULL(@his_chk, 0);
		*/
		
		SET v_str_result_code = 'D999';
		SET @full_error = CONCAT("ERROR ", @errno, " (", @sqlstate, "): ", @text);
		
		-- JSON_INSERT(JSON변수, '$.json key', 'json value')
		SET rtn_json = JSON_INSERT(rtn_json, '$.result_code', v_str_result_code);
		SET rtn_json = JSON_INSERT(rtn_json, '$.result_msg', func_result_msg(v_str_result_code));
		SET rtn_json = JSON_INSERT(rtn_json, '$.result_sys_msg', @full_error);
		SET rtn_json = JSON_INSERT(rtn_json, '$.bookmark', bookmark);
				
		INSERT t_api_res_his (api_req_seq_no, result_body, result_code, result_sys_msg, reg_id, reg_ts)
		VALUES (v_int_api_req_seq_no, rtn_json, v_str_result_code, @full_error, 'proc_api_filter', NOW());
		
		
		SELECT rtn_json;
		
		
	END;
	
	
	SET bookmark = 'apireq1';
	
	-- api_req_seq_no 생성
	SET v_int_api_req_seq_no = cast(CONCAT(date_format(NOW(6), '%Y%m%d%H%i%s'), NEXTVAL(sq_apihis)) as SIGNED INTEGER);
	
	SET bookmark = 'apireq2';

	#select v_int_api_req_seq_no;
	
	-- 모든 요청 history 기록
	INSERT t_api_req_his (api_req_seq_no, req_method, req_url, req_header, req_body, reg_id, reg_ts)
	VALUES (v_int_api_req_seq_no, pi_method, pi_url, pi_header, pi_body, 'proc_api_filter', NOW());
	
	SET bookmark = 'apireq3';
	

	-- 요청된 group, code로 호출 프로시저 SELECT
	SELECT api_seq_no, call_procedure INTO v_int_api_seq_no, v_str_call_procedure_req FROM t_api WHERE api_group = pi_group AND api_code = pi_code; 
	
	SET bookmark = 'apireq4';
	
	
	#SELECT v_int_api_seq_no, v_str_call_procedure_req;

	if ifnull(pi_sys_msg, '') = '' then
		
		SET bookmark = 'token1';
		
		-- pi_header 변수 추출
		SET v_str_access_token = JSON_UNQUOTE(JSON_EXTRACT(pi_header, '$."access_token"'));
		
		SET bookmark = 'token2';
	
		#select v_str_access_token ;
		
		-- 토큰으로 회원권한 조회
		SELECT
			member_seq_no
			, user_seq_no
			, token_valid_yn
			, user_valid_yn
			, member_valid_yn
			, auth_group_seq_no
			, test_user_yn
			INTO v_int_member_seq_no, v_int_user_seq_no, v_str_vaild_token, v_str_vaild_user, v_str_vaild_member, v_int_auth_group_seq_no, v_str_test_user_yn
		FROM view_token a
		WHERE access_token = v_str_access_token
			AND token_class = 'access';
			
		SET bookmark = 'auth1';
	
		-- 회원의 허용 api 권한, 접근 권한 조회
		select api_group_seq_no, menu_auth_group_seq_no into v_int_api_group_seq_no, v_int_menu_auth_group_seq_no from t_auth_group_role where auth_group_seq_no = v_int_auth_group_seq_no ;
		
		SET bookmark = 'apireq5';
		
		-- 회원 업데이트
		UPDATE t_api_req_his 
		SET member_seq_no = v_int_member_seq_no
			, user_seq_no = v_int_user_seq_no
			, api_seq_no = v_int_api_seq_no
		WHERE api_req_seq_no = v_int_api_req_seq_no;
		
		SET bookmark = 'token3';
		
		-- 토큰 유효 상태 체크
		if ifnull(v_str_vaild_token,'N') = 'N' 
			or ifnull(v_str_vaild_user,'N') = 'N' 
			or ifnull(v_str_vaild_member,'N') = 'N' then
			
			set v_str_result_code = 'D901';
			SET rtn_json = JSON_INSERT(rtn_json, '$.result_code', v_str_result_code);
		
		END if;
		
		SET bookmark = 'auth2';
	
			
		-- API 권한 체크
		if IFNULL(v_int_api_group_seq_no, 0) > 0 then
			
			select
				b.call_procedure
				, b.service_seq_no
				, service_method
				, service_url
				, service_header
				, service_body
				, log_yn
				INTO v_str_call_procedure, v_str_service, v_str_service_method, v_str_service_url, v_str_service_header, v_str_service_body, v_str_log_yn			
			FROM 
				t_api_group_role a
				JOIN t_api b ON a.api_seq_no = b.api_seq_no
				LEFT JOIN t_service c ON b.service_seq_no = c.service_seq_no
			WHERE b.method = pi_method
				AND b.api_group = pi_group
				AND b.api_code = pi_code
				AND api_use_yn = 'Y'
				AND api_group_seq_no = v_int_api_group_seq_no;
			
			#SELECT v_str_call_procedure, v_str_service, v_str_service_method, v_str_service_url, v_str_service_header, v_str_service_body, v_str_log_yn;
			
			if v_str_call_procedure IS null then
			
				SET v_str_result_code = 'D902';
				
			END if;
		
		ELSE 
		
			set v_str_result_code = 'D902';
		
		END if;
				
	ELSE 
		
		-- 오류 메시지 전달시 오류 처리
		SET v_str_result_code = pi_result_code;
	
	END if;
	
	
	SET bookmark = 'call1';
	
	-- call procedure 호출 (오류가 아닌 경우)
	if v_str_result_code IS null then
	
		START TRANSACTION;
		
		SET bookmark = 'call2';
		
		SET pi_body = (case when TRIM(IFNULL(pi_body, '')) = '' then '{}' ELSE pi_body END);
		SET @qry = CONCAT('call ', v_str_call_procedure , ' (\'', pi_method, '\',\'', pi_group, '\',\'', pi_code,'\',\'', pi_header , '\',\'', pi_body , '\', ? )');

		-- 다이나믹 쿼리 실행
		PREPARE stmt FROM @qry;
		EXECUTE stmt USING rtn_json;
		DEALLOCATE PREPARE stmt;
		
		SET v_str_result_code = JSON_UNQUOTE(JSON_EXTRACT(rtn_json, '$."result_code"'));
		
		if v_str_result_code = 'P000' then
			COMMIT;
		ELSE 
			ROLLBACK;					
		END if;
		
	ELSE 
	
		SET rtn_json = JSON_INSERT(rtn_json, '$.result_code', v_str_result_code);
	
	END if;
	
	
	SET bookmark = 'apires1';
	  	  
	-- result_msg 추가
	SET v_str_result_code = JSON_UNQUOTE(JSON_EXTRACT(rtn_json, '$."result_code"'));
	SET rtn_json = JSON_INSERT(rtn_json, '$.result_msg', func_result_msg(v_str_result_code));
	SET rtn_json = JSON_INSERT(rtn_json, '$.bookmark', bookmark);
	
	
	SET bookmark = 'apires2';
	
	-- response log 추가
	if IFNULL(v_str_log_yn, 'Y') = 'Y' AND ifnull(v_str_test_user_yn, 'N') = 'N' then
		
		INSERT t_api_res_his (api_req_seq_no, result_body, result_code, result_sys_msg, reg_id, reg_ts)
		VALUES (v_int_api_req_seq_no, rtn_json, v_str_result_code, pi_sys_msg, 'proc_api_filter', NOW());
	
	END if;
	

	-- 결과 출력
   SELECT rtn_json AS result;

END